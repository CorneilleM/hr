/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HR;



import HRalert.SendEmail;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.sql.*;
import java.util.*;
import javax.swing.*;

/**
 *
 * @author NIYIGENA
 */
public class DynamicPreparation extends JFrame{
    
    JLabel emp,derv,amou,mon;
    
    JTextField employee,deriverable,amount ;
    //JRadioButton percentage, fix, employee, company;
    JComboBox month;
    
    JButton Save;
    JButton view;
    Font police;
    Color color;
    Container content;
    DbHandler db;
    JPanel main= new JPanel();
    
    String s;
    String[] part;
String ti,ti_name;
 private JTable jTable6;
//LinkedList<preparation> prep = new LinkedList<preparation>();
//LinkedList<preparation> rapport = new LinkedList<preparation>();
    
    
     public void draw() {
        
        try{
        this.setTitle("Dynamic Preparation");
        enableEvents(WindowEvent.WINDOW_CLOSING);
        this.setResizable(false);
        this.setSize(600, 450);
         Pane();
        setSize(600, 450);
        setVisible(true);
         //();
        
        } catch (Throwable e) {
            System.out.println(" . . inv . exception thrown:" + e);
        }        
    } 
     
     public void load()
   {
       
       String[] months = {"JAN" ,"FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"};
   
   
   try{
        
        emp = new JLabel("EMPLOYEE: ");
        police = new Font("Rockwell", Font.BOLD, 15);
        emp.setFont(police);
        
        
        derv = new JLabel("DERIVERABLE: ");
        police = new Font("Rockwell", Font.BOLD, 15);
        derv.setFont(police);
        
        amou = new JLabel("AMOUNT: ");
        police = new Font("Rockwell", Font.BOLD, 15);
        amou.setFont(police);

        
        mon = new JLabel("MONTH: ");
        police = new Font("Rockwell", Font.BOLD, 15);
        mon.setFont(police);
        
       
        
        
        employee = new JTextField("");
        police = new Font("Consolas", Font.BOLD, 15);
        employee.setFont(police);
        employee.setEnabled(false);
        
        
                
        deriverable = new JTextField("");
        police = new Font("Consolas", Font.BOLD, 15);
        deriverable.setFont(police);  
        
        amount = new JTextField("");
        police = new Font("Consolas", Font.BOLD, 15);
        amount.setFont(police);  
        
        month=new JComboBox(months); 
        
        
        
        Save= new JButton("SAVE");
        police = new Font("Rockwell", Font.BOLD, 15);
        Save.setFont(police);
        Save.setBackground(Color.green);;
   
        view = new JButton("VIEW");
        police = new Font("Rockwell", Font.BOLD, 15);
        view.setFont(police);
        view.setBackground(Color.orange);
        
       main.setBorder(BorderFactory.createLineBorder(Color.black));
        
        
        
           
        } catch (Exception e ) {
            System.out.println(" . . makeFrameTiers . exception thrown:" + e);
//            insertError(e + "", " getBalanceVerification  ");
        }
   
   }
     
     
      public DynamicPreparation( DbHandler db, String s) {

        try {
            
            part = s.split("(?<=\\D)(?=\\d)");
            ti_name=part[0];
           ti=part[1];
             
            
             
            this.db=db;
            
            content = getContentPane();
            
          
            load();
            draw();
            
            employee.setText(s);
            save();
           view();
            
        } catch (Throwable e) {
            System.out.println("Failllll"+e);

        }
    }
      
      
      public JPanel Pane() {
        JPanel panel= new JPanel();
        panel.setLayout(new GridLayout(5, 2, 5, 5));
         panel.setPreferredSize(new Dimension(500,300));
      

        panel.add(emp);
        panel.add(employee);
        panel.add(derv);
      
        panel.add(deriverable);
        panel.add(amou);
        panel.add(amount);
        
        panel.add(mon);
        panel.add(month);
        
        panel.add(Save);
       panel.add(view);
      

        main.add(panel);
       

        content.add(main);
        return main;
    }
   
      public void save() {

        Save.addActionListener(new ActionListener() {
 @Override
                    public void actionPerformed(ActionEvent ae) {

                        if (ae.getSource() ==Save) {
                            
                            
                         String empl =ti;
                         String deriver=deriverable.getText();
                         double montant= Double.parseDouble((String)amount.getText());
                         String m=month.getSelectedItem().toString();
                         //String date=date_doc.getText();
                         
                          
                          db.insertDynamicP(empl, deriver, montant, m);
                          deriverable.setText("");
                          amount.setText("");
                          
                          
                        }
                    }
                });
    }  
  
      
      public void view() {

        view.addActionListener(new ActionListener() {
 @Override
                    public void actionPerformed(ActionEvent ae) {

                        if (ae.getSource() ==view) {
                            
                    DynamicView(db.Dynamicprep1(month.getSelectedItem().toString(),ti)) ;   
                          
                    //new SendEmail("baziramwabo@gmail.com","Hello"," Dore salaire yawe ya FEB: 400000Frw","","Odette");      
                        }
                    }
                });
    }  
  
   public void DynamicView(LinkedList<preparation> prep)
   {
   
               // prep = p;
                
                
                
                double total=0.0;

           
            String[][] s1 = new String[prep.size()+2][5];
            
            System.out.println("Dore Size: " +prep.size());

            int vv = 0;

            for (; vv < prep.size(); vv++) {

                preparation C = prep.get(vv);

                s1[vv][0] = ""+ C.num_tier;
                
                s1[vv][1] = "" + C.deriverable;
                s1[vv][2] = "" + C.amount;
                s1[vv][3] = "" + C.month;
                s1[vv][4] = "" + C.date;
                
            total=total+C.amount;
                
            }

            
            
            String title = "  ISHYIGA  COMPTA - DYNAMIC SALAIRES : "+total;
            JTable jTable1= new javax.swing.JTable();
            

            jTable1.setModel(new javax.swing.table.DefaultTableModel(s1, new String[]{"EMPLOYE", "DERIVERABLE", "AMOUNT","MONTH", "DATE_DOC"}));
            jTable1.setAutoResizeMode(jTable1.AUTO_RESIZE_ALL_COLUMNS);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(80);
            jTable1.getColumnModel().getColumn(1).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(2).setPreferredWidth(50);
            jTable1.getColumnModel().getColumn(3).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(4).setPreferredWidth(100);
            
            jTable1.doLayout();
            jTable1.validate();
            
            new showJTable(jTable1, title, s1, "",0.0, 0.0,db,"" , " SALAIRE","",""+total);
   
   
   }
   
    
}
