package HR;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author SHIMWE
 */
public class CodeJournalInterface extends JFrame
{
     JTextField CODE;
     JTextField INTITULE;


    Container content;
    JLabel CODEL,INTITULEL;

    static String num,nom,clas;

    JPanel mainJournal = new JPanel();

    JButton saveButton;
    JButton cancelButton;

    Font police;
    Color color;
    CodeJournal cd;
private Cashier cashier;
DbHandler db;
String what="";


public CodeJournalInterface(DbHandler db)
{
this.db=db;
content = getContentPane();
makeFrameCode("","");
draw();
insertCode();
cancelCode();
}
public CodeJournalInterface(DbHandler db,CodeJournal cd)
{
    what="UPDATE";
    this.db=db;
    this.cd=cd;
content = getContentPane();
makeFrameCode(cd.code,cd.codeIntitule);
draw();
update();

}
public void draw()
{
    this.setTitle("Saisie de code journaux");
//End the process when clicking on Close
enableEvents(WindowEvent.WINDOW_CLOSING);
//Make the Comptabilitée frame not resizable
this.setResizable(false);
//Define the size of the Comptabilité  ; here, 500 pixels of width and 650 pixels of height
this.setSize(500,300);
setVisible(true);
 if(what.equals("UPDATE"))
     UpDatePane(content);
       else
     westPane(content);
}
public void makeFrameCode(String code,String intitule)
{
    CODEL = new JLabel("CODE JOURNAUX :");
    police = new Font("Rockwell", Font.BOLD, 14);
    CODEL.setFont(police);

    CODE = new JTextField("");
    police = new Font("Consolas", Font.BOLD, 16);
    CODE.setFont(police);
    CODE.setForeground(Color.BLUE);
    color= new Color(175,215,255);//(r,g,b)
    CODE.setBackground(color);
    CODE.setText(code);


    INTITULEL = new JLabel("INTITULE CODE :");
    police = new Font("Rockwell", Font.BOLD, 14);
    INTITULEL.setFont(police);

    INTITULE = new JTextField("");
    police = new Font("Consolas", Font.BOLD, 14);
    INTITULE.setFont(police);
    INTITULE.setForeground(Color.BLUE);
    color= new Color(175,215,255);//(r,g,b)
    INTITULE.setBackground(color);
    INTITULE.setText(intitule);


saveButton = new JButton("SAVE");
police = new Font("Rockwell", Font.BOLD, 14);
saveButton.setFont(police);
color= new Color(55,155,255);//(r,g,b)
saveButton.setBackground(color);

cancelButton = new JButton("CANCEL");
police = new Font("Rockwell", Font.BOLD, 14);
cancelButton.setFont(police);


}

public JPanel westPane(Container content )
{
   JPanel panelJournal=new JPanel();
    panelJournal.setLayout(new GridLayout(2,2,5,20));//
    panelJournal.setPreferredSize(new Dimension(450,125));

    JPanel paneJournal=new JPanel();
    paneJournal.setLayout(new GridLayout(3,2,5,20));//
    paneJournal.setPreferredSize(new Dimension(50,50));

    panelJournal.add(CODEL);
    panelJournal.add(CODE);
    panelJournal.add(INTITULEL);
    panelJournal.add(INTITULE);

    JPanel panelJournal2 = new JPanel();
    panelJournal2.setLayout(new GridLayout(1,4,10,20));
    panelJournal2.setPreferredSize(new Dimension(450, 40));

    panelJournal2.add(saveButton);
    panelJournal2.add(cancelButton);

    mainJournal.add(panelJournal);
    mainJournal.add(paneJournal);
    mainJournal.add(panelJournal2);

    content.add(mainJournal);
    return mainJournal;
}
/************************************************************************************
 * UPDATE FRAME
 *****************************************************************/
public JPanel UpDatePane(Container content )
{

     JPanel panelJournal=new JPanel();
    panelJournal.setLayout(new GridLayout(2,2,5,20));//
    panelJournal.setPreferredSize(new Dimension(450,125));

    JPanel paneJournal=new JPanel();
    paneJournal.setLayout(new GridLayout(3,2,5,20));//
    paneJournal.setPreferredSize(new Dimension(50,50));

    CODE.setText(""+cd.code);
    INTITULE.setText(""+cd.codeIntitule);

    panelJournal.add(CODEL);
    panelJournal.add(CODE);
    panelJournal.add(INTITULEL);
    panelJournal.add(INTITULE);

    JPanel panelJournal2 = new JPanel();
    panelJournal2.setLayout(new GridLayout(1,4,10,20));
    panelJournal2.setPreferredSize(new Dimension(450, 40));
    saveButton.setText("UPDATE");

    panelJournal2.add(saveButton);
    panelJournal2.add(cancelButton);

    mainJournal.add(panelJournal);
    mainJournal.add(paneJournal);
    mainJournal.add(panelJournal2);

    content.add(mainJournal);
    return mainJournal;
}

public void insertCode()
{
   saveButton.addActionListener(new ActionListener() {
    public void actionPerformed(ActionEvent ae)
    {
    if(ae.getSource()==saveButton )
    {
        if(CODE.getText().equals("")||INTITULE.getText().equals(""))
        {
            JOptionPane.showMessageDialog(null,"VEUILLEZ REMPLIR LES CASES VIDE");
        }
        else
        {

        db.insertCode(CODE.getText(), INTITULE.getText());
        JOptionPane.showMessageDialog(null,"ENREGISTRER AVEC SUCCES!!");
        CODE.setText("");
        INTITULE.setText("");
        }
    }
 }});
}
public void cancelCode()
{
   cancelButton.addActionListener(new ActionListener() {
    public void actionPerformed(ActionEvent ae)
    {
    if(ae.getSource()==cancelButton )
    {
       int k=WindowEvent.WINDOW_CLOSED;
       System.out.println("INTEGER K= "+k);
    }

 }});
}

/**********************************************************************************************
MIS A JOUR TIERS
**********************************************************************************************/
public void update()
{
    saveButton.addActionListener(new ActionListener() {
    public void actionPerformed(ActionEvent ae)
    {
    if(ae.getSource()==saveButton)
    {
        String codejr=CODE.getText().toUpperCase();
        String inti=INTITULE.getText().toUpperCase();

    try
    {
        if(what.equals("UPDATE"))
        {
            db.upDateCode(new CodeJournal (codejr,inti),codejr);

        }
    }

    catch(Throwable ex)
    {
    JOptionPane.showMessageDialog(null," PROBLEME MODIFICATION "," Itariki ",JOptionPane.PLAIN_MESSAGE);
    }
    }
    }});

}
public static void main(String[] args) {
//    Cashier c=new Cashier(0,"localhost");
//new CodeJournalInterface(c);
    }
}