/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HR;

/**
 *
 * @author ishyiga
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 
import java.awt.*; 
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement; 
import java.util.Date;
import java.util.LinkedList;
import java.util.Properties;
import java.util.StringTokenizer;
import javax.swing.JFrame; 
import javax.swing.JOptionPane;
/**
 *
 * @author Kimenyi
 */
public class PrintLandscape extends JFrame {

LinkedList <Lot> lotList;
int pageNum = 0;
private Properties properties = new Properties();
int margin=0; 
double un,deux,trois; 
String  reference,mode;
String [] footer; 
String facture="";
Statement s; 
String leDoc;
Image img;

public PrintLandscape(LinkedList <Lot> lotList,Statement s,
        String mode , String reference,String leDoc,Image img )
{  

this.img=img;    
this.lotList=lotList;
this.s=s; 
this.mode=mode; 
this.reference= reference; 
this.leDoc=leDoc;
PrintCommand( ); 

} 
private void PrintCommand( )
{
    this.margin=getValue("margin");  

PrintJob pjob = getToolkit().getPrintJob(this," DOC "+reference, properties); 
 
    
    if (pjob != null) {
        {Graphics pg = pjob.getGraphics();

    if (pg != null) {
 
    { printCommandeA4(pjob, pg); } 
    pg.dispose();
}
    
    }
    pjob.end();
    }
} 

private void printCommandeA4 (PrintJob pjob, Graphics pg) {

 if (!(pg instanceof PrintGraphics)) {
      throw new IllegalArgumentException ("Graphics context not PrintGraphics");
    }
 
 
 
 
    int pageW = pjob.getPageDimension().width - margin; 
    
    
  if (pg != null)
  {
    
    try {
    Font helv = new Font("Helvetica", Font.BOLD, getValue("dateT"));
    pg.setFont(helv);
    
    int longeurLogo=40;  
     
    pg.drawString(getString("ville")+", printed " +(new Date()).toLocaleString(),  getValue("dateX"), getValue("dateY"));
    helv = new Font("Helvetica", Font.BOLD, 25);
    pg.setFont(helv); 
    
        Thread.sleep(2000);
    
    int largeurLogo=150;
    
      if(getValue("largeurLogo")!=0 && getValue("longeurLogo")!=0)
    {
    largeurLogo=getValue("largeurLogo");
    longeurLogo=getValue("longeurLogo");
    }
    pg.drawImage(img, getValue("logoX"), getValue("logoY"), largeurLogo, longeurLogo, rootPane);
 
    helv = new Font("Helvetica", Font.PLAIN, getValue("adresseT"));
    //have to set the font to get any output
    pg.setFont(helv);
    FontMetrics fm = pg.getFontMetrics(helv);
    int fontHeight ; 

    pg.drawString(getString("adresseV"), getValue("adresseX"),  getValue("adresseY"));
    pg.drawString(getString("rueV"), getValue("rueX"),  getValue("rueY"));
    pg.drawString(getString("localiteV"), getValue("localiteX"),  getValue("localiteY"));
    pg.drawString(getString("telV"), getValue("telX"), getValue("telY"));
    pg.drawString(getString("faxV"), getValue("faxX"), getValue("faxY"));
    pg.drawString(getString("emailV"), getValue("emailX"), getValue("emailY"));
    pg.drawString(getString("tvaV"), getValue("tvaX"),  getValue("tvaY"));
    pg.drawRect(getValue("clientX"), getValue("clientY"), getValue("clientLongeur"), getValue("clientLargeur"));
    Font helv2 = new Font("Helvetica", Font.BOLD, getValue("clientT"));
    
    // pg.drawString(getString("ville")+", le " + date.substring(0, 2) + "/" + date.substring(2, 4)
    //                + "/20" + date.substring(4, 6), getValue("dateX"), getValue("dateY")); 
    
    pg.setFont(helv2);
//System.err.println("MENEKA 3");
    pg.drawString(leDoc, getValue("clientX") + 10,  getValue("clientY") + 10);
    helv2 = new Font("Helvetica", Font.ITALIC, getValue("clientT")-2);
    pg.setFont(helv2);
    pg.drawString(" DATE "+reference, getValue("clientX" ) + 10, getValue("clientY" ) + 20);
    pg.drawString(" powered by Ishyiga", getValue("clientX" ) + 10,  getValue("clientY" ) + 30);
    pg.drawString(" ", getValue("clientX" ) + 10, getValue("clientY" ) + 40);
    pg.drawString(" ", getValue("clientX" ) + 10,  getValue("clientY" ) + 50);
    Font helv1 = new Font("Helvetica", Font.BOLD, getValue("factureT"));
 
    pg.setFont(helv1);
  //  pg.drawString(facture, getValue("factureX"), getValue("factureY"));

    helv2 = new Font("Helvetica", Font.PLAIN, getValue("referenceT"));
    pg.setFont(helv2);
   // pg.drawString(getString("referenceV") + reference, getValue("referenceX"), getValue("referenceY"));
    helv1 = new Font("Helvetica", Font.BOLD, 10);
    pg.setFont(helv1);
    pageNum++;
   // pg.drawString("Page  " + pageNum, pageW / 2, pageH - 9);

    helv2 = new Font("Helvetica", Font.BOLD, 10);
    pg.setFont(helv2);
            int g = getValue("length");
            String[] entete = new String [g];
            int[] longeur = new int[g+1];
            String[] variable= new String [g];
            int [] dimension=new int[g];
            longeur[0]=0;

            int page=1;
            int y=  getValue("entete"+page+"X");
            for(int k=0;k<g;k++)
            {
            entete[k]=getString("entete_"+(k+1)+"Valeur");
            longeur[k+1]=getValue("entete_"+(k+1)+"Largeur"); 
            variable[k]=getString("entete_"+(k+1)+"Variable");
            dimension[k]=getValue("entete_"+(k+1)+"Dimension");
 // System.out.println(" d   "   +getValue("entete_"+(k+1)+"Dimension")); 
            } 
            int w  ;

            int curHeightLigne = getValue("ligne"+page+"X");

            Font helvLot = new Font("Helvetica", Font.PLAIN, 10);
            pg.setFont(helvLot);
            FontMetrics fmLot = pg.getFontMetrics(helvLot);
            fontHeight = fmLot.getHeight();
            boolean done = true;
 
            int marginfooter =   getValue("marginfooter");
            int espaceHasi=20;
            int Xprix = getValue("longeurY"); 

////////// KWINJIZA DATA   //////////////////////////////////////////////////   
            
for (int j = 0; j < lotList.size(); j++) 
{
            helvLot = new Font("Helvetica", Font.PLAIN, getValue("tailleLigne"));
            pg.setFont(helvLot);
            Lot ci = lotList.get(j);
            w = margin;
            if (done) {
            curHeightLigne += fontHeight;
            } else {
            curHeightLigne += 2 * fontHeight;
            }
            done = true;
            for (int i = 0; i < g; i++) {
                
            w += (longeur[i]);
//            if(longeur[i]!=0)
//            {    
            String valeur=ci.vars[i];
            if(valeur==null)
                valeur="";
            String variab=variable[i];
            int dim      =dimension[i];
            
            if(variab.contains("int"))
            printInt(pg,setVirguleD(""+((int)Double.parseDouble( valeur))), w + (longeur[i + 1]) - 5, curHeightLigne);
            else if(variab.equals("double"))     
            printInt(pg,setVirguleD(valeur), w + (longeur[i + 1]) - 5, curHeightLigne);
            else  if(variab.equals("centreInt"))
            printIntCentre(pg,setVirguleD(valeur), w + (longeur[i + 1]) , curHeightLigne,longeur[i + 1]);
            else  if(variab.equals("centreS"))
            printStringCentre(pg,valeur , w   , curHeightLigne,longeur[i + 1]);
            else if (valeur.length() < dim)
            {                
            pg.drawString(valeur, w + 5, curHeightLigne);
            }
            else
            {
            done = false;
            int index=kataNeza(valeur,dim);
            pg.drawString(valeur.substring(0,index), w + 5, curHeightLigne);
            pg.drawString(valeur.substring(index, valeur.length()), w + 5, fontHeight + curHeightLigne);
            }
//            }
            } 
            
            
           int marginFotter =getValue("marginfooter");
          if ((curHeightLigne+20) >  marginFotter-20) {

            helv1 = new Font("Helvetica", Font.BOLD, getValue("enteteTaille"));
            pg.setFont  (helv1);
            w = margin;
            for (int i = 0; i < g; i++) {
            w += (longeur[i]);
            //if(i!=0 && longeur[i]!=0)
            {
            pg.drawLine(w,y, w, marginFotter             );//ligne zihagaze
            pg.drawString(entete[i], w + 2, y +15   );// amazina
            }
            
            }
            pg.drawLine(margin, marginFotter, w, marginFotter );//umurongo wo hasi
            pg.drawRect(margin, y , w - margin,25   ) ; // cadre ya entete
            /////////////////////////////////////////////////////////////////////////////////////////////
            pg.dispose();
            pg = pjob.getGraphics();
            if (pg != null)
            { 
            pg.setFont(helv);
            }
            helv1 = new Font("Helvetica", Font.PLAIN, getValue("factureT"));
            pg.setFont(helv1);
           // pg.drawString(  + (numero), margin, margin + 25); 
            helv1 = new Font("Helvetica", Font.BOLD, 10);
            pg.setFont(helv1);
            pageNum++;page=2;
            pg.drawString("Page  " + pageNum, pageW / 2,getValue("pageNum"));
              curHeightLigne=getValue("entete"+page+"X")+25;
              y=getValue("entete"+page+"X");
            done = true;
            }
 }
///////////////////////////////////////////////////////////////////////////////////////////////////////////
System.out.println("curHeightLigne    "+curHeightLigne);
System.out.println("Xprix      "+Xprix);
System.out.println("marginfooter    "+marginfooter);

            if((curHeightLigne+20) > ( Xprix) )
            { 
            helv1 = new Font("Helvetica", Font.BOLD, getValue("enteteTaille"));
            pg.setFont(helv1);
            w = margin;

            for (int i = 0; i < g; i++) {
            w += (longeur[i]);
            pg.drawLine(w,y, w, marginfooter );//ligne zihagaze
            pg.drawString(entete[i], w + 2, y +15);// amazina
            }
            System.out.println(" sup    "+curHeightLigne);
            pg.drawLine(margin, marginfooter , w, marginfooter );//umurongo wo hasi
            pg.drawRect(margin, y , w - margin,25 ); // cadre ya entete

            /////////////////////////////////////////////////////////////////////////////////////////////
            pg.dispose();
            pg = pjob.getGraphics();
            if (pg != null)
            {
            pg.setFont(helv);
            }
            helv1 = new Font("Helvetica", Font.PLAIN, getValue("factureT"));
            pg.setFont(helv1);
        // //System.out.println(facture+ getValue("factureX")+getValue("factureY"));
   
          pg.drawString(facture, margin, margin + 25); 
            helv1 = new Font("Helvetica", Font.BOLD, 10);
            pg.setFont(helv1);
            pageNum++;page=2;
            pg.drawString("Page  " + pageNum, pageW / 2, marginfooter+espaceHasi - 10); 
            helv1 = new Font("Helvetica", Font.BOLD, getValue("enteteTaille"));
            pg.setFont(helv1);
            w = margin;
            if(pageNum>1) {
                    page=2;
                }
            y=getValue("entete"+page+"X");
            for (int i = 0; i < g; i++) {
            w += (longeur[i]);
            pg.drawLine(w,y, w, Xprix-espaceHasi);//ligne zihagaze
            pg.drawString(entete[i], w + 2, y + 15);// amazina
            }
            pg.drawLine(margin, Xprix-espaceHasi, w, Xprix-espaceHasi);//umurongo wo hasi
            pg.drawRect(margin, y , w - margin, 25); // cadre ya entete 
            }
            else ////////////////////////////////// IYO DATA ZARENZE LONGEURY
            { 
                 System.out.println(" hasi    "+curHeightLigne);
            helv1 = new Font("Helvetica", Font.BOLD, getValue("enteteTaille"));
            pg.setFont(helv1);
            w = margin;
            if(pageNum>1) {page=2;}
            y=getValue("entete"+page+"X");
            for (int i = 0; i < g; i++) {
            w += (longeur[i]);
            pg.drawLine(w,y, w, Xprix-espaceHasi);//ligne zihagaze
            pg.drawString(entete[i], w + 2, y + 15);// amazina
            }
            pg.drawLine(margin, Xprix-espaceHasi, w, Xprix-espaceHasi);//umurongo wo hasi
            pg.drawRect(margin, y , w - margin, 25); // cadre ya entete
          
            }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        
                        helv2 = new Font("Helvetica", Font.BOLD, getValue("prixTaille"));
                        pg.setFont(helv2);
                     
      
    } catch (Throwable ex) {
                System.out.println(ex);
            }  
  } 
}



public static int kataNeza(String m,int l)
{
    String [] espaces=m.split(" ");
    int s=espaces.length;
    int index=0;
    for(int i=0;i<s-1;i++)
    {
        espaces[i]=espaces[i]+" ";
    }
     
    for(int i=0;i<s;i++)
    {  
        index+=espaces[i].length();
        if(index>l)
        {
            index-=espaces[i].length();
            break;
        }
    } 
    return index; 
}

public String l(String lang, String var,Statement stat) {
        String value = "";
        try {
   ResultSet   outResult = stat.executeQuery("  select  * from APP.LANGUE "
           + " where variable= '" + var + "'");
  
   while (outResult.next()) {
                value = outResult.getString(lang);
   }
        } catch (SQLException e) {
            System.err.println(var+e); 
        }
        if (value == null || value.equals("")) {
            System.err.println(" ????????????????      " + var);
        }

        return value;
    }

String getParam(String famille,String mode, String nom,Statement s )
{ 
try
{
ResultSet outResult = s.executeQuery("select  * from APP.VARIABLES where APP.variables.nom_variable= '"+nom+"' and APP.variables.value2_variable= '"+mode+"' and APP.variables.famille_variable= '"+famille+"' ");
while (outResult.next())
{
  //  System.out.println(outResult.getString(2));
        String num =outResult.getString(1);
        String value =outResult.getString(2);
        String fam =outResult.getString(3);
        String value2 =outResult.getString(4);
        return value;
}
}
catch (Throwable e){
     System.err.println(e);   
 }
return     "";
}


int getValue(String s1)
{ 
int ret=0;
String value=  getParam("PRINT",mode,s1+mode,s);
 if(value!=null)
 {
Double val=new Double (value);
ret = val.intValue();
}

return ret;
}
String getString(String s1)
{
String ret=" ";
return  getParam("PRINT",mode,s1+mode,s);
  
}
public void printInt(Graphics pg,int nbre,int w,int h)
  {
  String ss=setVirgule(nbre);

  int back=0;

  for(int i=ss.length()-1;i>=0;i--)
  {
      if(ss.charAt(i)==' ') {
          back+=2;
      }
      else {
          back+=5;
      }

      pg.drawString(""+ss.charAt(i),w-back,h); 
  }
}
public void printInt(Graphics pg,String s,int w,int h)
{
int back=0; 
for(int i=s.length()-1;i>=0;i--)
{
if(s.charAt(i)==' ') {
        back+=2;
    }
else {
        back+=5;
    } 
pg.drawString(""+s.charAt(i),w-back,h); 
} 
} 
public void printIntCentre(Graphics pg,String s,int w,int h,int length)
  {

int back=0;
int center=w-(length/2)+((6*s.length())/2);
  for(int i=s.length()-1;i>=0;i--)
  {
      if(s.charAt(i)==' ') {
          back+=2;
      }
      else {
          back+=6;
      }
      pg.drawString(""+s.charAt(i),center-back,h);
      //System.out.print (s.charAt(i)+" - "+(center-back));
  }
}
public void printStringCentre(Graphics pg,String s,int w,int h,int length)
  {
int back=0;
  int f=(length-s.length())/2;
  int center=w+f-16;
  
  //System.out.println(w+"   "+s+"   "+f+"   "+length);
     
for(int i=0;i<s.length();i++)
  {
      pg.drawString(""+s.charAt(i),center+back,h);
      //System.out.print (s.charAt(i)+" - "+(center+back));
      if(s.charAt(i)=='I') {
          back+=3;
      }
      else  if(s.charAt(i)=='W' || s.charAt(i)=='M') {
          back+=8;
      }
      else {
          back+=6;
      }

    
  }

}
 static String setVirgule(int frw)
{
String setString = ""+frw;
int l =setString.length();
if(l>3 && l<=6) {
        setString= setString.substring(0,l-3)+" "+setString.substring(l-3);
    }
if(l>6)
{
String s1 = setString.substring(0,l-3);
int sl=s1.length();
setString=s1.substring(0,sl-3)+" "+s1.substring(sl-3)+" "+setString.substring(l-3)  ;
}
return setString;
} 
 
String setVirgule(String setString)
{
int l =setString.length();
if(l>3 && l<=6) {
        setString= setString.substring(0,l-3)+" "+setString.substring(l-3) ;
    }
if(l>6)
{
String s1 = setString.substring(0,l-3);
int sl=s1.length();
setString=s1.substring(0,sl-3)+" "+s1.substring(sl-3)+" "+setString.substring(l-3)  ;
}
return setString;
} 

String setVirguleD(String frw)
{ 
String setString = frw;
if(frw.contains("."))
{
try
{     double d=Double.parseDouble(frw)  ;  
   if(d>99999)
  {BigDecimal myNumber = new BigDecimal(d);  
     int valeur=myNumber.intValue();
    System.out.println(valeur);
  return setVirgule( ""+valeur);
  }      
        frw=""+frw;
     //   System.out.println("********after**********"+frw);
StringTokenizer st1=new StringTokenizer (frw);
String entier =st1.nextToken(".");
//System.out.println(frw);
String decimal=st1.nextToken("").replace(".", "");
if(decimal.length()==1 && decimal.equals("0") ) {
        decimal=".00";
    }
else if (decimal.length()==1 && !decimal.equals("0") ) {
        decimal="."+decimal+"0";
    }
else {
        decimal="."+decimal.substring(0, 2);
    }
  setString = entier+decimal; 
  
if(mode.equals("CALCUL")  )
{
BigDecimal myNumber = new BigDecimal(frw);
setString =""+ myNumber.intValue();
return setVirgule(setString);
} }
    catch(Throwable e)
    {System.out.println(frw+e);} 
int l =setString.length();
if(l<2) {
        setString = "    "+frw;
    }
else if(l<3) {
        setString = "  "+frw;
    }
else if(l<4) {
        setString = "  "+frw;
    }
int up=6;
int ju=up+3;
if(l>up && l<=ju)
{
setString= setString.substring(0,l-up)+","+setString.substring(l-up) ;
}
else if(l>ju)
{
setString=setString.substring(0,l-9)+","+setString.substring(l-9,l-6)+","+setString.substring(l-6,l)  ;
}
}
else {
        return setVirgule(frw);
    }
    
return setString;
} 
  public static Statement connecting(String connectionURL ) {

        try {
          Connection  conn = DriverManager.getConnection(connectionURL );
             
        conn.setSchema("APP"); 
        System.out.println(conn.getSchema()+"   Connected database successfully.. voir db ."+connectionURL);
  
            return conn.createStatement(); 
    } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "CONNECTION FAIL TO voir "+connectionURL);
            System.exit(1);
            System.out.println("Connection Fail " + ex);
        } 
        return null;
    }
public static void main(String []args)
{  
   LinkedList <String> lotList = new LinkedList () ;
    
   lotList.add(" 455 ");
   lotList.add(" CAPITAUX SUBVENTIONS DINVESTISSEMENT ");
   lotList.add(" 88 ");
   lotList.add(" CAPITAUX SUBVENTIONS DINVESTISSEMENT ");
   String server="localhost";
   String dbName="PMSS2";
   String connectionURL= "jdbc:derby://" + server 
           + ":1527/" + dbName+";user=sa;password=ish" ;
   
//    new PrintLandscape ( lotList,  connecting(  connectionURL ), "BALANCCE", 
//                "010120", "2016-01-01","" );
}

}
