package HR;

import HRalert.SendEmail;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.event.*;
import java.io.*;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.*;
import java.util.Date;
import java.util.*;
import java.util.logging.*;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.Container;
import java.sql.Timestamp;


public class DbHandler {

    String driver = "org.apache.derby.jdbc.NetWorkDriver";
    String dbName,server;
    String  connectionURL2;
   
    public ResultSet tiersResult;
    public Connection conn = null;
    Connection conn2 = null;
    Connection conn3 = null;
    public Statement state, state2, state3,st;
    PreparedStatement psInsert,pst;
    int numOp ;
    LinkedList allEmploye;
    LinkedList<Compte> allCompte;
    LinkedList<Compte> allSousCompte; 
    //LinkedList<Compta> allDoc;
   // public LinkedList<Compta> intitule;
   // public LinkedList<Frss> fr;
    LinkedList<Tiers> allTiers;
    LinkedList<Journaux> allJournaux;
    LinkedList<Journaux> allJournal_cpt;
   // LinkedList<Product> allProduct = new LinkedList();
    Tiers[] tiersControl2;
    public double viewMaxQte = 0;
    JTable jTable1;
    JScrollPane jScrollPane2;
    public CodeJournal[] code; 
    String ip;
    String inv;
//    private Cashier cashier;
    int numtiers, numSousCpte;
    int idMax = 0;
    int idMin = 1000000;
    int tierFound;
    String dateToday;
   // LinkedList<ComptabiliseInvoice> invalideInvoices = new LinkedList();
   // LinkedList<ComptabiliseBL> invalideBL = new LinkedList();
   // LinkedList<ComptabiliseBL> BLvalides = new LinkedList();
    
    //LinkedList<ComptabiliseBL> invalideMVT = new LinkedList();
    //LinkedList<ComptabiliseBL> MVTvalides = new LinkedList();
    static JFrame frame;
    String idcmdfromsever = "";
    DbHandler d;

    public static LinkedList<Journal> journalListValide   ;
    public static LinkedList<Journal> journalListInvalide   ;
    AddCharge add;
    static Image img;
    
    DbHandler(String server, String dbName, String dateToday) {

        this.dateToday = dateToday; 
        this.dbName=dbName;
        this.server=server;
        connect(server, dbName);
        DbHandler.img=imgLogg();
        
    }

 
private  void connect(String server,String dbName)
{
  String  connectionURL = "jdbc:derby://"+server+":1527/"+dbName+"";  
  
  try { 		 
conn = DriverManager.getConnection(connectionURL+Main.derbyPwd);
conn2 = DriverManager.getConnection(connectionURL+Main.derbyPwd);
conn3 = DriverManager.getConnection(connectionURL+Main.derbyPwd);

conn.setSchema("APP"); conn2.setSchema("APP"); conn3.setSchema("APP"); 
state = conn.createStatement(); state2 = conn2.createStatement(); state3 = conn3.createStatement();

//System.out.println(conn.getSchema()+"   Connected database successfully.. cpta db ."+connectionURL);
    } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "CONNECTION FAIL TO cpta "+connectionURL);
            System.exit(1);
            System.out.println("Connection Fail " + ex);
        }
}
 
    public Image imgLogg() {

        //String imgPath = getValue("logo", "FACTURE"); 
        return getImage("");
 
    }

    

    public Image getImage(String path) {
        try {
            return ImageIO.read(new File("logo.gif"));
        } catch (IOException ex) {
            Logger.getLogger(Map.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void closeConnection() {
        try {

            conn.close();

            if (driver.equals("org.apache.derby.jdbc.EmbeddedDriver")) {
                boolean gotSQLExc = false;
                try {
                    DriverManager.getConnection("jdbc:derby:;shutdown=true");
                } catch (SQLException se) {
                    if (se.getSQLState().equals("XJ015")) {
                        gotSQLExc = true;
                    }
                }
                if (!gotSQLExc) {
                    System.out.println("Database did not shut down normally");
                } else {
                    System.out.println("Database shut down normally");
                }
            }
        } catch (SQLException e) {
        }
    }

   
    
    public double getOpenBalance2(Statement states, String sqlTier, String compte,String searchStart)
{
  String  sqlSearch="select sum(MONTANT_DEBIT*DEVISE_RATE)-sum(MONTANT_CREDIT*DEVISE_RATE) from APP.CPT_JOURNAL"
                + "  where "+sqlTier
                + " ( APP.CPT_JOURNAL.COMPTE_DEBIT like  '" + compte + "%'   OR "
                + " APP.CPT_JOURNAL.COMPTE_CREDIT like  '" + compte + "%') AND ETAT <>'DELETED'"
                + searchStart + " ";
  
  System.out.println(     sqlSearch);        

try 
{
ResultSet outResultb = states.executeQuery(sqlSearch);

while (outResultb.next()) { 
    
return outResultb.getDouble(1);  
 
}

} catch (SQLException e) {
//System.out.println(" . . method : getBalanceDette(). exception thrown: " + e);
insertError(e + "", " getOpenBalance  ");
}
return 0;
}
      
    
     Variable getParam(String famille, String mode, String nom) {

        // //System.out.println("select  * from APP.VARIABLES where APP.variables.nom_variable= '"+umukozi+"'   and APP.variables.famille_variable= '"+famille+"' ");
        Variable par = null;
        try {
          ResultSet outResult = state.executeQuery("select  * from APP.VARIABLES where APP.variables.nom_variable= '" + nom + "'  and APP.variables.famille_variable= '" + famille + "' ");

            while (outResult.next()) {
                //  //System.out.println(outResult.getString(2));
                String num = outResult.getString(1);
                String value = outResult.getString(2);
                String fam = outResult.getString(3);
                String value2 = outResult.getString(4);
                par = (new Variable(num, value, fam, value2));
            }

        } catch (SQLException e) {
            insertError(e + "", "getparm autoout");
        }
        return par;
    }

       public Variable getParamCmd(String famille, String mode, String nom) {
        //System.out.println("select  * from APP.VARIABLES where APP.variables.famille_variable= '"+umukozi+"' and APP.variables.value2_variable= '"+mode+"' and APP.variables.famille_variable= '"+famille+"' ");
        Variable par = null;
        try {
          ResultSet outResult = state.executeQuery("select  * from APP.VARIABLES where APP.variables.nom_variable= '" + nom + "' and APP.variables.value2_variable= '" + mode + "' and APP.variables.famille_variable= '" + famille + "' ");

            while (outResult.next()) {
                //  System.out.println(outResult.getString(2));
                String num = outResult.getString(1);
                String value = outResult.getString(2);
                String fam = outResult.getString(3);
                String value2 = outResult.getString(4);
                par = (new Variable(num, value, fam, value2));
            }
        } catch (SQLException e) {
            insertError(e + "", "getparm autoout");
        }
        return par;
    }

    
    public Tiers getTiers(String code, String mod) {
        Tiers c = null; 
        ResultSet outResult; 
        try {
//System.out.println("select * from APP.CPT_TIERS WHERE NUM_TIERS='" + code + "' OR (APP.CPT_TIERS.DESIGNATION LIKE '" + code + "%')  ORDER BY NUM_TIERS");
            switch (mod) {
                case "OUI":
                    outResult = state.executeQuery("select * from APP.CPT_TIERS WHERE NUM_TIERS='" + code
                            + "' OR (APP.CPT_TIERS.DESIGNATION LIKE '" + code + "%')  ORDER BY NUM_TIERS");
                    break;
                case "N":
                    outResult = state.executeQuery("select * from APP.CPT_TIERS WHERE (APP.CPT_TIERS.DESIGNATION LIKE '"
                            + code + "%')  ORDER BY NUM_TIERS");
                    break;
                default:
                   outResult = state.executeQuery("select * from APP.CPT_TIERS  WHERE NUM_TIERS='" + code + "'");
                    break;
            }

            while (outResult.next()) {

                String numTiers = outResult.getString(1);
                String compteT = outResult.getString(2);
                String designation = outResult.getString(3);
                String info = outResult.getString(4);
                String adresse = outResult.getString(5);
                String devise = outResult.getString(6);
                String sigle = outResult.getString(7);
                String numAffilie = outResult.getString(8);
int sal = outResult.getInt(9);
                String ext = outResult.getString(10);
                String email = outResult.getString("EMAIL");
                c = new Tiers(numTiers, compteT, designation, info, adresse,email, devise,
                        sigle, numAffilie,sal,ext);
            }
            //  Close the resultSet
            outResult.close();
        } catch (SQLException e) {

            System.out.println(" . . inv . exception thrown:");
            insertError(e + "", " V getAlltiers");
        }
        return c;
    }

    /**
     * *****************************************************************************
     * METHODE QUI SELECT LE TIERS QU'ON VEUT DANS LA BASE DE DONNEES
     * *****************************************************************************
     * @param aff
     * @return 
     */
    
    public Tiers getTiersb(String aff) {

        Tiers c = null;

        try {
            String sql="select * from APP.CPT_TIERS WHERE"
                    + " APP.CPT_TIERS.num_tiers = '" + aff + "'";
////// System.out.println(sql);
            try (ResultSet outResult = state.executeQuery(sql)) {
                while (outResult.next()) {
                    String numTiers = outResult.getString(1);
                    String compteT = outResult.getString(2);
                    String designation = outResult.getString(3);
                    String info = outResult.getString(4);
                    String adresse = outResult.getString(5);
                    String devise = outResult.getString(6);
                    String sigle = outResult.getString(7);
                    String numAffilie = outResult.getString(8);
                    int salaireBrute = outResult.getInt(9);
                    String EXT_NUM = outResult.getString(10);
                    String email = outResult.getString("EMAIL");
                    return new Tiers(numTiers, compteT, designation, info, adresse,email, devise, sigle, numAffilie, salaireBrute,EXT_NUM);
                }
                //  Close the resultSet
            }
        } catch (SQLException e) {
            
            System.out.println(" . . inv . exception thrown:");
            insertError(e + "", " V getAlltiers");
        }
        return c;
    }

    /**
     * *****************************************************************************
     * METHODE QUI SELECT LE TIERS QU'ON VEUT DANS LA BASE DE DONNEES
     * *****************************************************************************
     */
      Tiers getTiersDes(String des) {
        Tiers c = null;
        try {
            try ( //System.out.println("select * from APP.CPT_TIERS WHERE APP.CPT_TIERS.num_affiliation = '"+aff+"'");
                    ResultSet outResult2 = state.executeQuery("select * from APP.CPT_TIERS WHERE APP.CPT_TIERS.designation = '" + des + "'")) {
                while (outResult2.next()) {
                    String numTiers = outResult2.getString(1);
                    String compteT = outResult2.getString(2);
                    String designation = outResult2.getString(3);
                    String info = outResult2.getString(4);
                    String adresse = outResult2.getString(5);
                    String devise = outResult2.getString(6);
                    String sigle = outResult2.getString(7);
                    String numAffilie = outResult2.getString(8);
                    int sal = outResult2.getInt(9);
                    String ext = outResult2.getString(10);
                    String email = outResult2.getString("EMAIL");
                    return new Tiers(numTiers, compteT, designation, info, adresse,email, devise,
                            sigle, numAffilie,sal,ext);
                }
                //  Close the resultSet
            }
        } catch (SQLException e) {
            /*
             * Catch all exceptions and pass them to * the exception reporting
             * method
             */
            System.out.println(" . . inv . exception thrown:");
            insertError(e + "", " V getAlltiers des");
        }
        return c;
    }
 
    public Tiers getTiersNum(int code) {
        Tiers c = null;
        try {
            try (ResultSet outResult = state.executeQuery("select * from APP.CPT_TIERS WHERE NUM_TIERS=" + code + "  ")) {
                while (outResult.next()) {
                    String numTiers = outResult.getString(1);
                    String compteT = outResult.getString(2);
                    String designation = outResult.getString(3);
                    String info = outResult.getString(4);
                    String adresse = outResult.getString(5);
                    String devise = outResult.getString(6);
                    String sigle = outResult.getString(7);
                    String numAffilie = outResult.getString(8);
                    int sal = outResult.getInt(9);
                    String ext = outResult.getString(10);
                    String email = outResult.getString("EMAIL");
                    c = new Tiers(numTiers, compteT, designation, info, adresse,email, devise,
                            sigle, numAffilie,sal,ext);
                    allTiers.add(c);
                }
                //  Close the resultSet
            }
        } catch (SQLException e) {
            /*
             * Catch all exceptions and pass them to * the exception reporting
             * method
             */
            System.out.println(" . . inv . exception thrown:");
            insertError(e + "", " V getAlltiers");
        }
        return c;
    }

    public Rapport[] getAllProductSums() {

        Rapport[] rap = new Rapport[5000];
        try {
            try (ResultSet outResult = state.executeQuery("select id_product,list.price,list.prix_revient,quantite,product.name_product from APP.LIST,product where   code_uni=code and id_invoice < " + idMax + " and id_invoice > " + idMin + " ")) {
                while (outResult.next()) {
                    if (rap[outResult.getInt(1)] == null) {
                        rap[outResult.getInt(1)] = new Rapport();
                    }
                    
                    rap[outResult.getInt(1)].qte = rap[outResult.getInt(1)].qte + outResult.getInt(4);
                    rap[outResult.getInt(1)].chiffre = rap[outResult.getInt(1)].chiffre + (outResult.getInt(4) * outResult.getDouble(2));
                    rap[outResult.getInt(1)].benef = rap[outResult.getInt(1)].benef + (outResult.getInt(4) * (outResult.getDouble(2) - outResult.getDouble(3)));
                    rap[outResult.getInt(1)].name = outResult.getString(5);
                }
                //  Close the resultSet
                //  Beginning of the primary catch block: uses errorPrint method
            }
        } catch (SQLException e) {
            insertError(e + "", " sums rapport");
        }
        return rap;
    }

    public Rapport[] getMoisSums(String debut, String fin) {

        Rapport[] rap = new Rapport[100];

        String mois = "01";
        String year = "10";
        int moisInt = 1;
        int yearInt = 10;
        int totalPeriode = 0;
        String current = mois + year;
        int compteur = 0;

        try {
            //System.out.println("select    invoice.ID_INVOICE ,date,total from invoice where invoice.ID_INVOICE >" + debut + " AND invoice.ID_INVOICE <" + fin + " order by ID_INVOICE ");
            try (ResultSet outResult = state.executeQuery("select    invoice.ID_INVOICE ,date,total from invoice where invoice.ID_INVOICE >" + debut + " AND invoice.ID_INVOICE <" + fin + " order by ID_INVOICE ")) {
                //System.out.println("select    invoice.ID_INVOICE ,date,total from invoice where invoice.ID_INVOICE >" + debut + " AND invoice.ID_INVOICE <" + fin + " order by ID_INVOICE ");
                while (outResult.next()) {
                    int id_invoice = outResult.getInt(1);
                    String date = outResult.getString(2);
                    int total = outResult.getInt(3);
                    // System.out.println(date+" d    c "+current);
                    if (date.contains(current)) {
                        totalPeriode = totalPeriode + total;
                        // System.out.println(date+" d  contain  c "+current);
                    } else {
                        rap[compteur] = new Rapport();
                        rap[compteur].name = current;
                        rap[compteur].qte = totalPeriode;
                        compteur++;
                        moisInt++;
                        if (moisInt < 10) {
                            mois = "0" + moisInt;
                        } else {
                            mois = "" + moisInt;
                        }
                        
                        current = mois + year;
                        
//                    System.out.println(compteur + " d  ------dfgcgfd---" + totalPeriode + "------------------------------  c " + current);

if (viewMaxQte < totalPeriode) {
    viewMaxQte = totalPeriode;
    // //System.out.println(viewMaxQte+" d  ------dfgcgfd---"+totalPeriode+"------------------------------  c "+current);
}
totalPeriode = total;

                    }
                    
                }   {
                rap[compteur] = new Rapport();
                rap[compteur].name = current;
                rap[compteur].qte = totalPeriode;

                moisInt++;
                }
                //  Close the resultSet
                //  Beginning of the primary catch block: uses errorPrint method
            }
        } catch (SQLException e) {
            insertError(e + "", " sums mois rapport");
        }

        return rap;
    }

    public Rapport[] getClientSums(String sql, String debut, String fin) {

        Rapport[] rap = new Rapport[1000];

        int totalPeriode = 0;
        String current = "AAR";
        int compteur = 0;

        try {
            try ( //System.out.println("select    invoice.ID_INVOICE ," + sql + ",total from invoice where invoice.HEURE >'" + debut + "' AND invoice.HEURE <'" + fin + " order by " + sql + " ");
                    ResultSet outResult = state.executeQuery("select    invoice.ID_INVOICE ," + sql + ",total from invoice where invoice.HEURE >'" + debut + "' AND invoice.HEURE <'" + fin + "' order by " + sql + " ")) {
                while (outResult.next()) {
                    
                    int id_invoice = outResult.getInt(1);
                    if (idMax < id_invoice) {
                        idMax = id_invoice;
                    }
                    
                    if (idMin > id_invoice) {
                        idMin = id_invoice;
                    }
                    String date = outResult.getString(2);
                    int total = outResult.getInt(3);
                    
                    //System.out.println(date+" d  contain  c "+current);
                    if (date.equals(current)) {
                        totalPeriode = totalPeriode + total;
                        // System.out.println(date+" d  contain  c "+current);
                    } else {
                        
                        rap[compteur] = new Rapport();
                        rap[compteur].name = current;
                        rap[compteur].qte = totalPeriode;
                        compteur++;
                        current = date;
                        
                        totalPeriode = total;
                        
                    }
                }   {
                        rap[compteur] = new Rapport();
                        rap[compteur].name = current;
                        rap[compteur].qte = totalPeriode;
                }
                //  Close the resultSet
                //  Beginning of the primary catch block: uses errorPrint method
            }
        } catch (SQLException e) {
            insertError(e + "", " sums mois rapport");
        }

        return rap;
    }

   
    /**
     * ****************************************************************************
     * SELECTION DE TOUS LES UTILISATEUR DANS ET LE METTRE DANS LE LINKEDLIST DE
     * ALLUSER
     * ***************************************************************************
     */
    void emp() {
        try {

            try (ResultSet outResult = state.executeQuery("select * from APP.EMPLOYE ")) {
                allEmploye = new LinkedList();
                //  System.out.println("select * from APP.EMPLOYE ");
                //  Loop through the ResultSet and print the salariesButton
                while (outResult.next()) {
                    //    System.out.println("select * from APP.EMPLOYE ");
                    allEmploye.add(new Employe(outResult.getInt(1), outResult.getInt(5), outResult.getInt(6),
                            outResult.getInt(9), outResult.getString(2), outResult.getString(3),
                            outResult.getString(4), outResult.getString(7), outResult.getString(8),
                            outResult.getString("LANG"),outResult.getString("EMAIL")));
                }
                //  Close the resultSet
                //  Beginning of the primary catch block: uses errorPrint method
            }
        } catch (SQLException e) {
            System.out.println(e);
            insertError(e + "", "employe");
        }

    }

    public boolean check_db2(int id, int carte) {
        int c = allEmploye.size();
        boolean done = false;
//System.out.println(c);
        for (int i = 0; i < c; i++) {
          //  System.out.println(c);
            Employe e = ((Employe) (allEmploye.get(i)));
         //   System.out.println(e);
            if (e.check(id, carte)) {
                done = true;
            }
        }
        return done;
    }

    public Employe getName(int id) {
        Employe s1 = null;
        int c = allEmploye.size();
        
     //   System.out.println(c);
        
        for (int i = 0; i < c; i++) {
            Employe e = ((Employe) (allEmploye.get(i)));
            
            //System.out.println(e);
            if (e.ID_EMPLOYE == id) {
                s1 = e;
            }

        }
        return s1;
    }

    /**
     * *******************************************************************************************
     * INSERTION DES ERREURS DANS LA TABLE ERROR
     * *******************************************************************************************
     * @param id
     * @param in_out
     */
      void insertLogin(String id, String in_out) {

        try {
            psInsert = conn.prepareStatement("insert into APP.LOGIN (ID_EMPLOYE,IN_OUT,ARGENT,PACKAGES )  values (?,?,?,?)");

            psInsert.setString(1, id);
            psInsert.setString(2, in_out + "C");
            psInsert.setInt(3, 0);
            psInsert.setString(4, Main.versionIshyiga);

            psInsert.executeUpdate();
            psInsert.close();

        } catch (SQLException e) {
            insertError(e + "", "insertLogin");
        }

    }

    /**
     * *******************************************************************************************
     * INSERTION DES ERREURS DANS LA TABLE ERROR
     * *******************************************************************************************
     */
      void insertError(String desi, String ERR) {
        try {

            psInsert = conn.prepareStatement("insert into APP.ERROR(DESI,USERNAME)  values (?,?)");

            if (desi.length() > 160) {
                desi = desi.substring(0, 155);
            }
            if (ERR.length() > 160) {
                ERR = ERR.substring(0, 155);
            }

            psInsert.setString(1, desi);
            psInsert.setString(2, ERR);

            psInsert.executeUpdate();
            psInsert.close();

             JOptionPane.showMessageDialog(null, "  " + desi, " Ikibazo ", JOptionPane.PLAIN_MESSAGE);

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "  " + ex, " Ikibazo ", JOptionPane.PLAIN_MESSAGE);
        }

    }

    /**
     * *****************************************************************************************
     * FONCTION QUI SELECTIONNE TOUT LE COMPTE
     *
     * @param s1
     * @return
     * *******************************************************************************
     */
    LinkedList selectCpteJournalTier(boolean byname, String contient) {

        try {

            try (ResultSet outResult = state.executeQuery("select * from APP.CPT_COMPTE ORDER BY NOM_COMPTE")) {
                allCompte = new LinkedList();
                
                while (outResult.next()) {
                    
                    String numCompte = outResult.getString(1);
                    String nomCompte = outResult.getString(2);
                    int classe = outResult.getInt(3);
                    String cpteParent = outResult.getString(4);
                    String extr = outResult.getString(5);
                    
                    Compte ce = new Compte(  numCompte,   nomCompte,   cpteParent,   classe,  extr);
                    
//System.out.println(contient+ "   "+(""+numCompte).substring(0, 2));
//                if (contient.contains(("" + numCompte).substring(0, 2))) {
// System.err.println(contient+ "   "+numCompte);
allCompte.add(ce);
//                }
                }
            }
        } catch (SQLException e) {

            insertError(e + "", " V Compte");

        }

        return allCompte;
    }

    /// je suis d'accord par iyi ordre ariko turebe ukuntu twaha possibilite umuntu kuyihindura ukaba wa tria no kumazina
    public LinkedList<Compte> selectCpte() {
        try {
            try (ResultSet outResult = state.executeQuery("select * from APP.CPT_COMPTE ORDER BY NOM_COMPTE")) {
                allCompte = new LinkedList();
                
                while (outResult.next()) {
                    
                    String numCompte = outResult.getString(1);
                    String nomCompte = outResult.getString(2);
                    int classe = outResult.getInt(3);
                    String cpteParent = outResult.getString(4);
                    String extr = outResult.getString(5);
                    
                    Compte ce = new Compte(  numCompte,   nomCompte,   cpteParent,   classe,  extr);
                    
                    
                    allCompte.add(ce);
                }
            }
        } catch (SQLException e) {

            insertError(e + "", " V Compte");

            //System.out.println(".......selectCpte: " + e);
            /*
             * Catch all exceptions and pass them to * the exception reporting
             * method
             */
        }
        return allCompte;
    }

    public LinkedList<Compte> selectCpteCreation() {
        try {

            String order = (String) JOptionPane.showInputDialog(null, "Faites votre choix de triage", "order",
                    JOptionPane.QUESTION_MESSAGE, null, new String[]{"ORDER BY NOM_COMPTE", "ORDER BY NUM_COMPTE"}, "ORDER BY NOM_COMPTE");

            String CONTAINS = JOptionPane.showInputDialog(null, "", "ENTRE LE NOM A RECHERCHER", JOptionPane.INFORMATION_MESSAGE);
            String where = "";

            if (CONTAINS != null && CONTAINS.length() > 0) {
                where = "where num_compte like '%" + CONTAINS + "%' or nom_compte like '%" + CONTAINS + "%' or "
                        + " num_compte like '%" + CONTAINS.toUpperCase() + "%' or nom_compte like '%" + CONTAINS.toUpperCase() + "%' ";
            }

            try ( //System.out.println("select * from APP.CPT_COMPTE  " + where + order);
                    ResultSet outResult = state.executeQuery("select * from APP.CPT_COMPTE  " + where + order)) {
                allCompte = new LinkedList();
                while (outResult.next()) {
                    
                    String numCompte = outResult.getString(1);
                    String nomCompte = outResult.getString(2);
                    int classe = outResult.getInt(3);
                    String cpteParent = outResult.getString(4);
                    String extr = outResult.getString(5);
                    
                    Compte ce = new Compte(  numCompte,   nomCompte,   cpteParent,   classe,  extr);
                    
                    
                    allCompte.add(ce);
                }
            }
        } catch (HeadlessException | SQLException e) {

            insertError(e + "", " V Compte");

            //System.out.println(".......selectCpte: " + e);
            /*
             * Catch all exceptions and pass them to * the exception reporting
             * method
             */
        }
        return allCompte;
    }

///old on the 09/08/2013 tugiye gukorera kumazina nkabongereza
//    public LinkedList selectCpte(boolean byname) {
//        try {
//            if (byname == true) {
//              ResultSet outResult = state.executeQuery("select * from APP.CPT_COMPTE ORDER BY NOM_COMPTE");
//                //System.out.println("selection by umukozi compte ");
//            } else {
//              ResultSet outResult = state.executeQuery("select * from APP.CPT_COMPTE ORDER BY CLASSE,NUM_COMPTE");
//                System.out.println("selection by numero compte ");
//            }
//            allCompte = new LinkedList();
//            while (outResult.next()) {
//
//                String numCompte = outResult.getString(1);
//                String nomCompte = outResult.getString(2);
//                int classe = outResult.getInt(3);
//
//                Compte ce = new Compte(numCompte, nomCompte, classe);
//
//                allCompte.add(ce);
//            }
//            outResult.close();
//        } catch (SQLException e) {
//
//            insertError(e + "", " V Compte");
//
//            /*       Catch all exceptions and pass them to
//             **       the exception reporting method             */
//
//        }
//
//        return allCompte;
//    }
    /**
     * *****************************************************************************************
     * FONCTION QUI SELECTIONNE TOUT LE COMPTE
     *
     * @param cpte
     * @return
     * *******************************************************************************
     */
    /// je suis d'accord par iyi ordre ariko turebe ukuntu twaha possibilite umuntu kuyihindura ukaba wa tria no kumazina
      LinkedList selectSousCpte(int cpte) {

        try {
            try (ResultSet outResult = state.executeQuery("select * from APP.CPT_COMPTE WHERE APP.CPT_COMPTE.CPTE_PARENT=" + cpte + "ORDER BY APP.CPT_COMPTE.NUM_COMPTE")) {
                allSousCompte = new LinkedList();
                while (outResult.next()) {
                    String numCompte = outResult.getString(1);
                    String cpteParent = outResult.getString(4);
                    String nomCompte = outResult.getString(2);
                    int classe = outResult.getInt(3);
                    String extr = outResult.getString(5);
                    
                    Compte ce = new Compte(  numCompte,   nomCompte,   cpteParent,   classe,  extr);
                    
                    allSousCompte.add(ce);
                }
            }
        } catch (SQLException e) {
            insertError(e + "", " V Compte");

            /*
             * Catch all exceptions and pass them to * the exception reporting
             * method
             */
        }
        return allSousCompte;
    }

    public String hasChild(String numCOmpte) {
        try {
            try (ResultSet outResult = state.executeQuery("select * from APP.CPT_COMPTE where cpte_parent='" + numCOmpte + "'")) {
                while (outResult.next()) {
                    return outResult.getString("NOM_COMPTE");
                }
            }
        } catch (SQLException e) {
            insertError(e + "", " V has child");
        }
        return null;
    }

    public LinkedList<Lot> getDetteFournisseur(int id_bl, Statement sd) {

        int id_bonliv = id_bl;
        LinkedList<Lot> lotYazo = new LinkedList< >();

        try {

            try (ResultSet outResult = sd.executeQuery("select SOLDE,PAYED,NUMERO_QUITANCE from APP.CREDIT_FRSS WHERE ID_BON_LIVRAISON = " + id_bonliv)) {
                while (outResult.next()) {
                    
                    int solde = outResult.getInt(1);
                    int payed = outResult.getInt(2);
                    String numQuit = outResult.getString(3);
                    
                    lotYazo.add(new Lot(solde, payed, numQuit));
                }
            }
        } catch (SQLException e) {
            System.out.println(" . . getDetteFournisseur() . exception thrown:" + e);
            insertError(e + "", " getDetteFournisseur()  ");
        }
        return lotYazo;
    }

    public Tiers getTiersAff(String aff, Statement st) {

        Tiers c = null;

        try {
            try ( //System.out.println("select * from APP.CPT_TIERS WHERE APP.CPT_TIERS.num_affiliation = '"+aff+"'");
                    ResultSet outResult = st.executeQuery("select * from APP.CPT_TIERS WHERE APP.CPT_TIERS.NUM_AFFILIATION = '" + aff + "'")) {
                while (outResult.next()) {
                    String numTiers = outResult.getString(1);
                    String compteT = outResult.getString(2);
                    String designation = outResult.getString(3);
                    String info = outResult.getString(4);
                    String adresse = outResult.getString(5);
                    String devise = outResult.getString(6);
                    String sigle = outResult.getString(7);
                    String numAffilie = outResult.getString(8);
                    int sal = outResult.getInt(9);
                    String ext = outResult.getString(10);
                    String email = outResult.getString("EMAIL");
                    c = new Tiers(numTiers, compteT, designation, info, adresse,email, devise,
                            sigle, numAffilie,sal,ext);
                }
                //  Close the resultSet
            }
        } catch (SQLException e) {
            System.out.println(" getTiersAff(). exception thrown:" + e);
            insertError(e + "", " V getAlltiers");
        }
        return c;
    }

    public Tiers getTiersAff2(String Nom_client, String compte ,Statement s) {

        Tiers c = null;

        try {

            try (ResultSet outResult77 = s.executeQuery(
"select * from APP.CPT_TIERS  WHERE ( APP.CPT_TIERS.DESIGNATION = '" + Nom_client + "'  "
+ " OR APP.CPT_TIERS.SIGLE_TIERS = '"+ Nom_client + "' "
+ " OR APP.CPT_TIERS.NUM_AFFILIATION = '" + Nom_client + "' ) and (NUM_COMPTE  ) like '" + compte + "%' "
)) {
                while (outResult77.next()) {
                    
                    String numTiers = outResult77.getString(1);
                    String compteT = outResult77.getString(2);
                    String designationn = outResult77.getString(3);
                    String info = outResult77.getString(4);
                    String adresse = outResult77.getString(5);
                    String devise = outResult77.getString(6);
                    String sigle = outResult77.getString(7);
                    String numAffilie = outResult77.getString(8);
                    int sal = outResult77.getInt(9);
                String ext = outResult77.getString(10);
                String email = outResult77.getString("EMAIL");
                c = new Tiers(numTiers, compteT, designationn, info, adresse,email, devise,
                        sigle, numAffilie,sal,ext);
                }
            }

        } catch (SQLException e) {
            System.out.println(" getTiersAff2. exception thrown:" + e);
            insertError(e + "", " V getAlltiers");
        }

        return c;

    }

    public Tiers mumpe(String NumAffiliation ,String numCompte)
    {
        Tiers tier= getTiersCompte(  NumAffiliation ,  numCompte);
        
        if(tier!=null)
        {return tier;}
        else
        {int nextNumTiers = nextCpteFournisseurOrClient(numCompte) + 1;
 
            if (nextNumTiers != -1) { 
                if (nextNumTiers == 1) { 
                    String nextnumtiers = numCompte+ "001"; 
                    nextNumTiers = Integer.parseInt(nextnumtiers);
                    //to insert the new created tier-fournisseur muri table y'aba tiers
                    insertTiers("" + nextNumTiers, "" + numCompte, NumAffiliation, NumAffiliation,
                            "", "", NumAffiliation,"", NumAffiliation, "" + 0);
                  } else {
                   insertTiers("" + nextNumTiers, "" + numCompte, NumAffiliation, NumAffiliation,
                           "", "", NumAffiliation,"", NumAffiliation, "" + 0);
      } 
            }
            
            Tiers tt=getTiersCompte(  NumAffiliation ,  numCompte);
            
            if(tt==null)
            {
            JOptionPane.showMessageDialog(frame, " FATAL ERROR CONTACT ISHYIGA DEV TEAM\n "+numCompte+"\n "+NumAffiliation,
                    "CLOSING", JOptionPane.PLAIN_MESSAGE);
              //System.exit(0);
            }
            
           return                  tt;   
        }
    }
    
    public Tiers getTiersCompte(String NumAffiliation ,String numCompte) {
 
        Tiers c = null;

        try {

            String sql="select * from APP.CPT_TIERS WHERE "
                    + " (APP.CPT_TIERS.DESIGNATION = '" + NumAffiliation + "' "
                    + "OR APP.CPT_TIERS.SIGLE_TIERS = '" + NumAffiliation + "' OR "
                    + "APP.CPT_TIERS.NUM_AFFILIATION = '" + NumAffiliation + "' ) and (NUM_COMPTE  ) like '" + numCompte.substring(0,2 ) + "%'";
            
            System.out.println(sql);
            
            try (ResultSet outResult32 = state2.executeQuery(sql)) {
                while (outResult32.next()) {
                    String numTiers = outResult32.getString(1);
                    String compteT = outResult32.getString(2);
                    String designationn = outResult32.getString(3);
                    String info = outResult32.getString(4);
                    String adresse = outResult32.getString(5);
                    String devise = outResult32.getString(6);
                    String sigle = outResult32.getString(7);
                    String numAffilie = outResult32.getString(8);
                    
            int sal = outResult32.getInt(9);
                String ext = outResult32.getString(10);
                String email = outResult32.getString("EMAIL");
                c = new Tiers(numTiers, compteT, designationn, info, adresse,email, devise,
                        sigle, numAffilie,sal,ext);
                }
                //  Close the 
                outResult32.close();
            }

        } catch (SQLException e) {
            System.out.println(" getTiersAff3(). exception thrown:" + e);
            insertError(e + "", " V getAlltiers");
        }
        return c;
    }

    
     public String getClientName(String designation ) {

       
        try {

            try (ResultSet outResult32 = state2.executeQuery("select NUM_AFFILIATION from APP.CLIENT WHERE "
                    + " NUM_AFFILIATION = '" + designation + "' "
                    + "OR NOM_CLIENT = '" + designation + "'   ")) {
                while (outResult32.next()) {
                    return outResult32.getString(1);   
                }
                //  Close the resultSet
            }

        } catch (SQLException e) {
            System.out.println(" getTiersAff3(). exception thrown:" + e);
            insertError(e + "", " V getAlltiers");
        }
        return null;
    }

     
      public int getClientDelais(String designation ) {

       
        try {

            try (ResultSet outResult32 = state2.executeQuery("select DELAIS_LIMIT from APP.CLIENT WHERE "
                    + " NUM_AFFILIATION = '" + designation + "' "
                    + "OR NOM_CLIENT = '" + designation + "'   ")) {
                while (outResult32.next()) {
                    return outResult32.getInt(1);   
                }
                //  Close the resultSet
            }

        } catch (SQLException e) {
            System.out.println(" getTiersAff3(). exception thrown:" + e);
            insertError(e + "", " V getAlltiers");
        }
        return 0;
    }

    
    public int nextCpteFournisseurOrClient(String codeString) {

        String num_tie = ""; 
        try {

            try (ResultSet outResultnext = state.executeQuery("select max(CAST(APP.CPT_TIERS.NUM_TIERS AS INT)) from APP.CPT_TIERS WHERE APP.CPT_TIERS.NUM_COMPTE = '" + codeString + "'")) {
                while (outResultnext.next()) {
                    num_tie = "" + outResultnext.getInt(1);
                }
            }

        } catch (SQLException e) {

            //System.out.println(" . . inv . exception thrown:" + e);
            insertError(e + "", " V getAlltiers");

        }

        //System.out.println(" . . nextCpteClient, COD: " + num_tie);
        return Integer.parseInt(num_tie);
    }

    
     
public static void updateLive(String var,String value,Connection cc)
{ 
    DbHandler.write("ERROR", Main.umukozi.NOM_EMPLOYE+"  "+value);
try
{
boolean existePas = getStringRUN(  var,  cc.createStatement()).equals("");  
 
 System.out.println(existePas);
 
if(existePas)
{
  PreparedStatement  psInsert = cc.prepareStatement("insert into VARIABLES"
          + "(NOM_VARIABLE,VALUE_VARIABLE,FAMILLE_VARIABLE,VALUE2_VARIABLE,VALUE3_VARIABLE)  "
          + "values (?,?,?,?,?)");
 
    psInsert.setString(1,var );
    psInsert.setString(2,value); 
    psInsert.setString(3,"RUN"); 
    psInsert.setString(4,value);
    psInsert.setString(5,value);
    psInsert.executeUpdate();
    System.out.println("insert ........... "+value);
}
else
{
    System.out.println(" update APP.VARIABLES set VALUE_VARIABLE ='"+value+"',"
            + "VALUE2_VARIABLE ='"+value+"',VALUE3_VARIABLE ='"+value+"' "
            + "where NOM_VARIABLE='"+var+"' ");
    
    cc.createStatement().execute(" update APP.VARIABLES set VALUE_VARIABLE ='"+value+"',"
            + "VALUE2_VARIABLE ='"+value+"',VALUE3_VARIABLE ='"+value+"' "
            + "where NOM_VARIABLE='"+var+"' ");
    
    
          
}

}
catch (SQLException e){
  System.out.println( "PATH " + var + " INTROUVABLE \n" + e); 
  JOptionPane.showMessageDialog(null, "PATH " + var + " INTROUVABLE \n CAUSE : " + e, "ERROR", JOptionPane.ERROR_MESSAGE);
 }
 
}   

    
    String getPath(String info,Container content)
{

   JFileChooser chooser = new JFileChooser();
    String path=getString(info, state);
    System.out.println(path);
    chooser.setCurrentDirectory(new File(path));
    int returnVal = chooser.showOpenDialog(content);
    if (returnVal == JFileChooser.APPROVE_OPTION) {
        System.out.println("FILE"+ chooser.getSelectedFile().getName());
    }
    path="" +chooser.getCurrentDirectory();
    String s = "" + chooser.getCurrentDirectory() + '\\' + chooser.getSelectedFile().getName();
    System.out.println(s);
    updateLive(info, path ,conn);    
   return s; 
}

       public static  boolean isDouble(String var) {
        try {
            Double.parseDouble(var);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
    
  
public Compte getMiniCompte(String mini) {

  try { 
      
      String sql="select * from APP.CPT_COMPTE where EXT_NUM_COMPTE like '%" + mini + "%' ";
      try ( //System.out.println(sql);
              ResultSet outResult = state.executeQuery(sql)) {
          while (outResult.next()) {
              String numCompte = outResult.getString(1);
              String nomCompte = outResult.getString(2);
              int classe = outResult.getInt(3);
              String cpteParent = outResult.getString(4);
              String extr = outResult.getString(5);
              return new Compte(  numCompte,   nomCompte,   cpteParent,   classe,  extr);
              
          }
      } 
    } catch (SQLException e) {
        System.out.println(".....String getCompte(String num_compte)" + e);
            }
  
      return null;  
    } 

 String dateFormat(String date) {

     //2014-01-01
     //0123456789
        String day ,mm ,year  ;

        
            day  =  date.substring(2, 4);
            mm   =  date.substring(5, 7);
            year =  date.substring(8, 10);
        return year  + mm + day;
    }
String getMiniJournal(String compte) {
        
        try {
            String sql="select * from APP.CPT_JOURNAUX where  COMPTE_JOURNAL='"+compte+"'";
            try ( //System.out.println(sql);
                    ResultSet outResult = state.executeQuery(sql)) {
                while (outResult.next()) {
                    return outResult.getString(1);
                }
            }
        } catch (SQLException e) {
            System.out.println(".....cptJournaux " + e);
        }
        
       return null;
    }

boolean getMiniOperation(String libele,String NOM_UTILISATEUR,String JOURNAL,String PROFIT_CENTER)
{ 
                    
    String sql="select * from APP.CPT_JOURNAL   where APP.CPT_JOURNAL.LIBELLE = '" 
            + libele + "' AND NUM_TIERS='"+NOM_UTILISATEUR
            +"' AND ID_JOURNAUX='"+JOURNAL+"' AND PROFIT_CENTER='"+PROFIT_CENTER+"' "  ;      
               System.out.println(sql);     
    try { 
        try ( //System.out.println(sql);
                ResultSet outResult = state.executeQuery(sql)) {
            while (outResult.next()) {
                System.err.println(sql);
                return true;
            }   }
        } catch (SQLException e) {
            System.out.println(".....cptJournaux " + e);
        }
                      return false;
}

boolean getExisteOperation(String libele,String JOURNAL)
{ 
                    
    String sql="select * from APP.CPT_JOURNAL   where APP.CPT_JOURNAL.LIBELLE = '" 
            + libele + "' AND ID_JOURNAUX='"+JOURNAL+"' "  ;      
          System.out.println(sql);          
    try { 
        try ( 
                ResultSet outResult = state.executeQuery(sql)) {
            while (outResult.next()) {
                return true;
            }   }
        } catch (SQLException e) {
            System.out.println(".....cptJournaux " + e);
        }
                      return false;
}

boolean getExisteJournal(String CODE_JOURNAL)
{ 
                   
    String sql="select * from APP.CPT_JOURNAUX  where APP.CPT_JOURNAUX.CODE_JOURNAL = '" 
            + CODE_JOURNAL + "' "  ;      
          //System.out.println(sql);          
    try { 
        try ( 
                ResultSet outResult = state.executeQuery(sql)) {
            while (outResult.next()) {
                return true;
            }   }
        } catch (SQLException e) {
            System.out.println(".....getExisteJournal " + e);
        }
                      return false;
}


static JTable getJTableMini(LinkedList<Journal> inv) {

        JTable jTable3 = new JTable();
        int ligne2 = inv.size();
        String[][] s2 = new String[ligne2 + 1][9]; 
        double TOTAL_DEBIT=0;
        double TOTAL_CREDIT=0;
        int x = 0;
        for (; x < ligne2; x++) {

            Journal C = inv.get(x);
            s2[x][0] = "" + C.ctiers2;
            s2[x][1] = C.dateDoc;
            s2[x][2] = "" + C.libelle;
            s2[x][3] =  (C.compteDebit); 
            s2[x][4] = (C.compteCredit); 
            s2[x][5] = DbHandler.setVirgule(C.montantDebit, 1, 0.005);
            TOTAL_DEBIT += C.montantDebit;
            s2[x][6] = DbHandler.setVirgule(C.montantCredit, 1, 0.005); 
            TOTAL_CREDIT += C.montantCredit;
            s2[x][7] = (C.journaux); 
            s2[x][8] = (C.utilisateur); 
        }

        s2[x][1] = "TOTAL ";
        s2[x][6] = DbHandler.setVirgule(TOTAL_CREDIT, 1, 0.005); 
        s2[x][5] = DbHandler.setVirgule(TOTAL_DEBIT, 1, 0.005); 

        jTable3.setModel(new javax.swing.table.DefaultTableModel(s2, new String[]
        { "NUM_CLIENT", "DATE_INVOICE","LIBELLE", "COMPTE DEB", "COMPTE CRE", "MONTANT DEB", "MONTANT CRE", 
            "JOURNAL", "USER"}));


        jTable3.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        jTable3.getColumnModel().getColumn(1).setPreferredWidth(200);
        jTable3.getColumnModel().getColumn(1).setPreferredWidth(100);
        jTable3.doLayout();
        jTable3.validate();

        return jTable3;
    }
                

   
    static String getMac()
   {
       try {
                Process p = Runtime.getRuntime().exec("getmac /fo csv /nh");
                java.io.BufferedReader in = new java.io.BufferedReader(new  java.io.InputStreamReader(p.getInputStream()));
                String line;
                line = in.readLine();        
                String[] result = line.split(",");
                String res=result[0].replace('"', ' ').trim();
                System.out.println(res);
                return res;
        } catch (IOException ex) {
            //insertError(ex+""," getMac");
            System.out.println("getmac:"+ex);
        }
       return "Disabled"; 
   }
    
       public static String getString(String s,Statement stat) {
        
        try {
           ResultSet outResult = stat.executeQuery("select  *"
                   + " from APP.VARIABLES where APP.variables.nom_variable="
                   + " '" + s + "MAIN" + "'  and APP.variables.famille_variable= 'RUN' ");
//System.out.println("select  *"
//                   + " from APP.VARIABLES where APP.variables.nom_variable="
//                   + " '" + s + "MAIN" + "'  and APP.variables.famille_variable= 'RUN' ");
            while (outResult.next()) { 
                return outResult.getString(2); 
            }
        } catch (SQLException e) {
            System.out.println(e + " getparm autoout");
        }
        return "";
    }
           public static String getStringRUN(String s,Statement stat) {
        
        try {
           ResultSet outResult = stat.executeQuery("select  *"
                   + " from APP.VARIABLES where APP.variables.nom_variable="
                   + " '" + s + "'  and APP.variables.famille_variable= 'RUN' ");
//System.out.println("select  *"
//                   + " from APP.VARIABLES where APP.variables.nom_variable="
//                   + " '" + s + "MAIN" + "'  and APP.variables.famille_variable= 'RUN' ");
            while (outResult.next()) { 
                return outResult.getString(2); 
            }
        } catch (SQLException e) {
            System.out.println(e + " getStringRUN ");
        }
        return "";
    }
       
       
    public static boolean areYouLive(String database,String url,Connection conn2)
{
try {
String mac= getMac();           
String version =  (conn2.getMetaData().getDatabaseProductVersion()).split("-")[0].replaceAll(" ", "")+";"+mac;


String imLive=kohereza (url+"/areYouLive.php?"
        + "database="+database
        +"&databaseVersion="+version
        +"&PACKAGE=POS" 
        ,""); 
            
             //$ID_CLIENT.';'.$SERVER.';'.$COUNTRY_CODE;
            String [] t=imLive.split(";")  ;   
            
            DbHandler.updateLive("id_client",t[0],conn2);
            DbHandler.updateLive("syncurl",t[1],conn2);
            DbHandler.updateLive("COUNTRY_CODE",t[2],conn2); 
            return true;
        } catch (Exception ex) {
            System.out.println(ex +" areYouLive "+database);
             return false;
        }
}  
public static String kohereza(String url,  String valuetosend) throws Exception { 
    
		URL siteUrl = new URL(url);
		HttpURLConnection connection = (HttpURLConnection) siteUrl.openConnection();
		connection.setRequestMethod("POST");  
                connection.setDoOutput(true);
                connection.setDoInput(true); 
        try (DataOutputStream out = new DataOutputStream(connection.getOutputStream())) {
            //System.out.println(" URL  "+ url);
            //System.out.println(" START  "+new java.util.Date());
            out.writeBytes(valuetosend);
            out.flush();
            //System.out.println("FLUSH  "+new java.util.Date());
        }
        String responsesrver;
        //Thread.sleep(1000);
        try ( // Thread.sleep(1000);
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
            //Thread.sleep(1000);
            responsesrver = in.readLine();
            String line;
            String line2="";
            while ((line = in.readLine()) != null)
            {
                line2+="   "+(in.readLine() + '\n');
                //System.out.println(line);
            }  
            //System.out.println(responsesrver+" "+new java.util.Date()+" respo  "+line2);
        }
                return responsesrver;
	}

    public LinkedList<String> splitString(String tosplit,String split) {

        String[] arraydata = tosplit.split(split); 
        LinkedList<String> listfromserver = new LinkedList(); 
        listfromserver.addAll(Arrays.asList(arraydata)); //  System.out.println("....................content ya arraydata" + arraydata1);
        //System.out.println(".........DOWN"); 
        return listfromserver;

    }
 
     
    public LinkedList selectCpte(boolean byname) {

        try {
             ResultSet outResult;

            if (byname == true) {
                outResult = state.executeQuery("select * from APP.CPT_COMPTE ORDER BY NOM_COMPTE");
                //System.out.println("select * from APP.CPT_COMPTE ORDER BY NOM_COMPTE");
                //System.out.println("selection by nom compte ");

            } else {

               outResult = state.executeQuery("select * from APP.CPT_COMPTE ORDER BY CLASSE,NUM_COMPTE");

                //System.out.println("select * from APP.CPT_COMPTE ORDER BY CLASSE,NUM_COMPTE");
                //System.out.println("selection by numero compte ");
            }

            allCompte = new LinkedList();

            int i = 0;
            while (outResult.next()) {
                String numCompte = outResult.getString(1);
                String nomCompte = outResult.getString(2);
                int classe = outResult.getInt(3);
                String cpteParent = outResult.getString(4);
                String cat = outResult.getString(5);
                String extr = outResult.getString(6); 
                
                Compte ce = new Compte(  numCompte,   nomCompte,   cpteParent,   classe,  extr);
ce.category=cat;
ce.ext=extr; 
               //System.out.println(i + "-.-.-.-.-.-.-.-" + ce ); 
                allCompte.add(ce);
                i++; 
            }
            //System.out.println(allCompte.size()+" /////////////  before outResult.close();");

            outResult.close();
        } catch (SQLException e) {
            insertError(e + "", " V Compte");

            //System.out.println("---------error in selectCpte" + e);
        }
        return allCompte;
    }

    public LinkedList<Lot> getCompteResultat(String searchPeriode) {

        LinkedList<Compte> getCpte = ComptesCompteResultat(); //hano ni gukurura liste de tous les comptes ziri ranges par ordre de priorite muri balance de verification     
//        LinkedList<Compte> getCpte2 = CompteBalanceVer2();
        LinkedList<Lot> lotYazo = new LinkedList< >();

        try {
            for (int k = 0; k < getCpte.size(); k++) { //hano ni kunyura kuri buri compte....

                Compte compte = getCpte.get(k);
                String COMPTE = "" + compte.numCompte;
                String NAME_COMPTE = compte.nomCompte;
                try (ResultSet outResult = state.executeQuery("select sum(MONTANT_DEBIT),sum(MONTANT_CREDIT), sum(MONTANT_DEBIT)-sum(MONTANT_CREDIT) from APP.CPT_JOURNAL"
                        + "  where APP.CPT_JOURNAL.COMPTE_DEBIT = '" + COMPTE + "' " + searchPeriode + " OR "
                                + "  APP.CPT_JOURNAL.COMPTE_CREDIT = '" + COMPTE + "'" + searchPeriode)) {
                    while (outResult.next()) {
                        
                        double montantdebit = outResult.getDouble(1);
                        double montantcredit = outResult.getDouble(2);
                        
                        if (!(montantdebit == 0 && montantcredit == 0)) {
                            if (montantcredit == 0) {
                                getCpte.get(k).montantDebit = getCpte.get(k).montantDebit + outResult.getDouble(1);
                                lotYazo.add(new Lot(NAME_COMPTE, getCpte.get(k).montantDebit, getCpte.get(k).category, COMPTE));
                            } else if (montantdebit == 0) {
                                getCpte.get(k).montantCredit = getCpte.get(k).montantCredit + outResult.getDouble(2);
                                lotYazo.add(new Lot(NAME_COMPTE, getCpte.get(k).montantCredit, getCpte.get(k).category, COMPTE));
                            }       }
                    }
                }
            }
            
        } catch (SQLException e) {
            //System.out.println(" . . inv . exception thrown:" + e);
            insertError(e + "", " getBalanceVerification  ");
        }
//        return getCpte;
        return lotYazo;
    }

    public LinkedList<Lot> getCompteResultatV3(String search) {

        LinkedList<Compte> getCpte = ComptesCompteResultat(); //hano ni gukurura liste de tous les comptes ziri ranges par ordre de priorite muri balance de verification     
//        LinkedList<Compte> getCpte2 = CompteBalanceVer2();APP.CPT_CATEGORIES_COMPTE.CATEGORY_FAMILY = 'CHARGES'

        LinkedList<Lot> lotYazo = new LinkedList< >();
String res="";
        try {

            for (int k = 0; k < getCpte.size(); k++) { //hano ni kunyura kuri buri compte....

                Compte compte = getCpte.get(k);
                String COMPTE = "" + compte.numCompte;
                String NAME_COMPTE = compte.nomCompte;
 
                String sql = "select sum(MONTANT_DEBIT*DEVISE_RATE), sum(MONTANT_CREDIT*DEVISE_RATE), sum(MONTANT_DEBIT*DEVISE_RATE)-sum(MONTANT_CREDIT*DEVISE_RATE) from APP.CPT_JOURNAL"
                        + " where (APP.CPT_JOURNAL.COMPTE_DEBIT = '" + COMPTE + "' OR APP.CPT_JOURNAL.COMPTE_CREDIT = '" + COMPTE + "') "
                        + "and postdater = 0 AND ETAT != 'DELETED' " + search;

//                System.out.println(sql);
                try (ResultSet outResult = state.executeQuery(sql)) {
                    //                System.out.println(sql);
                    
                    while (outResult.next()) {
                        
                        
                        double montantdebit = outResult.getDouble(1);
                        double montantcredit = outResult.getDouble(2);
                        
                        if (!(montantdebit == 0 && montantcredit == 0)) {
                            
                            if(isCatsGood(getCpte.get(k)))
                            {
                                getCpte.get(k).montantDebit = getCpte.get(k).montantDebit + montantdebit;
                                getCpte.get(k).montantCredit = getCpte.get(k).montantCredit + montantcredit;
                                lotYazo.add(new Lot(NAME_COMPTE, montantdebit-montantcredit, getCpte.get(k).category, COMPTE));
                                
                            }
                            else
                            {
                                 System.err.println(getCpte.get(k).numCompte);
                                
                                res+= getCpte.get(k).toString1()+"\n";
                            }
                            
                                 
                            
                        }
                    }
                }
            }
           

        } catch (SQLException e) {

           System.out.println(" . . DEVISE_ . exception thrown:" + e);
            insertError(e + "", " getBalanceVerification  ");

        }
        
        alertError(res);
        
//        return getCpte;
        return lotYazo;
    }
 
    public LinkedList<Lot> getBilanCapital(String searchPeriode) {

        LinkedList<Compte> getCpte = ComptesBilan(); //hano ni gukurura liste de tous les comptes ziri ranges par ordre de priorite muri balance de verification     

        LinkedList<Lot> lotYazo = new LinkedList< >(); 
        try {
            for (int k = 0; k < getCpte.size(); k++) { //hano ni kunyura kuri buri compte....

                Compte compte = getCpte.get(k);

                //System.out.println("comptes du bilan .... numcompte: " + getCpte.get(k).numCompte);

                String COMPTE = "" + compte.numCompte;
                String NAME_COMPTE = compte.nomCompte;

                if (!(COMPTE.equalsIgnoreCase("101"))) {
                    //...ukurura solde yayo from the journal 
//              ResultSet outResult = state.executeQuery("select sum(MONTANT_DEBIT)-sum(MONTANT_CREDIT) from APP.CPT_JOURNAL"
//                        + "  where APP.CPT_JOURNAL.COMPTE_DEBIT = '" + COMPTE + "' OR"
//                        + "  APP.CPT_JOURNAL.COMPTE_CREDIT = '" + COMPTE + "' ");

                    //...ukurura solde yayo from the journal 
                    String sql = "select sum(MONTANT_DEBIT),sum(MONTANT_CREDIT), sum(MONTANT_CREDIT)-sum(MONTANT_DEBIT) from APP.CPT_JOURNAL"
                            + "  where APP.CPT_JOURNAL.COMPTE_DEBIT = '" + COMPTE + "' " + searchPeriode + " OR "
                            + "  APP.CPT_JOURNAL.COMPTE_CREDIT = '" + COMPTE + "'" + searchPeriode;
                    try (ResultSet outResult = state.executeQuery("select sum(MONTANT_DEBIT),sum(MONTANT_CREDIT), sum(MONTANT_CREDIT)-sum(MONTANT_DEBIT) from APP.CPT_JOURNAL"
                            + "  where APP.CPT_JOURNAL.COMPTE_DEBIT = '" + COMPTE + "' " + searchPeriode + " OR "
                                    + "  APP.CPT_JOURNAL.COMPTE_CREDIT = '" + COMPTE + "'" + searchPeriode)) {
                        while (outResult.next()) {
                            
                            double sumMontantdebit = outResult.getDouble(1);
                            double sumMontantcredit = outResult.getDouble(2);
                            double balanceCompte = outResult.getDouble(3);
//                    int md = 0;
//                    int mc = 0;

if (!(sumMontantdebit == 0 && sumMontantcredit == 0)) {
    lotYazo.add(new Lot(NAME_COMPTE, balanceCompte, getCpte.get(k).category, COMPTE));
//                        }
//                        System.out.println(".....check categories... current one: " + getCpte.get(k).category + ".......account number: " + getCpte.get(k).numCompte + ".......account name: " + getCpte.get(k).nomCompte + "...............montantdebit: " + sumMontantdebit + "...............montantcredit: " + sumMontantcredit);
//System.out.println(sql);
//                            System.out.println(".....check categories... current one: " + getCpte.get(k).category + ".......account number: "
//                                    + getCpte.get(k).numCompte + ".......account name: " + getCpte.get(k).nomCompte + "...............balanceCompte: " + balanceCompte);
//                        getCpte.get(k).montant = getCpte.get(k).montant + outResult.getInt(3);             
}
                        }
                    }
                } 
            }
            
        } catch (SQLException e) {
            System.out.println(" . . inv . exception thrown:" + e);
            insertError(e + "", " getBalanceVerification  ");
        }
//        return getCpte;
        return lotYazo;
    }

    public double getBalanceCapital() {

        double balanceCapital = 0;

        try {

            String COMPTE = "" + 101;

            try (ResultSet outResult = state.executeQuery("select sum(MONTANT_CREDIT)-sum(MONTANT_DEBIT) from APP.CPT_JOURNAL"
                    + "  where APP.CPT_JOURNAL.COMPTE_DEBIT = '" + COMPTE + "'  OR "
                            + "  APP.CPT_JOURNAL.COMPTE_CREDIT = '" + COMPTE + "'")) {
                while (outResult.next()) {
                    balanceCapital = outResult.getDouble(1);
                    
                    //System.out.println("capital: " + balanceCapital);
                }
            }
        } catch (SQLException e) {
            //System.out.println(" . . inv . exception thrown:" + e);
            insertError(e + "", " getBalanceVerification  ");
        }
        return balanceCapital;
    }

    public int daysBetween(Date time, Date time1) {
        return (int) ((time1.getTime() - time.getTime()) / (1000 * 60 * 60 * 24));
    }

    
    public LinkedList<Compte> ComptesCompteResultat() {

        LinkedList<Compte> allCpt = new LinkedList();

        try {
            //hano ni gukurura liste de tous les comptes ari ranges par ordre de priorite muri balance de verification  
         
String sql="select NUM_COMPTE,NOM_COMPTE,0,CATEGORY "
        + "from APP.CPT_COMPTE,APP.CPT_CATEGORIES_COMPTE where "
        + "   (CPT_COMPTE.CATEGORY = CPT_CATEGORIES_COMPTE.CATEGORY_NAME "
        + "AND CPT_CATEGORIES_COMPTE.CATEGORY_FAMILY = 'CHARGES') "
        + "OR (CPT_COMPTE.CATEGORY = CPT_CATEGORIES_COMPTE.CATEGORY_NAME"
        + " AND CPT_CATEGORIES_COMPTE.CATEGORY_FAMILY = 'CHARGES') ORDER BY ORDERR";
            try ( //System.out.println(sql);
                    ResultSet outResult = state.executeQuery(sql)) {
                while (outResult.next()) {
//                Compte ce = new Compte(outResult.getInt(1), outResult.getString(2), 0, 0, outResult.getString(4),0);
Compte ce = new Compte("" + outResult.getInt(1), outResult.getString(2), 0,
        "" + 0, outResult.getString(4), 0, 0);
allCpt.add(ce);
                }           }
        } catch (SQLException e) {
            //System.out.println(" . . inv . exception thrown: compte");
            insertError(e + "", " V getinvoice");
        }
        return allCpt;
    }

    public LinkedList<Lot> getCompteResultatProduit(String search) {

        LinkedList<Compte> getCpte = ComptesCompteResultat2(); //hano ni gukurura liste de tous les comptes ziri ranges par ordre de priorite muri balance de verification     
        LinkedList<Lot> lotYazo = new LinkedList< >();
        String res="";
        
        try {
            for (int k = 0; k < getCpte.size(); k++) { //hano ni kunyura kuri buri compte....

                Compte compte = getCpte.get(k);
                String COMPTE = "" + compte.numCompte;
                String NAME_COMPTE = compte.nomCompte;
 
       String sql = ("select sum(MONTANT_DEBIT*DEVISE_RATE),sum(MONTANT_CREDIT*DEVISE_RATE), sum(MONTANT_DEBIT*DEVISE_RATE)-sum(MONTANT_CREDIT*DEVISE_RATE) from APP.CPT_JOURNAL"
                        + "  where (APP.CPT_JOURNAL.COMPTE_DEBIT = '" + COMPTE + "' OR"
                        + "  APP.CPT_JOURNAL.COMPTE_CREDIT = '" + COMPTE + "') and postdater = 0 AND ETAT != 'DELETED'" + search);

                //  System.err.println(sql);
                try (ResultSet outResult = state.executeQuery(sql)) {
                    //System.out.println(sql);
                    
                    while (outResult.next()) {
                        
                        double montantdebit = outResult.getDouble(1);
                        double montantcredit = outResult.getDouble(2);
                        
                        if (!(montantdebit == 0 && montantcredit == 0)) {
                            
                            if(isCatsGood(getCpte.get(k)))
                            {
                                getCpte.get(k).montantDebit = getCpte.get(k).montantDebit + montantdebit;
                                getCpte.get(k).montantCredit = getCpte.get(k).montantCredit + montantcredit;
                                lotYazo.add(new Lot(NAME_COMPTE, montantcredit-montantdebit, getCpte.get(k).category, COMPTE));
                            }
                            else
                            {
                                System.err.println(getCpte.get(k).numCompte);
                                res+= getCpte.get(k).toString1()+"\n";
                            } 
                        } 
                    }   }
}
          
        } catch (SQLException e) {
           System.out.println(" . . DEVISE_ . exception thrown:" + e);
            insertError(e + "", " getBalanceVerification  ");
        }
        
   alertError(res);
        
//        return getCpte;
        return lotYazo;
    }

    public boolean isCatsGood(Compte compte)
    {
           System.out.println(compte.numCompte+" +++++++ "+compte.category+" +++ "+Main.lesCats);
        return compte!=null && compte.category!=null &&    Main.lesCats.contains(compte.category);
    }
    public void alertError(String res)
    {
      if(!res.equals(""))
        {
             System.out.println(Main.lesCats);
        JOptionPane.showMessageDialog(jScrollPane2, "PLEASE ATTACH FOLLOW ACCOUNT TO KNOWN CATEGORY \n OR BALLANCE ALL THE OPERATION "
                            +res, "FATAL ERROR!! ISHYIGA CLOSING", JOptionPane.ERROR_MESSAGE);
            insertError("PLEASE ATTACH FOLLOW ACCOUNT TO KNOWN CATEGORY", "alertError");
        System.exit(0);
        }
    }
   

   
    public LinkedList<String> categorieCompte() {

        LinkedList<String> allCpt = new LinkedList();

        try {
            //hano ni gukurura liste de tous les comptes ari ranges par ordre de priorite muri balance de verification  
            String sql = "select CATEGORY_NAME "
                    + "from APP.CPT_CATEGORIES_COMPTE ";
            try (ResultSet outResult2 = state.executeQuery(sql)) {
                while (outResult2.next()) {
                    allCpt.add(outResult2.getString(1));
                }
            }
        } catch (SQLException e) {
            //System.out.println(" categorieCompte");
            insertError(e + "", " categorieCompte");
        }
        return allCpt;
    }
    
      public  String  getCategorieCompte() {

         String  lesCats= "";

        try {
            //hano ni gukurura liste de tous les comptes ari ranges par ordre de priorite muri balance de verification  
            String sql = "select CATEGORY_NAME "
                    + "from APP.CPT_CATEGORIES_COMPTE ";
             try (ResultSet outResult2 = state.executeQuery(sql)) {
                 while (outResult2.next()) {
                     lesCats+="#"+(outResult2.getString(1));
                 }}
        } catch (SQLException e) {
            //System.out.println(" categorieCompte");
            insertError(e + "", " categorieCompte");
        }
        return lesCats;
    }

    public LinkedList<Compte> ComptesCompteResultat2() {

        LinkedList<Compte> allCpt = new LinkedList();

        try {
            //hano ni gukurura liste de tous les comptes ari ranges par ordre de priorite muri balance de verification  
            String sql = "select NUM_COMPTE,NOM_COMPTE,0,CATEGORY "
                    + "from APP.CPT_COMPTE,APP.CPT_CATEGORIES_COMPTE where "
                    + " (CPT_COMPTE.CATEGORY = CPT_CATEGORIES_COMPTE.CATEGORY_NAME "
                    + "AND CPT_CATEGORIES_COMPTE.CATEGORY_FAMILY = 'PRODUITS') "
                    + "OR (CPT_COMPTE.CATEGORY = CPT_CATEGORIES_COMPTE.CATEGORY_NAME "
                    + "AND CPT_CATEGORIES_COMPTE.CATEGORY_FAMILY = 'PRODUITS') "
                    + "ORDER BY ORDERR"; 
            
try (ResultSet outResult = state.executeQuery(sql)) {  
                while (outResult.next()) {
//                Compte ce = new Compte(outResult.getInt(1), outResult.getString(2), 0, 0, outResult.getString(4),0);
Compte ce = new Compte("" + outResult.getInt(1), outResult.getString(2), 0, "" + 0, outResult.getString(4), 0, 0);
allCpt.add(ce);

                }
            }

        } catch (SQLException e) {
            //System.out.println(" . . inv . exception thrown: compte");
            insertError(e + "", " V getinvoice");
        }
        return allCpt;
    }

    public LinkedList<Compte> ComptesBilan() {

        LinkedList<Compte> allCpt = new LinkedList();

        try {
            try ( //hano ni gukurura liste de tous les comptes ari ranges par ordre de priorite muri balance de verification
                    ResultSet outResult = state.executeQuery("select NUM_COMPTE,NOM_COMPTE,0,CATEGORY "
                            + "from APP.CPT_COMPTE,APP.CPT_CATEGORIES_COMPTE where "
                            + " (CPT_COMPTE.CATEGORY = CPT_CATEGORIES_COMPTE.CATEGORY_NAME AND CPT_CATEGORIES_COMPTE.CATEGORY_FAMILY = 'ACTIFS') OR (CPT_COMPTE.CATEGORY = CPT_CATEGORIES_COMPTE.CATEGORY_NAME AND CPT_CATEGORIES_COMPTE.CATEGORY_FAMILY = 'DETTES')  OR (CPT_COMPTE.CATEGORY = CPT_CATEGORIES_COMPTE.CATEGORY_NAME AND CPT_CATEGORIES_COMPTE.CATEGORY_FAMILY = 'CAPITAUX')   ORDER BY ORDERR")) {
                while (outResult.next()) {
//                Compte ce = new Compte(outResult.getInt(1), outResult.getString(2), 0, 0, outResult.getString(4),0);
Compte ce = new Compte("" + outResult.getInt(1), outResult.getString(2), 0, "" + 0, outResult.getString(4), 0, 0);
allCpt.add(ce);
                }
            }
        } catch (SQLException e) {
            //System.out.println(" . . inv . exception thrown: compte");
            insertError(e + "", " V getinvoice");
        }
        return allCpt;
    }

    public LinkedList<Lot> getCompteResultat2(String searchPeriode) {

        LinkedList<Compte> getCpte = ComptesCompteResultat(); //hano ni gukurura liste de tous les comptes ziri ranges par ordre de priorite muri balance de verification     
//        LinkedList<Compte> getCpte2 = CompteBalanceVer2();
        LinkedList<Lot> lotYazo = new LinkedList< >();

        try {
            for (int k = 0; k < getCpte.size(); k++) { //hano ni kunyura kuri buri compte....

                Compte compte = getCpte.get(k);
                String COMPTE = "" + compte.numCompte;
                String NAME_COMPTE = compte.nomCompte;

                try ( //...ukurura solde yayo from the journal 
//              ResultSet outResult = state.executeQuery("select sum(MONTANT_DEBIT)-sum(MONTANT_CREDIT) from APP.CPT_JOURNAL"
//                        + "  where APP.CPT_JOURNAL.COMPTE_DEBIT = '" + COMPTE + "' OR"
//                        + "  APP.CPT_JOURNAL.COMPTE_CREDIT = '" + COMPTE + "' ");
                //...ukurura solde yayo from the journal
                        ResultSet outResult = state.executeQuery("select sum(MONTANT_DEBIT),sum(MONTANT_CREDIT), sum(MONTANT_DEBIT)-sum(MONTANT_CREDIT) from APP.CPT_JOURNAL"
                                + "  where APP.CPT_JOURNAL.COMPTE_DEBIT = '" + COMPTE + "' " + searchPeriode + " OR "
                                        + "  APP.CPT_JOURNAL.COMPTE_CREDIT = '" + COMPTE + "'" + searchPeriode)) {
                    while (outResult.next()) {
                        
                        double montantdebit = outResult.getDouble(1);
                        double montantcredit = outResult.getDouble(2);
                        
                        if (!(montantdebit == 0 && montantcredit == 0)) {
                            if (montantcredit == 0) {
                                getCpte.get(k).montantDebit = getCpte.get(k).montantDebit + outResult.getDouble(1);
//                            md = (int)getCpte.get(k).montantDebit; 
//                            lotYazo.add(new Lot(NAME_COMPTE, md, getCpte.get(k).category ));
lotYazo.add(new Lot(NAME_COMPTE, getCpte.get(k).montantDebit, getCpte.get(k).category, COMPTE));
                            } else if (montantdebit == 0) {
                                getCpte.get(k).montantCredit = getCpte.get(k).montantCredit + outResult.getDouble(2);
//                            mc = (int)getCpte.get(k).montantCredit;
//                            lotYazo.add(new Lot(NAME_COMPTE, mc, getCpte.get(k).category));
lotYazo.add(new Lot(NAME_COMPTE, getCpte.get(k).montantCredit, getCpte.get(k).category, COMPTE));
                            }
//                        getCpte.get(k).montant = getCpte.get(k).montant + outResult.getInt(3);
                        }
                    }
                }
            }
            
        } catch (SQLException e) {
            //System.out.println(" . . inv . exception thrown:" + e);
            insertError(e + "", " getBalanceVerification  ");
        }
//        return getCpte;
        return lotYazo;
    }

    public LinkedList<Tiers> getAllEmployees() {
        try {
            allTiers = new LinkedList();

           // String numCompte = "42";

            try (ResultSet outResult = state.executeQuery("select * from APP.CPT_TIERS where NUM_COMPTE LIKE '42%' ORDER BY designation")) {
                while (outResult.next()) {
                    String numTiers = outResult.getString(1);
                    String compteT = outResult.getString(2);
                    String designation = outResult.getString(3);
                    String info = outResult.getString(4);
                    String adresse = outResult.getString(5);
                    String devise = outResult.getString(6);
                    String sigle = outResult.getString(7);
                    String numAffilie = outResult.getString(8);
                    int salaireBrute = outResult.getInt(9);
                    String EXT_NUM = outResult.getString(10);
                    String email = outResult.getString("EMAIL");
                    Tiers c = new Tiers(numTiers, compteT, designation, info, adresse,email, devise,
                            sigle, numAffilie, salaireBrute,EXT_NUM);
                    
                    allTiers.add(c);
                }
                //  Close the resultSet
            }
        } catch (SQLException e) {
            //System.out.println(" . . method : getAllEmployees(). exception thrown:" + e);
            insertError(e + "", " method :getAllEmployees() ");
            JOptionPane.showMessageDialog(null, "  " + e, " Ikibazo in: ", JOptionPane.PLAIN_MESSAGE);
        }
        return allTiers;
    }


    

    public boolean updateBLComptabilise(String bonLivraison) {


        boolean nn = false;

        try {

            state2.execute("update APP.BON_LIVRAISON set comptabilise ='OUI'  where BON_LIVRAISON = '" + bonLivraison + "' ");
 
            nn = true;

        } catch (SQLException e) {
            insertError(e + "", "updateBLComptabilise");
            //System.out.println(" . . method : updateBLComptabilise.exception thrown:" + e);

        }

        return nn;

    }
     public boolean updateMVTComptabilise(String ID) {


        boolean nn = false;

        try {

            state2.execute("update APP.BON_MVT_SORTIE set comptabilise ='OUI'  where ID_MVT_SORTIE ="+ID);
 
            nn = true;

        } catch (SQLException e) {
            insertError(e + "", "updateMVTComptabilise");
            //System.out.println(" . . method : updateBLComptabilise.exception thrown:" + e);

        }

        return nn;

    }

    public void updateInvoiceComptabilise(String serverVente, String databaseVente, int id_invoice) {

        try {
      
            Statement sIshyiga5 = conn3.createStatement();

            sIshyiga5.execute("update APP.INVOICE set comptabilise ='OUI'  where ID_INVOICE = " + id_invoice);

            //System.out.println("update APP.INVOICE set comptabilise ='OUI'  where ID_INVOICE = " + id_invoice);
         //System.out.println(" . . method : updateInvoiceComptabilise nayigezemo ...");

        } catch (SQLException e) {
            insertError(e + "", "updateInvoiceComptabilise");
            //System.out.println(" . . method : updateInvoiceComptabilise.exception thrown:" + e);
            JOptionPane.showMessageDialog(null, "  " + e, " Ikibazo in: updateInvoiceComptabilise", JOptionPane.PLAIN_MESSAGE);
        }

    }

    public void updateRefundComptabilise(String serverVente, String databaseVente, int id_refund) {

        try {
       
      Statement sIshyiga5 = conn3.createStatement();

            sIshyiga5.execute("update APP.REFUND set comptabilise ='OUI'  where ID_REFUND = " + id_refund);

            //System.out.println("update APP.INVOICE set comptabilise ='OUI'  where ID_REFUND = " + id_refund);

            //System.out.println(" . . method : updateRefundComptabilise nayigezemo ...");

        } catch (SQLException e) {
            insertError(e + "", "updateRefundComptabilise");
            //System.out.println(" . . method : updateRefundComptabilise.exception thrown:" + e);
            JOptionPane.showMessageDialog(null, "  " + e, " Ikibazo in: updateRefundComptabilise", JOptionPane.PLAIN_MESSAGE);
        }

    }

     
    String getDateRepete(String dd, int mm, int yy) {

        String ret ;

        if (mm == 12) {
            ret = dd + "01" + (yy + 1);
        } else if (mm > 8) {
            ret = dd + (mm + 1) + (yy);
        } else {
            ret = dd + "0" + (mm + 1) + yy;
        }
        return ret;
        
    }

    /**
     * *****************************************************************************************
     * FONCTION QUI SELECTIONNE TOUT LE COMPTE
     *
     
     * @return
     * *******************************************************************************
     */
    /// je suis d'accord par iyi ordre ariko turebe ukuntu twaha possibilite umuntu
//kuyihindura ukaba wa tria no kumazina
      LinkedList selectSousCpte(String k) {

        try {

            try (//            System.out.println("select * from APP.CPT_COMPTE"
//                    + " WHERE  (NUM_COMPTE  ) like '" + k + "%' ORDER BY NUM_COMPTE");
                    ResultSet outResult = state.executeQuery("select * from APP.CPT_COMPTE"
                            + " WHERE  (NUM_COMPTE  ) like '" + k + "%' ORDER BY NUM_COMPTE")) {
                allSousCompte = new LinkedList();
                
                while (outResult.next()) {
                    
                    String numCompte = outResult.getString(1);
                    String cpteParent = outResult.getString(4);
                    String nomCompte = outResult.getString(2);
                    int classe = outResult.getInt(3);
                    
                    String extr = outResult.getString(5);
                    Compte ce = new Compte(  numCompte,   nomCompte,   cpteParent,   classe,  extr);
                    
                    allSousCompte.add(ce);
                    
                }
            }
        } catch (SQLException e) {
            insertError(e + "", " V Compte");
        }
        return allSousCompte;
    }
    public LinkedList<Compte>  cpteListByNameCompte(String nameCompte) { 
      nameCompte=    JOptionPane.showInputDialog(dbName, nameCompte);
         LinkedList<Compte>  allSousCompte2 = new LinkedList(); 
        try {
String sql="select * from APP.CPT_COMPTE  WHERE  (NUM_COMPTE like '%" + nameCompte + "%' )"
        + " OR (NOM_COMPTE like '%" + nameCompte + "%') "
        + " ORDER BY NOM_COMPTE";
          try ( //System.out.println(sql);
                  ResultSet outResult = state.executeQuery(sql)) {
              while (outResult.next()) {
                  
                  String numCompte  = outResult.getString(1);
                  String cpteParent = outResult.getString(4);
                  String nomCompte  = outResult.getString(2);
                  int classe        = outResult.getInt(3);
                  String extr = outResult.getString(5);
                  
                  Compte ce = new Compte(  numCompte,   nomCompte,   cpteParent,   classe,  extr);
                  
                  allSousCompte2.add(ce);
                  
              }
          }
        } catch (SQLException e) {
            insertError(e + "", " V Compte");
        }
        return allSousCompte2;
    }
    String getNumTiers(String num) {

        int k=0;
        try {
            try (ResultSet tierResult = state.executeQuery("select max(CAST(APP.CPT_TIERS.NUM_TIERS AS INT)) from APP.CPT_TIERS where num_compte= '" + num + "'")) {
                while (tierResult.next()) {
                    k = tierResult.getInt(1);
                }
            }
        } catch (SQLException e) {
            insertError(e + "", " V getnumTiers");
        }
        if (k == 0) {
            String n = num + "000";
            k = Integer.parseInt(n);
        }
        numtiers = k + 1;
        //System.out.println("NUME tiers finale: " + k);
        return "" + numtiers;

    }
 
 
    
    /**
     * *****************************************************************************************
     * FONCTION QUI SELECTIONNE DANS LA TABLE BONLIVRAISON LES INTITULES
     * *******************************************************************************
     */
      

    /**
     * ***************************************************************
     * GUFATA CODE N'AMAZINA BIZAJYA MURI JLIST YA TYPE OPERATION
     * ****************************************************************
     */
      LinkedList<CodeJournal> getCode() {

        LinkedList<CodeJournal> f = new LinkedList();
        try { try ( //int i=0;
                ResultSet outResult = state.executeQuery("select * from APP.CPT_CODE_JOURNAUX ORDER BY CODE")) {
            while (outResult.next()) {

                String codeJournal = outResult.getString(1);
                String codeIntitule = outResult.getString(2);

                CodeJournal ce = new CodeJournal(codeJournal, codeIntitule);
                f.add(ce);
            }
            int g  ;
            code = new CodeJournal[f.size() + 3];
            for (g = 0; g < f.size(); g++) {
                code[g] = f.get(g);
            }
            CodeJournal fa = new CodeJournal("", "");
            code[g] = fa;
            }
        } catch (SQLException e) {
        }

        return f;
    }

    /**
     * ***************************************************************
     * GUFATA LISTE Y'ABA TIERS POUR CHOISIR QUAND ON FAIT L'OPERATION
     * ****************************************************************
     */
/// buri operation igira umutiers je me demande
    LinkedList<Tiers> cptTiers() {
        LinkedList<Tiers> f = new LinkedList();
        try {
            try (ResultSet outResult = state.executeQuery("select * from APP.CPT_TIERS")) {
                while (outResult.next()) {
                    
                    String numTiers = outResult.getString(1);
                    String numCompte = outResult.getString(2);
                    String designation = outResult.getString(3);
                    String info = outResult.getString(4);
                    String adresse = outResult.getString(5);
                    String devise = outResult.getString(6);
                    String sigle = outResult.getString(7);
                    String numAffilie = outResult.getString(8);
                    int sal = outResult.getInt(9);
                    String ext = outResult.getString(10);
                    String email = outResult.getString("EMAIL");
                    Tiers c = new Tiers(numTiers, numCompte, designation, info, adresse,email, devise,
                            sigle, numAffilie,sal,ext);
                    f.add(c);
                }   int g  ;
                tiersControl2 = new Tiers[f.size() + 3];
                for (g = 0; g < f.size(); g++) {
                    //System.out.println(journalYose.get(g).ASSURANCE);
                    tiersControl2[g] = f.get(g);
                }   Tiers fa = new Tiers("", "", "", "", "", "", "","", "",0,"");
                tiersControl2[g] = fa;
                //FRSS_PRINCIPALE.setListData(tiersControl2);
                //  Close the resultSet
            }
        } catch (SQLException e) {
            /*
             * Catch all exceptions and pass them to * the exception reporting
             * method
             */
            //System.out.print(".....Exceptiion");
        }
        return f;
    }

    public String getCompte(String num_compte) {

        String nomCompte = "";

        if (num_compte != null && (num_compte.length() >= 2)) {

            try {

                //System.out.println("select * from APP.CPT_COMPTE where NUM_COMPTE = '" + num_compte + "'");
                try (ResultSet outResult = state.executeQuery("select * from APP.CPT_COMPTE where NUM_COMPTE = '" + num_compte + "'")) {
                    //System.out.println("select * from APP.CPT_COMPTE where NUM_COMPTE = '" + num_compte + "'");
                    
                    while (outResult.next()) {
                        
                        nomCompte = outResult.getString("NOM_COMPTE");
                        
                    }
                }

            } catch (SQLException e) {
                System.out.println(".....String getCompte(String num_compte)" + e);
            }
        }
        return nomCompte;
    }

    LinkedList<Journaux> cptJournaux() {
        allJournaux = new LinkedList();
        try {
            try (ResultSet outResult = state.executeQuery("select * from APP.CPT_JOURNAUX order by code_journal")) {
                while (outResult.next()) {
                    allJournaux.add(new Journaux(outResult.getString(1), outResult.getString(2),
                            outResult.getString(4), outResult.getString(3), outResult.getString(5), outResult.getDouble(6)));
                }
            }
        } catch (SQLException e) {
            System.out.println(".....cptJournaux " + e);
        }
        return allJournaux;
    }

    /**
     * *********************************************************************************
     * GUFATA NUMERO OPERATION TWARI TUGEZEHO KUGIRANGO TUYIKOMEREZEHO
     * DUTERANYIJE HO RIMWE
     * *********************************************************************************
     */  
      int getNumOperation2(String JOURNAUX, Statement st) {
        int k=0;
        try {
            
            try (ResultSet outResult45 = st.executeQuery("select max(NUM_OPERATION) "
                    + "from APP.CPT_JOURNAL WHERE ID_JOURNAUX='" + JOURNAUX + "' ")) {
                while (outResult45.next()) { 
                    k = outResult45.getInt(1); 
                }
            }
            
        } catch (SQLException e) {
            //System.out.println(" . . method : getNumOperation(String JOURNAUX) . exception thrown:" + e);
            JOptionPane.showMessageDialog(null, "  " + e, " Ikibazo in getNumOperation(String JOURNAUX)", JOptionPane.PLAIN_MESSAGE);
        }
        
        if(k==0 && !getExisteJournal(JOURNAUX))
        {insertJournal(JOURNAUX, JOURNAUX, JOURNAUX, 
                             " ",JOURNAUX.substring(JOURNAUX.length()-3, JOURNAUX.length()),1);}
        numOp = k + 1;
        //System.out.println("NUME OPERATION finale: " + k);
        return numOp;
    }
 
    /**
     * *********************************************************************************
     * GUFATA NUMERO YA SOUS COMPTE TWARI TUGEZEHO KUGIRANGO TUYIKOMEREZEHO
     * DUTERANYIJE HO RIMWE
     * *********************************************************************************
     */
    String getNumSouscpte(String num) {

        //System.out.println(" >>>>>>> num : " + num);

        String ccc = "";

        try {

            int c = 0;


            try (ResultSet outResult = state.executeQuery("select max(CAST(APP.CPT_COMPTE.NUM_COMPTE AS INT)) from APP.CPT_COMPTE "
                    + "where (cpte_parent = '" + num + "')")) {
                while (outResult.next()) {
//                c = Integer.getInteger(outResult.getString(1));
c = outResult.getInt(1);

//System.out.println("iyo mbonye: " + c);

                }
            }
 
            //System.out.println("NUME sous compte avant finale: " + c);

            if (c == 0) {

                c = Integer.parseInt(num + "0");

            }


            ccc = "" + (c + 1);

            //System.out.println("NUME sous compte finale: " + ccc);



        } catch (SQLException | NumberFormatException e) { 
            insertError(e + "", " V getnumSouscompte"); 
        }

        return ccc;

    }

    String getNumSouscpte2(String num) {

        //System.out.println(" >>>>>>> num : " + num);

        String ccc  ;

        //System.out.println("NUME sous compte avant finale: " + num);
        ccc = "" + (Integer.parseInt(num + "0") + 1);
        //System.out.println("NUME sous compte finale: " + ccc);

        return ccc;

    }

    /**
     * ***********************************************************************************************************
     * INSERTION DES OPERATIONS DANS LA TABLE JOURNAL
     *
     *******************************************************************************************************
     */
    
      boolean insertJournal(int NumOperation, String numTiers, String dateOperation, String compteDebit,
            String compteCredit, String libelle, double montantDebit, double montantCredit, String utilisateur,
            double postDater, String dateDoc, String journaux, String Analytique,String devise,double rate) {

        System.out.println("N "+NumOperation+" T "+numTiers+" D "+dateOperation+" CD "+compteDebit+" CC "+
        compteCredit+" L "+libelle+" MD "+montantDebit+" MC "+montantCredit+" US "+utilisateur+" PD "+
        postDater+" DA "+dateDoc+" JO "+journaux+" AN "+Analytique+" DE "+devise+" RA "+rate);

        try {

            int dateDocInt = 0;

            if (dateDoc.length() == 6) {
                String dateRenv = "" + dateDoc.substring(4) + "" + dateDoc.substring(2, 4) + "" + dateDoc.substring(0, 2);
                Integer kkk = new Integer(dateRenv);
                dateDocInt = kkk;
            }

            psInsert = conn3.prepareStatement("insert into APP.CPT_JOURNAL(NUM_OPERATION,NUM_TIERS,DATE_OPERATION,"
                    + "COMPTE_DEBIT,COMPTE_CREDIT,LIBELLE,MONTANT_DEBIT,MONTANT_CREDIT,NOM_UTILISATEUR,POSTDATER,"
                    + "DATE_DOC,ID_JOURNAUX,DATE_DOC_INT,profit_center,devise,devise_rate)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
//

//            System.out.println("insert into APP.CPT_JOURNAL(NUM_OPERATION,NUM_TIERS,DATE_OPERATION,"
//                    + "COMPTE_DEBIT,COMPTE_CREDIT,LIBELLE,MONTANT_DEBIT,MONTANT_CREDIT,NOM_UTILISATEUR,POSTDATER,"
//                    + "DATE_DOC,ID_JOURNAUX,DATE_DOC_INT,profit_center)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

            psInsert.setInt(1, NumOperation);
            psInsert.setString(2, numTiers); // int
            psInsert.setString(3, dateOperation);
            psInsert.setString(4, compteDebit); // int
            psInsert.setString(5, compteCredit); // int
            psInsert.setString(6, libelle);
            psInsert.setDouble(7, montantDebit);
            psInsert.setDouble(8, montantCredit);
            psInsert.setString(9, utilisateur);
            psInsert.setDouble(10, postDater);
            psInsert.setString(11, dateDoc);
            psInsert.setString(12, journaux);
            psInsert.setInt(13, dateDocInt);
            psInsert.setString(14, Analytique);
psInsert.setString(15, devise);
psInsert.setDouble(16, rate);
            psInsert.executeUpdate();
            psInsert.close();
return true;
        } catch (NumberFormatException | SQLException ex) {
            
            JOptionPane.showMessageDialog(jScrollPane2, "INSERT FAIL \n "+ex);
            getMiniOperation(  libelle,numTiers,journaux,Analytique);
            System.err.println(NumOperation+" "+numTiers+" "+libelle+" "+journaux+" error in insertJournal()  :  " + ex);
        }
        return false;
    }
      
       boolean insertJournalTrackingUpdate(int NumOperation, String numTiers, String dateOperation, String compteDebit,
            String compteCredit, String libelle, double montantDebit, double montantCredit, String utilisateur,
            double postDater, String dateDoc, String journaux, String Analytique,String raison,String employe) {

           
        try {

            int dateDocInt = 0;

            if (dateDoc.length() == 6) {
                String dateRenv = "" + dateDoc.substring(4) + "" + dateDoc.substring(2, 4) + "" + dateDoc.substring(0, 2);
                Integer kkk = new Integer(dateRenv);
                dateDocInt = kkk;
            }

            psInsert = conn3.prepareStatement("insert into APP.CPT_JOURNAL_TRACKING"
                    + "(NUM_OPERATION,NUM_TIERS,DATE_OPERATION,"
                    + "COMPTE_DEBIT,COMPTE_CREDIT,LIBELLE,MONTANT_DEBIT,MONTANT_CREDIT,NOM_UTILISATEUR,POSTDATER,"
                    + "DATE_DOC,ID_JOURNAUX,DATE_DOC_INT,profit_center,employe,raison"
                    + ")values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
 
            psInsert.setInt(1, NumOperation);
            psInsert.setString(2, numTiers); // int
            psInsert.setString(3, dateOperation);
            psInsert.setString(4, compteDebit); // int
            psInsert.setString(5, compteCredit); // int
            psInsert.setString(6, libelle);
            psInsert.setDouble(7, montantDebit);
            psInsert.setDouble(8, montantCredit);
            psInsert.setString(9, utilisateur);
            psInsert.setDouble(10, postDater);
            psInsert.setString(11, dateDoc);
            psInsert.setString(12, journaux);
            psInsert.setInt(13, dateDocInt);
            psInsert.setString(14, Analytique);
psInsert.setString(15, employe);
psInsert.setString(16, raison);
            psInsert.executeUpdate();
            psInsert.close();
return true;
        } catch (NumberFormatException | SQLException ex) {
            System.out.println(numTiers+libelle+" error in insertJournal()  :  " + ex);
        }
        return false;
    }
 
    
    
    void insertPreparation( String numTiers, double amount, double rssb, double tpr,String month,double rama,double rssb2) {
        try {
            psInsert = conn.prepareStatement("insert into APP.CPT_PREPARATION(NUM_TIER,NET_SALARY,RSSB,TPR,MONTH,RAMA,RSSB2)values(?,?,?,?,?,?,?)");
            psInsert.setString(1, numTiers);
            psInsert.setDouble(2, amount);
            psInsert.setDouble(3, rssb);
            psInsert.setDouble(4,tpr);
            psInsert.setString(5, month);
            psInsert.setDouble(6,rama);
            psInsert.setDouble(7,rssb2);
            psInsert.executeUpdate();
            psInsert.close();
        } catch (SQLException ex) {
            System.out.println(" " + ex);
        }
    }
    
    cpt_lacharge getPreparation( String numTiers, String month) {
        double amount_pay=0.0;
        double rssb=0.0;
        double tpr=0.0;
        double rama=0.0;
        double rssb2=0.0;
        Timestamp date;
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("YY");
        java.text.SimpleDateFormat mois = new java.text.SimpleDateFormat("MM");
        
        java.util.Date date1= new java.util.Date();
        
        Timestamp date2=new Timestamp(date1.getTime());
        
        try {
            
            
            Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
        ResultSet.CONCUR_READ_ONLY
     );
    ResultSet rs = stmt.executeQuery("select * from APP.CPT_PREPARATION  WHERE NUM_TIER='"+ numTiers+"' AND MONTH='"+ month+"' ORDER BY ID" );
            
            if(rs.last()) {
    
                amount_pay =amount_pay+ rs.getDouble("NET_SALARY");
                rssb =rssb+ rs.getDouble("RSSB");
                tpr =tpr+ rs.getDouble("TPR");
                rama=rama+ rs.getDouble("RAMA");
                date=rs.getTimestamp("DATE_PREP");
                rssb2=rssb2+ rs.getDouble("RSSB2");
                
                
                cpt_lacharge CE = new cpt_lacharge(rssb,tpr,amount_pay,rama,rssb2,date);
                int i=Integer.parseInt(sdf.format(date));
                int ii=Integer.parseInt(sdf.format(date2));
                
                int m1=Integer.parseInt(mois.format(date));
                int m2=Integer.parseInt(mois.format(date2));
                
                if((i==ii)||(i<ii && m2<m1))
                {
                return CE;
                
                }
                
                }  
               
               
            rs.close();
        } catch (SQLException ex) {
            System.out.println(" " + ex);
        }
       
        
        return null;
        
    }

    void insertPayement( String numTiers,double avance, String month,double retenu,double prime) {
        try {
            psInsert = conn.prepareStatement("insert into APP.CPT_PAYEMENT(NUM_TIER,AVANCE,MONTH,RETENU,PRIME)values(?,?,?,?,?)");
            psInsert.setString(1, numTiers);
            psInsert.setDouble(2, avance);
            psInsert.setString(3, month);
            psInsert.setDouble(4, retenu);
            psInsert.setDouble(5, prime);
            psInsert.executeUpdate();
            psInsert.close();
        } catch (SQLException ex) {
            System.out.println(" " + ex);
        }
    }
    
    
    /**
     * ***********************************************************************************************************
     * INSERTION DES NOUVEAUX TIERS
     * ******************************************************************************************************
     */
     void insertTiers(String numTiers, String numCompte, String designation,
            String info, String adresse,String email, String devise, String sigle, String numAffilie,
            String salaireBrute) {

        try { 
             System.out.println(" numAffilie IN insertTiers(): " + numAffilie);

            psInsert = conn.prepareStatement("insert into APP.CPT_TIERS"
                    + "(NUM_TIERS,NUM_COMPTE,DESIGNATION,INFORMATION,ADRESSE,DEVISE,SIGLE_TIERS,"
                    + " NUM_AFFILIATION,SALAIRE_BRUTE,EMAIL)values"
                    + "(?,?,?,?,?,?,?,?,?,? )");

            psInsert.setString(1, numTiers);
            psInsert.setString(2, numCompte);
            psInsert.setString(3, designation);
            psInsert.setString(4, info);
            psInsert.setString(5, adresse);
            psInsert.setString(6, devise);
            psInsert.setString(7, sigle);
            psInsert.setString(8, numAffilie);
            psInsert.setString(9, salaireBrute); 
            psInsert.setString(10, email);

            psInsert.executeUpdate();
            psInsert.close();

        } catch (SQLException ex) {
            System.out.println(" method: insertTiers() : " + ex);
        }
    }
 void insertTiers2(String numTiers, String numCompte, String designation,
            String info, String adresse,String email, String devise, String sigle, String numAffilie,
            String salaireBrute,String EXT_NUM) { 
        try { 
            psInsert = conn.prepareStatement("insert into APP.CPT_TIERS"
                    + "(NUM_TIERS,NUM_COMPTE,DESIGNATION,INFORMATION,ADRESSE,DEVISE,SIGLE_TIERS,"
                    + " NUM_AFFILIATION,SALAIRE_BRUTE,EXT_NUMTIERS,EMAIL)values"
                    + "(?,?,?,?,?,?,?,?,?,?,?)");

            psInsert.setString(1, numTiers);
            psInsert.setString(2, numCompte);
            psInsert.setString(3, designation);
            psInsert.setString(4, info);
            psInsert.setString(5, adresse); 
            psInsert.setString(6, devise);
            psInsert.setString(7, sigle);
            psInsert.setString(8, numAffilie);
            psInsert.setString(9, salaireBrute);
            psInsert.setString(10, designation);
            psInsert.setString(11, email);

            psInsert.executeUpdate();
            psInsert.close();

        } catch (SQLException ex) {
            System.out.println(" method: insertTiers() : " + ex);
        }
    }
    /**
     * ***********************************************************************************************************
     * INSERTION DES CODE JOURNAUX
     * ******************************************************************************************************
     */
///insert code journal
      void insertCode(String code, String codeIntitule) {
        try {
            psInsert = conn.prepareStatement("insert into APP.CPT_CODE_JOURNAUX(CODE,CODE_INTITULE)values(?,?)");

            psInsert.setString(1, code);
            psInsert.setString(2, codeIntitule);
            psInsert.executeUpdate();
            psInsert.close();
        } catch (SQLException ex) {
            System.out.println(" " + ex);
        }
    }

    /**
     * ***********************************************************************************************************
     * INSERTION DES OPERATIONS DE NOUVEAU COMPTE
     *
     *******************************************************************************************************
     */
      void insertCompte(String NUMERO, String NOM, int CLASSE, String CPTEPARENT, String cat) {
        try {
            psInsert = conn.prepareStatement("insert into APP.CPT_COMPTE(NUM_COMPTE,NOM_COMPTE,CLASSE,"
                    + "CPTE_PARENT,CATEGORY)values(?,?,?,?,?)");

            psInsert.setString(1, NUMERO);
            psInsert.setString(2, NOM);
            psInsert.setInt(3, CLASSE);
            psInsert.setString(4, CPTEPARENT);
            psInsert.setString(5, cat);
            psInsert.executeUpdate();
            psInsert.close();
            JOptionPane.showMessageDialog(null, "ENREGISTRER AVEC SUCCES!!");
        } catch (SQLException ex) {
            System.out.println(" " + ex);
        }
    }
LinkedList<String> getVarListFam(String FAMILLE) {
        LinkedList<String> getVarList = new LinkedList();
        try {
          ResultSet  outResult = state3.executeQuery("select  * from APP.VARIABLES where "
                    + " APP.variables.famille_variable= '" + FAMILLE + "'");
            while (outResult.next()) {
                getVarList.add(outResult.getString("VALUE_VARIABLE"));
            }
        } catch (SQLException e) {
            insertError(conn, e + "", "getparm");
        }
        return getVarList;
    }

      void insertJournal(String ID, String NAME,  String GROUP, String CPT,
              String DEVISE,double rate) {
        try {
            psInsert = conn.prepareStatement
      ("insert into APP.CPT_JOURNAUX(CODE_JOURNAL,NAME_JOURNAL,GROUPE_COMPTE_JOURNAL,"
              + "COMPTE_JOURNAL,DEVISE,DEVISE_RATE)"
              + " values(?,?,?,?,?,?)");

            psInsert.setString(1, ID);
            psInsert.setString(2, NAME); 
            psInsert.setString(3, GROUP);
            psInsert.setString(4, CPT);
            psInsert.setString(5, DEVISE);
            psInsert.setDouble(6, rate);
            
            psInsert.executeUpdate();
            psInsert.close();
            JOptionPane.showMessageDialog(null, " JOURNAL "+NAME+" WELL SAVED !!");
        } catch (SQLException ex) {
            System.out.println(" " + ex);
        }
    }

    /**
     * ***********************************************************************************************************
     * INSERTION DES OPERATIONS DE NOUVEAU COMPTE
     *
     *******************************************************************************************************
     */
//void insertsousCompte(String numCompte,String numSous,String umukozi,int CLASSE)
//    {
//    int a=Integer.parseInt(numCompte);
//    int b=Integer.parseInt(numSous);
//try {
//psInsert = conn.prepareStatement("insert into APP.CPT_COMPTE(NUM_SOUS_COMPTE,NUM_COMPTE,NOM_SOUS_COMPTE,CLASSE)values(?,?,?,?)");
//
//psInsert.setInt(2, a);
//psInsert.setInt(1, b);
//psInsert.setString(3,umukozi);
//psInsert.setInt(4, CLASSE);
//psInsert.executeUpdate();
//psInsert.close();
//} catch (SQLException ex) {
//System.out.println(" "+ex);
//      }
//}
    /**
     * ***********************************************************************************************************
     * FONCTION QUI CAPTE TOUS LES COMPTE
     * ******************************************************************************************************
     */
    LinkedList<Compte> getCompte() {
        LinkedList<Compte> f = new LinkedList();//arrete de creer les variables za alphabet
        try {

            try ( /// why <8
                    ResultSet outResult = state.executeQuery("select * from APP.CPT_COMPTE  order by NUM_COMPTE")) {
                while (outResult.next()) {
                    String numCompte = outResult.getString(1);
                    String nomCompte = outResult.getString(2);
                    int classe = outResult.getInt(3);
                    String cpteParent = outResult.getString(4);
                    String extr = outResult.getString(5);
                    Compte ce = new Compte(  numCompte,   nomCompte,   cpteParent,   classe,  extr);
                    f.add(ce);
                }
            }
        } catch (SQLException e) {

            /*
             * Catch all exceptions and pass them to * the exception reporting
             * method
             */
        }

        return f;
    }

    public void getAllCompte() {
        try {
            allCompte = new LinkedList();

            try (ResultSet outResult = state.executeQuery("select * from APP.CPT_COMPTE ORDER BY NUM_COMPTE")) {
                while (outResult.next()) {
                    String numCompte = outResult.getString(1);
                    String nomCompte = outResult.getString(2);
                    int classe = outResult.getInt(3);
                    
                    String cpteParent = outResult.getString(4);
                    String extr = outResult.getString(5);
                    Compte c = new Compte(  numCompte,   nomCompte,   cpteParent,   classe,  extr);
                    c.category=outResult.getString("CATEGORY"); 
                    allCompte.add(c);
                }
                //  Close the resultSet
            }
        } catch (SQLException e) {
            /*
             * Catch all exceptions and pass them to * the exception reporting
             * method
             */
            //System.out.println(" . . getAllCompte()  . exception thrown: " + e);
            insertError(e + "", " V getallCompte");
        }

    }

    public JTable listeCompte() {

        jScrollPane2 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        int linge = allCompte.size();
        String[][] s1 = new String[linge][5];
        for (int i = 0; i < linge; i++) {

            Compte C = allCompte.get(i);

            s1[i][0] = "" + C.numCompte;
            s1[i][1] = "" + C.nomCompte;
            s1[i][2] = "" + C.classe;
            s1[i][3] = "" + C.cpteParent;
            s1[i][4] = "" + C.category;
        }

        //setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        jTable1.setModel(new javax.swing.table.DefaultTableModel(s1,
                new String[]{
                    "N°COMPTE", "NOM COMPTE", "CLASSE", "COMPTE PARENT", "CATEGORY"
                }) {

            Class[] types = new Class[]{
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,java.lang.String.class 
            };
//    public Class getColumnClass(int columnIndex) {
//    return types [columnIndex];
//    }
        });

        return jTable1;
    }

    public LinkedList<Tiers> getAllTiers() {
        try {
            allTiers = new LinkedList(); 
            try (ResultSet outResult = state.executeQuery("select * from APP.CPT_TIERS ORDER BY designation")) {
                while (outResult.next()) {
                    String numTiers = outResult.getString(1);
                    String compteT = outResult.getString(2);
                    String designation = outResult.getString(3);
                    String info = outResult.getString(4);
                    String adresse = outResult.getString(5);
                    String devise = outResult.getString(6);
                    String sigle = outResult.getString(7);
                    String numAffilie = outResult.getString(8);
                    int salaireBrute = outResult.getInt(9);
                    String EXT_NUM = outResult.getString(10);
                    String email = outResult.getString("EMAIL");
                    Tiers c = new Tiers(numTiers, compteT, designation, info, adresse,email,
                            devise, sigle, numAffilie, salaireBrute,EXT_NUM);
                    
                    allTiers.add(c);
                }
                //  Close the resultSet
            }
        } catch (SQLException e) {
            /*
             * Catch all exceptions and pass them to * the exception reporting
             * method
             */
            //System.out.println(".......getAllTiers: " + e);
            //System.out.println(" . . inv . exception thrown:");
            insertError(e + "", " V getAlltiers");
        }
        return allTiers;
    }
    
     public LinkedList<Tiers> getAllTiersCompteName(String nameCompte) {
        try {
            allTiers = new LinkedList(); 
            nameCompte=    JOptionPane.showInputDialog(dbName, nameCompte);
         
String sql="select * from APP.CPT_TIERS  WHERE  (designation like '%" + nameCompte + "%' )" 
        + " ORDER BY designation";
            try (ResultSet outResult = state.executeQuery(sql)) {
                while (outResult.next()) {
                    String numTiers = outResult.getString(1);
                    String compteT = outResult.getString(2);
                    String designation = outResult.getString(3);
                    String info = outResult.getString(4);
                    String adresse = outResult.getString(5);
                    String devise = outResult.getString(6);
                    String sigle = outResult.getString(7);
                    String numAffilie = outResult.getString(8);
                    int salaireBrute = outResult.getInt(9);
                    String EXT_NUM = outResult.getString(10);
                    String email = outResult.getString("EMAIL");
                    Tiers c = new Tiers(numTiers, compteT, designation, info, adresse,email,
                            devise, sigle, numAffilie, salaireBrute,EXT_NUM);
                    
                    allTiers.add(c);
                }
                //  Close the resultSet
            }
        } catch (SQLException e) {
            /*
             * Catch all exceptions and pass them to * the exception reporting
             * method
             */
            //System.out.println(".......getAllTiers: " + e);
            //System.out.println(" . . inv . exception thrown:");
            insertError(e + "", " V getAlltiers");
        }
        return allTiers;
    }

    public JTable listeTiers() {

        jScrollPane2 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        int linge = allTiers.size();
        String[][] s1 = new String[linge][6];
        for (int i = 0; i < linge; i++) {
            Tiers T = allTiers.get(i);

            s1[i][0] = "" + T.numTiers;
            s1[i][1] = "" + T.numCompte;
            s1[i][2] = "" + T.designation;
            s1[i][3] = "" + T.info;
            s1[i][4] = "" + T.adresse;
            s1[i][5] = "" + T.devise;
        }

        //setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        jTable1.setModel(new javax.swing.table.DefaultTableModel(s1,
                new String[]{
                    "N°TIERS", "N°COMPTE", "DESIGNATION", "INFORMATION", "ADRESSE", "DEVISE"
                }) {

            Class[] types = new Class[]{
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.String.class
            };
//    public Class getColumnClass(int columnIndex) {
//    return types [columnIndex];
//    }
        });
        return jTable1;
    }

    public LinkedList<Tiers> assurance(String date, String fin, String mode) {

        LinkedList<Tiers> ass = new LinkedList();
        try {
            ResultSet outResult;
            
            if (mode.contains("journalier")) {
                outResult = state.executeQuery("select distinct NUM_CLIENT,CLIENT.NUM_AFFILIATION from APP.invoice,CLIENT WHERE DATE='" + date + "' AND INVOICE.NUM_CLIENT=CLIENT.NOM_CLIENT");
                //System.out.println("select distinct NUM_CLIENT,NUM_AFFILIATION from APP.invoice,CLIENT WHERE DATE='" + date + "' AND INVOICE.NUM_CLIENT=CLIENT.NOM_CLIENT");
            } else {
                //System.out.println("select distinct NUM_CLIENT,CLIENT.NUM_AFFILIATION from APP.invoice,CLIENT WHERE heure>='" + date + "' and heure<='" + fin + "' AND INVOICE.NUM_CLIENT=CLIENT.NOM_CLIENT");

                outResult = state.executeQuery("select distinct NUM_CLIENT,CLIENT.NUM_AFFILIATION from APP.invoice,CLIENT WHERE heure>='" + date + "' and heure<='" + fin + "' AND INVOICE.NUM_CLIENT=CLIENT.NOM_CLIENT");
            }

            while (outResult.next()) {
                
                    String numTiers = outResult.getString(1);
                String numCompte = outResult.getString(2);
                String designation = outResult.getString(3);
                String info = outResult.getString(4);
                String adresse = outResult.getString(5);
                String devise = outResult.getString(6);
                String sigle = outResult.getString(7);
                String numAffilie = outResult.getString(8);
int sal = outResult.getInt(9);
                String ext = outResult.getString(10);
                String email = outResult.getString("EMAIL");
               Tiers c = new Tiers(numTiers, numCompte, designation, info, adresse,email, devise,
                        sigle, numAffilie,sal,ext);
                ass.add(c);
            }

//        control=new String[ass.size()];
//        for(int i=0;i<ass.size();i++)
//            control[i]=ass.get(i);
            //  Close the resultSet
            outResult.close();

        } catch (SQLException e) {

            insertError(e + "", " V ass");
        }
        return ass;
    }

    public String[] dateUsed(String m, String j) {
        String[] control = null;
        LinkedList<String> ass = new LinkedList();
        try {
            try (ResultSet outResult = state.executeQuery("select distinct date from APP.COMPTA WHERE mois='" + m + "' and moyen='" + j + "'")) {
                while (outResult.next()) {
                    
                    ass.add(outResult.getString(1));
                }   control = new String[ass.size()];
                for (int i = 0; i < ass.size(); i++) {
                    control[i] = ass.get(i);
                }
                //  Close the resultSet
            }

        } catch (SQLException e) {

            insertError(e + "", " V ass");
        }
        return control;
    }

//yabaye comptabiliser
    public boolean yabayeCompta(String moyen, String mois, boolean f) {
        boolean done = false;
        try {
            
            ResultSet outResult;
            if (f) {
                outResult = state.executeQuery("select * from  compta where mois='" + mois + "' and moyen='" + moyen + "' ");
            } else {
                outResult = state.executeQuery("select * from  compta where date='" + mois + "' and moyen='" + moyen + "' ");
            }

            while (outResult.next()) {
                done = true;
            }
            //System.out.println("yakozwe " + done);

        } catch (SQLException ex) {
            insertError(ex + "", " NotCompta");
        }
        return done;
    }

    public boolean notCompta(String date, String client) {
        boolean done = true;
        try {

          ResultSet outResult = state.executeQuery("select * from  compta where date='" + date + "' and intitule='" + client + "' ");
            while (outResult.next()) {
                done = false;
            }
            //System.out.println("yakozwe " + done);

        } catch (SQLException ex) {
            insertError(ex + "", " NotCompta");
        }
        return done;
    }

    
    public void insertCompta(int doc, String date, Tiers client, int debit, int credit, int compte, String libele, int tva, int valeur_mse, String moyen, String mois) {
        try {
            int id = 0;
            psInsert = conn.prepareStatement("insert into APP.compta(date,compte,intitule,libele,montant_cash,montant_credit,tva,document,valeur_mse,moyen,mois,num_affiliation)  values (?,?,?,?,?,?,?,?,?,?,?,?)");
            psInsert.setString(1, date);
            psInsert.setInt(2, compte);
            psInsert.setString(3, client.designation);
            psInsert.setString(4, libele);
            psInsert.setInt(5, debit);
            psInsert.setInt(6, credit);
            psInsert.setInt(7, tva);
            psInsert.setInt(8, doc);
            psInsert.setInt(9, valeur_mse);
            psInsert.setString(10, moyen);
            psInsert.setString(11, mois);
            psInsert.setString(12, client.numAffilie);

            psInsert.executeUpdate();
            psInsert.close();

//if(!client.designation.contains("Client"))
//{
//if(client.equals("CASHNORM"))
//{state = conn.createStatement();
//String createString1 = " update APP.invoice set document=" + doc + " where APP.invoice.num_client= '"+client+"' and  APP.INVOICE.DATE LIKE '%"+date+"%' ";
//s.execute(createString1);}
//else
//{
//s = conn.createStatement();
//String createString1 = " update APP.invoice set document=" + doc + " where APP.invoice.num_client= '"+client+"' and  APP.INVOICE.DATE LIKE '%"+date.substring(3, 6)+"%' ";
//s.execute(createString1);
//}
//
//}
        } catch (SQLException ex) {
            insertError(ex + "", " insertCompta");
        }
    }

    public int getMaxCompta() {
        int max = 0;
        try {

          ResultSet outResult = state.executeQuery("select max(document) from  compta ");
            while (outResult.next()) {
                max = outResult.getInt(1);
            }
            //System.out.println("maximum doc: " + max);

        } catch (SQLException ex) {
            insertError(ex + "", " NotCompta");
        }
        return max;

    }

    public void comptabiliseDocument(String heureDebut, String heureFin) {
//if(notCompta(d,"RAMA")) a faire
        //  {

        LinkedList<Tiers> ass = assurance("", "", "");

        for (int i = 0; i < ass.size(); i++) {
            try {
                Tiers cli = ass.get(i);
                String sql = "";

                int doc = getMaxCompta();
                String Debut = JOptionPane.showInputDialog("ENTREZ LE TEMPS du debut", "2010-01-01 08:00:00");
                String FIN = JOptionPane.showInputDialog("ENTREZ LE TEMPS de la fin ", "2010-01-31 23:00:00");
                try (/////System.out.println(        "select INVOICE.id_INVOICE,date,code,name_product,num_lot,quantite,list.price,list.prix_revient,employe,heure,num_client  from product,APP.INVOICE,APP.LIST where code=code_uni and  list.id_invoice=invoice.id_invoice  and invoice.HEURE >'"+Debut+"' AND invoice.HEURE <'"+FIN+"' ORDER BY invoice.ID_INVOICE");
                        ResultSet outResult = state.executeQuery("select INVOICE.id_INVOICE,date,code,name_product,num_lot,quantite,list.price,list.prix_revient,employe,heure,num_client  from product,APP.INVOICE,APP.LIST where code=code_uni and  list.id_invoice=invoice.id_invoice  and invoice.HEURE >'" + Debut + "' AND invoice.HEURE <'" + FIN + "' ORDER BY invoice.ID_INVOICE")) {
                    while (outResult.next()) {
                        int sum = outResult.getInt(1);
                        int tva = outResult.getInt(2);
                        if (sum != 0) {
//somme=somme+sum;
//compteL.add(new  Compta("FU100"+(doc+1),"Ventes médicaments",cli,411000,0,sum,d,cli,tva));
//insertCompta(doc + 1, "", cli, 0, sum, cli, 411000, "" + new Date(), tva);
                        }
                    }
                }
            } catch (SQLException ex) {
                Logger.getLogger(DbHandler.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    public boolean arimo(String t) {
        boolean a = false;
        for (Tiers allTier : allTiers) {
            if (t.equals(allTiers.element().designation)) {
                a = true;
                break;
            }
        }
        return a;
    }
 
      


    void updateParam(String var, String value) {

        try {
            state.execute(" update APP.VARIABLES set VALUE_VARIABLE ='" + value + "'  where nom_variable='" + var + "'");

        } catch (SQLException e) {
            insertError(e + "", "updateparm");
        }

    }

    void updateInvoiceComptabilise(Statement ishyiga, String debut, String fin) {
        try {
            ishyiga.execute(" update APP.invoice set comptabilise ='OUI'  where HEURE<'" + fin + "' AND HEURE>'" + debut + "' ");
        } catch (SQLException e) {
            insertError(e + "", "updateparm");
        }
    }
 
    public Compte getCpteName(String konti) {

        Compte HavanaClub = null;
        String cpt_parent = "0";

        try {

            String sql="select * from APP.CPT_COMPTE WHERE  NUM_COMPTE  = '" + konti + "' " ;
            System.out.println(sql);
            try (ResultSet outResult = state.executeQuery(sql)) {
                while (outResult.next()) {
                    
                    String numCompte = outResult.getString(1);
                    String nomCompte = outResult.getString(2);
                    int classe = outResult.getInt(3);
                    String cpteParent = outResult.getString(4);
                    String extr = outResult.getString(5);
                    
                    HavanaClub = new Compte(  numCompte,   nomCompte,   cpteParent,   classe,  extr);
                    
                }
            }

        } catch (SQLException e) {
            insertError(e + "", " V Compte");
        }
        return HavanaClub;
    }

    public Compte getCpteName2(String konti) {

        Compte HavanaClub = null;
        String cpt_parent = "0";

        try {

         String sql=   "select * from APP.CPT_COMPTE WHERE NUM_COMPTE = '" + konti + "' "
                    + "AND CPTE_PARENT <> '" + cpt_parent + "'";
         
            try ( //System.out.println(sql);
                    ResultSet outResult = state.executeQuery(sql)) {
                while (outResult.next()) {
                    String numCompte = outResult.getString(1);
                    String nomCompte = outResult.getString(2);
                    int classe = outResult.getInt(3);
                    String cpteParent = outResult.getString(4);
                    String extr = outResult.getString(5);
                    
                    HavanaClub = new Compte(  numCompte,   nomCompte,   cpteParent,   classe,  extr);
                }
//dsdsd
            }

        } catch (SQLException e) {
            insertError(e + "", " V Compte");
        }
        return HavanaClub;
    }

   
public Client getClient(String entier, String server, String database) {

        Client BB = null;

        try {

            Statement sIshyiga = conn3.createStatement(); 
            String sql = ("select * from APP.CLIENT where ASSURANCE = '" + entier + "'"); 
            //System.out.println("select * from APP.CLIENT where ASSURANCE = '" + entier + "'");

            ResultSet rs = sIshyiga.executeQuery(sql); 
if (rs == null) {

    //System.out.println(" . .  . no client found");
    JOptionPane.showMessageDialog(null, "no client foung for assurance " + entier, " Ikibazo ", JOptionPane.PLAIN_MESSAGE);

} else {

                while (rs.next()) {

                    String NUM_AFFILIATIONa = rs.getString(1);
                    String NOM_CLIENTa = rs.getString(2);
                    String PRENOM_CLIENTa = rs.getString(3);

                    String ln2 = "";
                    String ln3 = "";

                    int PERCENTAGEa = rs.getInt(4);
                    int DATE_EXPa = rs.getInt(5);
                    String ASSURANCEa = rs.getString(6);
                    String EMPLOYEURa = rs.getString(7);
                    String SECTEURa = rs.getString(8);
                    String VISA = rs.getString(9);
                    String CODE = rs.getString(10);

                    String devise = "";
                    int DELAIS = 0;
                    double cmdMin = 0.0;

                    BB = new Client(NUM_AFFILIATIONa, NOM_CLIENTa, PRENOM_CLIENTa, PERCENTAGEa, DATE_EXPa, ASSURANCEa, EMPLOYEURa, SECTEURa, VISA, CODE);

                }
                rs.close();

                if (BB != null) {
                    System.out.println(" . . .client tugezeho:" + BB.ASSURANCE + "-------" + BB.NOM_CLIENT);
                }
            } 
        } catch (SQLException | HeadlessException e) {
            //System.out.println(" . .  . exception thrown in getClient() :" + e);
            JOptionPane.showMessageDialog(null, "  " + e, " Ikibazo ", JOptionPane.PLAIN_MESSAGE);
            insertError(e + "", "getclintB");
        }
        return BB;
    }

    public void importerFile(String fileName) {
        try {
            LinkedList<String> list = getFichier(fileName);
            for (int i = 0; i < list.size(); i++) {
                String line = list.get(i);
                StringTokenizer st1 = new StringTokenizer(line);
                String code2 = st1.nextToken(";");
                String prix = st1.nextToken(";");
//1	KIPHARMA.	280512	4130001		ALGO1201 janv-12	500000.0	0.0
//1	KIPHARMA.	280512		710000	ALGO1201 janv-12	0.0	500000.0
//insertJournal(num,refclient,dateToday, numTier, "",id+"  "+reference,total, 0,"AUTO",0,dateN,"VENTES",PROFIT_CENTER);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DbHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DbHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    LinkedList<String> getFichier(String fichier) throws FileNotFoundException, IOException {
        LinkedList<String> give = new LinkedList();

        File productFile = new File(fichier);
        String ligne  ; 
        try {
            try (BufferedReader entree = new BufferedReader(new FileReader(productFile))) {
                ligne = entree.readLine();
                int i = 0;
                while (ligne != null) {
                    // System.out.println(i+ligne);
                    give.add(ligne);
                    i++;
                    try {
                        ligne = entree.readLine();
                    } catch (IOException e) {
                        System.out.println(e);
                    }
                }   }
        } catch (IOException e) {
            System.out.println(e);
        } 
        return give;
    }

   
    
      
    public LinkedList<Journal> getJournalSearch(String toSearch) {

        LinkedList<Journal> liste = new LinkedList();

        try {

           String sql= "select * from APP.CPT_JOURNAL " + toSearch + "  AND ETAT != 'DELETED' order by date_doc_int,NUM_OPERATION";
            
           System.out.println(sql);
           
            try ( //System.out.println("select * from APP.CPT_JOURNAL " + toSearch + "  AND ETAT != 'DELETED' order by date_doc_int");
//          ResultSet outResult = state.executeQuery("select * from APP.CPT_JOURNAL " + toSearch + "  AND ETAT != 'DELETED' order by date_doc_int");
                    ResultSet outResult = state.executeQuery (sql)) {
                while (outResult.next()) {
                    int numOperation = outResult.getInt(2);
                    int numJournal = outResult.getInt(1);
                    String numTiers = outResult.getString(3);
                    String dateOperation = outResult.getString(4);
                    String heure = outResult.getString(14);
                    String compteDebit = outResult.getString(6);
                    String compteCredit = outResult.getString(7);
                    String libelle = outResult.getString(8);
                    double montantDebit = outResult.getDouble(9);
                    double montantCredit = outResult.getDouble(10);
                    String nomUtilisateur = outResult.getString(11);
                    int sousCpte = outResult.getInt(12);
                    String dateDoc = outResult.getString("DATE_DOC");
                    String JOURNAUX = outResult.getString("ID_JOURNAUX");
                    String profit = outResult.getString("PROFIT_CENTER");
                    
Journal c = new Journal(numOperation, numTiers, dateOperation, compteDebit, compteCredit, libelle,
        montantDebit, montantCredit, nomUtilisateur, 0, dateDoc, "", "", JOURNAUX, profit, numJournal
,outResult.getString("DEVISE"),outResult.getDouble("DEVISE_RATE"));
                    
                    liste.add(c);
                }
                //  Close the resultSet
            }
        } catch (SQLException e) {
            //System.out.println(" . . inv . exception thrown:" + e);
            insertError(e + "", " V getGrandlivre");
        }

        return liste;

    }

    public int getSoldeCompteDebitMointCredit(String toSearch) {
        try {
            try ( //System.out.println("select sum(MONTANT_DEBIT)-sum(MONTANT_CREDIT) from APP.CPT_JOURNAL " + toSearch + "  AND ETAT='OPEN' ");
                    ResultSet outResult = state.executeQuery("select sum(MONTANT_DEBIT)-sum(MONTANT_CREDIT) from APP.CPT_JOURNAL " + toSearch + "  AND ETAT!='DELETED'  ")) {
                while (outResult.next()) {
                    return outResult.getInt(1);
                }
                //  Close the resultSet
            }
        } catch (SQLException e) {
            /*
             * Catch all exceptions and pass them to * the exception reporting
             * method
             */
            //System.out.println(" . . inv . exception thrown:" + e);
            insertError(e + "", " V sum(MONTANT_DEBIT)-sum(MONTANT_CREDIT)");
        }
        return 0;
    }
 
    /**
     * **************************************************
     * fonction qui retourne operation a post dater
     * **************************************************
     */
      LinkedList<Journal> getOperation(Journal jour) {
        LinkedList<Journal> liste = new LinkedList();
        try {
            //System.out.println("select * from APP.CPT_JOURNAL where CPT_JOURNAL.num_operation=" + jour.NumOperation
//                    + " and CPT_JOURNAL.POSTDATER=0 and CPT_JOURNAL.ID_JOURNAUX='" + jour.journaux + "'");
            try (ResultSet outResult = state.executeQuery("select * from APP.CPT_JOURNAL where CPT_JOURNAL.num_operation=" + jour.NumOperation
                    + " and CPT_JOURNAL.POSTDATER=0 and CPT_JOURNAL.ID_JOURNAUX='" + jour.journaux + "'")) {
                //System.out.println("select * from APP.CPT_JOURNAL where CPT_JOURNAL.num_operation=" + jour.NumOperation
//                    + " and CPT_JOURNAL.POSTDATER=0 and CPT_JOURNAL.ID_JOURNAUX='" + jour.journaux + "'");
                while (outResult.next()) {
                    int numOperation = outResult.getInt(2);
                    int numJournal = outResult.getInt(1);
                    String numTiers = outResult.getString(3);
                    String dateOperation = outResult.getString(4);
                    String heure = outResult.getString(14);
                    String compteDebit = outResult.getString(6);
                    String compteCredit = outResult.getString(7);
                    String libelle = outResult.getString(8);
                    double montantDebit = outResult.getDouble(9);
                    double montantCredit = outResult.getDouble(10);
                    String nomUtilisateur = outResult.getString(11);
                    //int sousCpte=outResult.getInt(12);
                    String dateDoc = outResult.getString("DATE_DOC");
                    String JOURNAUX = outResult.getString("ID_JOURNAUX");
                    String profit = outResult.getString("PROFIT_CENTER");
                    String Journal = outResult.getString("id_journaux");
                    Journal c = new Journal(numOperation, numTiers, dateOperation, compteDebit, compteCredit, libelle,
                            montantDebit, montantCredit, nomUtilisateur, 0, dateDoc, Journal,
                            "", JOURNAUX, profit, numJournal,outResult.getString("DEVISE"),outResult.getDouble("DEVISE_RATE"));
                    liste.add(c);
                }
                //  Close the resultSet
            }
        } catch (SQLException e) {
            /*
             * Catch all exceptions and pass them to * the exception reporting
             * method
             */
            //System.out.println(" . . inv . exception thrown:" + e);
            insertError(e + "", " V getGrandlivre");
        }
        return liste;
    }

    public Journal getOperationTiers(int op, String jour) {

        Journal liste = null;

        try {

            try (ResultSet outResult = state.executeQuery("select * from APP.CPT_JOURNAL where CPT_JOURNAL.num_operation=" + op
                    + " and CPT_JOURNAL.POSTDATER=0 and CPT_JOURNAL.ID_JOURNAUX='" + jour + "' ")
            // //System.out.println("select * from APP.CPT_JOURNAL where CPT_JOURNAL.num_operation="+code+" and CPT_JOURNAL.POSTDATER=0");
            ) {
                while (outResult.next()) {
                    int numOperation = outResult.getInt(2);
                    int numJournal = outResult.getInt(1);
                    String numTiers = outResult.getString(3);
                    String dateOperation = outResult.getString(4);
                    String heure = outResult.getString(14);
                    String compteDebit = outResult.getString(6);
                    String compteCredit = outResult.getString(7);
                    String libelle = outResult.getString(8);
                    double montantDebit = outResult.getDouble(9);
                    double montantCredit = outResult.getDouble(10);
                    String nomUtilisateur = outResult.getString(11);
                    //int sousCpte=outResult.getInt(12);
                    String dateDoc = outResult.getString("DATE_DOC");
                    String JOURNAUX = outResult.getString("ID_JOURNAUX");
                    String profit = outResult.getString("PROFIT_CENTER");
                    
                    Journal c = new Journal(numOperation, numTiers, dateOperation, compteDebit, compteCredit, libelle,
                            montantDebit, montantCredit, nomUtilisateur, 0, dateDoc, numTiers, "", JOURNAUX,
                            profit, numJournal,outResult.getString("DEVISE"),outResult.getDouble("DEVISE_RATE"));
                    
                    return c;
                }
                //  Close the resultSet
            }
        } catch (SQLException e) {
            /*
             * Catch all exceptions and pass them to * the exception reporting
             * method
             */
            //System.out.println(" . . inv . exception thrown:" + e);
            insertError(e + "", " V getGrandlivre");
        }
        return liste;
    }

    /**
     * *****************************************************************************
     * SELECTION DU GRAND LIVRE POUR LE COMPTE CHOISI
     * ****************************************************************************
     */
      
      LinkedList<Journal> getGrandLivreTierOnly(String cpte, int cpteParent, String tiers) {
        LinkedList<Journal> liste = new LinkedList();
        try {
            try ( //System.out.println("select * from APP.CPT_JOURNAL where  compte_debit=" + cpte + " or compte_credit=" + cpte + " or ((compte_debit=" + cpteParent + " or compte_credit=" + cpteParent + ") and num_tiers='" + tiers + "' ) ");
                    ResultSet outResult = state.executeQuery("select * from APP.CPT_JOURNAL where  compte_debit='" + cpte + "' or compte_credit='" + cpte + "' or ((compte_debit='" + cpteParent + "' or compte_credit='" + cpteParent + "') and num_tiers='" + tiers + "' ) order by date_doc_int")) {
                while (outResult.next()) {
                    int numJournal = outResult.getInt(1);
                    int numOperation = outResult.getInt(2);
                    String numTiers = outResult.getString(3);
                    String dateOperation = outResult.getString(4);
                    String heure = outResult.getString(14);
                    String compteDebit = outResult.getString(6);
                    String compteCredit = outResult.getString(7);
                    String libelle = outResult.getString(8);
                    double montantDebit = outResult.getDouble(9);
                    double montantCredit = outResult.getDouble(10);
                    String nomUtilisateur = outResult.getString(11);
                    int postdater = outResult.getInt(12);
                    String dateDoc = outResult.getString("DATE_DOC");
                    String JOURNAUX = outResult.getString("ID_JOURNAUX");
                    String profit = outResult.getString("PROFIT_CENTER");
                    
                    Journal c = new Journal(numOperation, numTiers, dateOperation, compteDebit, compteCredit, libelle,
                            montantDebit, montantCredit, nomUtilisateur, postdater, dateDoc, "", "", JOURNAUX, profit,
                            numJournal,outResult.getString("DEVISE"),outResult.getDouble("DEVISE_RATE"));
                    liste.add(c);
                }
                //  Close the resultSet
            }
        } catch (SQLException e) {
            /*
             * Catch all exceptions and pass them to * the exception reporting
             * method
             */
            //System.out.println(" . . inv . exception thrown:");
            insertError(e + "", " V getGrandlivre");
        }
        return liste;
    }

    /**
     * ************************************************************************************
     * MODIFICATION TIERS
     * **********************************************************************************
     */
      void upDateTiers(Tiers c, String code) {
        try {
           
            Statement sU = conn.createStatement();
String createString1 = "update CPT_TIERS set CPT_TIERS.DESIGNATION='" + c.designation + "', CPT_TIERS.INFORMATION='" + c.info + "',"
        + " CPT_TIERS.ADRESSE='" + c.adresse + "',CPT_TIERS.EMAIL='" + c.email + "', CPT_TIERS.DEVISE='" + c.devise + "',CPT_TIERS.SIGLE_TIERS='" 
        + c.sigleTiers + "',CPT_TIERS.EXT_NUMTIERS='" + c.numExt + "',CPT_TIERS.SALAIRE_BRUTE=" + c.salaireBrute + " "
        + "where CPT_TIERS.NUM_TIERS='" + code + "'";
 //System.out.println(createString1);      
 
sU.execute(createString1);
            JOptionPane.showMessageDialog(null, " MIS A JOUR EFFECTUE ", " MERCI ", JOptionPane.PLAIN_MESSAGE);
        } catch (SQLException ex) {
            //System.out.println("echec update:");
            JOptionPane.showMessageDialog(null, "  " + ex, " MIS A JOUR ECHOUER ", JOptionPane.ERROR_MESSAGE);
            insertError(ex + "", "update TIERS");
        }

    }

    public int possibleUpdate(String code) {
        int row = 0;
        try {
          ResultSet outResult = state.executeQuery("select compte_debit, compte_credit from app.cpt_journal where CPT_JOURNAL.COMPTE_DEBIT ='" + code + "' OR CPT_JOURNAL.COMPTE_CREDIT ='" + code + "' ");
            while (outResult.next()) {
                row += 1;
            }

            //System.out.println("number of row is: " + row);
        } catch (SQLException ex) {
            //System.out.println("echec update:");
            JOptionPane.showMessageDialog(null, "  " + ex, " MIS A JOUR ECHOUER ", JOptionPane.ERROR_MESSAGE);
            insertError(ex + "", "compte used");
        }
        return row;
    }

    /**
     * ************************************************************************************
     * MODIFICATION COMPTE
     * **********************************************************************************
     */
      void upDateCompte(Compte c, String code) {
        try { 
             
Statement sU = conn.createStatement();
String createString1 = "update CPT_COMPTE set NOM_COMPTE='" + c.nomCompte + "' ,"
        + "CLASSE=" + c.classe + ", EXT_NUM_COMPTE='" + c.ext + "', CATEGORY='" + c.cpteParent + "' "
        + "  where NUM_COMPTE='" + code + "'";
//System.out.println(createString1);
            sU.execute(createString1);
            JOptionPane.showMessageDialog(null, " MIS A JOUR EFFECTUE ", " MERCI ", JOptionPane.PLAIN_MESSAGE);

        } catch (SQLException ex) {
            //System.out.println("echec update:");
            JOptionPane.showMessageDialog(null, "  " + ex, " MIS A JOUR ECHOUER ", JOptionPane.ERROR_MESSAGE);
            insertError(ex + "", "update COMPTE");
        }
    }

      
       void updateJournal(String ID, String NAME_JOURNAL, 
               String GROUPE_COMPTE_JOURNAL, String COMPTE_JOURNAL
               ,String DEVISE,String rate) {
        try {  
Statement sU = conn.createStatement(); 
String createString1 = 
          " update CPT_JOURNAUX set GROUPE_COMPTE_JOURNAL='" + GROUPE_COMPTE_JOURNAL + "' ,"
        + " COMPTE_JOURNAL='" + COMPTE_JOURNAL + "', NAME_JOURNAL='" +NAME_JOURNAL + "'"
        + " ,DEVISE='" +DEVISE + "',DEVISE_RATE=" +rate + " "
        + "  where CODE_JOURNAL='" + ID + "'";

System.out.println(createString1);

            sU.execute(createString1);
            JOptionPane.showMessageDialog(null, " WELL UPDATED", " TKS ", JOptionPane.PLAIN_MESSAGE);

        } catch (SQLException ex) {
            //System.out.println("echec update:");
            JOptionPane.showMessageDialog(null, "  " + ex, " UPDATE FAIL", JOptionPane.ERROR_MESSAGE);
            insertError(ex + "", "update COMPTE");
        }
    }
      
    public LinkedList<Compte> CompteBilan() {
        LinkedList<Compte> allCpt = new LinkedList();
//   try
//    {
//outResult = state.executeQuery("select CPT_TIERS.NUM_TIERS,"
//        + "CPT_TIERS.DESIGNATION,CPT_COMPTE.CLASSE,CPT_TIERS.NUM_COMPTE "
//        + "from APP.CPT_TIERS,CPT_COMPTE where CPT_TIERS.NUM_COMPTE=CPT_COMPTE.NUM_COMPTE AND CPT_COMPTE.CLASSE < 6 and cpte_parent=0 ORDER BY NUM_TIERS ");
//
//    while (outResult.next())
//    {
//    int numCompte=outResult.getInt(1);
//    String nomCompte=outResult.getString(2);
//    int classe=outResult.getInt(3);
//    int ncpte=outResult.getInt(4);
//
//Compte  ce =new Compte(numCompte,nomCompte,classe,ncpte);
//
//  allCpt.add(ce);
//    }
//    //  Close the resultSet
//     outResult.close();
//    }  catch (SQLException e)  {
//    /*       Catch all exceptions and pass them to
//    **       the exception reporting method             */
//    //System.out.println(" . . inv . exception thrown: compte");
//    insertError(e+""," V getinvoice");
//    }

        try {
            try ( // allCpt = new LinkedList();
                    ResultSet outResult = state.executeQuery("select * from APP.CPT_COMPTE where CPT_COMPTE.CLASSE < 6 and cpte_parent=0 ORDER BY NUM_COMPTE")) {
                while (outResult.next()) {
                    String numCompte = outResult.getString(1);
                    String nomCompte = outResult.getString(2);
                    int classe = outResult.getInt(3);
                    String cpteParent = outResult.getString(4);
                    String extr = outResult.getString(5);
                    Compte ce = new Compte(  numCompte,   nomCompte,   cpteParent,   classe,  extr);
                    
                    
                    allCpt.add(ce);
                }
                //  Close the resultSet
            }
        } catch (SQLException e) {
            /*
             * Catch all exceptions and pass them to * the exception reporting
             * method
             */
            //System.out.println(" . . inv . exception thrown: compte");
            insertError(e + "", " V getinvoice");
        }
        return allCpt;
    } 
    
    
    
         boolean upDateJournal2 (Journal jo, String etat,String raison,String employe) { 
          
        try { 
            Statement sU = conn.createStatement(); 
            String dateDoc = jo.dateDoc; 
            int dateDocInt  ;

            if (dateDoc.length() == 6) {

                String dateRenv = "" + dateDoc.substring(4) + "" + dateDoc.substring(2, 4) + "" + dateDoc.substring(0, 2);

                Integer kkk = new Integer(dateRenv); 
                dateDocInt = kkk ;
 
String createString1 = "update APP.CPT_JOURNAL set NUM_TIERS = '" + jo.numTiers 
        + "',DATE_DOC='" + jo.dateDoc
        + "',LIBELLE='" + jo.libelle + "',DATE_DOC_INT=" + dateDocInt
        + ",MONTANT_DEBIT=" + jo.montantDebit + ", MONTANT_CREDIT=" + jo.montantCredit + ""
        + ",COMPTE_DEBIT='" + jo.compteDebit + "',COMPTE_CREDIT='" + jo.compteCredit + "'  "
        + etat + " where NUM_JOURNAL = " + jo.numJournal;

               System.out.println(createString1);
Journal C =getOperationNumOP(jo.numJournal);
if(C!=null)
{
     insertJournalTrackingUpdate(C.NumOperation, C.numTiers, C.dateOperation, C.compteDebit,
                                        C.compteCredit, C.libelle, C.montantDebit, C.montantCredit, C.utilisateur, C.postDater,
                                        C.dateDoc, C.journaux, C.profitCenter,employe,raison); 
               
                sU.execute(createString1);
                return true; 
}
else
{
    if(C!=null)
    {
        JOptionPane.showMessageDialog(null, " MISSING ORIGINAL OPERATION "+C.numJournal, " UPDATE FAILED ", JOptionPane.ERROR_MESSAGE);
    }
    else
    {
            JOptionPane.showMessageDialog(null, " NULL ORIGINAL OPERATION "+C, " UPDATE FAILED ", JOptionPane.ERROR_MESSAGE);
   
    }
            return false;
}

            } else {return false;       }

        } catch (SQLException ex) {
            //System.out.println("echec update:");
            JOptionPane.showMessageDialog(null, "  " + ex, " UPDATE FAILED ", JOptionPane.ERROR_MESSAGE);
            insertError(ex + "", "update COMPTE");
        }
        return false;
    } 
    
    
    
    /**
     * ************************************************************************************
     * MODIFICATION JOURNAL POSTDATER
     * **********************************************************************************
     */
      void upDateJournalLettrage (int numOp, String Journal,String OLDlibele,String newLibelle) { 
          
        try { 
            Statement sU = conn.createStatement();  
            
String createString1 = "update APP.CPT_JOURNAL set LETTRAGE = '" + newLibelle + "'"
        + " where LIBELLE = '" + OLDlibele+"'";
System.out.println(createString1);
        sU.execute(createString1); 
        createString1 = "update APP.CPT_JOURNAL set LETTRAGE = '" + newLibelle + "'"
        + " where CPT_JOURNAL.num_operation=" + numOp + "  and CPT_JOURNAL.ID_JOURNAUX='" + Journal + "'";
        sU.execute(createString1); 
        
     System.out.println(createString1);   

        } catch (SQLException ex) {
            //System.out.println("echec update:");
            JOptionPane.showMessageDialog(null, "  " + ex, " UPDATE FAILED ", JOptionPane.ERROR_MESSAGE);
            insertError(ex + "", "update COMPTE");
        }
        
    } 
      
    /**
     * ************************************************************************************
     * MODIFICATION CODE JOURNAUX
     * **********************************************************************************
     */
      void upDateCode(CodeJournal c, String code) {
        try {
            Statement sU = conn.createStatement();
            String createString1 = "update CPT_CODE_JOURNAUX set CODE='" + c.code + "', CODE_INTITULE='" + c.codeIntitule + "' where CODE='" + code + "'";
            sU.execute(createString1);
            JOptionPane.showMessageDialog(null, " MIS A JOUR EFFECTUE ", " MERCI ", JOptionPane.PLAIN_MESSAGE);

        } catch (SQLException ex) {
            //System.out.println("echec update:");
            JOptionPane.showMessageDialog(null, "  " + ex, " MIS A JOUR ECHOUER ", JOptionPane.ERROR_MESSAGE);
            insertError(ex + "", "update CODE");
        }
    }

    /**
     * ************************************************************************************
     * MODIFICATION DETTE
     * **********************************************************************************
     */
      void upDateDetteIdOp(int op, String etat, String etatOld) {
        try {
            Statement sU = conn.createStatement();
            String createString1 = "update CPT_DETTE set CPT_DETTE.ETATS_DETTE='" + etat + "' where CPT_DETTE.NUM_OPERATION=" + op + " AND ETATS_DETTE='" + etatOld + "'";

            sU.execute(createString1);
        } catch (SQLException ex) {
            insertError(ex + "", "update dette");
        }
    }

    public void upDateDetteIdDette(int IdDette, String etat) {
        try {
            Statement sU = conn.createStatement();
            String createString1 = "update CPT_DETTE set CPT_DETTE.ETATS_DETTE='" + etat
                    + "' where CPT_DETTE.ID_DETTE=" + IdDette + " ";
            sU.execute(createString1);

        } catch (SQLException ex) {
            insertError(ex + "", "update dette");
        }
    }

    public void upDateDetteId(int op, String etatNew, int montant) {

        try {

            Statement sU = conn.createStatement();

            String createString1 = "update CPT_DETTE set CPT_DETTE.ETATS_DETTE='" + etatNew + "', "
                    + "CPT_DETTE.MONTANT_DETTES=" + montant + " where CPT_DETTE.ID_DETTE=" + op;

            sU.execute(createString1);

        } catch (SQLException ex) {

            //System.out.println("echec upDateDetteId:");
            JOptionPane.showMessageDialog(null, "  " + ex, " PAIEMENT ECHOUER _ upDateDetteId", JOptionPane.ERROR_MESSAGE);
            insertError(ex + "", "upDateDetteId");

        }

    }

    /**
     * ************************************************************************************
     * MODIFICATION DETTE
     * **********************************************************************************
     */
      void upDateDette(int op, String code, String p, String type, boolean g) {
        try {
            Statement sU = conn.createStatement();
            String createString1  = "update CPT_DETTE set CPT_DETTE.ETATS_DETTE='" + code + "' where CPT_DETTE.NUM_OPERATION=" + op + " AND ETATS_DETTE='" + p + "'";
            sU.execute(createString1);

        } catch (SQLException ex) {
            insertError(ex + "", "update dette");
        }
    }

    /**
     * *************************************************************************************
     * QUAND ON CLICK LE BOUTON SQL
     * **************************************************************************************
     */
      void sql(final JButton sqlButton) {
        sqlButton.addActionListener((ActionEvent ae) -> {
            if (ae.getSource() == sqlButton) {
                
                try {
                    
                    String pwd = JOptionPane.showInputDialog(null, " PWD ");
                    
                    if (pwd.equals("kilo")) {
                        String createString1 = "delete";
                        String createString2 = JOptionPane.showInputDialog(null, " Ecrire votre syntaxe SQL ", "update APP.--  set  --   where --=--");
                        
                        //if(!createString1.contains(" delete from lot"))
                        state = conn.createStatement();
                        state.execute(createString1);
                        
                    }
                    
                    if (pwd.equals("gamma")) {
                        
                        String createString1 = JOptionPane.showInputDialog(null, " Ecrire votre syntaxe SQL ", "update APP.--  set  --   where --=--");
                        
                        //if(!createString1.contains(" delete from lot"))
                        state = conn.createStatement();
                        state.execute(createString1);
                    }
                    
                } catch (HeadlessException | SQLException e) {
                    insertError(e + "", " V sql");
                }
                
            }
        });
    }

   

    String setVirgule(int frw) {
        String setString = "" + frw;
        int l = setString.length();

        if (l > 3 && l <= 6) {
            setString = setString.substring(0, l - 3) + " " + setString.substring(l - 3);
        }
        if (l > 6) {

            String s1 = setString.substring(0, l - 3);
            int sl = s1.length();
            setString = s1.substring(0, sl - 3) + " " + s1.substring(sl - 3) + " " + setString.substring(l - 3);

        }
        return setString;
    }

    public LinkedList<Compte> getBalanceVerification(String searchPeriode) {

        LinkedList<Compte> getCpte = CompteBalanceVer2(); //hano ni gukurura liste de tous les comptes ziri ranges par ordre de priorite muri balance de verification     
        LinkedList<Compte> getUsedCpte = new LinkedList<   >();

        try {
            for (int k = 0; k < getCpte.size(); k++) { //hano ni kunyura kuri buri compte....

Compte compte = getCpte.get(k);
String COMPTE = "" + compte.numCompte;
                
String sql="select sum(MONTANT_DEBIT),sum(MONTANT_CREDIT), sum(MONTANT_DEBIT)-sum(MONTANT_CREDIT)"
        + " from APP.CPT_JOURNAL "
+ "  where ( APP.CPT_JOURNAL.COMPTE_DEBIT = '" + COMPTE + "' OR  APP.CPT_JOURNAL.COMPTE_CREDIT = '" + COMPTE + "' ) AND ETAT <> 'DELETED' " 
        + searchPeriode  ;
 
                try (//System.out.println(sql);
                        ResultSet outResult = state.executeQuery(sql)) {
                    while (outResult.next()) {
                        
                        double montantdebit = outResult.getDouble(1);
                        double montantcredit = outResult.getDouble(2);
                        
                        if (!(montantdebit == 0 && montantcredit == 0)) {
//                        getCpte.get(k).montant = getCpte.get(k).montant + outResult.getInt(3);
getCpte.get(k).montantDebit = getCpte.get(k).montantDebit + outResult.getDouble(1);
getCpte.get(k).montantCredit = getCpte.get(k).montantCredit + outResult.getDouble(2);
getUsedCpte.add(getCpte.get(k));
                        }
                    }
                }
            }
           
        } catch (SQLException e) {
            //System.out.println(" . . inv . exception thrown:" + e);
            insertError(e + "", " getBalanceVerification  ");
        }
//        return getCpte;
        return getUsedCpte;
    }

public LinkedList<Compte> CompteBalanceVer2() {

    LinkedList<Compte> allCpt = new LinkedList();

    try {
        //hano ni gukurura liste de tous les comptes ari ranges par ordre de priorite muri balance de verification  
      
//        String sql="select NUM_COMPTE,NOM_COMPTE,0,CATEGORY "
//                + "from APP.CPT_COMPTE,APP.CPT_CATEGORIES_COMPTE where "
//                + " CPT_COMPTE.CATEGORY = CPT_CATEGORIES_COMPTE.CATEGORY_NAME"
//                + " ORDER BY NOM_COMPTE";
        
        String sql=" select NUM_COMPTE,NOM_COMPTE,0,CATEGORY "
                + " from APP.CPT_COMPTE ORDER BY NOM_COMPTE";
        
        System.out.println(sql);
        
        try (ResultSet outResult = state.executeQuery(sql)) {
            while (outResult.next()) {
//                Compte ce = new Compte(outResult.getInt(1), outResult.getString(2), 0, 0, outResult.getString(4),0);
Compte ce = new Compte(outResult.getString(1), outResult.getString(2), 0, "",
        outResult.getString(4), 0, 0);
allCpt.add(ce);
            }
        }
    } catch (SQLException e) {
        //System.out.println(" . . inv . exception thrown: compte");
        insertError(e + "", " V getinvoice");
    }
    return allCpt;
}
 

 public LinkedList<Journal> getOperation(int code, String jour) {

LinkedList<Journal> liste = new LinkedList();
try {
            
    try (ResultSet outResult = state.executeQuery(
            "select * from APP.CPT_JOURNAL "
                    + "where CPT_JOURNAL.num_operation=" + code + " and CPT_JOURNAL.POSTDATER = 0 AND ETAT != 'DELETED' "
                            + "and CPT_JOURNAL.ID_JOURNAUX='" + jour + "'")
    // //System.out.println("select * from APP.CPT_JOURNAL where CPT_JOURNAL.num_operation="+code+" and CPT_JOURNAL.POSTDATER=0");
    ) {
        while (outResult.next()) {
            int numOperation = outResult.getInt(2);
            int numJournal = outResult.getInt(1);
            String numTiers = outResult.getString(3);
            String dateOperation = outResult.getString(4);
            String heure = outResult.getString(14);
            String compteDebit = outResult.getString(6);
            String compteCredit = outResult.getString(7);
            String libelle = outResult.getString(8);
            double montantDebit = outResult.getDouble(9);
            double montantCredit = outResult.getDouble(10);
            String nomUtilisateur = outResult.getString(11);
            //int sousCpte=outResult.getInt(12);
            String dateDoc = outResult.getString("DATE_DOC");
            String JOURNAUX = outResult.getString("ID_JOURNAUX");
            String profit = outResult.getString("PROFIT_CENTER");
            
            Journal c = new Journal(numOperation, numTiers, dateOperation, compteDebit, compteCredit, libelle,
                    montantDebit, montantCredit, nomUtilisateur, 0, dateDoc, numTiers, "", JOURNAUX, profit,
                    numJournal,outResult.getString("DEVISE"),outResult.getDouble("DEVISE_RATE"));
            
            liste.add(c);
        }
        //  Close the resultSet
    }
        } catch (SQLException e) {
            //System.out.println(" . . inv . exception thrown:" + e);
            insertError(e + "", " V getGrandlivre");
        }
        return liste;
    }
 
 public LinkedList<Journal> getLesVente(  String jour, String periode) {

LinkedList<Journal> liste = new LinkedList();
try {
    String sql=
            "select * from APP.CPT_JOURNAL "
                    + "where  ETAT != 'DELETED'  and CPT_JOURNAL.ID_JOURNAUX like '" + jour + "%' "+periode+ " ORDER BY NUM_TIERS";
          System.out.println(sql);
    
    try (ResultSet outResult = state.executeQuery(sql)
    ) {
        while (outResult.next()) {
            
            int numOperation = outResult.getInt(2);
            int numJournal = outResult.getInt(1);
            String numTiers = outResult.getString(3);
            String dateOperation = outResult.getString(4);
            String heure = outResult.getString(14);
            String compteDebit = outResult.getString(6);
            String compteCredit  = outResult.getString(7);
            String libelle       = outResult.getString(8);
            double montantDebit  = outResult.getDouble(9);
            double montantCredit = outResult.getDouble(10);
            String nomUtilisateur = outResult.getString(11);
            //int sousCpte=outResult.getInt(12);
            String dateDoc = outResult.getString("DATE_DOC");
            String JOURNAUX = outResult.getString("ID_JOURNAUX");
            String profit = outResult.getString("PROFIT_CENTER");
            
            Journal c = new Journal(numOperation, numTiers, dateOperation, compteDebit, compteCredit, libelle,
                    montantDebit, montantCredit, nomUtilisateur, 0, dateDoc, numTiers, "", JOURNAUX, profit,
                    numJournal,outResult.getString("DEVISE"),outResult.getDouble("DEVISE_RATE"));
            c.lettre = outResult.getString("LETTRAGE");
            liste.add(c);
        }
        //  Close the resultSet
    }
        } catch (SQLException e) {
            //System.out.println(" . . inv . exception thrown:" + e);
            insertError(e + "", " V lettrage");
        }
        return liste;
    }
 
    public String getRacine() {
     
        
        try {
            
            String sql="select  * from APP.INVOICE_COMPTEUR WHERE TABLE_CPTE='INVOICE'";
            
            
            
            try (ResultSet outResult2 = state.executeQuery(sql)) {
                while (outResult2.next()) {
                    String rac = outResult2.getString("RACINE_CPTE");
                     
                    return rac.substring(0, rac.length()-2);
                    
                }
            }
        } catch (SQLException e) {
            insertError(e + "", "getNextNum");
        }
       
        return "000";
    }

 
 public LinkedList<Journal> getLesPaiement(    String periode) {

LinkedList<Journal> liste = new LinkedList();
try {
            
    try (ResultSet outResult = state.executeQuery(
            "select * from APP.CPT_JOURNAL "
                    + "where  ETAT != 'DELETED'  and COMPTE_DEBIT like '5%' "+periode)
    // //System.out.println("select * from APP.CPT_JOURNAL where CPT_JOURNAL.num_operation="+code+" and CPT_JOURNAL.POSTDATER=0");
    ) {
        while (outResult.next()) {
            int numOperation = outResult.getInt(2);
            int numJournal = outResult.getInt(1);
            String numTiers = outResult.getString(3);
            String dateOperation = outResult.getString(4);
            String heure = outResult.getString(14);
            String compteDebit = outResult.getString(6);
            String compteCredit  = outResult.getString(7);
            String libelle       = outResult.getString(8);
            double montantDebit  = outResult.getDouble(9);
            double montantCredit = outResult.getDouble(10);
            String nomUtilisateur = outResult.getString(11);
            //int sousCpte=outResult.getInt(12);
            String dateDoc = outResult.getString("DATE_DOC");
            String JOURNAUX = outResult.getString("ID_JOURNAUX");
            String profit = outResult.getString("PROFIT_CENTER");
            
            Journal c = new Journal(numOperation, numTiers, dateOperation, compteDebit, compteCredit, libelle,
                    montantDebit, montantCredit, nomUtilisateur, 0, dateDoc, numTiers, "", JOURNAUX, profit,
                    numJournal,outResult.getString("DEVISE"),outResult.getDouble("DEVISE_RATE"));
            c.lettre = outResult.getString("LETTRAGE");
            liste.add(c);
        }
        //  Close the resultSet
    }
        } catch (SQLException e) {
            //System.out.println(" . . inv . exception thrown:" + e);
            insertError(e + "", " V getGrandlivre");
        }
        return liste;
    }
 
 
 public  Journal  getOperationNumOP(int numJour ) {
 
try {
            
    try (ResultSet outResult = state.executeQuery(
            "select * from APP.CPT_JOURNAL "
                    + "where CPT_JOURNAL.NUM_JOURNAL=" +  numJour  )
    // //System.out.println("select * from APP.CPT_JOURNAL where CPT_JOURNAL.num_operation="+code+" and CPT_JOURNAL.POSTDATER=0");
    ) {
        while (outResult.next()) {
            int numOperation = outResult.getInt(2);
            int numJournal = outResult.getInt(1);
            String numTiers = outResult.getString(3);
            String dateOperation = outResult.getString(4);
            String heure = outResult.getString(14);
            String compteDebit = outResult.getString(6);
            String compteCredit = outResult.getString(7);
            String libelle = outResult.getString(8);
            double montantDebit = outResult.getDouble(9);
            double montantCredit = outResult.getDouble(10);
            String nomUtilisateur = outResult.getString(11);
            //int sousCpte=outResult.getInt(12);
            String dateDoc = outResult.getString("DATE_DOC");
            String JOURNAUX = outResult.getString("ID_JOURNAUX");
            String profit = outResult.getString("PROFIT_CENTER");
            
            return new Journal(numOperation, numTiers, dateOperation, compteDebit, compteCredit, libelle,
                    montantDebit, montantCredit, nomUtilisateur, 0, dateDoc, numTiers, heure, JOURNAUX, profit,
                    numJournal,outResult.getString("DEVISE"),outResult.getDouble("DEVISE_RATE"));
        }
        //  Close the resultSet
    }
        } catch (SQLException e) {
            //System.out.println(" . . inv . exception thrown:" + e);
            insertError(e + "", " V getGrandlivre");
        }
        return null;
    }
 
 public  Journal  getOperationLibelle(String lib ) {
  LinkedList <Journal> laliste= new LinkedList<>();
try {
   
    
    try (ResultSet outResult = state.executeQuery(
            "select * from APP.CPT_JOURNAL "
                    + "where CPT_JOURNAL.LIBELLE='" +  lib+"'"  )
    // //System.out.println("select * from APP.CPT_JOURNAL where CPT_JOURNAL.num_operation="+code+" and CPT_JOURNAL.POSTDATER=0");
    ) {
        while (outResult.next()) {
            int numOperation = outResult.getInt(2);
            int numJournal = outResult.getInt(1);
            String numTiers = outResult.getString(3);
            String dateOperation = outResult.getString(4);
            String heure = outResult.getString(14);
            String compteDebit = outResult.getString(6);
            String compteCredit = outResult.getString(7);
            String libelle = outResult.getString(8);
            double montantDebit = outResult.getDouble(9);
            double montantCredit = outResult.getDouble(10);
            String nomUtilisateur = outResult.getString(11);
            //int sousCpte=outResult.getInt(12);
            String dateDoc = outResult.getString("DATE_DOC");
            String JOURNAUX = outResult.getString("ID_JOURNAUX");
            String profit = outResult.getString("PROFIT_CENTER");
            
            laliste.add(new Journal(numOperation, numTiers, dateOperation, compteDebit, compteCredit, libelle,
                    montantDebit, montantCredit, nomUtilisateur, 0, dateDoc, numTiers, heure, JOURNAUX, profit,
                    numJournal,outResult.getString("DEVISE"),outResult.getDouble("DEVISE_RATE")));
        }
        //  Close the resultSet
    }
        } catch (SQLException e) {
            //System.out.println(" . . inv . exception thrown:" + e);
            insertError(e + "", " V getGrandlivre");
        }
 
        return (Journal) JOptionPane.showInputDialog
    (null, " CHOOSE THE RIGTH INVOICE", "LETTRAGE", JOptionPane.QUESTION_MESSAGE, null,
           laliste.toArray(),"" );
    }
 
 
void insertChangeTier(String USERNAME, String PRODUCTNAME, double PRICE, String code,double ancien) {
        try {
            psInsert = conn.prepareStatement("insert into APP.CHANGEMENT(PRICE,USERNAME,PRODUCTNAME,CODE)  "
                    + " values (?,?,?,?)");

            psInsert.setDouble(1, PRICE);
            psInsert.setString(2, USERNAME);
            psInsert.setString(3, PRODUCTNAME+"  "+ancien);
            psInsert.setString(4, code);
            psInsert.executeUpdate();
            psInsert.close();
        } catch (SQLException ex) {
            insertError(ex + "", "insertChangePrice");
        }


    }
    static String startTime() {

        java.util.Date toDay = new java.util.Date();

        String year = "" + (2000 + (toDay.getYear() - 100));//garantie de un millenaire, a changer muri 3000
         String mounth  ;
         if ((toDay.getMonth() + 1) < 10) {
            mounth = "0" + (toDay.getMonth() + 1);
        } else {
            mounth = "" + (toDay.getMonth() + 1);
        } 
        return year + "-" + mounth + "-01";

    }

    static String startYear() {

        java.util.Date toDay = new java.util.Date();

        String year = "" + (2000 + (toDay.getYear() - 100));//garantie de un millenaire, a changer muri 3000

        String mounth = "01";
        return year + "-" + mounth + "-01";

    }

    static String EndTime() {

        java.util.Date toDay = new java.util.Date();

        int y = (2000 + (toDay.getYear() - 100));//garantie de un millenaire, a changer muri 3000

        String year = "" + y; 
        String mounth  ;  String day  ; 
        if ((toDay.getMonth() + 1) < 10) {
            mounth = "0" + (toDay.getMonth() + 1);
        } else {
            mounth = "" + (toDay.getMonth() + 1);
        } 
        day = "" + getMois(mounth, y); 
        return year + "-" + mounth + "-" + day; 
    }

    static String getMois(String s1, int year) {

        String m = "";



        if (s1.equals("01")) {
            m = "31";
        }

        if (s1.equals("03")) {
            m = "31";
        }

        if (s1.equals("04")) {
            m = "30";
        }

        if (s1.equals("05")) {
            m = "31";
        }

        if (s1.equals("06")) {
            m = "30";
        }

        if (s1.equals("07")) {
            m = "31";
        }

        if (s1.equals("08")) {
            m = "31";
        }

        if (s1.equals("09")) {
            m = "30";
        }

        if (s1.equals("10")) {
            m = "31";
        }

        if (s1.equals("11")) {
            m = "30";
        }

        if (s1.equals("12")) {
            m = "31";
        }



        if (s1.equals("02")) {

            if (year % 4 == 0) {
                m = "29";
            } else {
                m = "28";
            }

        }
        return m;

    }

    public static String setSansVirgule(double frw) {
        return setVirgule(frw, 1, 0.005).replaceAll(",", "");

    }

    public static String setVirgule(double frw, int NEG, double arr) {

//        //System.out.println("frw:"+frw);
        if (arr == 0.005) {
            frw = frw + 0.0055999999999;
        }
//        //System.out.println("frw arre:"+frw);
        BigDecimal big = new BigDecimal(frw);
//        big = big.setScale(2, BigDecimal.ROUND_UP);
        String getBigString = big.toString();
//        //System.out.println("big:"+getBigString);
        getBigString = getBigString.replace('.', ';');
//  //System.out.println("after;"+getBigString);
        String[] sp = getBigString.split(";");

        String decimal = "";
        if (sp.length > 1) {
            decimal = sp[1];
        }

        if (decimal.equals("") || (decimal.length() == 1 && decimal.equals("0"))) {
            decimal = ",00";
        } else if (decimal.length() == 1 && !decimal.equals("0")) {
            decimal = "," + decimal + "0";
        } else {
            decimal = "," + decimal.substring(0, 2);
        }


        String toret = setVirgule(sp[0]) + decimal;

        if (NEG == -1 && frw > 0) {
            toret = "-" + toret;
        }

        return toret;
    }

    public static String setVirgule(String frw) {
        String setString = "";
        int k = 0;
        for (int i = (frw.length() - 1); i >= 0; i--) {
            k++;
            if ((k - 1) % 3 == 0 && (k - 1) != 0) {
                setString += " ";
            }
            setString += frw.charAt(i);
        }
        return inverse(setString);
    }

    
    String setVirgule(double frw) {
        //System.out.println("  bb"+frw);
        StringTokenizer st1 = new StringTokenizer("" + frw);

        String entier = st1.nextToken(".");
//System.out.println(frw);
        String decimal = st1.nextToken("").replace(".", "");

        if (decimal.length() == 1 && decimal.equals("0")) {
            decimal = ".00";
        } else if (decimal.length() == 1 && !decimal.equals("0")) {
            decimal = "." + decimal + "0";
        } else {
            decimal = "." + decimal.substring(0, 2);
        }

        String setString = entier + decimal;

        int l = setString.length();

        if (l < 2) {
            setString = "    " + frw;
        } else if (l < 3) {
            setString = "  " + frw;
        } else if (l < 4) {
            setString = "  " + frw;
        }

        int up = 6;
        int ju = up + 3;
        if (l > up && l <= ju) {
            setString = setString.substring(0, l - up) + "," + setString.substring(l - up);
        } else if (l > ju) {
            setString = setString.substring(0, l - 9) + "," + setString.substring(l - 9, l - 6) + "," + setString.substring(l - 6, l);
        }
        return setString;
    }

    public LinkedList<String> getLibelles (  ) {
LinkedList<String> liste = new LinkedList<>();  
try {  
String  toSearch = "select DISTINCT LIBELLE from APP.CPT_JOURNAL where   ETAT <> 'DELETED' ";

System.out.println(toSearch);

ResultSet outResult = state.executeQuery(toSearch);
 
while (outResult.next()) { 
  liste.add(outResult.getString(1)); 
}  
} catch (SQLException e) {
System.out.println(" . . method : getLibelles(). exception thrown: " + e);
insertError(e + "", " getLibelles  ");
}
//        return getCpte;
return liste;
}        
    
    public static String set4Virgule(String frw) {
        String setString = "";
        int k = 0;
        for (int i = (frw.length() - 1); i >= 0; i--) {
            k++;
            if ((k - 1) % 4 == 0 && (k - 1) != 0) {
                setString += "-";
            }
            setString += frw.charAt(i);
        }
        return inverse(setString);
    }

    static String inverse(String frw) {
        String l = "";
        for (int i = (frw.length() - 1); i >= 0; i--) {
            l += frw.charAt(i);
        }
        return l;
    }

    

    public LinkedList<Tiers> getAllTier2() {
        try {
            allTiers = new LinkedList();

            //System.out.println("select * from APP.CPT_TIERS   ORDER BY designation");
            try (ResultSet outResult = state.executeQuery("select * from APP.CPT_TIERS   ORDER BY designation")) {
                //System.out.println("select * from APP.CPT_TIERS   ORDER BY designation");
                while (outResult.next()) {
                    String numTiers = outResult.getString(1);
                    String compteT = outResult.getString(2);
                    String designation = outResult.getString(3);
                    String info = outResult.getString(4);
                    String adresse = outResult.getString(5);
                    String devise = outResult.getString(6);
                    String sigle = outResult.getString(7);
                    String numAffilie = outResult.getString(8);
                    int salaireBrute = outResult.getInt(9);
                    String EXT_NUM = outResult.getString(10);
                    String email = outResult.getString("EMAIL");
                    Tiers c = new Tiers(numTiers, compteT, designation, info, adresse,email,
                            devise, sigle, numAffilie, salaireBrute,EXT_NUM);
                    
                    allTiers.add(c);
                }
                //  Close the resultSet
            }
        } catch (SQLException e) {
            //System.out.println(" . . method : getAllTier2(). exception thrown:" + e);
            insertError(e + "", " method :getAllTier2() ");
            JOptionPane.showMessageDialog(null, "  " + e, " Ikibazo in: ", JOptionPane.PLAIN_MESSAGE);
        }
        return allTiers;
    }

    
    public LinkedList<Tiers> getAllTierGroup(String group) {
        try {
            allTiers = new LinkedList();

            String sql="select * from APP.CPT_TIERS "+group+"  ORDER BY designation";
            
             System.out.println(sql);
            try (ResultSet outResult = state.executeQuery(sql)) {
                while (outResult.next()) {
                    String numTiers = outResult.getString(1);
                    String compteT = outResult.getString(2);
                    String designation = outResult.getString(3);
                    String info = outResult.getString(4);
                    String adresse = outResult.getString(5);
                    
                    if(adresse== null || adresse.length()!=10)
                    {
                        adresse=getFirstInvoice(designation,state3);
                    }
                    else
                    {
                        
                        adresse=adresse+" 00:00:00";
                    }
                    
                    String devise = outResult.getString(6);
                    String sigle = outResult.getString(7);
                    String numAffilie = outResult.getString(8);
                    int salaireBrute = outResult.getInt(9);
                    String EXT_NUM = outResult.getString(10);
                    String email = outResult.getString("EMAIL");
                    Tiers c = new Tiers(numTiers, compteT, designation, info, adresse,email,
                            devise, sigle, numAffilie, salaireBrute,EXT_NUM);
                    System.out.println(c.toString3());
                    allTiers.add(c);
                }
                //  Close the resultSet
            }
        } catch (SQLException e) {
            //System.out.println(" . . method : getAllTier2(). exception thrown:" + e);
            insertError(e + "", " method :getAllTier2() ");
            JOptionPane.showMessageDialog(null, "  " + e, " Ikibazo in: ", JOptionPane.PLAIN_MESSAGE);
        }
        return allTiers;
    }

    
    public LinkedList<String> getUsedAccounts() {

        LinkedList<String> jean = new LinkedList();

        try {

          ResultSet outResult = state.executeQuery("select distinct(COMPTE_CREDIT) "
                    + "from APP.CPT_JOURNAL order by COMPTE_CREDIT");

            //System.out.println("select distinct(COMPTE_CREDIT) from APP.CPT_JOURNAL order by COMPTE_CREDIT");

            while (outResult.next()) {

                String Compte = outResult.getString(1);

                if (!(Compte.equals("  ") || Compte.equals("")) && numubare(Compte)) {

                    jean.add(Compte);

                    //System.out.println(" distinct used account : " + Compte);

                }

            }
            //  Close the resultSet
            outResult.close();

            outResult = state.executeQuery("select distinct(COMPTE_DEBIT) from APP.CPT_JOURNAL order by COMPTE_DEBIT");

            //System.out.println("select distinct(COMPTE_DEBIT) from APP.CPT_JOURNAL order by COMPTE_DEBIT");

            while (outResult.next()) {

                String Compte = outResult.getString(1);

                if (!(jean.contains(Compte)) && !(Compte.equals("  ") || Compte.equals("")) && numubare(Compte)) {

                    jean.add(Compte);

                    //System.out.println(" distinct used account : " + Compte);

                }

            }
            //  Close the resultSet
            outResult.close();

            Collections.sort(jean);


        } catch (SQLException e) {
            //System.out.println(" . . method : getAllTier2(). exception thrown:" + e);
            insertError(e + "", " method :getAllTier2() ");
            JOptionPane.showMessageDialog(null, "  " + e, " Ikibazo in: ", JOptionPane.PLAIN_MESSAGE);
        }
        return jean;

    }

    public int getDateDocOperation(int numOperation, String num_tiers) {

        int date_doc = 0;

        try {
            try (ResultSet outResult = state.executeQuery("select DATE_DOC_INT from APP.CPT_JOURNAL where NUM_OPERATION = " + numOperation + " AND NUM_TIERS = '" + num_tiers + "' ")) {
                while (outResult.next()) {
                    date_doc = outResult.getInt(1);
                }
            }
        } catch (SQLException e) {
            insertError(e + "", " V has child");
        }
        return date_doc;
    }

    public String getLibelleOperation(int numOperation, String num_tiers) {

        String Libelle = "";

        try {
            try (ResultSet outResult = state.executeQuery("select LIBELLE from APP.CPT_JOURNAL where NUM_OPERATION = " + numOperation + " AND NUM_TIERS = '" + num_tiers + "' ")) {
                while (outResult.next()) {
                    Libelle = outResult.getString(1);
                }
            }
        } catch (SQLException e) {
            insertError(e + "", " V has child");
        }
        return Libelle;
    }

    public String getCompteResultatV2Un(String search, String NAME_COMPTE) {

        // LinkedList<Lot> lotYazo = new LinkedList ();
        String res = "";
        String COMPTE = getCompteCpte(NAME_COMPTE);

        try {

            String sql = "select NUM_TIERS,DATE_OPERATION,LIBELLE, (MONTANT_DEBIT), (MONTANT_CREDIT)  from APP.CPT_JOURNAL"
                    + "  where (APP.CPT_JOURNAL.COMPTE_DEBIT = '" + COMPTE + "' OR"
                    + "  APP.CPT_JOURNAL.COMPTE_CREDIT = '" + COMPTE + "') " + search;

            try (ResultSet outResult = state.executeQuery(sql)
            //System.out.println(sql);
            ) {
                while (outResult.next()) {
                    
                    
                    String NUM_TIERS = outResult.getString(1);
                    String DATE_OPERATION = outResult.getString(2);
                    String LIBELLE = outResult.getString(3);
                    int montantdebit = outResult.getInt(4);
                    int montantcredit = outResult.getInt(5);
                    
                    if (!(montantdebit == 0 && montantcredit == 0)) {
                        if (montantcredit == 0) {
                            res += NUM_TIERS + "  " + DATE_OPERATION + "  " + LIBELLE + " D  " + montantdebit + " \n";
                        }
                        //  lotYazo.add(new Lot(NUM_TIERS, montantdebit,DATE_OPERATION,LIBELLE));
                    } else if (montantdebit == 0) {
                        res += NUM_TIERS + "  " + DATE_OPERATION + "  " + LIBELLE + " C  " + montantcredit + " \n";
                        // lotYazo.add(new Lot(NUM_TIERS, montantcredit,DATE_OPERATION,LIBELLE));  }
                    }
                }
            }
        } catch (SQLException e) {
            //System.out.println(" . . inv . exception thrown:" + e);
            insertError(e + "", " getBalanceVerification  ");
        }
//        return getCpte;
        return res;
    }

    String getCompteCpte(String nom_compte) {
        if (nom_compte != null && (nom_compte.length() >= 2)) {
            try {
                try (ResultSet outResult = state.executeQuery("select * from APP.CPT_COMPTE where NoM_COMPTE = '" + nom_compte + "'")
                //System.out.println("select * from APP.CPT_COMPTE where NoM_COMPTE = '" + nom_compte + "'");
                ) {
                    while (outResult.next()) {
                        return outResult.getString("NuM_COMPTE");
                    }
                }
            } catch (SQLException e) {
                //System.out.println(".....compte" + e);
            }
        }
        return " ";
    }

    public static boolean numubare(String str) {
        if (str == null) {
            return false;
        }
        int length = str.length();
        if (length == 0) {
            return false;
        }
        int i = 0;
        if (str.charAt(0) == '-') {
            if (length == 1) {
                return false;
            }
            i = 1;
        }
        for (; i < length; i++) {
            char c = str.charAt(i);
            if (c <= '/' || c >= ':') {
                return false;
            }
        }
        return true;
    }

    

    static public void insertError(Connection conn, String desi, String ERR) {
        try {
            try (PreparedStatement psInsert = conn.prepareStatement("insert into APP.ERROR(DESI,USERNAME)  values (?,?)")) {
                if (desi.length() > 160) {
                    desi = desi.substring(0, 155);
                }
                if (ERR.length() > 160) {
                    ERR = ERR.substring(0, 155);
                }
                
                psInsert.setString(1, desi);
                psInsert.setString(2, ERR);
                
                psInsert.executeUpdate();
            }

            JOptionPane.showMessageDialog(null, " insertError  " + desi, " Ikibazo ", JOptionPane.PLAIN_MESSAGE);

        } catch (SQLException | HeadlessException ex) {
            JOptionPane.showMessageDialog(null, " insertError " + ex, " Ikibazo ", 
                    JOptionPane.PLAIN_MESSAGE); 
        }
    }

   
    
     public boolean sleepClient( String nAff) {
        try {

String createString1 = " update APP.CLIENT set  STATUS='SOMMEIL' where NUM_AFFILIATION='" + nAff + "'";

state.execute(createString1);
JOptionPane.showMessageDialog(null, " TIER UPDATED ", " THANK YOU ! ", JOptionPane.PLAIN_MESSAGE);
return true;
        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(null, "  " + ex, " UpDate Client Fail ", JOptionPane.ERROR_MESSAGE);
            insertError(ex + "", "update client");
            return false;
        }
    }

     
    
      boolean estComptabilise(String num_affiliation) {
        try {
            try (ResultSet outResultC = state.executeQuery("select  * from APP.INVOICE  WHERE  num_affiliation = '"
                    + num_affiliation + "' and comptabilise='NON'")) {
                while (outResultC.next()) {
                    outResultC.close();
                    return true;
                }
            }
        } catch (SQLException e) {
            insertError(e + "", "getNextNum");
        }

        return false;
    }
    

    public boolean upDateJournalTier(String oldTier, String newTier,String employe) {

        try { 
            Statement sU = conn.createStatement(); 
            
   String sql = "update APP.CPT_JOURNAL set NUM_TIERS = '" + newTier + "'  where  NUM_TIERS = '" + oldTier + "' "  ;
   System.out.println(sql); 
   sU.execute(sql); 
     insertChangeTier(employe, sql, 0,oldTier,0);
   return true;
        } catch (SQLException ex) {
            //System.out.println("echec update:");
            JOptionPane.showMessageDialog(null, "  " + ex, " MIS A JOUR ECHOUER ", JOptionPane.ERROR_MESSAGE);
            insertError(ex + "", "update COMPTE");
        }
        
        return true;
    }
    
       boolean deleteTier(Tiers t,String employe) {
        try {
           // NUM_TIERS FROM APP.CPT_TIERSf
           
           //System.out.println(" delete from APP.CPT_TIERS  where NUM_TIERS='" + numTier+ "'");
            state.execute(" delete from APP.CPT_TIERS  where NUM_TIERS='" + t.numTiers+ "'");
            
            insertChangeTier(employe, "DEL;TIER;"+t, 0,t.numTiers,0);
            return true;
        } catch (SQLException e) {
            insertError(conn, e + "", "deleteTier");
            return false;
        }
    } 
    
       boolean deleteCompte(Compte cpt,String employe) {
        try { 
            
            state.execute(" delete from APP.CPT_COMPTE  where NUM_COMPTE='" + cpt.numCompte+ "'"); 
            insertChangeTier(employe, "DEL;CPT;"+cpt, 0,cpt.numCompte,0);
            return true;
        } catch (SQLException e) {
            insertError(conn, e + "", "deleteTier");
            return false;
        }
    } 
    
       
    

    String getTransitaire(int id_bon_cmd, Statement s) {

        String transitaire = null; 
        try {

            try ( 
                    ResultSet outResult33 = s.executeQuery("select TRANSITAIRE from APP.BON_CMD where "
                            + "  APP.BON_CMD.ID_BON_CMD = " + id_bon_cmd + " ")) {
                while (outResult33.next()) {
                    transitaire = outResult33.getString("TRANSITAIRE");
                }
            }

        } catch (SQLException e) {
            insertError(conn, e + "", "getTransitaire");
            //System.out.println("getTransitaire   ....... " + e);
        }

        return transitaire;

    }

    //888888888888888888888888888888888888888888888888888888888888888
   
      public static String todayTime() {

        java.util.Date toDay = new java.util.Date();

        String year = "" + (2000 + (toDay.getYear() - 100));//garantie de un millenaire, a changer muri 3000

        String mounth,dd;

        if ((toDay.getMonth() + 1) < 10) {
            mounth = "0" + (toDay.getMonth() + 1);
        } else {
            mounth = "" + (toDay.getMonth() + 1);
        }
  if ((toDay.getDate() + 1) < 10) {
            dd = "0" + (toDay.getDate());
        } else {
            dd = "" + (toDay.getDate() );
        }


        return year + "-" + mounth + "-" + dd  ;

    }
    
    
    public String howItis(Compte cici) {

        String nukubimeze = null;

        Compte C = cici;
 
            if (C.montantDebit > C.montantCredit) {

                double balance = C.montantDebit - C.montantCredit;
                nukubimeze = "Balance Debiteur" + setVirgule(balance, 1, 0.005, " ");

            } else if (C.montantCredit > C.montantDebit) {

                double balance = C.montantCredit - C.montantDebit;
                nukubimeze = "Balance Debiteur" + setVirgule(balance, 1, 0.005, " "); 

            }
 

        return nukubimeze;
    }

    public Tiers getTiersMultiChoice(String Nom_client) {

        Tiers c = null;

        try {

            try (ResultSet outResult77 = state.executeQuery("select * from APP.CPT_TIERS "
                    + "WHERE APP.CPT_TIERS.DESIGNATION = '" + Nom_client + "'  OR APP.CPT_TIERS.SIGLE_TIERS = '"
                    + Nom_client + "' OR APP.CPT_TIERS.NUM_AFFILIATION = '" + Nom_client + "' ")) {
                while (outResult77.next()) {
                    
                    String numTiers = outResult77.getString(1);
                    String numCompte = outResult77.getString(2);
                    String designationn = outResult77.getString(3);
                    String info = outResult77.getString(4);
                    String adresse = outResult77.getString(5);
                    String devise = outResult77.getString(6);
                    String sigle = outResult77.getString(7);
                    String numAffilie = outResult77.getString(8);
                    
                    int sal = outResult77.getInt(9);
                String ext = outResult77.getString(10);
                String email = outResult77.getString("EMAIL");
                 c = new Tiers(numTiers, numCompte, designationn, info, adresse,email, devise,
                        sigle, numAffilie,sal,ext);
                     
                }
            }

        } catch (SQLException e) {
            //System.out.println(" getTiersAff2. exception thrown:" + e);
            insertError(e + "", " V getAlltiers");
        }

        return c;

    }

    //88888888888888888888888888888888888888888888888888888888888888
    public static String setVirgule(double frw, int NEG, double arr, String separ) {

        String sep2 = ".";

        if (separ.equals(".")) {
            sep2 = ",";
        }
//        System.out.println("frw:"+frw);
        if (arr == 0.005) {
            frw = frw + 0.00500000000;
        }
//        System.out.println("frw arre:"+frw);
        BigDecimal big = new BigDecimal(frw);
//        big = big.setScale(2, BigDecimal.ROUND_UP);
        String getBigString = big.toString();
//        System.out.println("big:"+getBigString);
        getBigString = getBigString.replace('.', ';');
//  System.out.println("after;"+getBigString);
        String[] sp = getBigString.split(";");

        String decimal = "";
        if (sp.length > 1) {
            decimal = sp[1];
        }

        if (decimal.equals("") || (decimal.length() == 1 && decimal.equals("0"))) {
            decimal = sep2 + "00";
        } else if (decimal.length() == 1 && !decimal.equals("0")) {
            decimal = sep2 + "" + decimal + "0";
        } else {
            decimal = sep2 + "" + decimal.substring(0, 2);
        }


        String toret = setVirgule(sp[0], separ) + decimal;

        if (NEG == -1 && frw > 0) {
            toret = "-" + toret;
        }

        return toret;
    }

    public static String setVirgule(String frw, String separateur) {
        String setString = "";
        int k = 0;
        for (int i = (frw.length() - 1); i >= 0; i--) {
            k++;
            if ((k - 1) % 3 == 0 && (k - 1) != 0) {
                setString += "" + separateur;
            }
            setString += frw.charAt(i);
        }
        return inverse(setString);
    }
    
    
    
    String getFirstInvoice(String designation, Statement s) {

        String transitaire = "";

        try {
 String sql="select min(heure) from APP.INVOICE where NAME_CLIENT='"+designation+"'";
           // System.out.println(sql);
         ResultSet   outResult21 = s.executeQuery(sql);
          
            while (outResult21.next()) { 
               return ""+ outResult21.getTimestamp(1); 
            }
            

        } catch (SQLException e) {
            insertError(conn, e + "", "getTransitaire");
            //System.out.println("getTransitaire   ....... " + e);
        }

        return transitaire;

    }
    
    static public String TodayTime() {

        Date toDay = new Date(); 

        int y = (2000 + (toDay.getYear() - 100));//garantie de un millenaire, a changer muri 3000 
        String year = "" + y;
        String mounth;
        String day;
        if ((toDay.getMonth() + 1) < 10) {
            mounth = "0" + (toDay.getMonth() + 1);
        } else {
            mounth = "" + (toDay.getMonth() + 1);
        }
        
        if ((toDay.getDate() + 1) < 10) {
            day = "0" + (toDay.getDate() + 1);
        } else {
            day = "" + (toDay.getDate() + 1);
        }
        return year + "-" + mounth + "-" + day;

    }
     
   
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 public LinkedList<Compte> compteAmortissement(String val) { 
        LinkedList<Compte> compteamortList = new LinkedList();
        try {
            try (ResultSet outResult = state.executeQuery("select NUM_COMPTE,NOM_COMPTE,CLASSE,CPTE_PARENT,CATEGORY from APP.CPT_COMPTE WHERE"
                    + " NUM_COMPTE LIKE '"+val+"%' ")) {
                while (outResult.next()) {
                    compteamortList.add(new Compte(outResult.getString(1),
                            outResult.getString(2),outResult.getInt(3),outResult.getString(4),
                            outResult.getString(5)));
                }
                //  Close the resultSet
            }

        } catch (SQLException e) {

            insertError(e + "", " clients");
        }
         
        return compteamortList;

    }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
 
 public String l(String lang, String var) {
        String value = "";
        try {
           ResultSet outResult = state.executeQuery("  select  * from APP.LANGUE where variable= '" + var + "'");
                         // System.out.println("  select  * from APP.LANGUE where variable= '"+var+"'");
            while (outResult.next()) {
                value = outResult.getString(lang);
// System.out.println(value);
            }
        } catch (SQLException e) {
            System.err.println(var);
            insertError(conn,e + "", "langue");
        }
        if (value == null || value.equals("")) {
            System.err.println(" ????????????????      " + var);
        }

        return value;
    }

 
public static boolean checkIfComptabilise(String date_doc, String libelle,Statement state) { 
        try {

 String sql= "select * FROM APP.CPT_JOURNAL WHERE LIBELLE = '"+libelle+"' "
           + " AND DATE_DOC = '"+date_doc+"'" ;          
System.out.println(sql );             
ResultSet outResult = state.executeQuery(sql); 
            while (outResult.next()) { 
                return  true;
            }
        } catch (SQLException ex) { System.err.println("revientBefore  "+ex);}
        return false;
    }

 public static void write(String ref, String desi) {
        try {
            Date date=new Date();
            String file = ref + "/" + date.getTime() + ".txt";
            File f = new File(file);
            try (PrintWriter sorti = new PrintWriter(new FileWriter(f))) {
                sorti.println(desi);
                // JOptionPane.showMessageDialog(null, "  " + file, " Success ", JOptionPane.WARNING_MESSAGE);
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "  " + ex, " CONTACT ISHYIGA TEAM ", JOptionPane.WARNING_MESSAGE);    //FUNGA FATAL ERROR
        }
    }


public static boolean errorHandler(String errorMsg, String action,int optionIcon)
{
    
    JOptionPane.showMessageDialog(null,errorMsg,action,optionIcon); 
    
    switch (action) {
        case "EXIT":
            JOptionPane.showMessageDialog(null,"EXITING IMPORT \n"+errorMsg);
            DbHandler.write("ERROR", errorMsg);
            System.exit(1);
        case "BREAK":
        {   JOptionPane.showMessageDialog(null,"BREAKING IMPORT \n"+errorMsg);
        DbHandler.write("ERROR", errorMsg);
        int n = JOptionPane.showConfirmDialog(null, "BREAK LOOP ?",
                "CHOOSE?", JOptionPane.YES_NO_OPTION);
        return n != 0;
        }
        case "ASK":
        {  int n = JOptionPane.showConfirmDialog(null, "EXIT ?",
                "CHOOSE?", JOptionPane.YES_NO_OPTION);
        
        if (n == 0) {
            JOptionPane.showMessageDialog(null,"EXITING IMPORT \n"+errorMsg);
            DbHandler.write("ERROR", errorMsg);
            System.exit(1);
        }
        else
        {
            DbHandler.write("ERROR", errorMsg);
            return true;
        }
        } 
        default:
            break;
    }
    return false;
}

public static boolean isPackageImport(Statement state)
{   
try
{  
  
String sql=" select * from APP.BLT ";
 System.out.println(sql);
ResultSet outResult = state.executeQuery(sql); 

while (outResult.next())
{ 
    
 
}
return true;
}  
catch (SQLException e) 
{return false;
}

}


public static Compte getBillingClient
 (String numTiers, String start, double monthly_rent ,String status,Statement state) {

String FIN=DbHandler.EndTime()+" 23:59:00";
String journal = "41"; 
try {  
if (   numTiers != null )
{   
String  toSearch = "select sum(MONTANT_DEBIT),sum(MONTANT_CREDIT) from APP.CPT_JOURNAL where " + " CPT_JOURNAL.NUM_TIERS ='"+  numTiers + "'  "
+ " and postdater=0 AND ETAT <> 'DELETED'   "+Main.searchPeriodePublic(start,DbHandler.EndTime())+" "
+ " AND (CPT_JOURNAL.COMPTE_CREDIT like '"+journal+"%' OR CPT_JOURNAL.COMPTE_DEBIT like '"+journal+"%') ";

System.out.println(toSearch); 
ResultSet outResult = state.executeQuery(toSearch); 
Compte ce =null;
while (outResult.next()) { 
double montantdebit = outResult.getDouble(1);
double montantcredit = outResult.getDouble(2); 
if (!(montantdebit == 0 && montantcredit == 0)) { 
  ce = new Compte( numTiers, "", 0, "", "", montantdebit, montantcredit);
 }
}
if(ce!=null)
{  
    
 
   
if(status!=null && status.equals("RECURRENT"))
{ce.nombreDemois =   TiersAnalytics.nb_joursBetweenToDate2(start+" 00:00:00",DbHandler.EndTime()+" 23:59:00")/30;
ce.moisInvoiced=(int)((ce.montantInvoice-ce.montantRefund)/monthly_rent);
ce.moisCompta  =(int)(ce.montantDebit  /monthly_rent);
ce.moisPayed   =(int)(ce.montantCredit /monthly_rent);
ce.moisToBill  =ce.nombreDemois -ce.moisInvoiced;
ce.moisToPay   =ce.nombreDemois -ce.moisPayed;
ce.toPay =ce.moisToPay*monthly_rent;}
else
{
  ce.nombreDemois =1;
  
  ce.toPay = ce.soldeOpen+ce.montantDebit-ce.montantCredit;
} 

return (ce); 

}
                    
} 
} catch (SQLException e) {
System.out.println(numTiers+" . . method : getBillingCpte(). exception thrown: " + e);
//insertError(e + "", " getBillingCpte  ");
}
//        return getCpte;
return null;
}


public void  salaire_prep(String numtiers,LinkedList<cpt_lacharge> gutya,LinkedList<cpt_lacharge> rapport,LinkedList<Lot> ayangaya, 
           String designation,double taxable,double salaireBrute,String email, String address,String devise, String sigletiers,int bb, String mounth) {

       double solde = 0.0;
                               double net=0.0;
                               double tpr = 0.0,tpr2=0.0;
                               double Total_charge=0.0;
                               double transport=0.0,other=0.0;
                               double housing=0.0;
                               double rssb=0.0;
                               double pension_c=0.0;
                               double pension_e=0.0;
                               double materni_c=0.0;
                               double materni_e=0.0;
                               double rssb_comp=0.0;
                               double rssb_emp=0.0;
                               double tot_p=0.0;
                               double tot_m=0.0;
                               double rra=0.0;
                               double medical=0.0;
                               double medical2=0.0;
                               double restauration=0.0;
                               double solidarite=0.0;
                               double indemnite=0.0;
                               double primee=0.0;
                               double farg=0.0;
                              
                    
//gutya.add(new cpt_lacharge(designation,"" , "","","", 0, 0,numtiers,""));
                               
try{
    
    
    
         ResultSet outResult = state.executeQuery("select * from APP.CPT_EMPLOYEE_CHARGE,APP.CPT_CHARGE,APP.CPT_COMPTE"
                  + " where CPT_EMPLOYEE_CHARGE.CODE_CHARGE=CPT_CHARGE.CODE and CPT_CHARGE.COMPTE_DEBITER=CPT_COMPTE.NUM_COMPTE and CPT_EMPLOYEE_CHARGE.TIER_NUMBER ='"+numtiers+"'");
            
          
            while (outResult.next()) {                                 
           
           int cde=outResult.getInt("CODE");
           String name_cpt=outResult.getString("NOM_COMPTE");
           String COMPTE_DEBITER=outResult.getString("COMPTE_DEBITER"); 
           String COMPTE_CREDITER=outResult.getString("COMPTE_CREDITER"); 
           String name_CRD=outResult.getString("CRED_NAME");
           String type=outResult.getString("TYPE");
           String pay=outResult.getString("PAYABLE");
           double value=outResult.getDouble("AMOUNT");
           String name=outResult.getString("NAME");
         
         if(COMPTE_CREDITER.equals("42")&& pay.equalsIgnoreCase("company"))
         {
            
             if(type.equalsIgnoreCase("PERCENTAGE"))
             {
             value=salaireBrute*value/100;
             }
             taxable=taxable+value;
             System.out.println(" "+name+":" +value );
             
             gutya.add(new cpt_lacharge("", COMPTE_DEBITER,"",name_cpt,"", value,0.0,numtiers,mounth+"-"+name));
             gutya.add(new cpt_lacharge("", "",COMPTE_CREDITER,"",name_CRD,0.0, value,numtiers,mounth+"-"+name));
         
            if(name.toUpperCase().contains("TRANSPORT"))
             {
             
             transport=transport+value;
             }
            
            if(name.toUpperCase().contains("PRIME"))
             {
             
             primee=primee+value;
             }
            
           else if(name.toUpperCase().contains("LOGEMENT")|| name.toUpperCase().contains("HOUSING"))
             {
             
             housing=housing+value;
             }
           else if(name.toUpperCase().contains("INDEMNITE")|| name.toUpperCase().contains("INDEMNITY"))
             {
             
             indemnite=indemnite+value;
             }
            
            else
           {
           other=other+value;
           }
        
         }
           
           else
           {
           taxable=taxable+0.0;
           }
           
           
           }
            System.out.println(" Total brute taxable " + taxable);
            
           
           if (taxable <= 30000) {
           tpr = 0;
           } else if (taxable >= 30001 && taxable <= 70000) {
           tpr = 14000;
           } else if (taxable >= 70001) {
           tpr = ((taxable - 100000) * 30 / 100.0) + 14000;
           }
 
           
           rssb=taxable-transport;
    
          ResultSet rss=state.executeQuery("select * from APP.CPT_EMPLOYEE_CHARGE,APP.CPT_CHARGE,APP.CPT_COMPTE"
                  + " where CPT_EMPLOYEE_CHARGE.CODE_CHARGE=CPT_CHARGE.CODE and CPT_CHARGE.COMPTE_DEBITER=CPT_COMPTE.NUM_COMPTE and CPT_EMPLOYEE_CHARGE.TIER_NUMBER ='"+numtiers+"'");       
           
           
           
           while (rss.next()) {                                 
           
           int cde=rss.getInt("CODE");
           String name_cpt=rss.getString("NOM_COMPTE");
           String COMPTE_DEBITER=rss.getString("COMPTE_DEBITER"); 
           String COMPTE_CREDITER=rss.getString("COMPTE_CREDITER"); 
           String name_CRD=rss.getString("CRED_NAME");
           String type=rss.getString("TYPE");
           String pay=rss.getString("PAYABLE");
           double value=rss.getDouble("AMOUNT");
           String name=rss.getString("NAME");
           
           
        if(name.toUpperCase().contains("TPR"))
         {
             
             
             
          System.out.println(" TPR charge " + tpr);
          gutya.add(new cpt_lacharge("", COMPTE_DEBITER,"",name_cpt,"", tpr,0.0,numtiers,mounth+"-"+name));
             gutya.add(new cpt_lacharge("", "",COMPTE_CREDITER,"",name_CRD,0.0, tpr,numtiers,mounth+"-"+name));
          Total_charge=Total_charge+tpr;
          
          rra=rra+tpr;
          
          tpr2=tpr;
          
          
         }
        
        else if(name.toUpperCase().contains("SOLIDARITE"))
         {
          rssb=taxable-transport;   
          solidarite=solidarite+value;   
          
          System.out.println(" Dore solidalite:"+solidarite);
          
          gutya.add(new cpt_lacharge("", COMPTE_DEBITER,"",name_cpt,"", solidarite,0.0,numtiers,mounth+"-"+name));
             gutya.add(new cpt_lacharge("", "",COMPTE_CREDITER,"",name_CRD,0.0, solidarite,numtiers,mounth+"-"+name));
          Total_charge=Total_charge+solidarite;
          if(type.equalsIgnoreCase("PERCENTAGE"))
             {
              
               solidarite=(rssb *value/100);
             }
          
         }
        
        else if(name.toUpperCase().contains("RESTAURATION"))
         {
             
          restauration=restauration+value;   
             
          gutya.add(new cpt_lacharge("", COMPTE_DEBITER,"",name_cpt,"", restauration,0.0,numtiers,mounth+"-"+name));
             gutya.add(new cpt_lacharge("", "",COMPTE_CREDITER,"",name_CRD,0.0, restauration,numtiers,mounth+"-"+name));
          Total_charge=Total_charge+restauration;
          
          
          if(type.equalsIgnoreCase("PERCENTAGE"))
             {
                
               restauration=(rssb *value/100);
             }
          
         }
        
        
        
       else if(name.toUpperCase().contains("RAMA")||name.toUpperCase().contains("MEDICAL")||name.toUpperCase().contains("MEDICAUX"))
         {
          
         medical=(taxable *value/100);
         
         
          if(!type.equalsIgnoreCase("PERCENTAGE"))
             {
           medical=value;
             } 
          gutya.add(new cpt_lacharge("", COMPTE_DEBITER,"",name_cpt,"",medical,0.0,numtiers,mounth+"-"+name));
             gutya.add(new cpt_lacharge("", "",COMPTE_CREDITER,"",name_CRD,0.0,medical,numtiers,mounth+"-"+name));
          if(pay.equalsIgnoreCase("employee"))
          {
         
          Total_charge=Total_charge+medical;
   
          }
          else
              
          {
                 
          Total_charge=Total_charge+0.0;
          medical2=medical2+medical;
     
          }   
          
        
         }
       
       
       
       else if(name.toUpperCase().contains("PENSION")||name.toUpperCase().contains("CSR"))
         {
             
         double  p=(rssb *value/100);
         if(value==5.0)
         {pension_c=p;}
         else if(value==3.0) {pension_e=p;}
         
         
         
          if(!type.equalsIgnoreCase("PERCENTAGE"))
             {
           p=value;
             } 
          gutya.add(new cpt_lacharge("", COMPTE_DEBITER,"",name_cpt,"",p,0.0,numtiers,mounth+"-"+name));
             gutya.add(new cpt_lacharge("", "",COMPTE_CREDITER,"",name_CRD,0.0,p,numtiers,mounth+"-"+name));
          if(pay.equalsIgnoreCase("employee"))
          {
          rssb_emp=rssb_emp+p;
          Total_charge=Total_charge+p;
          tot_p=tot_p+p;
          }
          else
              
          {
              
          rssb_comp=rssb_comp+p;    
          Total_charge=Total_charge+0.0;
          tot_p=tot_p+p;;
          }   
          
        
         }
       
       
        
        else if(name.toUpperCase().contains("MATERNITE")||name.toUpperCase().contains("MATERNITY"))
         {
            
         double  m=(rssb *value/100);
         if(value==0.3)
         {materni_c=m;
          materni_e=m;
         }
         
         
         if(!type.equalsIgnoreCase("PERCENTAGE"))
             {
           m=value;
             }
          gutya.add(new cpt_lacharge("", COMPTE_DEBITER,"",name_cpt,"", m,0.0,numtiers,mounth+"-"+name));
             gutya.add(new cpt_lacharge("", "",COMPTE_CREDITER,"",name_CRD,0.0,m,numtiers,mounth+"-"+name));
        
          if(pay.equalsIgnoreCase("employee"))
          {
          rssb_emp=rssb_emp+m;
          Total_charge=Total_charge+m;
          tot_m=tot_m+m;
          }
          else
              
          {
              
          rssb_comp=rssb_comp+m;    
          Total_charge=Total_charge+0.0;
          tot_m=tot_m+m;
          }   
          
          
         }
        
        else if(name.toUpperCase().contains("FARG"))
         {
             
         farg=farg+(salaireBrute *value/100);
        
          gutya.add(new cpt_lacharge("", COMPTE_DEBITER,"",name_cpt,"", farg,0.0,numtiers,mounth+"-"+name));
             gutya.add(new cpt_lacharge("", "",COMPTE_CREDITER,"",name_CRD,0.0,farg,numtiers,mounth+"-"+name));
        
          
          Total_charge=Total_charge+farg;
         
         }
         
           
    
   }
          
           ayangaya.add(new Lot(Integer.parseInt(numtiers),"", designation, 
            email, address,devise, sigletiers, salaireBrute, 0, Total_charge, 0,0,net));
           
           net=taxable-Total_charge;
           
           
           
           double retenu=getPayement(numtiers,mounth).retenu;
            double avance=getPayement(numtiers,mounth).avance;
            double prime=getPayement(numtiers,mounth).prime;
            
            
            double netapayer=net+prime-retenu-avance;
        
           gutya.add(new cpt_lacharge("","64","","CHARGES DE PERSONNEL","", netapayer, 0.0,numtiers,mounth+"-NET SALARY"));
           gutya.add(new cpt_lacharge("","","42","","TIERS PERSONNEL", 0.0, netapayer,numtiers,mounth+"-"+"NET SALARY"));
            
            double gross=salaireBrute+transport+housing+primee;
            
         double med=medical+medical2;
        
           insertPreparation(numtiers, net,tot_p,rra,mounth,med,tot_m);
            
            
            
            rapport.add(new cpt_lacharge( designation, salaireBrute,housing,transport,prime,indemnite,gross,rssb,farg,pension_e,pension_c,materni_e,materni_c,tpr2,retenu,net,medical,medical2,avance,restauration,solidarite,netapayer) );
          
            //String messa="<html><body><table><tr><th><b>SALAIRE PREPARATION<br><hr>"+designation+"<br><hr> MONTH"+mounth+"</b></th><tr></tr></tr></table></body></html>";
            
            String message="<html><table BORDER=2 CELLPADDING=2 CELLSPACING=2  > <TR BGCOLOR=#FFC0CB ><th colspan=2> "
                    + "<b> SALAIRE PREPARATION <br><hr> "+designation+" </b>   <br><hr>MONTH: "+mounth+" </Th>\n" +
                    "</TR> <TR><TD> BASIC SALARY:</TD><TD>"+salaireBrute+"</TD></TR><TR><TD> PRIME ET COMMISSION:</TD><TD>"+prime+"</TD></TR><TR>"
                    + "<TD> TOTAL CHARGES:</TD><TD>"+Total_charge+"</TD></TR><TR><TD> RETENUE DE COMPTE:</TD><TD>"+retenu+"</TD></TR><TR><TD> AVANCE:</TD><TD>"+avance+"</TD></TR>"
                    + "<TR>"
                    + "<TD  BGCOLOR=#FFC0CB colspan=2><B> NET A PAYER:"+netapayer+"</B></TD></TR></table></html>";
            
            new  SendEmail(email, "Salaire Preparation",message,dbName,"ISHYIGA_Comptabilite");
            
            insertSentmails(designation,mounth,salaireBrute,prime,Total_charge,retenu,netapayer,avance);
            
         Total_charge=0.0;
          
                              tpr = 0.0;
                              
                             transport=0.0;
                             rssb=0.0;
           
                                
    } 
    
    
    catch (SQLException | NumberFormatException e) {
    
                        System.out.println("1-byakwamye kabisa: " + e);

                        JOptionPane.showMessageDialog(null, "" + e, " Error ", JOptionPane.ERROR_MESSAGE);
                    } 
   
   
   }
   


public void calcul(String numtiers,double salaireBrute,String mounth)
{

}

public void  salaire_prep2(String numtiers,LinkedList<cpt_lacharge> gutya,LinkedList<cpt_lacharge> rapport,LinkedList<Lot> ayangaya, 
           String designation,double taxable,double salaireBrute,String email, String address,String devise, String sigletiers,int bb, String mounth) {

       double solde = 0.0;
                               double net=0.0;
                               double tpr = 0.0,tpr2=0.0;
                               double Total_charge=0.0;
                               double transport=0.0,other=0.0;
                               double housing=0.0;
                               double rssb=0.0;
                               double pension_c=0.0;
                               double pension_e=0.0;
                               double materni_c=0.0;
                               double materni_e=0.0;
                               double rssb_comp=0.0;
                               double rssb_emp=0.0;
                               double tot_p=0.0;
                               double tot_m=0.0;
                               double rra=0.0;
                               double medical=0.0;
                               double medical2=0.0;
                               double restauration=0.0;
                               double solidarite=0.0;
                               double indemnite=0.0;
                               double primee=0.0;
                               double farg=0.0;
                              
                    
//gutya.add(new cpt_lacharge(designation,"" , "","","", 0, 0,numtiers,""));
                               
try{
    
    
    
         ResultSet outResult = state.executeQuery("select * from APP.CPT_EMPLOYEE_CHARGE,APP.CPT_CHARGE,APP.CPT_COMPTE"
                  + " where CPT_EMPLOYEE_CHARGE.CODE_CHARGE=CPT_CHARGE.CODE and CPT_CHARGE.COMPTE_DEBITER=CPT_COMPTE.NUM_COMPTE and CPT_EMPLOYEE_CHARGE.TIER_NUMBER ='"+numtiers+"'");
            
          
            while (outResult.next()) {                                 
           
           int cde=outResult.getInt("CODE");
           String name_cpt=outResult.getString("NOM_COMPTE");
           String COMPTE_DEBITER=outResult.getString("COMPTE_DEBITER"); 
           String COMPTE_CREDITER=outResult.getString("COMPTE_CREDITER"); 
           String name_CRD=outResult.getString("CRED_NAME");
           String type=outResult.getString("TYPE");
           String pay=outResult.getString("PAYABLE");
           double value=outResult.getDouble("AMOUNT");
           String name=outResult.getString("NAME");
         
         if(COMPTE_CREDITER.equals("42")&& pay.equalsIgnoreCase("company"))
         {
            
             if(type.equalsIgnoreCase("PERCENTAGE"))
             {
             value=salaireBrute*value/100;
             }
             taxable=taxable+value;
             System.out.println(" "+name+":" +value );
             
             //gutya.add(new cpt_lacharge("", COMPTE_DEBITER,"",name_cpt,"", value,0.0,numtiers,mounth+"-"+name));
             //gutya.add(new cpt_lacharge("", "",COMPTE_CREDITER,"",name_CRD,0.0, value,numtiers,mounth+"-"+name));
         
            if(name.toUpperCase().contains("TRANSPORT"))
             {
             
             transport=transport+value;
             }
            
            if(name.toUpperCase().contains("PRIME"))
             {
             
             primee=primee+value;
             }
            
           else if(name.toUpperCase().contains("LOGEMENT")|| name.toUpperCase().contains("HOUSING"))
             {
             
             housing=housing+value;
             }
           else if(name.toUpperCase().contains("INDEMNITE")|| name.toUpperCase().contains("INDEMNITY"))
             {
             
             indemnite=indemnite+value;
             }
            
            else
           {
           other=other+value;
           }
        
         }
           
           else
           {
           taxable=taxable+0.0;
           }
           
           
           }
            System.out.println(" Total brute taxable " + taxable);
            
           
           if (taxable <= 30000) {
           tpr = 0;
           } else if (taxable >= 30001 && taxable <= 70000) {
           tpr = 14000;
           } else if (taxable >= 70001) {
           tpr = ((taxable - 100000) * 30 / 100.0) + 14000;
           }
 
           
           rssb=taxable-transport;
    
          ResultSet rss=state.executeQuery("select * from APP.CPT_EMPLOYEE_CHARGE,APP.CPT_CHARGE,APP.CPT_COMPTE"
                  + " where CPT_EMPLOYEE_CHARGE.CODE_CHARGE=CPT_CHARGE.CODE and CPT_CHARGE.COMPTE_DEBITER=CPT_COMPTE.NUM_COMPTE and CPT_EMPLOYEE_CHARGE.TIER_NUMBER ='"+numtiers+"'");       
           
           
           
           while (rss.next()) {                                 
           
           int cde=rss.getInt("CODE");
           String name_cpt=rss.getString("NOM_COMPTE");
           String COMPTE_DEBITER=rss.getString("COMPTE_DEBITER"); 
           String COMPTE_CREDITER=rss.getString("COMPTE_CREDITER"); 
           String name_CRD=rss.getString("CRED_NAME");
           String type=rss.getString("TYPE");
           String pay=rss.getString("PAYABLE");
           double value=rss.getDouble("AMOUNT");
           String name=rss.getString("NAME");
           
           
        if(name.toUpperCase().contains("TPR"))
         {
             
             
             
         // System.out.println(" TPR charge " + tpr);
          //gutya.add(new cpt_lacharge("", COMPTE_DEBITER,"",name_cpt,"", tpr,0.0,numtiers,mounth+"-"+name));
             //gutya.add(new cpt_lacharge("", "",COMPTE_CREDITER,"",name_CRD,0.0, tpr,numtiers,mounth+"-"+name));
          Total_charge=Total_charge+tpr;
          
          rra=rra+tpr;
          
          tpr2=tpr;
          
          
         }
        /*
        else if(name.toUpperCase().contains("SOLIDARITE"))
         {
          rssb=taxable-transport;   
          solidarite=solidarite+value;   
          
          System.out.println(" Dore solidalite:"+solidarite);
          
          gutya.add(new cpt_lacharge("", COMPTE_DEBITER,"",name_cpt,"", solidarite,0.0,numtiers,mounth+"-"+name));
             gutya.add(new cpt_lacharge("", "",COMPTE_CREDITER,"",name_CRD,0.0, solidarite,numtiers,mounth+"-"+name));
          Total_charge=Total_charge+solidarite;
          if(type.equalsIgnoreCase("PERCENTAGE"))
             {
              
               solidarite=(rssb *value/100);
             }
          
         }
        
        else if(name.toUpperCase().contains("RESTAURATION"))
         {
             
          restauration=restauration+value;   
             
          gutya.add(new cpt_lacharge("", COMPTE_DEBITER,"",name_cpt,"", restauration,0.0,numtiers,mounth+"-"+name));
             gutya.add(new cpt_lacharge("", "",COMPTE_CREDITER,"",name_CRD,0.0, restauration,numtiers,mounth+"-"+name));
          Total_charge=Total_charge+restauration;
          
          
          if(type.equalsIgnoreCase("PERCENTAGE"))
             {
                
               restauration=(rssb *value/100);
             }
          
         }*/
        
        
        
       else if(name.toUpperCase().contains("RAMA")||name.toUpperCase().contains("MEDICAL")||name.toUpperCase().contains("MEDICAUX"))
         {
          
         medical=(taxable *value/100);
         
         
          if(!type.equalsIgnoreCase("PERCENTAGE"))
             {
           medical=value;
             } 
          //gutya.add(new cpt_lacharge("", COMPTE_DEBITER,"",name_cpt,"",medical,0.0,numtiers,mounth+"-"+name));
             //gutya.add(new cpt_lacharge("", "",COMPTE_CREDITER,"",name_CRD,0.0,medical,numtiers,mounth+"-"+name));
          if(pay.equalsIgnoreCase("employee"))
          {
         
          Total_charge=Total_charge+medical;
   
          }
          else
              
          {
                 
          Total_charge=Total_charge+0.0;
          medical2=medical2+medical;
     
          }   
          
        
         }
       
       
       
       else if(name.toUpperCase().contains("PENSION")||name.toUpperCase().contains("CSR"))
         {
             
         double  p=(rssb *value/100);
         if(value==5.0)
         {pension_c=p;}
         else if(value==3.0) {pension_e=p;}
         
         
         
          if(!type.equalsIgnoreCase("PERCENTAGE"))
             {
           p=value;
             } 
          gutya.add(new cpt_lacharge("", COMPTE_DEBITER,"",name_cpt,"",p,0.0,numtiers,mounth+"-"+name));
             gutya.add(new cpt_lacharge("", "",COMPTE_CREDITER,"",name_CRD,0.0,p,numtiers,mounth+"-"+name));
          if(pay.equalsIgnoreCase("employee"))
          {
          rssb_emp=rssb_emp+p;
          Total_charge=Total_charge+p;
          tot_p=tot_p+p;
          }
          else
              
          {
              
          rssb_comp=rssb_comp+p;    
          Total_charge=Total_charge+0.0;
          tot_p=tot_p+p;;
          }   
          
        
         }
       
       
        
        else if(name.toUpperCase().contains("MATERNITE")||name.toUpperCase().contains("MATERNITY"))
         {
            
         double  m=(rssb *value/100);
         if(value==0.3)
         {materni_c=m;
          materni_e=m;
         }
         
         
         if(!type.equalsIgnoreCase("PERCENTAGE"))
             {
           m=value;
             }
          gutya.add(new cpt_lacharge("", COMPTE_DEBITER,"",name_cpt,"", m,0.0,numtiers,mounth+"-"+name));
             gutya.add(new cpt_lacharge("", "",COMPTE_CREDITER,"",name_CRD,0.0,m,numtiers,mounth+"-"+name));
        
          if(pay.equalsIgnoreCase("employee"))
          {
          rssb_emp=rssb_emp+m;
          Total_charge=Total_charge+m;
          tot_m=tot_m+m;
          }
          else
              
          {
              
          rssb_comp=rssb_comp+m;    
          Total_charge=Total_charge+0.0;
          tot_m=tot_m+m;
          }   
          
          
         }
        
       /* else if(name.toUpperCase().contains("FARG"))
         {
             
         farg=farg+(salaireBrute *value/100);
        
          gutya.add(new cpt_lacharge("", COMPTE_DEBITER,"",name_cpt,"", farg,0.0,numtiers,mounth+"-"+name));
             gutya.add(new cpt_lacharge("", "",COMPTE_CREDITER,"",name_CRD,0.0,farg,numtiers,mounth+"-"+name));
        
          
          Total_charge=Total_charge+farg;
         
         }*/
         
           
    
   }
          
           ayangaya.add(new Lot(Integer.parseInt(numtiers),"", designation, 
            email, address,devise, sigletiers, salaireBrute, 0, Total_charge, 0,0,net));
           
           net=taxable-Total_charge;
           
           
           
           double retenu=getPayement(numtiers,mounth).retenu;
            double avance=getPayement(numtiers,mounth).avance;
            double prime=getPayement(numtiers,mounth).prime;
            
            
            double netapayer=net+prime-retenu-avance;
        
           gutya.add(new cpt_lacharge("","64","","CHARGES DE PERSONNEL","", netapayer, 0.0,numtiers,mounth+"-NET SALARY"));
           gutya.add(new cpt_lacharge("","","42","","TIERS PERSONNEL", 0.0, netapayer,numtiers,mounth+"-"+"NET SALARY"));
            
            double gross=salaireBrute+transport+housing+primee;
            
         double med=medical+medical2;
        
           insertPreparation(numtiers, net,tot_p,rra,mounth,med,tot_m);
            
            
            
            rapport.add(new cpt_lacharge( designation, salaireBrute,housing,transport,prime,indemnite,gross,rssb,farg,pension_e,pension_c,materni_e,materni_c,tpr2,retenu,net,medical,medical2,avance,restauration,solidarite,netapayer) );
          
            //String messa="<html><body><table><tr><th><b>SALAIRE PREPARATION<br><hr>"+designation+"<br><hr> MONTH"+mounth+"</b></th><tr></tr></tr></table></body></html>";
            
            String message="<html><table BORDER=2 CELLPADDING=2 CELLSPACING=2  > <TR BGCOLOR=#FFC0CB ><th colspan=2> "
                    + "<b> SALAIRE PREPARATION <br><hr> "+designation+" </b>   <br><hr>MONTH: "+mounth+" </Th>\n" +
                    "</TR> <TR><TD> BASIC SALARY:</TD><TD>"+salaireBrute+"</TD></TR><TR><TD> PRIME ET COMMISSION:</TD><TD>"+prime+"</TD></TR><TR>"
                    + "<TD> TOTAL CHARGES:</TD><TD>"+Total_charge+"</TD></TR><TR><TD> RETENUE DE COMPTE:</TD><TD>"+retenu+"</TD></TR><TR><TD> AVANCE:</TD><TD>"+avance+"</TD></TR>"
                    + "<TR>"
                    + "<TD  BGCOLOR=#FFC0CB colspan=2><B> NET A PAYER:"+netapayer+"</B></TD></TR></table></html>";
            
            new  SendEmail(email, "Salaire Preparation",message,dbName,"ISHYIGA_Comptabilite");
            
            insertSentmails(designation,mounth,salaireBrute,prime,Total_charge,retenu,netapayer,avance);
            
         Total_charge=0.0;
          
                              tpr = 0.0;
                              
                             transport=0.0;
                             rssb=0.0;
           
                                
    } 
    
    
    catch (SQLException | NumberFormatException e) {
    
                        System.out.println("1-byakwamye kabisa: " + e);

                        JOptionPane.showMessageDialog(null, "" + e, " Error ", JOptionPane.ERROR_MESSAGE);
                    } 
   
   
   }

   
   
   void insertCharge(String cmptd, String cmpt_name, String type,
            String pay, String cpt_name_c, String name) {

        try {

            psInsert = conn3.prepareStatement("INSERT INTO APP.CPT_CHARGE (COMPTE_DEBITER,COMPTE_CREDITER,TYPE,PAYABLE,CRED_NAME,NAME) VALUES(?,?,?,?,?,?)");
//

            psInsert.setString(1,cmptd);
                psInsert.setString(2,cmpt_name);
                psInsert.setString(3,type);
               psInsert.setString(4,pay);
                psInsert.setString(5,cpt_name_c);
                psInsert.setString(6,name);

            psInsert.executeUpdate();
            psInsert.close();
            JOptionPane.showMessageDialog(null,"Charge added Successfully");
            
            System.out.println("Inserted Successfully" );

        } catch (NumberFormatException | SQLException ex) {
            System.out.println("error in insertCharge()  :  " + ex);
        }
   }

void updateCharge(String typ, int code, String cpt_c, String cpt_d,String payable,String name)
        
{
try {
st =conn.createStatement();
           st.executeUpdate("update APP.CPT_CHARGE set NAME='" +name + "', PAYABLE = '" + payable + "', TYPE = '" + typ + "',"
                   + " COMPTE_DEBITER='" + cpt_d + "', COMPTE_CREDITER='" + cpt_c + "' where  CODE ="+code);
                JOptionPane.showMessageDialog(null,"Updated Successfully");
                
      
             
            }

            catch (SQLException err) {
                System.out.println("FAIL"+err.getMessage());
            }  



}


void chargeslist(DefaultListModel listModel, JList chrglist)
           
{

    try
    {
      
        //name.setText((String) interf.employees.getSelectedValue());
        ResultSet outResult = state.executeQuery("select * from APP.CPT_CHARGE,APP.CPT_COMPTE where CPT_CHARGE.COMPTE_DEBITER=CPT_COMPTE.NUM_COMPTE");
            while(outResult.next())
              
            {
                
                int code=outResult.getInt("CODE");
                String name=outResult.getString("NAME");
                String compte=outResult.getString("COMPTE_DEBITER");
                String type=outResult.getString("TYPE");
                String payable=outResult.getString("PAYABLE");
                String NOM_COMPTE=outResult.getString("COMPTE_CREDITER");
                
           
          cpt_lacharge lacharge= new cpt_lacharge(code,name,compte,type,0.0,payable, NOM_COMPTE);
                

           listModel.addElement(lacharge);
           chrglist.setModel(listModel);
           
   
           }
    }
    catch(SQLException e)
                {
                  System.out.println("Fail"+e.getMessage());  
                }
    
}


void fillcombo2(JComboBox chargee)
{
    try
    {
        st =conn.createStatement();
          ResultSet rs =st.executeQuery("select * from APP.CPT_CHARGE ,APP.CPT_COMPTE where CPT_CHARGE.COMPTE_DEBITER=CPT_COMPTE.NUM_COMPTE");
            while(rs.next())
            {
                int code=rs.getInt("CODE");
                String name=rs.getString("NAME");
                String compte=rs.getString("COMPTE_DEBITER");
                String type=rs.getString("TYPE");
                String payable=rs.getString("PAYABLE");
                String NOM_COMPTE=rs.getString("COMPTE_CREDITER");
           
          cpt_lacharge lacharge= new cpt_lacharge(code,name,compte,type,0.0,payable,NOM_COMPTE);
           chargee.addItem(lacharge);
           
            
           }
    }
    catch(SQLException e)
                {
                  System.out.println("Fail"+e.getMessage());  
                }
}

void ToOne(String tie,int ct,double amount)
{


try {

                Statement stt;
                stt =conn.createStatement();
                ResultSet rss =stt.executeQuery("select * from CPT_EMPLOYEE_CHARGE where TIER_NUMBER = '" + tie + "' and CODE_CHARGE ="+ct);
                
                if(!rss.next())
                {
            String sql="INSERT INTO APP.CPT_EMPLOYEE_CHARGE VALUES(?,?,?)";
                pst=conn.prepareStatement(sql);
                
               
                pst.setString(1,tie);
                pst.setInt(2,ct);
                pst.setDouble(3,amount);
               pst.executeUpdate();
              JOptionPane.showMessageDialog(null," Added Successfully");  
            
           }
                else
                {
                JOptionPane.showMessageDialog(null, "CHARGE ALREADY APPLIED", "ERROR", JOptionPane.ERROR_MESSAGE);
                }
             
            }

            catch (SQLException err) {
                System.out.println("FAIL"+err.getMessage());
            }  


}

void ToAll(String numCompte, int ct,double amount ) {
    
    
    try {
                               
                               
                               
                               st =conn.createStatement();
        ResultSet   rs =st.executeQuery("select * from APP.CPT_TIERS where NUM_COMPTE = '" + numCompte + "' ORDER BY designation");
            while(rs.next())
                
            {
                String ti=rs.getString("NUM_TIERS");
                
                Statement stt;
                stt =conn.createStatement();
                ResultSet rss =stt.executeQuery("select * from CPT_EMPLOYEE_CHARGE where TIER_NUMBER = '" + ti + "' and CODE_CHARGE ="+ct);
                
                if(!rss.next())
                {
            String sql="INSERT INTO APP.CPT_EMPLOYEE_CHARGE VALUES(?,?,?)";
                pst=conn.prepareStatement(sql);
                
               
                pst.setString(1,ti);
                pst.setInt(2,ct);
                pst.setDouble(3,amount);
               pst.executeUpdate();
               
            
           }
                
             else
                {
                    JOptionPane.showMessageDialog(null, "CHARGE ALREADY APPLIED", "ERROR", JOptionPane.ERROR_MESSAGE);
                }
                
                
            }
            
            
            
          JOptionPane.showMessageDialog(null," Applied Successfully");                     
                               

               
             
            }

            catch (SQLException err) {
                System.out.println("Ntago byagiye ho"+err.getMessage());
            }  

    
}

void Applicable_charges( String ti,DefaultListModel listModel, JList chrglist )
{

try
    {
      
       
        st =conn.createStatement();
         ResultSet  rs =st.executeQuery("select * from APP.CPT_EMPLOYEE_CHARGE,APP.CPT_CHARGE where CODE=CODE_CHARGE and TIER_NUMBER='" + ti+ "' ");
            while(rs.next())
              
            {
                
                int code=rs.getInt("CODE");
                String name=rs.getString("NAME");
                String type=rs.getString("TYPE");
                String payable=rs.getString("PAYABLE");
                double valeur=rs.getDouble("AMOUNT");
                
                //System.out.println(" Dore Ibyinjiye"+name+"---"+valeur);
           cpt_lacharge lacharge= new cpt_lacharge(code,name,"",type,valeur,payable,"");    
           
          //cpt_lacharge lacharge= new cpt_lacharge(name,valeur);

           listModel.addElement(lacharge);
           chrglist.setModel(listModel);
           
           
           
           
           
           
            
           }
    }
    catch(SQLException e)
                {
                  System.out.println("Fail"+e.getMessage());  
                }
    

}
   

void remove_charge(String ti,int i)
{

try
    {
      
      
        st =conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                                  ResultSet.CONCUR_UPDATABLE);
           st.executeUpdate("DELETE from APP.CPT_EMPLOYEE_CHARGE where CPT_EMPLOYEE_CHARGE.TIER_NUMBER='" + ti + "'and CPT_EMPLOYEE_CHARGE.CODE_CHARGE= " +i);
    
           //rs.next();  
           System.out.println("YES DELETED");
           
    }
    catch(SQLException e)
                {
                  System.out.println("Not deleted"+e.getMessage());  
                }
                  
}



void delete_charge(int i)
{

try
    {
      
      
        st =conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                                  ResultSet.CONCUR_UPDATABLE);
           st.executeUpdate("DELETE from APP.CPT_CHARGE where CODE= " +i);
    
           //rs.next();  
           System.out.println("YES DELETED");
           
    }
    catch(SQLException e)
                {
                  System.out.println("Not deleted"+e.getMessage());  
                }
                  
}




cpt_lacharge getPayement( String numTiers, String month) {
        double avance=0.0;
        double retenu=0.0;
        double prime=0.0;
        Timestamp date=null;
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("YY");
        java.text.SimpleDateFormat mois = new java.text.SimpleDateFormat("MM");
        
        java.util.Date date1= new java.util.Date();
        
        Timestamp date2=new Timestamp(date1.getTime());
        
        try {
            
            
            Statement stmt = conn.createStatement();
    ResultSet rs = stmt.executeQuery("select * from APP.CPT_PAYEMENT  WHERE NUM_TIER='"+ numTiers+"' AND MONTH='"+ month+"'" );
      
            while(rs.next()) {
                date=rs.getTimestamp("DATE_PAY");
                
                int i=Integer.parseInt(sdf.format(date));
                int ii=Integer.parseInt(sdf.format(date2));
                
                int m1=Integer.parseInt(mois.format(date));
                int m2=Integer.parseInt(mois.format(date2));
                
                if((i==ii)||(i<ii && m2<m1))
                {
                    
                avance=avance+ rs.getDouble("AVANCE");
                retenu=retenu+rs.getDouble("RETENU");
                prime=prime+rs.getDouble("PRIME");
           }
                 
            }
            
            
            rs.close();
        } catch (SQLException ex) {
            System.out.println(" " + ex);
        }
       
        
       cpt_lacharge CE = new cpt_lacharge(avance,retenu,prime,date);
                return CE;
        
    }


int getSalaireBrute(String numTiers) {

        int sal_bru = 0;

        try {
           ResultSet tierResult = state.executeQuery("select APP.CPT_TIERS.SALAIRE_BRUTE from APP.CPT_TIERS where APP.CPT_TIERS.NUM_TIERS = '" + numTiers + "'");
            while (tierResult.next()) {
                sal_bru = tierResult.getInt(1);
            }
            tierResult.close();
        } catch (SQLException e) {
            insertError(e + "", " V getnumTiers");
            System.out.println("......ERROR IN getSalaireBrute(): " + e);
        }
        return sal_bru;
    }

LinkedList<Journaux> getJournaux() {
      LinkedList  Journaux = new LinkedList();
        try {
            try (ResultSet outResult = state.executeQuery("select * from APP.CPT_JOURNAUX where COMPTE_JOURNAL like '57%' OR COMPTE_JOURNAL like '56%' ")) {
                while (outResult.next()) {
                    Journaux.add(new Journaux(outResult.getString(1), outResult.getString(2),
                            outResult.getString(4), outResult.getString(3), outResult.getString(5), outResult.getDouble(6)));
                }
            }
        } catch (SQLException e) {
            System.out.println(".....cptJournaux " + e);
        }
        return Journaux;
    }



void updateCharge_employee(String ti, int code, double amount)
        
{
try {
st =conn.createStatement();
           st.executeUpdate("update APP.CPT_EMPLOYEE_CHARGE set AMOUNT = " + amount + " where TIER_NUMBER='" + ti + "' AND CODE_CHARGE ="+code);
                JOptionPane.showMessageDialog(null,"Updated Successfully");
           
             
            }

            catch (SQLException err) {
                System.out.println("FAIL"+err.getMessage());
            }  



}

cpt_lacharge getPayement2( String numTiers, String month,Timestamp date3) {
        double avance=0.0;
        double retenu=0.0;
        double prime=0.0;
        Timestamp date=null;
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("YY");
        java.text.SimpleDateFormat mois = new java.text.SimpleDateFormat("MM");
        
        java.util.Date date1= new java.util.Date();
        
        Timestamp date2=new Timestamp(date1.getTime());
        
        try {
            
            
            Statement stmt = conn.createStatement();
    ResultSet rs = stmt.executeQuery("select * from APP.CPT_PAYEMENT  WHERE NUM_TIER='"+ numTiers+"' AND MONTH='"+ month+"' AND DATE_PAY> '" + date3 + "'" );
      
            while(rs.next()) {
                date=rs.getTimestamp("DATE_PAY");
                
                int i=Integer.parseInt(sdf.format(date));
                int ii=Integer.parseInt(sdf.format(date2));
                
                int m1=Integer.parseInt(mois.format(date));
                int m2=Integer.parseInt(mois.format(date2));
                
                if((i==ii)||(i<ii && m2<m1))
                    
                {
                    
                avance=avance+ rs.getDouble("AVANCE");
                retenu=retenu+rs.getDouble("RETENU");
                prime=prime+rs.getDouble("PRIME");
           }
                 
            }
            
            
            rs.close();
        } catch (SQLException ex) {
            System.out.println(" MURI QUERRY WAPI:" + ex);
        }
        
        
       cpt_lacharge CE = new cpt_lacharge(avance,retenu,prime,date);
                return CE;
        
     
    }


void insertDynamicP(String employee, String deriverable, double amount,
            String month) {

        try {

            psInsert = conn3.prepareStatement("INSERT INTO APP.CPT_DYNAMIC_PREP (EMPLOYEE,DERIVERABLE,AMOUNT,MONTH,DATE_DOC) VALUES(?,?,?,?,?)");
//

            psInsert.setString(1,employee);
                psInsert.setString(2,deriverable);
                psInsert.setDouble(3,amount);
               psInsert.setString(4,month);
                psInsert.setString(5,dateToday);

            psInsert.executeUpdate();
            psInsert.close();
            JOptionPane.showMessageDialog(null,"Saved Successfully");
            
            System.out.println("Inserted Successfully" );

        } catch (NumberFormatException | SQLException ex) {
            System.out.println("error in insertCharge()  :  " + ex);
        }
   }



double GetDynamicSalaire(String month, String tier)
{
    Double dynamicS=0.0;
    try
    {
        st =conn.createStatement();
          ResultSet rs =st.executeQuery("select AMOUNT from CPT_DYNAMIC_PREP WHERE EMPLOYEE='"+ tier+"' AND MONTH='"+ month+"'");
            while(rs.next())
            {
                dynamicS = dynamicS+rs.getDouble("AMOUNT");
                
            
           }
            
            return dynamicS;
    }
    catch(SQLException e)
                {
                  System.out.println("Fail"+e.getMessage());  
                }
    
    return 0;
}



LinkedList<preparation> Dynamicprep(  String mois)
{
   LinkedList<preparation> prep= new LinkedList(); 
    
    try
    {
        st =conn.createStatement();
          ResultSet rs =st.executeQuery("select * from CPT_DYNAMIC_PREP where MONTH='"+mois+"' ");
            while(rs.next())
            {
               String employee= rs.getString("EMPLOYEE");
                String deriverable=rs.getString("DERIVERABLE");
                
                double amount= rs.getDouble("AMOUNT");
               String month= rs.getString("MONTH");
               String date= rs.getString("DATE_DOC");
               
         
               prep.add(new preparation(employee,deriverable,amount,month,date));
                
            
           }
        
    }
    catch(SQLException e)
                {
                  System.out.println("Fail"+e.getMessage());  
                }
    
  return prep;
}


LinkedList<preparation> Dynamicprep1(  String mois, String tier)
{
    
   LinkedList<preparation> prep= new LinkedList() ;
    try
    {
        st =conn.createStatement();
          ResultSet rs =st.executeQuery("select * from CPT_DYNAMIC_PREP where MONTH='"+mois+"' AND EMPLOYEE='"+tier+"' ");
            while(rs.next())
            {
               String employee= rs.getString("EMPLOYEE");
                String deriverable=rs.getString("DERIVERABLE");
                
                double amount= rs.getDouble("AMOUNT");
               String month= rs.getString("MONTH");
               String date= rs.getString("DATE_DOC");
               
         
               prep.add(new preparation(employee,deriverable,amount,month,date));
                
            
           }
        
    }
    catch(SQLException e)
                {
                  System.out.println("Fail"+e.getMessage());  
                }
    
  return prep;
}

void insertSentmails(String name, String month, double basic,double prime,double charge,double retenu, double net, double avance) {

        try {

            psInsert = conn3.prepareStatement("INSERT INTO APP.CPT_SENT_MAILS (NAME,MONTH,BASIC_SALAIRE,PRIME,CHARGES,RETENU,NET,AVANCE) VALUES(?,?,?,?,?,?,?,?)");
//

                psInsert.setString(1,name);
                psInsert.setString(2,month);
                psInsert.setDouble(3,basic);
                psInsert.setDouble(4,prime);
                psInsert.setDouble(5,charge);
                psInsert.setDouble(6,retenu);
                psInsert.setDouble(7,net);
                psInsert.setDouble(8,avance);
                psInsert.executeUpdate();
                psInsert.close();
            //JOptionPane.showMessageDialog(null,"Saved Successfully");
            
            System.out.println("Email inserted" );

        } catch (NumberFormatException | SQLException ex) {
            System.out.println("error in insertEmail()  :  " + ex);
        }
   }


public void sendAllMails(String month)
        
{
    
    String message1="";
    String message2="";
    
    double Totalnet=0;
    int no=1;
try
    {
        st =conn.createStatement();
          ResultSet outResult =st.executeQuery("select * from CPT_SENT_MAILS where MONTH='"+month+"' ");
            while(outResult.next())
            {
          Totalnet=Totalnet+ outResult.getDouble("NET"); 
          
               
          message2+=   "<TR  > "
           + "<TD> "+no+"  </TD> "
           + "<TD> "+outResult.getString("NAME")+"  </TD> "
           + "<TD> "+outResult.getDouble("BASIC_SALAIRE")+" </TD> "  
           + "<TD> "+outResult.getDouble("PRIME")+" </TD> " 
           + "<TD> "+outResult.getDouble("CHARGES")+" </TD> " 
           + "<TD> "+outResult.getDouble("RETENU")+" </TD> " 
           + "<TD> "+outResult.getDouble("AVANCE")+" </TD> " 
           + "<TD> "+ setVirgule(outResult.getDouble("NET"))+" </TD> "  
           + " </TR>";
          
          no=no+1;
     
           }
        
    }
    catch(SQLException e)
                {
                  System.out.println("Fail"+e.getMessage());  
                }
    
message1 = " <table BORDER=2 CELLPADDING=2 CELLSPACING=2  > "
        + "<TR BGCOLOR=#FFC0CB ><TH colspan=8> <b> SALAIRE PREPARATION FOR MONTH: "+month+" </b>   <br></TH></TR>" 
           +   " <TR BGCOLOR=#FFC0CB > "
           + "<TD> NO </TD> "
           + "<TD> NAME </TD> "
           + "<TD> BASIC SALARY  </TD> "
           + "<TD> PRIME </TD> "  
           + "<TD> CHARGES </TD> " 
           + "<TD> RETENU DE COMPTE </TD> "
           + "<TD> AVANCE </TD> "
           + "<TD> NET TO PAY </TD>  </TR> "
        + " "+message2 +" <TR><TH colspan=8 BGCOLOR=#FFC0CB> TOTAL AMOUNT TO PAY: "+ setVirgule(Totalnet)+" </TH></TR></table>" ;

LinkedList<String> authorised = empAutorise();

for(int i=0;i< authorised.size();i++)
{       

new  SendEmail(authorised.get(i), "Salaire Preparation",message1,dbName,"ISHYIGA_Comptabilite");

}
}

LinkedList<String> empAutorise() {
        LinkedList<String> AutorisedEmail = new LinkedList();
        try {
          
            st =conn.createStatement();
          ResultSet outResult =st.executeQuery("select email from APP.EMPLOYE where autorisation like '%EMAIL%' ");
            while (outResult.next()) {
                AutorisedEmail.add(outResult.getString(1));
            }
            outResult.close();
        } catch (Throwable e) {
            System.out.println(e +  "employe");
        }
        return AutorisedEmail;

    }



}
