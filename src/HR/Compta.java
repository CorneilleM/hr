/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package HR;


import java.sql.Date;

/**
 *
 * @author Kimenyi Mabilisi
 */
public class Compta {

    public String livre,libele,intitule,date,cpte,comptabilise,moyen,mois,numAffilie;
    public int compte,montant_cash,montant_credit,tva,valeur,doc;

    public Compta(String livre,String libele,String intitule,int compte,int montant_cash,int montant_credit,String date,String cpte,int tva,String numAffilie)
    {
    this.libele=libele;
    this.livre=livre;
    this.compte=compte;
    this.intitule=intitule;
    this.montant_cash=montant_cash;
    this.montant_credit=montant_credit;
    this.date=date;
    this.cpte=cpte;
    this.numAffilie=numAffilie;
    this.tva=tva;
    }
    
    public Compta(int doc,String date,int compte,String intitule,String libele,int cash,int credit,int tva,int valeur,String cptb,String moyen,String mois)
            
    {
        this.doc=doc;
        this.date=date;
        this.compte=compte;
        this.intitule=intitule;
        this.libele=libele;
        this.montant_cash=cash;
        this.montant_credit=credit;
        this.tva=tva;
        this.valeur=valeur;
        this.comptabilise=cptb;
        this.moyen=moyen;
        this.mois=mois;
    }
    public Compta(String intitule)
    {
        this.intitule=intitule;        
    }
    
    @Override
    public String toString()
    {
        return intitule;
        
    }
}
