/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package HR;

import java.awt.*;
import java.util.LinkedList;
import java.util.Properties;
import java.util.StringTokenizer; 
import javax.swing.JFrame;

/**
 *
 * @author Kimenyi
 */
public class Print extends JFrame {

LinkedList <Lot> lotList;
int pageNum = 0;
private Properties properties = new Properties();
int margin=0;
int numero;
double total,tva;
Tiers frss;
String date,reference,mode;
String [] footer;
DbHandler db;
public Print(LinkedList <Lot> lotList,DbHandler db,String mode ,String date,Tiers frss,int numero,int tva,double total,String [] footer)
{
//    System.out.println("wewe   "+footer[1]);
this.lotList=lotList;
this.db=db;
this.mode=mode;
this.margin=getValue("margin");
this.date=date;
this.frss=frss;
this.numero=numero;
this.tva=tva;
this.total=total;
this.footer=footer;
PrintCommand();

}
private void PrintCommand()
{
      String facture="";

 
    //facture=getString("factureV") + (numero);
    facture="";
    PrintJob pjob = getToolkit().getPrintJob(this,"COMPTA", properties);
    if (pjob != null) {
    Graphics pg = pjob.getGraphics();
    if (pg != null) {

    printCommande(pjob, pg);
    pg.dispose();
    }
    pjob.end();
    }
}
private void printCommande (PrintJob pjob, Graphics pg) {



   if (!(pg instanceof PrintGraphics)) {
      throw new IllegalArgumentException ("Graphics context not PrintGraphics");
    }

    int pageH = pjob.getPageDimension().height - margin;
    int pageW = pjob.getPageDimension().width - margin;

  if (pg != null)
  {
   
    Font helv = new Font("Helvetica", Font.BOLD, getValue("dateT"));
    pg.setFont(helv);
  
    pg.drawString(getString("logoV"), getValue("logoX"), getValue("logoY") );

    helv = new Font("Helvetica", Font.PLAIN, getValue("adresseT"));
    //have to set the font to get any output
    pg.setFont(helv);
    FontMetrics fm = pg.getFontMetrics(helv);
    int fontHeight = fm.getHeight();
    int fontDescent = fm.getDescent();

    pg.drawString(getString("adresseV"), getValue("adresseX"),  getValue("adresseY"));

    pg.drawString(getString("rueV"), getValue("rueX"),  getValue("rueY"));

    pg.drawString(getString("localiteV"), getValue("localiteX"),  getValue("localiteY"));

    pg.drawString(getString("telV"), getValue("telX"), getValue("telY"));

    pg.drawString(getString("faxV"), getValue("faxX"), getValue("faxY"));
    pg.drawString(getString("emailV"), getValue("emailX"), getValue("emailY"));
    pg.drawString(getString("tvaV"), getValue("tvaX"),  getValue("tvaY"));
    pg.drawRect(getValue("clientX"), getValue("clientY"), getValue("clientLongeur"), getValue("clientLargeur"));
    Font helv2 = new Font("Helvetica", Font.BOLD, getValue("clientT"));
    pg.setFont(helv2);

    pg.drawString(frss.sigleTiers, getValue("clientX") + 10,  getValue("clientY") + 10);
    helv2 = new Font("Helvetica", Font.ITALIC, getValue("clientT")-2);
    pg.setFont(helv2);
    pg.drawString(frss.designation, getValue("clientX" ) + 10, getValue("clientY" ) + 20);
    pg.drawString(frss.adresse, getValue("clientX" ) + 10,  getValue("clientY" ) + 30);
    pg.drawString(frss.info, getValue("clientX" ) + 10, getValue("clientY" ) + 40);
    pg.drawString(frss.devise, getValue("clientX" ) + 10,  getValue("clientY" ) + 50);
    Font helv1 = new Font("Helvetica", Font.BOLD, getValue("factureT"));
    
    String facture="";
 
             facture=getString("factureV") + (numero);
             facture="";
    pg.setFont(helv1);
    pg.drawString(facture, getValue("factureX"), getValue("factureY"));

    helv2 = new Font("Helvetica", Font.PLAIN, getValue("referenceT"));
    pg.setFont(helv2);
   // pg.drawString(getString("referenceV") + reference, getValue("referenceX"), getValue("referenceY"));
    helv1 = new Font("Helvetica", Font.BOLD, 10);
    pg.setFont(helv1);
    pageNum++;
   // pg.drawString("Page  " + pageNum, pageW / 2, pageH - 9);

    helv2 = new Font("Helvetica", Font.BOLD, 10);
    pg.setFont(helv2);
            int g = getValue("length");
            String[] entete = new String [g];
            int[] longeur = new int[g+1];
            String[] variable= new String [g];
            int [] dimension=new int[g];
            longeur[0]=0;

            int page=1;
            int y=  getValue("entete"+page+"X");
            for(int k=0;k<g;k++)
            {
            entete[k]=getString("entete_"+(k+1)+"Valeur");
            longeur[k+1]=getValue("entete_"+(k+1)+"Largeur");
          
            variable[k]=getString("entete_"+(k+1)+"Variable");
            dimension[k]=getValue("entete_"+(k+1)+"Dimension");
 // System.out.println(" d   "   +getValue("entete_"+(k+1)+"Dimension"));
            }

            int w = margin;

            int curHeightLigne = getValue("ligne"+page+"X");

            Font helvLot = new Font("Helvetica", Font.PLAIN, 10);
            pg.setFont(helvLot);
            FontMetrics fmLot = pg.getFontMetrics(helvLot);
            fontHeight = fmLot.getHeight();
            boolean done = true;

for (int j = 0; j < lotList.size(); j++) {

            helvLot = new Font("Helvetica", Font.PLAIN, getValue("tailleLigne"));
            pg.setFont(helvLot);
            Lot ci = lotList.get(j);

            w = margin;

            if (done) {
            curHeightLigne += fontHeight;
            } else {
            curHeightLigne += 2 * fontHeight;
            }
            done = true;
            for (int i = 0; i < g; i++) {
            w += (longeur[i]);
            
            String valeur=ci.vars[i];

            String variab=variable[i];
            int dim      =dimension[i];
            
           System.out.println(valeur+" dim  "+dim);// setVirguleD(String frw)
            
            if(variab.equals("intString"))
            {printInt(pg, (valeur), w + (longeur[i + 1]) - 10, curHeightLigne);}
            else if(variab.equals("int"))
            {printInt(pg,setVirgule(valeur), w + (longeur[i + 1]) - 10, curHeightLigne);}
            else  if(variab.equals("double"))
            {printInt(pg,setVirguleD(valeur), w + (longeur[i + 1]) - 10, curHeightLigne);}
            else if (valeur.length() < dim)
            {
            pg.drawString(valeur, w + 5, curHeightLigne);
            }
            else
            {
            done = false;
            pg.drawString(valeur.substring(0, dim - 1), w + 5, curHeightLigne);
            pg.drawString(valeur.substring(dim - 1, valeur.length()), w + 5, fontHeight + curHeightLigne);
            }
            }
 

            int hauteur = pageH - getValue("marginfooter");

            if ((curHeightLigne+20) >  hauteur-20) {

            /////////////////////////////////////////////////////////////////////////////////////////////
          
            helv1 = new Font("Helvetica", Font.BOLD, getValue("enteteTaille"));
            pg.setFont(helv1);
            w = margin;

            for (int i = 0; i < g; i++) {
            w += (longeur[i]);
            pg.drawLine(w,y, w, hauteur);//ligne zihagaze
            pg.drawString(entete[i], w + 2, y +15);// amazina
            }
            pg.drawLine(margin, hauteur, w, hauteur);//umurongo wo hasi
            pg.drawRect(margin, y , w - margin,25 ); // cadre ya entete

            /////////////////////////////////////////////////////////////////////////////////////////////
            pg.dispose();
            pg = pjob.getGraphics();
            if (pg != null)
            {
            pg.setFont(helv);
            }
            helv1 = new Font("Helvetica", Font.PLAIN, getValue("factureT"));
            pg.setFont(helv1);
   
             facture=getString("factureV") + (numero);
             facture="";
            pg.drawString(facture, margin, margin + 25);
            helv1 = new Font("Helvetica", Font.BOLD, 10);
            pg.setFont(helv1);
            pageNum++;page=2;
        //    pg.drawString("Page  " + pageNum, pageW / 2, pageH - 10);
              curHeightLigne=getValue("entete"+page+"X")+25;
              y=getValue("entete"+page+"X");
            done = true;
            }
 } 
///////////////////////////////////////////////////////////////////////////////////////////////////////////

            if((curHeightLigne+20) > ( pageH -getValue("longeurLigne")) )
            {
                 int hauteur = pageH - getValue("marginfooter");
            helv1 = new Font("Helvetica", Font.BOLD, getValue("enteteTaille"));
            pg.setFont(helv1);
            w = margin;

            for (int i = 0; i < g; i++) {
            w += (longeur[i]);
            pg.drawLine(w,y, w, hauteur);//ligne zihagaze
            pg.drawString(entete[i], w + 2, y +15);// amazina
            }
            pg.drawLine(margin, hauteur, w, hauteur);//umurongo wo hasi
            pg.drawRect(margin, y , w - margin,25 ); // cadre ya entete

            /////////////////////////////////////////////////////////////////////////////////////////////
            pg.dispose();
            pg = pjob.getGraphics();
            if (pg != null)
            {
            pg.setFont(helv);
            }
            helv1 = new Font("Helvetica", Font.PLAIN, getValue("factureT"));
            pg.setFont(helv1);
         
             facture=getString("factureV") + (numero);
             facture="";
            pg.drawString(facture, margin, margin + 25);

            helv1 = new Font("Helvetica", Font.BOLD, 10);
            pg.setFont(helv1);
            pageNum++;page=2;
            pg.drawString("Page  " + pageNum, pageW / 2, pageH - 10);
              curHeightLigne=getValue("entete"+page+"X")+25;
            done = true;
            hauteur = pageH - getValue("longeurLigne");
            helv1 = new Font("Helvetica", Font.BOLD, getValue("enteteTaille"));
            pg.setFont(helv1);
            w = margin;
            if(pageNum>1)
                page=2;
            y=getValue("entete"+page+"X");
            for (int i = 0; i < g; i++) {
            w += (longeur[i]);
            pg.drawLine(w,y, w, hauteur);//ligne zihagaze
            pg.drawString(entete[i], w + 2, y + 15);// amazina
            }
            pg.drawLine(margin, hauteur, w, hauteur);//umurongo wo hasi
            pg.drawRect(margin, y , w - margin, 25); // cadre ya entete

            }
            else
            {

            int hauteur = pageH - getValue("longeurLigne");
            helv1 = new Font("Helvetica", Font.BOLD, getValue("enteteTaille"));
            pg.setFont(helv1);
            w = margin;
            if(pageNum>1)
                page=2;
            y=getValue("entete"+page+"X");
            for (int i = 0; i < g; i++) {
            w += (longeur[i]);
            pg.drawLine(w,y, w, hauteur);//ligne zihagaze
            pg.drawString(entete[i], w + 2, y + 15);// amazina
            }
            pg.drawLine(margin, hauteur, w, hauteur);//umurongo wo hasi
            pg.drawRect(margin, y , w - margin, 25); // cadre ya entete
            }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        w = margin;
                        helv2 = new Font("Helvetica", Font.BOLD, getValue("prixTaille"));
                        pg.setFont(helv2);
                        int lp = getValue("lengthPrix");
                        String[] enteteP = new String [lp+1];
                        int[] longeurP = new int[lp+1];
                        String[] variableP= new String [lp];
                        longeurP[0]=0;



                        for(int k=0;k<lp;k++)
                        {
                        
                        enteteP[k]=getString("prix_"+(k+1)+"Valeur");
                        if(enteteP[k].length()>2)
                        enteteP[k]=enteteP[k]+frss.devise;
                        longeurP[k+1]=getValue("prix_"+(k+1)+"Largeur");
                        variableP[k]=getString("prix_"+(k+1)+"Variable");
                        System.out.println(longeurP[k+1]);
                        }
                        int Xprix = getValue("longeurPrix");
                        int marginP=getValue("marginPrix");
                        w =marginP ;
                        for (int i = 0; i < lp; i++) {
                        w += (longeurP[i]);
                        System.out.println(( w + (longeurP[i + 1]) - 40)+"  kkk "+(Xprix+25)+"  total "+total+"   i "+i);

                        int wid=w + (longeurP[i + 1]) - 20;
                        if(i==0)
                        printInt(pg,setVirgule( ""+(int)(tva)),  wid, Xprix+15);
                        if(i==1)
                        printInt(pg,setVirgule(""+ (int)total),  wid, Xprix+15);
                        if(i==2)
                        {
                            int solde=(int)(total-tva);
                            if(solde<0)
                                printInt(pg,setVirgule( ""+solde*-1),  wid, Xprix+15);
                            else
                                printInt(pg,setVirgule( ""+solde),  wid, Xprix+15);
                                
                        }

                        pg.drawLine(w,Xprix-20, w, Xprix+20);//ligne zihagaze
                        System.out.println( w +" x1 y1 "+(Xprix-20)+"  y2 "+ Xprix+20);
                        pg.drawString(enteteP[i], w + 2, Xprix - 5);// amazina
                       System.out.println(enteteP[i]+"entete   x=w+2   yxprix-5= "+(Xprix - 5));
//+frss.devise
                        }
                         if(lp!=0)
                        {
                        pg.drawLine(marginP, Xprix, w, Xprix);//umurongo wo hasi
                        System.out.println(marginP+"marginP  lineMidlle y=xprix   y= "+w);
                        pg.drawRect(marginP, Xprix-20 , w-marginP , 40); // cadre ya entete
                         System.out.println(marginP+" x   cadre y Xprix-20"+( Xprix-20)+" length= "+(w-marginP));

                        }
                        
 //System.out.println(getValue("lengthfooter")+" "+footer.length);
 //System.out.println( "dedede "+footer [1]);
    if(mode.equals("CALCUL"))
    for(int v=1;v<getValue("lengthfooter") && footer.length>v;v++)
    {
    System.out.println( getValue("footer_"+v+"X" )+" dedede "+footer [v]+getValue("footer_"+v+"Y" ));
    pg.drawString( footer [v] , getValue("footer_"+v+"X" ), getValue("footer_"+v+"Y" ));
    }

    else
    for(int v=1;v<getValue("lengthfooter")&& footer.length>v ;v++)
    if(v<6)
    pg.drawString(getString("footer_"+v+"Valeur")+  ""+footer[v], getValue("footer_"+v+"X" ), getValue("footer_"+v+"Y" ));
    else
    pg.drawString(getString("footer_"+v+"Valeur"), getValue("footer_"+v+"X" ), getValue("footer_"+v+"Y" ));
  
  }

}

int getValue(String s)
{

int ret=0;
Variable var= db.getParamCmd("PRINT",mode,s+mode);
if(var!=null)
{
//System.out.println(var.value+" nnnnn  "+var.nom);
Double val=new Double (var.value);
ret = val.intValue();
}
return ret;
}
String getString(String s)
{
String ret=" ";
 Variable var= db.getParamCmd("PRINT",mode,s+mode);
 if(var!=null)
     ret=var.value;
     return ret;
}
public void printInt(Graphics pg,int nbre,int w,int h)
  {
  String s=setVirgule(nbre);

  int back=0;

  for(int i=s.length()-1;i>=0;i--)
  {
      if(s.charAt(i)==' ')
      back+=2;
      else
       back+=5;

      pg.drawString(""+s.charAt(i),w-back,h);

  }
}
public void printInt(Graphics pg,String s,int w,int h)
  {

  int back=0;

  for(int i=s.length()-1;i>=0;i--)
  {
      if(s.charAt(i)==' ')
      back+=2;
      else
       back+=5;


      pg.drawString(""+s.charAt(i),w-back,h);

  }




  }
  String setVirgule(int frw)
        {

String setString = ""+frw;
int l =setString.length();



if(l>3 && l<=6)
setString= setString.substring(0,l-3)+" "+setString.substring(l-3) ;
if(l>6)
{

String s1 = setString.substring(0,l-3);
int sl=s1.length();
setString=s1.substring(0,sl-3)+" "+s1.substring(sl-3)+" "+setString.substring(l-3)  ;

}

return setString;
}
  String setVirgule(String setString)
        {


int l =setString.length();  
if(l>3 && l<=6)
setString= setString.substring(0,l-3)+" "+setString.substring(l-3) ;
if(l>6)
{

String s1 = setString.substring(0,l-3);
int sl=s1.length();
setString=s1.substring(0,sl-3)+" "+s1.substring(sl-3)+" "+setString.substring(l-3)  ;

}

return setString;
}
  String setVirguleD(String frw)
        {
      String setString="";
if(frw.contains("."))
{
StringTokenizer st1=new StringTokenizer (frw);

    String entier =st1.nextToken(".");
System.out.println(frw);
    String decimal=st1.nextToken("").replace(".", "");

    if(decimal.length()==1 && decimal.equals("0") )
    decimal=".00";
    else if (decimal.length()==1 && !decimal.equals("0") )
          decimal="."+decimal+"0";
    else
    decimal="."+decimal.substring(0, 2);

 setString = entier+decimal;

int l =setString.length();
if(l<2)
   setString = "    "+frw;
else if(l<3)
   setString = "  "+frw;
else if(l<4)
   setString = "  "+frw;

int up=6;
int ju=up+3;
if(l>up && l<=ju)
{
    setString= setString.substring(0,l-up)+","+setString.substring(l-up) ;
}
else if(l>ju)
{
setString=setString.substring(0,l-9)+","+setString.substring(l-9,l-6)+","+setString.substring(l-6,l)  ;

}
}
else
  setString=  setVirgule( frw);
return setString;
}

public static void main(String[] arghs)
{
    //new PrintDoc();
}




}
