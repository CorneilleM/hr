package HR;
 
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener; 
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
 
/**
 *
 * @author aimable
 */
public class showJTable extends JFrame {

    
    JMenuItem print = new JMenuItem("Print ");
    JMenuItem exporter = new JMenuItem("Export ");
    JMenu menu1 = new JMenu("FILE");
    JMenuBar menu_bar1 = new JMenuBar();
    
    private JButton comptabiliser;
    private JButton cancel;
    
    LinkedList<Journal> joList;
    LinkedList<String> INVvalideGroupeesConfirmation;
    LinkedList<String> BLvalideGroupeesConfirmation;
   // LinkedList<ComptabiliseRefund> REFvalide;
    LinkedList<String> InvGroups;
    String serverDepot;
    String databaseDepot;
    String serverSell;
    String databaseSell;
    DbHandler db;
   // ComptabiliseBL cbl;
    JTable debit, credit,dettesJtable;
    String[][] sDebit, sCredit;
    String search;
    String timeSpan;
    Thread thread;
        String[][] s1;
        boolean isBilling=false;
        String titlo;
    public showJTable(JTable j, String s) {

        Container content = getContentPane();
        westPane(content, j);
//        eastPane(content, jj);
        this.setSize(1200, 800);
        setTitle(s + "");
        
        menu1.add(exporter);  menu1.add(print); 
        
        menu_bar1.add(menu1);
        this.setJMenuBar(menu_bar1); 
        this.setVisible(true);
        
       // exporter();

    }
String dateSearchPeriod;


 public showJTable(JTable dettesJtable, String s, String[][] s1, String compte,
            double montantDebit, double montantCredit, DbHandler db, Image img,
            String dateDoc, String mode,String dateSearchPeriod) {

        Container content = getContentPane(); 
        westPane(content, dettesJtable);
        this.s1=s1;
        menu1.add(print);
        menu1.add(exporter);   
        menu_bar1.add(menu1);
        this.setJMenuBar(menu_bar1);
        
        this.dettesJtable=dettesJtable;
        this.dateSearchPeriod=dateSearchPeriod;
        this.db=db;
//        eastPane(content, jj);
        this.setSize(1200, 800); 
        setTitle(s + ""); 
        this.setVisible(true);
        //exporter();
        print();

        // viewDette();
        
    }
 
 
 public showJTable(JTable dettesJtable, String s, String[][] s1, String compte,
            double montantDebit, double montantCredit, DbHandler db,
            String dateDoc, String mode,String dateSearchPeriod, String total) {

        Container content = getContentPane(); 
        westPane1(content, dettesJtable, total);
        this.s1=s1;
        /*menu1.add(print);
        menu1.add(exporter);   
        menu_bar1.add(menu1);
        this.setJMenuBar(menu_bar1);
        */
        this.dettesJtable=dettesJtable;
        this.dateSearchPeriod=dateSearchPeriod;
        this.db=db;
//        eastPane(content, jj);
        this.setSize(1300, 600); 
        setTitle(s + ""); 
        this.setVisible(true);
        //exporter();
        //print();

         //viewDette();
        
    }
 
 public showJTable(JTable dettesJtable, String s, String[][] s1, String compte,
            double montantDebit, double montantCredit, DbHandler db, Image img,
            String dateDoc, String mode,String dateSearchPeriod,boolean billing) {

        Container content = getContentPane(); 
        westPane(content, dettesJtable);
        this.s1=s1;
        menu1.add(exporter);  
        menu_bar1.add(menu1);
        this.setJMenuBar(menu_bar1);
        
        this.dettesJtable=dettesJtable;
        this.dateSearchPeriod=dateSearchPeriod;
        this.db=db;
        this.isBilling=billing;
//        eastPane(content, jj);
        this.setSize(1300, 800); 
        setTitle(s + ""); 
        this.setVisible(true);
        //exporter(); 
        //viewDette();
        print();
        
    }
 
 
    String eEspace(String s)
    {
        if(s==null)
            return "";
        else
        return s.replaceAll(" ", "");
    }

    public showJTable(JTable debit, JTable credit,
                    String[][] sDebit, String[][] sCredit, 
                    String s, String searchRange, DbHandler db) {

        Container content = getContentPane();

        this.debit = debit;
        this.credit = credit;
        this.sCredit = sCredit;
        this.sDebit = sDebit;
        this.search = searchRange;
        this.db = db;
        westPane2(content);

        this.setSize(1200, 500);
        setTitle(s + " ISHYIGA");
        jtableDebitClicked();
        this.setVisible(true);
    }

    private void westPane2(Container content) {

        JPanel productPane = new JPanel();
        productPane.setPreferredSize(new Dimension(900, 380));

        JScrollPane jTableList = new JScrollPane(debit);
//        jTableList.setPreferredSize(new Dimension(820, 670));
        jTableList.setPreferredSize(new Dimension(450, 380));

        JScrollPane jTableList2 = new JScrollPane(credit);
//        jTableList.setPreferredSize(new Dimension(820, 670));
        jTableList2.setPreferredSize(new Dimension(450, 380));

        productPane.add(jTableList);
        productPane.add(jTableList2);

        content.add(productPane);

    }
    
    private void westPane1(Container content, JTable j, String total) {
        JPanel productPane = new JPanel();
        productPane.setPreferredSize(new Dimension(1200, 500));

        JScrollPane jTableList = new JScrollPane(j);
        jTableList.setPreferredSize(new Dimension(1200, 500));
        
        JLabel lab= new JLabel(" TOTAL TO PAY:"+ total);

        productPane.add(jTableList);
        productPane.add(lab);

        content.add(productPane);
      
    } 
    

    public void jtableDebitClicked() {
        debit.addMouseListener(new MouseListener() {

            public void actionPerformed(ActionEvent ae) {
            }

            @Override
            public void mouseExited(MouseEvent ae) {
            }

            @Override
            public void mouseClicked(MouseEvent e) {


                if (debit.getSelectedRow() != -1) {
                    String account_Name = sDebit[ debit.getSelectedRow()][0];
                    String res = db.getCompteResultatV2Un(search, account_Name);
                    JOptionPane.showMessageDialog(rootPane, res);

                }

            }

            ;

            @Override
            public void mousePressed(MouseEvent e) {
            }
 @Override
            public void mouseReleased(MouseEvent e) {
            }
 @Override
            public void mouseEntered(MouseEvent e) {
            }
        });
    }

   
   

    public showJTable(JTable j, JTable jj, String s,String[][] s1,String[][] s2,
             double montantDebit, double montantCredit, DbHandler db, Image img,
            String dateDoc, String mode,String dateSearchPeriod,String titleDoc) {
        Container content = getContentPane();
        westPane2(content, j, jj);
//        eastPane(content, jj);

        this.setSize(1200, 500);
        setTitle(s + " ISHYIGA");
 
        this.setVisible(true);
        
        
        LinkedList<Lot> lotList = new LinkedList(); 
        for (String[] s11 : s1) {
            Lot lot = new Lot(
                    (s11[0]),
                    "",   
                    (s11[1]),
                    "", 
                    eEspace(s11[2]),
                    "",
                    "", "", "", "", "");
          //  System.out.println(lot);
            lotList.add(lot); 
        }
        
          for (String[] s11 : s2) {
            Lot lot = new Lot(
                    "",
                    (s11[0]),
                    "",  
                    (s11[1]),   
                   "", 
                    eEspace(s11[2]), 
                   "",  "", "", "", "");
          //  System.out.println(lot);
            lotList.add(lot); 
        }

//        BalanceGeneralDesComptes st1 = new BalanceGeneralDesComptes("kadissgi@yahoo.fr", s1); 
//        ArrayList<BalanceGeneralDesComptes> MyArrayList = BalanceGeneralDesComptes.getList4(s1);  
        PrintTiers pt = new PrintTiers(titleDoc, dateSearchPeriod, "", "", "", "");  
        String Facture = " ";  
//        String libele = "RAPPORT AU : " + new Date();
        String libele = "Printed By Ishyiga Compta @ : " + (new Date()).toLocaleString(); 
        PrintVars pv = new PrintVars(dateDoc, libele, mode, "", Facture, "FRW", "CODE", 1); 
        double[] ibiciro = new double[]{montantDebit, montantCredit,montantDebit, montantCredit}; 
        PrintV2 printer = new PrintV2(db, img, pt, pv, lotList,
                ibiciro, new String[]{"", "", "", "", "", ""},750); 
        // viewDette(); 
    }

    private void westPane(Container content, JTable j) {
        JPanel productPane = new JPanel();
        productPane.setPreferredSize(new Dimension(1200, 700));

        JScrollPane jTableList = new JScrollPane(j);
        jTableList.setPreferredSize(new Dimension(1200, 700));

        productPane.add(jTableList);

        content.add(productPane);
    }

    //le panel de ventes
    public void westPane2(Container content, JTable j, JTable jj) {

        JPanel productPane = new JPanel();
        productPane.setPreferredSize(new Dimension(1150, 380));

        JScrollPane jTableList = new JScrollPane(j);
//        jTableList.setPreferredSize(new Dimension(820, 670));
        jTableList.setPreferredSize(new Dimension(550, 380));

        JScrollPane jTableList2 = new JScrollPane(jj);
//        jTableList.setPreferredSize(new Dimension(820, 670));
        jTableList2.setPreferredSize(new Dimension(550, 380));

        productPane.add(jTableList);
        productPane.add(jTableList2);

        content.add(productPane);

    }

    public void westPane3(Container content, JTable jtableInvalides, JTable jtablevalive) {

        JPanel CommonNorthPane = new JPanel();
        //   CommonNorthPane.setLayout(new GridLayout(5, 1, 5, 5));
        CommonNorthPane.setSize(new Dimension(1200, 600));

        ///////////////////////////////////////////////////////////////

        JPanel productPane1 = new JPanel();
        productPane1.setPreferredSize(new Dimension(1200, 30));
        JLabel label1 = new JLabel("LES DOC INVALIDES: ");

        productPane1.add(label1);

        /////////////////////////////////////////////////

        JPanel productPane11 = new JPanel();
        productPane11.setPreferredSize(new Dimension(1000, 200));
        JScrollPane jTableList11 = new JScrollPane(jtableInvalides);
        jTableList11.setPreferredSize(new Dimension(1000, 150));
        productPane11.add(jTableList11);
        ////////////////////////////////////////////////////////

        JPanel productPane12 = new JPanel();
        productPane12.setPreferredSize(new Dimension(1000, 30));

        JLabel label2 = new JLabel("LES DOC VALIDES: ");

        productPane12.add(label2);
        /////////////////////////////////////////////////

        JPanel productPane = new JPanel();
        productPane.setPreferredSize(new Dimension(1000, 250));

        JScrollPane jTableList = new JScrollPane(jtablevalive);
        jTableList.setPreferredSize(new Dimension(1000, 150));

        productPane.add(jTableList);

        /////////////////////////////////////////////////////////

        JPanel productPane2 = new JPanel();
        productPane2.setPreferredSize(new Dimension(900, 30));

        comptabiliser = new JButton("COMPTABILISER LES DOC VALIDES");
        comptabiliser.setBackground(Color.white);

        cancel = new JButton("CANCEL");
        cancel.setBackground(Color.LIGHT_GRAY);

        productPane2.add(comptabiliser);
        productPane2.add(cancel);
//        content.add(productPane);
//        content.add(productPane2);
        CommonNorthPane.add(productPane1);
        CommonNorthPane.add(productPane11);
        CommonNorthPane.add(productPane12);
        CommonNorthPane.add(productPane);
        CommonNorthPane.add(productPane2);

        content.add(CommonNorthPane);
    }

   
    

    
    

  
  
   void print(){
    
print.addActionListener((ActionEvent e) -> {
    if(e.getSource()==print  )
    {    
LinkedList<Lot> lotList = new LinkedList(); 

for (String[] s11 : s1) {
   Lot lot2 = new Lot(s11[0], s11[1], s11[2], s11[3], s11[4], s11[5], s11[6], s11[7], "", "", "");
   lotList.add(lot2); 
}
lotList.removeFirst();

    }
});
}  
    
}
