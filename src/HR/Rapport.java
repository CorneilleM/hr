/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package HR;

/**
 *
 * @author Kimenyi
 */
public class Rapport implements Comparable {

    public int id = 0;
    public double qte = 0, chiffre = 0, benef = 0;
    public String code = "", name = "";

    public void Rapport(int id, int qte, int chiffre, int benef, String code, String name) {
        this.id = id;
        this.qte = qte;
        this.chiffre = chiffre;
        this.code = code;
        this.benef = benef;
        this.name = name;
    }

    public void Rapport(int id) {
        this.id = id;
    }

    public int compareTo(Object r1) {

        int ret = 0;
        if (r1 == null) {
            return -1;
        }
        if ((int) (((Rapport) r1).qte - qte) == 0) {
            return 1;
        } else {
            return (int) (((Rapport) r1).qte - qte);
        }

    }

    /*
     * public int compareTo(Object o) { throw new
     * UnsupportedOperationException("Not supported yet."); }
     *
     */
}
