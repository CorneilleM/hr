/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HR;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.sql.*;
import java.util.*;
import javax.swing.*;


/**
 *
 * @author Odette
 */
public class AddCharge extends JFrame {
    
    JLabel c_name,compted,comptec,type,payable;
    JTextField name_c;
    JTextField searchCompte,searchCompte2 ;
    JRadioButton percentage, fix, employee, company;
    JComboBox compte_d,compte_c;
    JPanel main= new JPanel();
   
    JButton Save;
    JButton view;
    Font police;
    Color color;
    Container content;
    JList cptlist1 = new JList();
    JList cptlist2 = new JList();
    DbHandler db;
    DefaultListModel listModel = new DefaultListModel();
    LinkedList<Compte> allCompte2 = new LinkedList<>();
   
    
   public void draw() {
        
        try{
        this.setTitle("Add charge");
        enableEvents(WindowEvent.WINDOW_CLOSING);
        this.setResizable(false);
        this.setSize(950, 350);
         Pane();
        setSize(950, 350);
        setVisible(true);
         //();
        
        } catch (Throwable e) {
            System.out.println(" . . inv . exception thrown:" + e);
        }        
    } 
   
   
   public void load()
   {
   
   
   try{
        
        compted = new JLabel("COMPTE DEBIT: ");
        police = new Font("Rockwell", Font.BOLD, 15);
        compted.setFont(police);
        
        c_name = new JLabel("CHARGE NAME: ");
        police = new Font("Rockwell", Font.BOLD, 15);
        c_name.setFont(police);
        
        comptec = new JLabel("COMPTE CREDIT: ");
        police = new Font("Rockwell", Font.BOLD, 15);
        comptec.setFont(police);

        
        type = new JLabel("TYPE: ");
        police = new Font("Rockwell", Font.BOLD, 15);
        type.setFont(police);
        
        
        percentage = new JRadioButton("PERCENTAGE");
        fix = new JRadioButton("FIX");
        
        
        name_c = new JTextField("");
        police = new Font("Consolas", Font.BOLD, 15);
        name_c.setFont(police);
        
        
                
        searchCompte = new JTextField("");
        police = new Font("Consolas", Font.BOLD, 15);
        searchCompte.setFont(police);  
        
        searchCompte2 = new JTextField("");
        police = new Font("Consolas", Font.BOLD, 15);
        searchCompte2.setFont(police);  
        
        
        payable = new JLabel("PAYABLE");
        police = new Font("Rockwell", Font.BOLD, 15);
        payable.setFont(police);
        
        employee = new JRadioButton("EMPLOYEE");
        company = new JRadioButton("COMPANY");
        Save= new JButton("SAVE");
        police = new Font("Rockwell", Font.BOLD, 15);
        Save.setFont(police);
        Save.setBackground(Color.green);;
   
        view = new JButton("VIEW");
        police = new Font("Rockwell", Font.BOLD, 15);
        view.setFont(police);
        view.setBackground(Color.orange);
        
       main.setBorder(BorderFactory.createLineBorder(Color.black));
        
        
        
           
        } catch (Exception e ) {
            System.out.println(" . . makeFrameTiers . exception thrown:" + e);
//            insertError(e + "", " getBalanceVerification  ");
        }
   
   }
   
   
   
   
   public AddCharge( DbHandler db) {

        try {
            
             
            this.db=db;
            
            content = getContentPane();
            
          
            load();
            draw();
            groupButton( );
            save();
            cancel();
            searchCompte();
            searchCompte2();
            
            showCompte();
            
           
        } catch (Throwable e) {
            System.out.println("Failllll"+e);

        }
    }
   
   
   
   public JPanel Pane() {
        JPanel panel= new JPanel();
        panel.setLayout(new GridLayout(4, 2, 5, 20));
      
        JPanel radio1 = new JPanel();
        radio1.setLayout(new GridLayout(1, 2, 5, 5));
        
        radio1.add(percentage);
        radio1.add(fix);
        
        JPanel radio2 = new JPanel();
        radio2.setLayout(new GridLayout(1, 2, 5, 5));
        radio2.add(employee);
        radio2.add(company);
        JScrollPane scroll1 = new JScrollPane(cptlist1);
            scroll1.setPreferredSize(new Dimension(250, 100));
            cptlist1.setBackground(new Color(255, 175, 160));
            
       JScrollPane scroll2 = new JScrollPane(cptlist2);
       scroll2.setPreferredSize(new Dimension(250, 100));
       cptlist2.setBackground(new Color(255, 175, 160));
       
        JPanel list_title = new JPanel(new GridLayout(2, 2, 5, 5));
        list_title.add(compted);
        list_title.add(comptec);
        list_title.add(searchCompte);
        list_title.add(searchCompte2);
        
        JPanel lists = new JPanel(new GridLayout(1, 2, 5,5));
        lists.add(scroll1);
        lists.add(scroll2);
        
       JPanel list = new JPanel(new GridLayout(2, 1, 0,0));
        list.add(list_title);
       list.add(lists );


        panel.add(c_name);
        panel.add(name_c);
        panel.add(type);
      
        panel.add(radio1);
        panel.add(payable);
        panel.add(radio2);
        panel.add(Save);
       panel.add(view);
      

        main.add(panel,BorderLayout.WEST);
        main.add(list,BorderLayout.NORTH);
        

        content.add(main);
        return main;
    }
   
   
 private void groupButton( ) {

ButtonGroup group1 = new ButtonGroup( );

group1.add(percentage);
group1.add(fix);

ButtonGroup group2 = new ButtonGroup( );

group2.add(employee);
group2.add(company);

} 
    
    
  public void save() {

        Save.addActionListener(new ActionListener() {
 @Override
                    public void actionPerformed(ActionEvent ae) {

                        if (ae.getSource() ==Save) {
                            
                            
                         String name =name_c.getText();
                          String cmptd= ((Compte) cptlist1.getSelectedValue()).numCompte;
                          String cmpt_name= ((Compte) cptlist2.getSelectedValue()).numCompte;
                          
                     
                          String cpt_name_c=((Compte) cptlist2.getSelectedValue()).nomCompte;
                          String typ="";
                          if(percentage.isSelected())
                          {typ=percentage.getText();}
                          else if(fix.isSelected())
                        {typ=fix.getText();}
                          
                          String pay="";
                          if(company.isSelected())
                          {pay=company.getText();}
                          else if(employee.isSelected())
                            {pay=employee.getText();}
                          
                          db.insertCharge(cmptd, cmpt_name, typ, pay, cpt_name_c, name);
                          percentage.setSelected(false);
                          fix.setSelected(false);
                          company.setSelected(false);
                          employee.setSelected(false);
                          
                        }
                    }
                });
    }  
  
  
  
  
  
  public void cancel() {

        view.addActionListener(new ActionListener() {
 @Override
                    public void actionPerformed(ActionEvent ae) {

                        if (ae.getSource()==view) {
                            
                     new Viewcharge(db);
                        }
                    }
                });
    }
/*  
  
  void fillcombo1()
{
    try
    {
        st =conn.createStatement();
       

           

           rs =st.executeQuery("select * from APP.CPT_COMPTE ");
            while(rs.next())
            {
                 String numCompte = rs.getString(1);
               
                String nomCompte = rs.getString(2);
              
               Compte ce = new Compte(  numCompte,nomCompte);
        
           compte_d.addItem(ce);
           compte_c.addItem(ce);
            
           }
    }
    catch(SQLException e)
                {
                  System.out.println("Fail"+e.getMessage());  
                }
    
}
 */ 
   public void showCompte() {
        {
    
          
cptlist1.setListData(db.selectCpte().toArray());
cptlist2.setListData(db.selectCpte().toArray());

            
    
}

    }


   
  

     public void searchCompte() {

        searchCompte.addKeyListener(
                new KeyListener() {
            public void actionPerformed(ActionEvent ae) {  }
 @Override  public void keyTyped(KeyEvent e) {

                        String cc = "" + e.getKeyChar();
                        String find = (searchCompte.getText() + cc).toUpperCase();
                        if (cc.hashCode() == 8 && find.length() > 0) {
                            find = (find.substring(0, find.length() - 1));
                        }

                        searchedCpte(find);
                    }
 @Override public void keyPressed(KeyEvent e) { }
 @Override public void keyReleased(KeyEvent e) { }
                });
    }
     
     
     
  void searchedCpte(String findIt) { 
        int linge = db.allCompte.size(); 
        LinkedList<Compte> allCompte2 = new LinkedList<>(); 
        for (int x = 0; x < linge; x++) { 
            Compte C = db.allCompte.get(x);

            if ((("" + C.nomCompte).contains(findIt)) || (("" + C.numCompte).contains(findIt))) {
                allCompte2.add(C);
            }
        }
        cptlist1.setListData(allCompte2.toArray());
    }
   
    
     
     public void searchCompte2() {

        searchCompte2.addKeyListener(
                new KeyListener() {
            public void actionPerformed(ActionEvent ae) {  }
 @Override  public void keyTyped(KeyEvent e) {

                        String cc = "" + e.getKeyChar();
                        String find = (searchCompte2.getText() + cc).toUpperCase();
                        if (cc.hashCode() == 8 && find.length() > 0) {
                            find = (find.substring(0, find.length() - 1));
                        }

                        searchedCpte2(find);
                    }
 @Override public void keyPressed(KeyEvent e) { }
 @Override public void keyReleased(KeyEvent e) { }
                });
    }
     
     
      void searchedCpte2(String findIt) { 
        int linge = db.allCompte.size(); 
        LinkedList<Compte> allCompte2 = new LinkedList<>(); 
        for (int x = 0; x < linge; x++) { 
            Compte C = db.allCompte.get(x);

            if ((("" + C.nomCompte).contains(findIt)) || (("" + C.numCompte).contains(findIt))) {
                allCompte2.add(C);
            }
        }
        cptlist2.setListData(allCompte2.toArray());
    }
    

    
    
    
}
