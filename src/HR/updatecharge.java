/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HR;

import static com.barcodelib.barcode.a.f.d.w;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import javax.swing.*;
import java.sql.*;
import java.util.LinkedList;


/**
 *
 * @author Odette
 */
public class updatecharge extends JFrame {
    
    
   
            
            String tie;
            String chrg;
    
    
    JLabel tier,charge;
    JTextField name;
    
    JButton add;
    JButton remove,update;
    JPanel main = new JPanel();
    Font police;
    Color color; 
    
    DbHandler db;
    
    Container content;
    JList chrglist = new JList();
    DefaultListModel listModel = new DefaultListModel();
    InterfaceSalaire interf;
    String s;
    String[] part;
String ti,ti_name;
int salaire;
private static JFrame frame;

    
    
     public void draw() {
        
        try{
        this.setTitle("Update charge to employee");
        enableEvents(WindowEvent.WINDOW_CLOSING);
        this.setResizable(false);
        this.setSize(500, 300);
         Pane();
        
        setSize(500, 300);
        setVisible(true);
        
        } catch (Throwable e) {
            System.out.println(" . . inv . exception thrown:" + e);
        }        
    } 
   
   
   public void load()
   {
   
   
   try{
        
        tier = new JLabel("NAMES: ");
        police = new Font("Rockwell", Font.BOLD, 15);
        tier.setFont(police);
        
        name=new JTextField("");
        police = new Font("Rockwell", Font.BOLD, 15);
        name.setFont(police);
        
        
        charge = new JLabel("APPLICABLE CHARGES: ");
        police = new Font("Rockwell", Font.BOLD, 15);
        charge.setFont(police);
 
        add= new JButton("ADD");
        police = new Font("Rockwell", Font.BOLD, 15);
        add.setFont(police);
        add.setBackground(Color.orange);
   
       update= new JButton("UPDATE");
        police = new Font("Rockwell", Font.BOLD, 15);
        update.setFont(police);
        update.setBackground(Color.blue);
   
        remove = new JButton("REMOVE");
        police = new Font("Rockwell", Font.BOLD, 15);
        remove.setFont(police);
        remove.setBackground(Color.green);
        
        } catch (Exception e ) {
            System.out.println(" exception thrown:" + e);
//            
        }
   
   }
   
   
   
   
   public updatecharge(DbHandler db, String s ) {

        try {
            
            part = s.split("(?<=\\D)(?=\\d)");
            ti_name=part[0];
           ti=part[1];
             
            this.db=db;
            
            content = getContentPane();
            

            load();
            draw();
            
            
            
           
            
            chargeslist(s);
            add(s);
            remove(ti);
            update();
          
            
            
            //showCompte();
        } catch (Throwable e) {
            System.out.println(e);
//            insertError(e + "", " getBalanceVerification  ");
        }
    }
   
   
   
   public JPanel Pane() {
        JPanel panelmain = new JPanel();
        panelmain.setLayout(new GridLayout(3, 2, 5, 5));//
        panelmain.setPreferredSize(new Dimension(450,200));
        
        JPanel buttons = new JPanel();
        buttons.setLayout(new GridLayout(1, 2, 5, 5));//
        buttons.setPreferredSize(new Dimension(450,200));
        
JScrollPane scroll = new JScrollPane(chrglist);
            scroll.setPreferredSize(new Dimension(w - 170 - 5, 200));
            chrglist.setBackground(new Color(255, 175, 160));

        panelmain.add(tier);
        panelmain.add(name);
        panelmain.add(charge);
        
//    
        panelmain.add(scroll);
        buttons.add(add);
        buttons.add(update);
     panelmain.add(buttons);
        panelmain.add(remove);
        
        


        main.add(panelmain);
        

        content.add(main);
        return main;
    }
   
   void chargeslist(String t)
           
{
    
            name.setText(t);
            

    db.Applicable_charges(ti, listModel, chrglist);
}
   
   
   public void add(String t) {

        add.addActionListener(new ActionListener() {
 @Override
                    public void actionPerformed(ActionEvent ae) {

                        if (ae.getSource() ==add) {
                            new employee_charge(db,t);
                            
                   
                        }
                    }
                });
    }  
   
   
   public void remove(String ti) {

        remove.addActionListener(new ActionListener() {
 @Override
                    public void actionPerformed(ActionEvent ae) {

                        if (ae.getSource() ==remove) {
                            int i =((cpt_lacharge)chrglist.getSelectedValue()).code;
                            
                            System.out.println("Dore CODE:----:"+ i);
                            
                            int index = chrglist.getSelectedIndex();
                            listModel.removeElementAt(index);
                            
                            
                            
    db.remove_charge(ti, i);
                            
                            
     }
                    }
                });
    }  
   
  
     public void update() {
      
        update.addActionListener(new ActionListener() {
 @Override
                    public void actionPerformed(ActionEvent ae) {
    
                        if (ae.getSource() ==update) {
                            
                            double montant =((cpt_lacharge)chrglist.getSelectedValue()).valeur;
                            int code =((cpt_lacharge)chrglist.getSelectedValue()).code;
                            String charge =((cpt_lacharge)chrglist.getSelectedValue()).name;
                            String nn = JOptionPane.showInputDialog(frame, "SALARIE: "+ti_name+"\n AMOUNT FOR "+charge+":\n "+montant+" \n ENTER NEW AMOUNT: ");
                            
                           double nn1 = Double.parseDouble(nn);
                           
                           db.updateCharge_employee(ti,code , nn1);
                           
                            
                            
                            
                   
}
                    }
                });
    }    
    
}
