/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HR;
import static com.barcodelib.barcode.a.f.d.w;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import javax.swing.*;
import java.sql.*;

/**
 *
 * @author Odette
 */
public class Viewcharge extends JFrame {
    
    
            Container content;
            JPanel main = new JPanel();
            JButton add,update,delete;
            Font police;
            Color color;
            
            Cashier cashier;
            DbHandler db;
            
           static JList chrglist = new JList();
    DefaultListModel listModel = new DefaultListModel();
    public void draw() {
        
        try{
        this.setTitle(" View all charges");
        enableEvents(WindowEvent.WINDOW_CLOSING);
        this.setResizable(false);
        this.setSize(500, 300);
         Pane();
        setSize(500, 300);
        setVisible(true);
        
        } catch (Throwable e) {
            System.out.println(" . . inv . exception thrown:" + e);
        }        
    }
    
    public void load()
   {
   
   
   try{
        
        
 
        add= new JButton("ADD NEW");
        police = new Font("Rockwell", Font.BOLD, 15);
        add.setFont(police);
        add.setBackground(Color.orange);
   
        update = new JButton("UPDATE");
        police = new Font("Rockwell", Font.BOLD, 15);
        update.setFont(police);
        update.setBackground(Color.green);
        
        delete = new JButton("DELETE");
        police = new Font("Rockwell", Font.BOLD, 15);
        delete.setFont(police);
        delete.setBackground(Color.blue);
        
        
        } catch (Exception e ) {
            System.out.println(" exception thrown:" + e);
//            
        }
   
   }
    
    public Viewcharge(DbHandler db ) {

        try {
             
            this.db=db;
            
            
            content = getContentPane();
            

            load();
            draw();
            
            
           db.chargeslist(listModel, chrglist);
            
           
            add();
            update();
            delete();
            
        } catch (Throwable e) {
            System.out.println(e);
//            insertError(e + "", " getBalanceVerification  ");
        }
    }
    
    public JPanel Pane() {
        JPanel panelmain = new JPanel();
        panelmain.setLayout(new GridLayout(2, 1, 5, 5));//
        panelmain.setPreferredSize(new Dimension(450,200));
        
        
        JPanel b = new JPanel();
        b.setLayout(new GridLayout(1, 3, 5, 5));//
        b.setPreferredSize(new Dimension(450,50));
        //panelmain.setBorder(BorderFactory.createLineBorder(Color.black));
        
JScrollPane scroll = new JScrollPane(chrglist);
            scroll.setPreferredSize(new Dimension(w - 170 - 5, 200));
            chrglist.setBackground(new Color(255, 175, 160));

        
//    
        panelmain.add(scroll);
        b.add(add);
        b.add(update);
        b.add(delete);
        main.add(panelmain);
        main.add(b);
        

        content.add(main);
        return main;
    }
  
      public void add() {

        add.addActionListener(new ActionListener() {
 @Override
                    public void actionPerformed(ActionEvent ae) {

                        if (ae.getSource() ==add) {
                            new AddCharge(db);
                            
                   
                        }
                    }
                });
    }  
      
      
      public void update() {

        update.addActionListener(new ActionListener() {
 @Override
                    public void actionPerformed(ActionEvent ae) {

                        if (ae.getSource() ==update && chrglist.getSelectedValue() == null ) {
                             
                             JOptionPane.showMessageDialog(null," No Charge Selected");
                       
                            
                       
                        }
                        else
                        {
                        new Update(db);
                        }
                    }
                });
    }
      
      
      public void delete() {

        delete.addActionListener(new ActionListener() {
 @Override
                    public void actionPerformed(ActionEvent ae) {

                        if (ae.getSource() ==delete && chrglist.getSelectedValue() == null ) {
                             
                             JOptionPane.showMessageDialog(null," No Charge Selected");
                       
                            
                       
                        }
                        else
                        {
                        int i =((cpt_lacharge)chrglist.getSelectedValue()).code;
                            
                            System.out.println("Dore CODE:----:"+ i);
                            
                            int index = chrglist.getSelectedIndex();
                            listModel.removeElementAt(index);
                            
                            
                            
    db.delete_charge(i);
                        }
                    }
                });
    }
      
      
      
      
      
      public static cpt_lacharge getselected(){
       
        int c =((cpt_lacharge)chrglist.getSelectedValue()).code;
        String cc =((cpt_lacharge)chrglist.getSelectedValue()).name;
        String co =((cpt_lacharge)chrglist.getSelectedValue()).NOM_COMPTE;
        String i =((cpt_lacharge)chrglist.getSelectedValue()).compte;
        String p =((cpt_lacharge)chrglist.getSelectedValue()).payable;
        String t =((cpt_lacharge)chrglist.getSelectedValue()).type;
        double va=((cpt_lacharge)chrglist.getSelectedValue()).valeur;
        
        
        cpt_lacharge get=new cpt_lacharge(c,cc,i,t,va,p,co);
        return get;
    
    }

    
      
   
}
