/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package HR;

import java.util.Arrays;


public class Compte {

        public int classe, nombreDemois,moisInvoiced, moisCompta , moisPayed , moisToBill, moisToPay ;
    public String cpteParent,numCompte,nomCompte;
    public String[] cpt;
    public String category,ext;
    public double montant;
    public double montantDebit;
    public double montantCredit;
    public double soldeOpen, toPay;
        public double montantInvoice;
    public double montantRefund;
     
    public Compte(String numCompte, String nomCompte, String cpteParent, 
                int classe,String cat,String ext) {
        this.numCompte = numCompte;
        this.nomCompte = nomCompte;
        this.classe = classe;
        this.ext=ext;
        this.cpteParent = cpteParent;
        this.category=cat;
    }
    
    public Compte(String numCompte, String nomCompte, String cpteParent, 
                int classe,String ext ) {
        this.numCompte = numCompte;
        this.nomCompte = nomCompte;
        this.classe = classe;
        this.ext=ext;
        this.cpteParent = cpteParent; 
    }


    public String toString2() {
        return "Compte{" + "classe=" + classe + ", cpteParent=" + cpteParent + ", numCompte=" + numCompte + ", "
                + "nomCompte=" + nomCompte + ", cpt=" + Arrays.toString(cpt) + ", category=" + category + ", montant=" + 
                montant + ", montantDebit=" + montantDebit + ", montantCredit=" + montantCredit + '}';
    }
    
  
    

    @Override
    public String toString() {
        return nomCompte + "*****" + numCompte  ;
    } 
    
    
     public String toString1() {
        return nomCompte + "*****" + numCompte+ "*****" + montantDebit+ "*****"+montantCredit+ "*****"+soldeOpen+ "*****"+category+ "*****"+classe;
    } 
    
    public Compte(String numCompte, String nomCompte, int classe, String cpteParent,
            String category,double montantDebit,double montantCredit) {
        this.numCompte = numCompte;
        this.nomCompte = nomCompte;
        this.classe = classe;
        this.cpteParent = cpteParent;
        this.category = category;     
        this.montantDebit=montantDebit;
        this.montantCredit=montantCredit;
    }
    
    
    public Compte(String numCompte, String nomCompte, int classe, String cpteParent, String category) {
        this.numCompte = numCompte;
        this.nomCompte = nomCompte;
        this.classe = classe;
        this.cpteParent = cpteParent;
        this.category = category;     
    }
    

    
    
    
//
//public String getIdCompte() {
//return numCompte;
//  }
//public void setIdCompte(String numCompte) {
//this.numCompte = numCompte;
//    }
//
//public String getCompte() {
//return nomCompte;
//    }
///**
// * @param compte the compte to set
// */
//public void setCompte(String nomCompte) {
//        this.nomCompte = nomCompte;
//    }
///**
// * @return the libelle
// */
//public int getClasse() {
//return classe;
//    }
// /**
// * @param libelle the libelle to set
//  */
//public void setLibelle(int classe) {
//this.classe = classe;
// }
//
//
//
//public Cpt[] selectCpte(){
//
//Cpt[] cpte=new Cpt [100];
//
//try{
//db.read();
//db.outResult = db.s.executeQuery("select*  from APP.COMPTE "   );// guselectiona muri sbase sql
//
//int i=0;
//
//while (db.outResult.next())// genda ushyiramo amadonne
//{
// cpte [i]=new Cpt( db.outResult.getString(1),db.outResult.getString(2),db.outResult.getInt(3));
//
//
// i++; // jyenda wongera imirongo(row) y'amadonne
//
//    }
//
//}
//catch(Throwable e)
//
//{
////db.insertError(e+"","error inserting code");
////e.printStackTrace();
//}
//return cpte;
//}
}
