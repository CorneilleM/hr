/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package HR;

public class Tiers implements Comparable {

    public String numTiers, numCompte, designation, info, adresse, devise,email, sigleTiers;
    public String numAffilie,numExt;
    int salaireBrute;
 
    public Tiers(String numTiers, String numCompte, String designation, String info, 
            String adresse,String email, String devise, String sigleTiers, String numAffilie,
            int salaireBrute,String numExt) {
        this.adresse = adresse;
        this.designation = designation;
        this.devise = devise;
        this.info = info;
        this.numCompte = numCompte;
        this.email=email;
        this.numTiers = numTiers;
        this.sigleTiers = sigleTiers;
        this.salaireBrute = salaireBrute;
        this.numExt=numExt;
        this.numAffilie=numAffilie;
    }
 
    @Override
    public String toString() {
        String k = "" + designation + " " + numTiers;
        return k;
    }

//    @Override
    public String toString3() {
        return "Tiers{" + "numTiers=" + numTiers + ", numCompte=" + numCompte + ", designation=" + designation + ", info=" 
                + info + ", adresse=" + adresse + ", devise=" + devise + ", sigleTiers=" + sigleTiers + ", numAffilie=" + numAffilie
                + ", salaireBrute=" + salaireBrute + '}';
    }
    
    
    
    
    public String toString2() {
        String k = "" + salaireBrute;
        return k;
    }

     public String tS4() {
        String k = designation+" " + salaireBrute;
        return k;
    }
    
    @Override
    public int compareTo(Object o) {
        return designation.compareTo(((Tiers) o).designation);
    }
}
