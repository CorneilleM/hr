package HR;

public class Employe {

    public int ID_EMPLOYE, CATRE_INDETITE, LEVEL_EMPLOYE, TEL_EMPLOYE;
    public String NOM_EMPLOYE, PRENOM_EMPLOYE, BP_EMPLOYE, TITRE_EMPLOYE, PWD_EMPLOYE,LANG,EMAIL;

    public Employe(int ID_EMPLOYE, int CATRE_INDETITE, int LEVEL_EMPLOYE, int TEL_EMPLOYE,
            String NOM_EMPLOYE, String PRENOM_EMPLOYE, String BP_EMPLOYE, String TITRE_EMPLOYE,
            String PWD_EMPLOYE,String LANG,String EMAIL) {
        this.ID_EMPLOYE = ID_EMPLOYE;
        this.CATRE_INDETITE = CATRE_INDETITE;
        this.LEVEL_EMPLOYE = LEVEL_EMPLOYE;
        this.TEL_EMPLOYE = TEL_EMPLOYE;
        this.NOM_EMPLOYE = NOM_EMPLOYE;
        this.PRENOM_EMPLOYE = PRENOM_EMPLOYE;
        this.BP_EMPLOYE = BP_EMPLOYE;
        this.TITRE_EMPLOYE = TITRE_EMPLOYE;
        this.PWD_EMPLOYE = PWD_EMPLOYE;
        this.LANG=LANG;
        this.EMAIL=EMAIL;
    }

    public boolean check(int id, int carte) {
        return id == ID_EMPLOYE && CATRE_INDETITE == carte;
    }

    @Override
    public String toString() {
        return "" + NOM_EMPLOYE ;
    }
}
