
package HR;

public class Variable {

    public String nom, value, value2, value3, famille;

    public Variable(String nom, String value, String famille) {
        
        this.nom = nom;
        this.value = value;
        this.famille = famille;
        
    }

    public Variable(String nom, String value, String famille, String value2) {
        
        this.nom = nom;
        this.value = value;
        this.famille = famille;
        this.value2 = value2;

    }
    
    public Variable(String nom, String value, String famille, String value2, String value3) {
        this.nom = nom;
        this.value = value;
        this.famille = famille;
        this.value2 = value2;
        this.value3 = value3;
    }

    @Override
    public String toString() {
        return value;
    }


    public String toString2() {
        return "Variable{" + "nom=" + nom + ", value=" + value + ", value2=" + value2 + 
                ", value3=" + value3 + ", famille=" + famille + '}';
    }
    
    
    
}