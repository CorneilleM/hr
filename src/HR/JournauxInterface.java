/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HR;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

/**
 *
 * @author ishyiga
 */
public class JournauxInterface extends JFrame {

    
    Container content;
    
    JTextField ID;
    JTextField NAME;
    JTextField GROUPE;
    JTextField CPTE,RATE ;
    JLabel IDL, NAMEL, GROUPEL, CPTEL,RATEL; 
    JButton saveButton;
    JButton cancelButton;
    JList  deviseList;
    Font police; 
    DbHandler db; 
    Journaux journal;
    public JournauxInterface(DbHandler db, boolean isUpdate,Journaux journal)  
   {
        this.db=db;
        this.journal=journal;
        draw(isUpdate);
    }
    private void draw(boolean isUpdate) {
        
        this.setTitle("Saisie de nouveau compte"); 
        enableEvents(WindowEvent.WINDOW_CLOSING); 
        this.setResizable(false); 
      
        ID=new JTextField();
        NAME=new JTextField();
        GROUPE=new JTextField();
        CPTE=new JTextField();
        RATE=new JTextField("1");
                
        IDL= new JLabel(" CODE JOURNAL ");
        NAMEL= new JLabel(" NAME ");
        GROUPEL= new JLabel(" GROUPE ");
        CPTEL= new JLabel(" COMPTE ");
         RATEL= new JLabel(" EXCHANGE RATE ");
        deviseList=new JList();
        deviseList.setListData(db.getVarListFam("DEVISE").toArray());
      saveButton= new JButton(" SAVE ");
      saveButton.setBackground(Color.GREEN);
      cancelButton= new JButton(" CANCEL"); 
     
        
if(isUpdate && journal!=null)
{ saveButton= new JButton(" UPDATE ");
saveButton.setBackground(Color.YELLOW);
getContentPane().add(UpDatePane(content));  

update();  }
else
{ 
getContentPane().add(laPane(content));
insertjOURNAL();} 




    this.setSize(800, 600);
    setVisible(true);
}
    
public JPanel laPane(Container content) {
        JPanel panelJournal = new JPanel();
        panelJournal.setLayout(new GridLayout(7, 2, 5, 20));//
        panelJournal.setPreferredSize(new Dimension(780, 125));
  
        panelJournal.add(IDL);
        panelJournal.add(ID);
        panelJournal.add(NAMEL);
        panelJournal.add(NAME);
        panelJournal.add(GROUPEL);
        panelJournal.add(GROUPE);
        panelJournal.add(CPTEL);
        panelJournal.add(CPTE); 
        
        panelJournal.add(new JLabel("DEVISE"));
        JScrollPane  scrollPaneList =new JScrollPane(deviseList);
scrollPaneList.setPreferredSize(new Dimension (40,40)); deviseList.setBackground(Color.pink);
 
        panelJournal.add(scrollPaneList);
        panelJournal.add(RATEL);
        panelJournal.add(RATE); 
        panelJournal.add(saveButton);
        panelJournal.add(cancelButton);

        return panelJournal;
    } 

     
     
     
     public JPanel UpDatePane(Container content) {
        
   
        ID.setText("" + journal.id);
        NAME.setText("" + journal.name);
        GROUPE.setText("" + journal.groupe);
        CPTE.setText("" + journal.cpte);
        deviseList.setSelectedValue( journal.devise,true);
           RATE.setText("" + journal.rate); 
        return laPane(content);
    } 
    


    public void insertjOURNAL() {
        
        saveButton.addActionListener((ActionEvent ae) -> {
            if (ae.getSource() == saveButton) {
                
                  System.out.println(ID.getText() +" | "+NAME.getText()+" | "+GROUPE.getText() 
                        +" | "+CPTE.getText() +" | "+ deviseList.getSelectedValue() +" | "+
                        DbHandler.isDouble(RATE.getText())
                        );
                  
                if (   ID.getText() == null ||
                        NAME.getText() == null ||
                        GROUPE.getText() == null ||
                        CPTE.getText() == null ||  ID.getText().equals("") ||
                        NAME.getText().equals("") ||  !DbHandler.isDouble(RATE.getText())||
                        GROUPE.getText().equals("") || deviseList.getSelectedValue()==null||
                        CPTE.getText().equals("") ) {
                    
                    JOptionPane.showMessageDialog(null, " FILL WELL EMPTY FIELD ");
                    
                } else {
                    
                    db.insertJournal(ID.getText(), NAME.getText(), GROUPE.getText(), 
                            CPTE.getText(),(String)deviseList.getSelectedValue(),
                            Double.parseDouble(RATE.getText()));
                    
                    NAME.setText("");
                    GROUPE.setText("");
                    ID.setText("");
                    CPTE.setText("");
                    saveButton.disable();
                }

            }
        });
    }

    public void update() {
        saveButton.addActionListener((ActionEvent ae) -> {
            if (ae.getSource() == saveButton ) {
                
                System.out.println(ID.getText() +" | "+NAME.getText()+" | "+GROUPE.getText() 
                        +" | "+CPTE.getText() +" | "+ deviseList.getSelectedValue() +" | "+
                        DbHandler.isDouble(RATE.getText())
                        );
                
                if (     ID.getText() == null || deviseList.getSelectedValue()==null||
                        NAME.getText() == null ||
                        GROUPE.getText() == null ||
                        CPTE.getText() == null || 
                        ID.getText().equals("") ||
                        NAME.getText().equals("") ||
                        GROUPE.getText().equals("") ||
                        CPTE.getText().equals("") || !DbHandler.isDouble(RATE.getText()) ) {
                    
                    JOptionPane.showMessageDialog(null, " FILL WELL EMPTY FIELD");
                    
                } else {
                    
                    db.updateJournal(ID.getText(), NAME.getText(), GROUPE.getText(),
                            CPTE.getText(),(String)deviseList.getSelectedValue(),RATE.getText() );
                    
                    NAME.setText("");
                    GROUPE.setText("");
                    ID.setText("");
                    CPTE.setText("");
                    saveButton.disable();
                } 
            }   });

    }

    
    
}
