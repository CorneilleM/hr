/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HR;

/**
 *
 * @author AIMABLE
 */
 
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.PopupMenu;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Date;
import java.util.LinkedList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;


public class TiersAnalytics extends JFrame {
 
    private JButton cancel; 
    DbHandler db;  
    Tiers tier;  
    double soldeResInvoice = 0;
    double soldeResCompta = 0; 
    double sommeResDebit = 0;
    double sommeResCredit = 0;
    double sommeResCredit5 = 0;
    String searchPeriodeSup="";
    JTextArea textAgetMM;
    
    public TiersAnalytics(  Tiers tier,  DbHandler db) {

       

        this.tier = tier; 
        searchPeriodeSup=searchPeriodeSup(tier.adresse);
        tier.adresse=tier.adresse+" 00:00:00";
        this.db = db;
        westPane2(); 
       
    }

    private void westPane2( ) {
 Container content = getContentPane();
        JPanel productPane = new JPanel();
        productPane.setPreferredSize(new Dimension(900, 380));

        //JScrollPane jTableList = new JScrollPane(getTableInvoice()); 
        //jTableList.setPreferredSize(new Dimension(300, 380));

        JScrollPane jTableList2 = new JScrollPane(getTableCompta()); 
        jTableList2.setPreferredSize(new Dimension(600, 380));
     
        JScrollPane jTableList3 = new JScrollPane(new JTextArea(getTotal())); 
        jTableList3.setPreferredSize(new Dimension(250, 100));
          textAgetMM=new JTextArea(getMM());
        JScrollPane jTableList4 = new JScrollPane(textAgetMM); 
        jTableList4.setPreferredSize(new Dimension(250, 100));
        
       JTextArea area= new JTextArea(getMMaction());
       area.setBackground(Color.green);
       Font police = new Font("Rockwell", Font.BOLD, 14);
            area.setFont(police);
            
          JScrollPane jTableList5 = new JScrollPane(area); 
        jTableList5.setPreferredSize(new Dimension(250, 100));
        
        //productPane.add(jTableList);
        productPane.add(jTableList2);
        productPane.add(jTableList3);
        productPane.add(jTableList4);
        productPane.add(jTableList5);
        
         content.add(productPane);
        
          this.setSize(1100, 600);
        setTitle(getTotal()); 
        jTableList4Clicked();
        this.setVisible(true);
    }
 
      String getMM()
    {
        
      int mois=  nb_joursBetweenToDate2(tier.adresse,DbHandler.EndTime()+" 23:59:00")/30;
      
       int moisToBill  =0;
      int moisToPay= (int) soldeResCompta;
      
            if(tier.salaireBrute!=0)
            {
                int moisInvoiced=(int)(soldeResInvoice/tier.salaireBrute);
                int moisCompta  =(int)(sommeResDebit  /tier.salaireBrute);
                int moisPayed   =(int)(sommeResCredit /tier.salaireBrute);
                moisToBill  =mois-moisInvoiced;
                moisToPay   =mois-moisPayed;
                 return "MM: "+ mois  
                +"\n INV: "+moisInvoiced  
                +"\n CPT: "+moisCompta  
                +"\n PAYED: "+moisPayed  +" TOBILL: "+moisToBill  +" TOPAY: "+moisToPay ;
            }
            else
            {
                return "MM: "+ mois  
               +"\n NO CONTRACT " 
               +"\n TOBILL: "+moisToBill  
               +"\n TOPAY: "+moisToPay   ;
            }
      
   
          
    }
    
String getMMaction()
    {
        
      int mois=  nb_joursBetweenToDate2(tier.adresse,DbHandler.EndTime()+" 23:59:00")/30;
      int moisToBill  =0;
      int moisToPay= (int) soldeResCompta;
      if(tier.salaireBrute==0)
      { int moisInvoiced=(int)(soldeResInvoice/tier.salaireBrute);
      int moisCompta  =(int)(sommeResDebit  /tier.salaireBrute);
      int moisPayed   =(int)(sommeResCredit5 /tier.salaireBrute);
        moisToBill  =mois-moisInvoiced;
        moisToPay   =mois-moisPayed; 
      }
      
    return " TOBILL: "+DbHandler.setVirgule( moisToBill, 1, 0.005)
              +"\n TOPAY: "+ DbHandler.setVirgule( moisToPay, 1, 0.005) ;
          
    }
     
    String getTotal()
    {
    return tier.tS4() +" \n INV: "+DbHandler.setVirgule( soldeResInvoice, 1, 0.005)  
          +"\n DEB: "+DbHandler.setVirgule( sommeResDebit, 1, 0.005)  
          +"\n CRE: "+DbHandler.setVirgule( sommeResCredit, 1, 0.005)  
          +"\n SOLDE: "+DbHandler.setVirgule( soldeResCompta, 1, 0.005) ;
          
    }
    
 public static int nb_joursBetweenToDate2(String debut,String fin)
{
   Date Omega=mDateSt(fin );
   Date Alfa= mDateSt(debut); 
    
    if(Omega== null || Alfa==null)
    { return 0; }
    else
    { 
        System.out.println(DaysIn(Omega.getTime()) +"   DaysIn(Alfa.getTime())  "+DaysIn(Alfa.getTime()));
        return DaysIn(Omega.getTime())-DaysIn(Alfa.getTime()); }
}  
public static int DaysIn(long time)
{
return (int)(time/1000/60/60/24);
}
 public static Date mDateSt(String comingDate )
{
    //2016-01-01 00;00;00
    //0123456789012345678
    
    if(comingDate==null || comingDate.length()<17 )
    {JOptionPane.showMessageDialog(null, " DATE FORMAT NOT ACCEPTED "+comingDate);
    return null;
    }
    else
    {
        try
        {
    int year=Integer.parseInt(comingDate.substring(0, 4))-1900;
    
    int mm=Integer.parseInt(comingDate.substring(5, 7))-1;
    int dd=Integer.parseInt(comingDate.substring(8, 10));
    
    Date date=new Date(year, mm, dd);
     System.out.println(date +"   datedatedate  "+comingDate);
return date;
        }
        catch (NumberFormatException ex)
                {
                    JOptionPane.showMessageDialog(null, ex+" DATE FORMAT NOT ACCEPTED "+comingDate);
                    return null;
                } 
    
    
    }
//System.out.println(date +" "+date.getTime());
}     
    

private String searchPeriodeSup(String debut) { 
       String de = debut.substring(2, 4) + debut.substring(5, 7) + debut.substring(8, 10); 
                    int debb ;  
                    try { 
                        debb = Integer.parseInt(de); 

                    } catch (NumberFormatException e) {
                        JOptionPane.showMessageDialog(null, "FORMAT ACCEPTE: 2013-01-01 \n   " + e);
                        return "";
                    } 
                        return " AND DATE_DOC_INT >= " + debb  ; 

    }

JTable getTableCompta()
{
    
  
String  toSearch = " where " + " CPT_JOURNAL.NUM_TIERS ='"+ tier.numTiers + "'  "
+ " and postdater=0 AND ETAT <> 'DELETED'   "+searchPeriodeSup+" "
+ " AND (CPT_JOURNAL.COMPTE_CREDIT like '41%' OR CPT_JOURNAL.COMPTE_DEBIT like '41%') ";

LinkedList <Journal>  joList2 = db.getJournalSearch(toSearch  );
int linge = joList2.size(); 
  sommeResDebit = 0;
  sommeResCredit = 0;
  soldeResCompta = 0; 
String[][] s1 = new String[linge][8]; 
for (int x = 0; x < linge; x++) { 
Journal C = joList2.get(x); 
s1[x][0] = "" + C.journaux;
s1[x][1] = "" + C.NumOperation;
s1[x][2] = "" + C.dateDoc;  
s1[x][3] = "" + C.libelle;
s1[x][4] = "" + C.compteDebit;
s1[x][5] = "" + C.compteCredit;
//            s1[x][6] = "" + DbHandler.setVirgule(C.montantDebit, 1, 0.005) ;
//            s1[x][7] = "" +  DbHandler.setVirgule(C.montantCredit, 1, 0.005) ;
s1[x][6] = DbHandler.setVirgule(  C.montantDebit, 1, 0.005) ;
s1[x][7] = DbHandler.setVirgule( C.montantCredit, 1, 0.005) ; 
sommeResDebit = sommeResDebit + C.montantDebit;
sommeResCredit = sommeResCredit + C.montantCredit;

if(C.compteCredit!=null && C.compteCredit.length()>0 && C.compteCredit.charAt(0)== '5')
{
    sommeResCredit5 =sommeResCredit5+ C.montantCredit;
} 
}  

String solde = "    "; 
soldeResCompta = Math.abs(sommeResDebit-sommeResCredit );  
    
JTable jTable2 = new JTable(); 
jTable2.setModel(new javax.swing.table.DefaultTableModel(s1, 
new String[]{"JOURNAL", "OP", "DATE",   "LIBELLE", 
             "CPTE D" , "CPTE C"    , "DEBIT", " CREDIT"}));

jTable2.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
jTable2.getColumnModel().getColumn(3).setPreferredWidth(200);
jTable2.getColumnModel().getColumn(6).setPreferredWidth(100); 
jTable2.getColumnModel().getColumn(7).setPreferredWidth(100); 
jTable2.doLayout();
jTable2.validate();
 
return jTable2;
 
}
      
public void jTableList4Clicked() {
 textAgetMM.addMouseListener(new MouseListener() {
 public void actionPerformed(ActionEvent ae) { }
@Override public void mouseExited(MouseEvent ae) { }
@Override  public void mouseClicked(MouseEvent e) {


JOptionPane.showConfirmDialog(rootPane, "I M IN ");


            } ;
@Override  public void mousePressed(MouseEvent e) { } ;
@Override  public void mouseReleased(MouseEvent e) { } ;
@Override  public void mouseEntered(MouseEvent e) {  }
        });
} 
        
private static class JLabel extends PopupMenu {
 public JLabel(String string) {
        }
    }
     
}

