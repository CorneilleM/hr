
package HR;

import java.sql.Timestamp;

/**
 *
 * @author Odette
 */
public class cpt_lacharge {
    
    
     
    String name_cpt,COMPTE_DEBITER,COMPTE_CREDITER,name_CRD,type, pay, employee,num_tier,designation,charge_name;
    double montant_deb,montant_cred,salaire_base,charge_amount,housing, transport, other,g_budget,ass_rssb ,pension_employee,pension_employer,maternity_employee,maternity_employer,tot_retenu,net_salary;
    String name, compte,payable,NOM_COMPTE;
    double valeur,value,rssb,tpr,medical1,medical2,rssb2,retenu,prime,avance,rama,restauration,solidarite,commission,farg;
    int code;
    Timestamp date;
    double indemnite,netapayer;
    

    public cpt_lacharge(String employee,String COMPTE_DEBITER, String COMPTE_CREDITER,String name_cpt, String name_CRD,double montant_deb, double montant_cred, String num_tiers,String charge_name) {
        
        this.employee = employee;
        this.COMPTE_DEBITER = COMPTE_DEBITER;
        this.COMPTE_CREDITER = COMPTE_CREDITER;
        this.name_cpt=name_cpt;
        this.name_CRD=name_CRD;
        
       this.montant_deb = montant_deb;
        this.montant_cred = montant_cred;
        this.num_tier = num_tiers;
        this.charge_name=charge_name;
    }
    
    
    
    
    
    
    
    
    public cpt_lacharge(int code,String COMPTE_DEBITER ,String COMPTE_CREDITER,double value,String type,String name) {
        
        this.code = code;
        this.COMPTE_DEBITER = COMPTE_DEBITER;
        this.COMPTE_CREDITER = COMPTE_CREDITER;
       this.value = value;
        this.type = type;
        this.name = name;
    }
    
    public cpt_lacharge(String designation,double salaire_base,double housing,double transport, double prime,double indemnite,double g_budget,
            double ass_rssb ,double farg,double pension_employee,double pension_employer,double maternity_employee, double maternity_employer,double tpr,
            double tot_retenu,double net_salary,double medical1 ,double medical2,double avance,double restauration, double solidarite,double netapayer)
    {
    this.designation=designation;
    this.salaire_base=salaire_base;
    this. housing= housing;
    this.transport=transport;
    this.prime=prime;
    this.farg=farg;
    this.g_budget=g_budget;
    this.ass_rssb=ass_rssb;
    this.pension_employee=pension_employee;
    this.pension_employer=pension_employer;
    this.maternity_employee=maternity_employee;
    this.maternity_employer=maternity_employer;
    this.tpr=tpr;
    this.tot_retenu=tot_retenu;
    this.net_salary=net_salary;
    this.medical1=medical1;
    this.medical2=medical2;
    this.avance=avance;
    this.restauration=restauration;
    this.solidarite=solidarite;
    this.netapayer=netapayer;
    
    
    }
    
    //// REVERSE SALARY OBJECTS///////
    
    
    
    
    
    
     public cpt_lacharge(String charge_name,double charge_amount)
     {
     
     this.charge_name=charge_name;
    this.charge_amount=charge_amount;
     
     }
     
   public cpt_lacharge(int code,String name, String compte, String type, double valeur,String payable, String NOM_COMPTE)
    {
     this.NOM_COMPTE=NOM_COMPTE;  
     this.name=name;
    this.code=code;
    this.compte=compte;
    this.type=type;
    this.valeur=valeur;
    this.payable=payable;

  }
   
  
   
  public cpt_lacharge(double rssb,double tpr,double net_salary,double rama,double rssb2,Timestamp date)
     {
     
     this.rssb=rssb;
     this.tpr=tpr;
     this.net_salary=net_salary;
     this.rama=rama;
     this.rssb2=rssb2;
     this.date=date;
     
     
     } 
         
        
  public cpt_lacharge(double avance,double retenu,double prime,Timestamp date)
     {
     
     this.avance=avance;
     this.retenu=retenu;
     this. prime= prime;
     this.date=date;
     
     
    } 
  /*       
    public String toString1() {
 
        return name+"--"+payable;
    }
  */
    
    public String toString() {
    
        return name+"--"+valeur+"---"+payable+"--"+type;
    } 
    
    
}
