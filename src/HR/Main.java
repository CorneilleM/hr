package HR;
import static HR.DbHandler.kohereza;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import javax.swing.*; 
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.MaskFormatter;

public class Main extends JFrame {

    JList allCompteList = new JList(); 
    JList allTiersList  = new JList();
    JList allJournaux   = new JList();
    JList profitJList;
    JButton upDateDocButton,
            conge;
    JButton compteButton;
    JButton salariesButton;
    
    JButton attendance;
    JButton dataButton,journalButton;
    
    JRadioButton RADIO_OPERATION,RADIO_LIBELE;
    
    boolean MODE_RAPPORT=true;   
    boolean MODE_SAISIE=false; 
   
    private JButton resetButton;
    private JButton removeButton;
    private JButton open, save, delete, print;
    private JButton actualise;
    private JButton tiersButton,  voirTierButton, salaireButton;
    private final Properties properties = new Properties();
    JTextArea totalDebit = new JTextArea();
    JTextArea totalCredit = new JTextArea();
    JTextArea totalDebitRES = new JTextArea();
    JTextArea totalCreditRES = new JTextArea();
    JLabel soldeLabel = new JLabel("          ");
    JTable jTable2 = new JTable();
    LinkedList<Journal> joList2;
    LinkedList<Journal> joList2ResTmt;
    LinkedList<Lot> compteResultatD = new LinkedList();
    LinkedList<Lot> compteResultatC = new LinkedList();
    String[][] s1, s2;
    JLabel resLabel = new JLabel("J: JOURNAL \nT: TIER ");
    JLabel montantL = new JLabel("Montant    ");
    JTextField MONTANT = new JTextField(); 
    JTextArea libelle = new JTextArea(5, 2);
    JFormattedTextField dateOperation2;
    private JPanel salePane;
    private JPanel sommePane;
    public static String Title; 
    JPanel commonEasttPane, commonWestPane;
    String[][] s1Compta;
    JTable jTable;
    public static String versionIshyiga="1.0";
     static public String derbyPwd=";user=sa;password=ish";
    private static JFrame frame;
    private JScrollPane scrollPaneCompteList;     // si il ya trop des produits qui depasse le Jpanel
    private JScrollPane scrollPaneallSale, jScrollPane1;        // si il ya trop des operation qui depasse le cadre
    // private double sommeDebit=0,sommeCredit=0;
    private double sommeDebit = 0, sommeCredit = 0, sommeResDebit, sommeResCredit, 
            sommeTotalDebit, sommeTotalCredit, soldeRes;
    double SDebit = 0, SCredit = 0, dette = 0;
    public static boolean isDebit, byname = false;//,isOpen=false;
    
    public static String CURRENT_CURRENCY = "";
     public static String LOCAL_CURRENCY2 = "";
     
    String NUM_JOURNAL,
            LIB, DATEDOC = "",
            DATE_OPERATION,
            TYPE_OPERATION,
            NUM_TIERS, CTIERS;
    String CPTE_DEBIT,
            CPTE_CREDIT;
    int NUM_OPERATION;
    int sousCompte;
    double montantDebit = 0,
            montantCredit = 0;
    String fataCompte;
    String fataTiers;
    String fatacode;
    String dataBase;
    String Server;
    public static Employe umukozi;
    boolean credit = false;
    final int W = 1250, H = 750;
    Cashier cashier;
    Color color;
    Font police;
    Container content;
    LinkedList<Lot> toPrint = new LinkedList();
    LinkedList<Journal> toUpdate = new LinkedList();
//LinkedList<Journal>jolist=new LinkedList();
//LinkedList<Journal>banque=new LinkedList();
    JLabel labelCurrentOp = new JLabel(" N° OPERATION: ");
    Compte[] control2;
    Tiers t;
    CodeJournal codejr;
//Employe umukozi;
    //LinkedList<Variable> lesProfitCenter = new LinkedList();
    JTextField search = new JTextField();
    JTextField searchTier = new JTextField();
    JTextField searchCompte = new JTextField();
    int today;
    String datedoc = "";
    JTable jTable5 = new JTable();
    JRadioButton datePeriode = new JRadioButton("PERIODE");
    JFormattedTextField deb,fin;
   
    double totalCharges = 0, totalProduits = 0, totalActif = 0, totalPassif = 0;
    JTable jTable3 = new JTable();
    JTable jTable4 = new JTable();
    
    String modeTiersViewMode = "APPARITION";
    Thread thread, thread2;
    int   expLength=0;
    public static String lesCats;
    public static String LANG="FRA";
    String LibelleLettrage="NOPE";
    Journal lautre;
    
    Main(Employe umukozi, Cashier c, String Server, String dataBase) {
        //this.umukozi=umukozi;
        this.dataBase = dataBase;
        this.Server = Server;
        Main.umukozi = umukozi;
        cashier = c; 
        datePeriode.setSelected(true); 
        draw(); 
        datedoc = Cashier.getDate();  
      Main.lesCats=cashier.db.getCategorieCompte();     
/*
        if(DbHandler.isPackageImport(cashier.db.state))
        {
            importAuto();
        }*/
    }

    
// on dessine les composants d@43
    private void draw() {

        content = getContentPane();
        int w = 500;
        gauchePane(content, 240);
        JPanel hagati =centralPane( w   , (H-50) /2);  
        JPanel res =resPane( 1000, (H-50)/2);
        
        JPanel center = new JPanel();
        //center.setPreferredSize(new Dimension(w-240, H));
        center.setLayout(new GridLayout(2, 1));
        
        center.add(hagati);
        center.add(res);
        
        content.add(center, BorderLayout.CENTER);
        
        
        showCompte();
        //addDebit();
        //addCredit();
        // actualise(); 
        compte();
        save();
        print();
        delete();
       // merge();
        updateDoc();

        journauxClicked();
        tiersClicked();
        jtable2Clicked();
        jtable3Clicked();
        jtable4Clicked();
        jtableClicked();
        profitClicked();
        compteprofitClicked();
        
        removeLine();
        tiers();
        data();
        data3();
        
        all();
       
        superSearchIt();
        searchTier();
        searchCompte();
        numOperRadioClicked();
        libRadioClicked();
//    Azero();
//    salaire();
        montantAkadomo();
      
        journalButton(); 
        
        Main.Title=umukozi + "'s Ishyiga HR session : "+versionIshyiga;
        setTitle(Main.Title);
        
        enableEvents(WindowEvent.WINDOW_CLOSING);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(W, H);
        setVisible(true);

    }

    /**
     * *******************************************************************************************
     * partie west du programme
     *
      * @param w
     * @param h
     */
    
    
    public JPanel centralPane(  int w, int h) {
  JPanel center = new JPanel();
  center.setPreferredSize(new Dimension(w, h));
  center.setLayout(new GridLayout(1, 2));
  JPanel upCenterPane = new JPanel();
  upCenterPane.setPreferredSize(new Dimension(w, h ));
  int tiersW = 150;
  JPanel tier = tierPanel(tiersW, (h ));
  JPanel comptabilitePaneByose = new JPanel();
  comptabilitePaneByose.setPreferredSize(new Dimension(w - tiersW, h ));
  comptabilitePaneByose.setLayout(new GridLayout(1, 1));
  int HH = (h / 4);
  JPanel comptabilitePaneUp = new JPanel();
  comptabilitePaneUp.setPreferredSize(new Dimension(w - tiersW, HH-100));
  JPanel cptPane = new JPanel();
  cptPane.setSize(new Dimension(w, 30));
  cptPane.setPreferredSize(new Dimension(w - tiersW - 5, 30));
  cptPane.setLayout(new GridLayout(1, 2, 2, 0));
  compteButton = new JButton("COMPTE");
  police = new Font("Rockwell", Font.BOLD, 14);
  compteButton.setFont(police);
  color = new Color(255, 175, 160);
  compteButton.setBackground(color);
  cptPane.add(compteButton, BorderLayout.CENTER);
  cptPane.add(searchCompte, BorderLayout.EAST);
  comptabilitePaneUp.add(cptPane, BorderLayout.NORTH);
  JPanel centerPaneN = new JPanel();
  centerPaneN.setSize(new Dimension(w - tiersW - 5, 400));
  scrollPaneCompteList = new JScrollPane(allCompteList);
  scrollPaneCompteList.setPreferredSize(new Dimension(w - tiersW - 5, 125));
  allCompteList.setBackground(new Color(255, 175, 160));
  centerPaneN.add(scrollPaneCompteList, BorderLayout.NORTH);
  comptabilitePaneUp.add(centerPaneN, BorderLayout.CENTER);
  comptabilitePaneByose.add(comptabilitePaneUp);
  upCenterPane.add(tier);
  upCenterPane.add(comptabilitePaneByose);
  center.add(upCenterPane);
  center.add(centerDownPane(  h ));
return center;
}
    
    
    public void gauchePane(Container content, int w) {
 try {
        int k = 20;
        int v2 = 100;
        JPanel paneWest = new JPanel();
        paneWest.setSize(new Dimension(150, 200));
        paneWest.setLayout(new GridLayout(3, 1));

        JScrollPane scrollPaneallJournaux = new JScrollPane(allJournaux);
        journalButton= new JButton("JOURNAL");
        journalButton.setBackground(new Color(250, 250, 160)); 
        journalButton.setFont(police);
        
        scrollPaneallJournaux.setPreferredSize(new Dimension((w - k) / 2, 350));
        allJournaux.setBackground(new Color(222, 250, 160));

        profitJList = new JList();
        JScrollPane scrollPaneallProfit = new JScrollPane(profitJList);
        scrollPaneallProfit.setPreferredSize(new Dimension((w - k) / 2, 200));

        JPanel commonCenterPaneJournauz = new JPanel();
        commonCenterPaneJournauz.setSize(new Dimension((w - v2) / 2, 30));
        commonCenterPaneJournauz.setLayout(new GridLayout(2, 1));
        ////////////////////////////////////////////////////

        JPanel jx = new JPanel();
        jx.setSize(new Dimension((w - v2) / 2, 30));
        jx.setLayout(new GridLayout(1, 1));

        JPanel jxPane = new JPanel();
        jxPane.setSize(new Dimension(w, 30));
        jxPane.setPreferredSize(new Dimension((w - v2) / 2, 30));
        jxPane.setLayout(new GridLayout(4, 1, 2, 0));
        
            Font policeDate = new Font("Consolas", Font.BOLD, 16);
            MaskFormatter exp2;
       
            exp2 = new MaskFormatter("####-##-##");
             deb  = new JFormattedTextField(exp2);
            deb.setFont(policeDate);
            deb.setForeground(Color.BLUE);
            deb.setText(DbHandler.startYear()); 
            
            fin  = new JFormattedTextField(exp2);
            fin.setFont(policeDate);
            fin.setForeground(Color.BLUE);
            fin.setText(DbHandler.EndTime());  
            
        jxPane.add(journalButton );
        jxPane.add(datePeriode );
        jxPane.add(deb);
        jxPane.add(fin);
        
       
           
        jx.add(jxPane, BorderLayout.NORTH);
       // jx.add(scrollPaneallProfit , BorderLayout.SOUTH);

        ////////////////////////////////////////////////////////
        
        //commonCenterPaneJournauz.add(new JLabel("PROFIT CENTER"));
       //  commonCenterPaneJournauz.add(journalButton );
        commonCenterPaneJournauz.add(scrollPaneallJournaux);
        commonCenterPaneJournauz.add(jx);
        paneWest.add(commonCenterPaneJournauz);

        paneWest.add(buttonEast(w));

        content.add(paneWest, BorderLayout.WEST);
         } catch (ParseException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * *******************************************************************************************
     * partie west du programme
     *
     * @param w
     * @param h 
     * *******************************************************************************************
     * @return 
     */
    public JPanel tierPanel(int w, int h) {

        JScrollPane scrollPaneTierList = new JScrollPane(allTiersList);
        scrollPaneTierList.setPreferredSize(new Dimension((w), h - 50));

        allTiersList.setBackground(new Color(222, 210, 160));
        tiersButton = new JButton("TIER");
        tiersButton.setBackground(new Color(222, 210, 160));
        tiersButton.setPreferredSize(new Dimension(80, 20));
        


        JPanel commonCenterPaneTiers = new JPanel();
        commonCenterPaneTiers.setPreferredSize(new Dimension((w), h));

        JPanel tPane = new JPanel();
        tPane.setSize(new Dimension(w, 30));
        tPane.setPreferredSize(new Dimension(w, 30));
        tPane.setLayout(new GridLayout(1, 2, 2, 0));

        tPane.add(tiersButton, BorderLayout.WEST);
        tPane.add(searchTier, BorderLayout.EAST);


        commonCenterPaneTiers.add(tPane, BorderLayout.NORTH);
        commonCenterPaneTiers.add(scrollPaneTierList, BorderLayout.SOUTH);

        return commonCenterPaneTiers;
    }

    public JPanel buttonEast(int w) {

        JPanel commonEastPane = new JPanel();
        commonEastPane.setPreferredSize(new Dimension(100, 200));
        commonEastPane.setLayout(new GridLayout(4, 1));
 

        conge = new JButton("CONGE");
        police = new Font("Rockwell", Font.BOLD, 14);
        conge.setFont(police);
        conge.setBackground(Color.ORANGE);

        salariesButton = new JButton("SALAIRIES");
        police = new Font("Rockwell", Font.BOLD, 14);
        salariesButton.setFont(police);
        color = new Color(40, 200, 255);//(r,g,b)
        salariesButton.setBackground(color);

        attendance = new JButton("ATTENDANCE");
        police = new Font("Rockwell", Font.BOLD, 14);
        attendance.setFont(police);
        color = new Color(255, 250, 55);//(r,g,b)
        attendance.setBackground(color);

        dataButton = new JButton("DATA");
        police = new Font("Rockwell", Font.BOLD, 14);
        dataButton.setFont(police);
        color = new Color(192, 155, 0);//(r,g,b)
        dataButton.setBackground(color);

        commonEastPane.add(salariesButton);
        commonEastPane.add(conge);
        commonEastPane.add(attendance);
        commonEastPane.add(dataButton);

        return commonEastPane;

    }

    public JPanel centerDownPane(  int h) {
        int k = 500;
        commonEasttPane = new JPanel();
//commonEasttPane.setPreferredSize(new Dimension (w-k,350)); 

        JPanel upPane = new JPanel();
        upPane.setPreferredSize(new Dimension( k, 30));

        JPanel upPaneLABEL = new JPanel();
        upPaneLABEL.setPreferredSize(new Dimension( k, 30));
        upPaneLABEL.setLayout(new GridLayout(1, 1));

        resetButton = new JButton(" ACTUALISE ");
        resetButton.setBackground(Color.red);
        removeButton = new JButton("ONE");
        removeButton.setBackground(Color.MAGENTA);
        upDateDocButton = new JButton("ACT");
        upDateDocButton.setBackground(Color.lightGray);

        upPane.add(resetButton, BorderLayout.WEST);
        //upPane.add(upDateDocButton, BorderLayout.CENTER);
        //upPane.add(removeButton, BorderLayout.EAST);

       // upPaneLABEL.add(labelCurrentOp);
        upPaneLABEL.add(upPane);

        salePane = new JPanel();
        salePane.setPreferredSize(new Dimension(k, h));

        jTable = facture();
        jTable.setGridColor(Color.PINK);
//            scrollPaneallSale =new JScrollPane(saleJList);
        scrollPaneallSale = new JScrollPane(jTable);
        scrollPaneallSale.setPreferredSize(new Dimension( k, h - 100));
        jTable.setBackground(Color.white);

        sommePane = new JPanel();
        sommePane.setPreferredSize(new Dimension(k, 80));
        // sommePane.setLayout(new GridLayout(1,8,0,0));

        JLabel debitLabel = new JLabel("SD");
        debitLabel.setFont(new Font("Algerian", Font.PLAIN, 12));
        totalDebit.setFont(new Font("Helvetica", Font.PLAIN, 12));
        totalDebit.setText(montantDebit + "  " +CURRENT_CURRENCY);

        JLabel creditLabel = new JLabel("SC");
        creditLabel.setFont(new Font("Algerian", Font.PLAIN, 12));
        totalCredit.setFont(new Font("Helvetica", Font.PLAIN, 12));
        totalCredit.setText(montantCredit + "  " +CURRENT_CURRENCY);

        JPanel buttons = new JPanel();
        buttons.setLayout(new GridLayout(1, 3));

        save = new JButton("SAVE");
        color = new Color(136, 136, 136);//(r,g,b
        save.setBackground(new Color(105, 200, 155));
        delete = new JButton("DELETE");
        delete.setBackground(new Color(255, 100, 55));
        print = new JButton("PRINT");
        print.setBackground(Color.pink);
        //merge = new JButton("MERGE");
        //merge.setBackground(Color.white);
        buttons.add(save);
        buttons.add(delete);
        buttons.add(print);
       // buttons.add(merge);

        sommePane.add(debitLabel, BorderLayout.WEST);
        sommePane.add(totalDebit, BorderLayout.EAST);
        sommePane.add(creditLabel, BorderLayout.WEST);
        sommePane.add(totalCredit, BorderLayout.EAST);
        sommePane.add(buttons, BorderLayout.EAST);

        salePane.add(upPaneLABEL, BorderLayout.NORTH);
        salePane.add(scrollPaneallSale, BorderLayout.CENTER);
        salePane.add(sommePane, BorderLayout.SOUTH);


        return salePane;
    }

    public JPanel resPane(  int w, int h) {

        salePane = new JPanel();
        salePane.setPreferredSize(new Dimension(w, h));
        RADIO_OPERATION = new JRadioButton("OP");
       
        RADIO_LIBELE = new JRadioButton("LIBELE");
        
        JLabel l =new JLabel(" Order By:");
        
        JPanel lesRadios = new JPanel();
        lesRadios.setLayout(new GridLayout(1, 3));
        lesRadios.add(l);
        lesRadios.add(RADIO_OPERATION);
        lesRadios.add(RADIO_LIBELE);
        
        
        
        jTable2.setGridColor(Color.PINK);
//      scrollPaneallSale =new JScrollPane(saleJList);
        JScrollPane scrollPaneallSale2 = new JScrollPane(jTable2);
        scrollPaneallSale2.setPreferredSize(new Dimension(w, h - 100));
        jTable2.setBackground(Color.white);

        JPanel salePaneUp = new JPanel();
        salePaneUp.setPreferredSize(new Dimension(w, 30));
        salePaneUp.setLayout(new GridLayout(1, 2));

        salePaneUp.add(resLabel, BorderLayout.NORTH);
        salePaneUp.add(search, BorderLayout.NORTH);

        JLabel debitLabel = new JLabel("SD");
        debitLabel.setFont(new Font("Algerian", Font.PLAIN, 20));
        totalDebitRES.setFont(new Font("Helvetica", Font.PLAIN, 20));
        totalDebitRES.setText(0 + "  " +LOCAL_CURRENCY2);

        JLabel creditLabel = new JLabel("SC");
        creditLabel.setFont(new Font("Algerian", Font.PLAIN, 20));
        totalCreditRES.setFont(new Font("Helvetica", Font.PLAIN, 20));
        totalCreditRES.setText(0 + "  " +LOCAL_CURRENCY2);

        sommePane = new JPanel();
        sommePane.setPreferredSize(new Dimension(w, 60));
        sommePane.setLayout(new GridLayout(2, 1));

        JPanel sommePane2 = new JPanel();
        sommePane2.setPreferredSize(new Dimension(w, 30));

        sommePane2.add(new JLabel("TOTAL "+Main.LOCAL_CURRENCY2), BorderLayout.EAST);
        sommePane2.add(debitLabel, BorderLayout.WEST);
        sommePane2.add(totalDebitRES, BorderLayout.EAST);
        sommePane2.add(creditLabel, BorderLayout.WEST);
        sommePane2.add(totalCreditRES, BorderLayout.EAST);
sommePane2.add(soldeLabel, BorderLayout.EAST);
        sommePane.add(sommePane2);
       // sommePane.add(soldeLabel, BorderLayout.WEST);



        salePane.add(salePaneUp, BorderLayout.NORTH);
        salePane.add(lesRadios, BorderLayout.NORTH);
        salePane.add(scrollPaneallSale2, BorderLayout.CENTER);
        salePane.add(sommePane, BorderLayout.SOUTH);

        

        return salePane;
    }

    private JTable facture() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable = new javax.swing.JTable();

        //int ligne=cashier.current.getOperation().size();
        int ligne = cashier.jolist.size();

        String[][] s11 = new String[ligne][8];

        for (int x = 0; x < ligne; x++) {
            // Journal C =cashier.current.getOperation().get(x);

            Journal C = cashier.jolist.get(x);
            s11[x][0] = "" + C.NumOperation;
            s11[x][1] = C.numTiers;
            s11[x][2] = "" + C.dateOperation;
            s11[x][3] = "" + C.compteDebit;
            s11[x][4] = "" + C.compteCredit;
            s11[x][5] = "" + C.libelle;
            s11[x][6] = "" + C.montantDebit;
            s11[x][7] = "" + C.montantCredit;
            // s1[x][9]=""+C.utilisateur;
        }
        jTable.setModel(new javax.swing.table.DefaultTableModel(s11, new String[]{"CPTE DEBIT", "CPTE CREDIT", "DEBIT", " CREDIT"}));
        jTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        jTable.getColumnModel().getColumn(0).setPreferredWidth(20);
        jTable.getColumnModel().getColumn(1).setPreferredWidth(50);//.setWidth(200);
        jTable.getColumnModel().getColumn(2).setPreferredWidth(50);
        jTable.getColumnModel().getColumn(3).setPreferredWidth(50);
        jTable.doLayout();
        jTable.validate();
        // Class[] types = new Class [] {   java.lang.String.class, java.lang.String.class,java.lang.String.class,       java.lang.String.class,                java.lang.String.class,                java.lang.String.class,                 java.lang.String.class                         };
        return jTable;
    }

    public void showCompte() {

        allCompteList.setListData(cashier.db.selectCpte().toArray());
       // listeLibelle=cashier.db.getLibelles(); 
        allTiersList.setListData(cashier.db.getAllTiers().toArray());
        allJournaux.setListData(cashier.db.cptJournaux().toArray());
        //lesProfitCenter = cashier.db.getParam("PROFIT");
        //profitJList.setListData(lesProfitCenter.toArray());

    }

    void setSelectedCompte(String numCompte) {
        boolean nayibonye = false;
        for (int i = 0; i < cashier.db.allCompte.size(); i++) {

            System.out.println(numCompte + (" ---  " + cashier.db.allCompte.get(i).numCompte));
//           if (numCompte.contains("41") || numCompte.contains("40") || numCompte.contains("42")
//                    || numCompte.equals(("" + cashier.db.allCompte.get(i).numCompte)))  
            if (numCompte.equals(("" + cashier.db.allCompte.get(i).numCompte))) {
                nayibonye = true;
                allCompteList.setSelectedIndex(i);
                System.out.println(i + (" ----------------------------------------------------  " + cashier.db.allCompte.get(i).numCompte));
            }

        }
        if (!nayibonye) {
            JOptionPane.showMessageDialog(frame, numCompte + "  N'est pas dans la liste",
                    "Annomalie", JOptionPane.WARNING_MESSAGE);
        }
    }

    void setSelectedTiers(String NUMTIER) {
        boolean nayibonye = false;
        // System.out.println("nahageze  size tiers"+ cashier.db.allTiers.size()+" ndashaka "+num);        
        for (int i = 0; i < cashier.db.allTiers.size(); i++) {
            //    System.out.println(cashier.db.allTiers.get(i).numTiers);
            if (cashier.db.allTiers.get(i).numTiers.equals(NUMTIER)) {
                nayibonye = true;
                allTiersList.setSelectedIndex(i);
                break;
            }
        }
        if (!nayibonye) {
            JOptionPane.showMessageDialog(frame, NUMTIER + "  N'est pas dans la liste",
                    "Annomalie", JOptionPane.WARNING_MESSAGE);
        }

    }

    void setSelectedJournaux(String designation) {
        boolean nayibonye = false;
        for (int i = 0; i < cashier.db.allJournaux.size(); i++) {
            //System.out.println(cashier.db.allTiers.get(i).sigleTiers);
            if (cashier.db.allJournaux.get(i).id.equals(designation)) {
                nayibonye = true;
                allJournaux.setSelectedIndex(i);
                break;
            }
        }
        if (!nayibonye) {
            JOptionPane.showMessageDialog(frame, designation + "  N'est pas dans la liste",
                    "Annomalie", JOptionPane.WARNING_MESSAGE);
        }

    }

    public void jtable2Clicked() {

        jTable2.addMouseListener(new MouseListener() { 
            public void actionPerformed(ActionEvent ae) { }
            @Override public void mouseExited(MouseEvent ae) { }
@Override             public void mouseClicked(MouseEvent e) {
                if (allJournaux.getSelectedValue() != null) {

                    Journaux jour = (Journaux) allJournaux.getSelectedValue();
                    Main.CURRENT_CURRENCY=jour.devise;
                    if (jTable2.getSelectedRow() != -1 && cashier.jolist.isEmpty()) {

                        int ope = joList2.get(jTable2.getSelectedRow()).NumOperation;

                        System.out.println(".......selected operation # : " + ope + " ..... journal = " + joList2.get(jTable2.getSelectedRow()).journaux + " /////// " + jour.id);
//                        cashier.jolist = cashier.db.getOperation(ope, jour.id);
                        cashier.jolist = cashier.db.getOperation(ope, joList2.get(jTable2.getSelectedRow()).journaux);

                        save.setText("UPDATE");

                        save.setBackground(Color.YELLOW);
                        setData();
                        labelCurrentOp.setText("UPDATING OPERATION " + jour.id + ": " + ope);

                    } else if (jTable2.getSelectedRow() != -1) {

                        int n = JOptionPane.showConfirmDialog(frame, "DO YOU WANT TO CLOSE : " + labelCurrentOp.getText(), "MESSAGE", JOptionPane.YES_NO_OPTION);

                        if (n == 0) {

                            int ope = joList2.get(jTable2.getSelectedRow()).NumOperation;
                            cashier.jolist = cashier.db.getOperation(ope, jour.id);
                            setData();
                            labelCurrentOp.setText("UPDATING OPERATION " + jour.id + ": " + ope);

                        }

                    }
                } else {
                    JOptionPane.showMessageDialog(null, "VEUILLEZ SELECTIONNER UN JOURNAL !! ");
                }
            }

            ;
@Override   public void mousePressed(MouseEvent e) {  }   ;
@Override  public void mouseReleased(MouseEvent e) {  }  ;
@Override   public void mouseEntered(MouseEvent e) {  }  });
    }

public void jtable3Clicked() { 
        jTable3.addMouseListener(new MouseListener() { 
          public void actionPerformed(ActionEvent ae) {  }
@Override public void mouseExited(MouseEvent ae) { }
@Override public void mouseClicked(MouseEvent e) { 
                LinkedList<Lot> compteResultatArrivage = new LinkedList<>(); 
                compteResultatArrivage = compteResultatD;

                if (jTable3.getSelectedRow() != -1) {

                    String NumCompte = s1[jTable3.getSelectedRow()][0];

                    System.out.println(".......selected account # : " + NumCompte);

                    save.setText("UPDATE");

                    save.setBackground(Color.YELLOW);

                    setData33(NumCompte);

                }
            } ;
@Override public void mousePressed(MouseEvent e) {  } ;
@Override public void mouseReleased(MouseEvent e) {  }  ;
@Override public void mouseEntered(MouseEvent e) { }  });
    }

    public void jtable4Clicked() {

        jTable4.addMouseListener(new MouseListener() {

            public void actionPerformed(ActionEvent ae) {
            }
@Override
            public void mouseExited(MouseEvent ae) {
            }
@Override
            public void mouseClicked(MouseEvent e) {

                if (jTable4.getSelectedRow() != -1) {

                    String NumCompte = s2[jTable4.getSelectedRow()][0];

                    System.out.println(".......selected account # : " + NumCompte);

                    save.setText("UPDATE");

                    save.setBackground(Color.YELLOW);

                    setData33(NumCompte);

                }
            }

            ;
@Override
            public void mousePressed(MouseEvent e) {
            }

            ;
@Override
            public void mouseReleased(MouseEvent e) {
            }

            ;
@Override
            public void mouseEntered(MouseEvent e) {
            }
        });
    }

    public void jtableClicked() {
        jTable.addMouseListener(new MouseListener() {

            public void actionPerformed(ActionEvent ae) {
            }
 @Override
            public void mouseExited(MouseEvent ae) {
            }
 @Override
            public void mouseClicked(MouseEvent e) {

                if (jTable.getSelectedRow() != -1) {

                    if (labelCurrentOp.getText().contains("BILAN")) {

                        Journal jo = cashier.jolist.get(jTable.getSelectedRow());



                        if (jo.compteDebit != null && jo.compteDebit.length() > 1) {
                            setData4(jo.compteDebit);
                        } else {
                            setData4(jo.compteCredit);
                        }

                    } else {
                        Journal jo = cashier.jolist.get(jTable.getSelectedRow());

                        System.out.println("++++++++ " + jo.toString());

                        if (jo.montantDebit != 0) {
                            isDebit = true;
                            MONTANT.setText(setVirgule("" + (int) jo.montantDebit));
                            setSelectedCompte(jo.compteDebit);

                           // System.out.println("%%%%%%%%%" + jo.compteDebit);
                        } else {
                            isDebit = false;
                            MONTANT.setText(setVirgule("" + (int) jo.montantCredit));
                           // System.out.println("%%%%%%%%%" + jo.compteCredit);
                            setSelectedCompte(jo.compteCredit);
                        }
                       // System.out.println("tiers " + jo.numTiers);
                        setSelectedTiers(jo.numTiers);
                        setSelectedJournaux(jo.journaux);
 //Tiers ti=cashier.db.getTiersDes(jo.numTiers);
//System.out.println("compte "+ti.numCompte);
//allTiersList.setSelectedValue(jo.numTiers, true);

                        libelle.setText(jo.libelle);
                        String D = jo.dateDoc;

//310112
//012345
//                        int yy = Integer.parseInt("20" + D.substring(4, 6)) - 1900;
//                        int mm = Integer.parseInt(D.substring(3, 4)) - 1;
//                        int dd = Integer.parseInt(D.substring(0, 2));
//                        System.out.println(yy + " " + mm + " " + dd);
//
//                        Date d = new Date(yy, mm, dd);
//                        System.out.println(d);
                        dateOperation2.setText(D);
                    }
                }


            }

            ;
@Override
            public void mousePressed(MouseEvent e) {
            }

            ;
@Override
            public void mouseReleased(MouseEvent e) {
            }

            ;
@Override
            public void mouseEntered(MouseEvent e) {
            }
        });
    }

    
    
           

            
            
    public void compteprofitClicked() {

        allCompteList.addMouseListener(new MouseListener() { 
            public void actionPerformed(ActionEvent ae) { } 
            @Override  public void mouseExited(MouseEvent ae) {  }
            @Override  public void mouseClicked(MouseEvent e) {

//                if (allCompteList.getSelectedValue() != null ) {
                setData2();
//                }
//                setData2();

                controlColor("compte");

            } 
            ;
 @Override public void mousePressed(MouseEvent e) { }  ;
 @Override  public void mouseReleased(MouseEvent e) {  }  ;
 @Override   public void mouseEntered(MouseEvent e) { }
        });
    }

    public void profitClicked() {
        profitJList.addMouseListener(new MouseListener() {

            public void actionPerformed(ActionEvent ae) {
            }
@Override
            public void mouseExited(MouseEvent ae) {
            }
@Override
            public void mouseClicked(MouseEvent e) {

                controlColor("profit");

                setData2();

            }

            ;
@Override
            public void mousePressed(MouseEvent e) {
            }

            ;
@Override
            public void mouseReleased(MouseEvent e) {
            }

            ;
@Override
            public void mouseEntered(MouseEvent e) {
            }
        });
    }

    public void tiersClicked() {

        allTiersList.addMouseListener(new MouseListener() {
        public void actionPerformed(ActionEvent ae) { }
       @Override public void mouseExited(MouseEvent ae) {  }
      @Override  public void mouseClicked(MouseEvent e) {
 if(MODE_RAPPORT)
          {  
          String choix; 
                choix = (String) JOptionPane.showInputDialog(null, " MAKE A CHOICE !",
                        "ACTIVITÉS DU TIERS", JOptionPane.QUESTION_MESSAGE, null,
                        new String[]{"APPARITION", "COMPTE","Modifier Tiers"}, "");

            switch (choix) {
                case "APPARITION":
                    modeTiersViewMode = "APPARITION"; 
                    setData2();
                    break;
                case "COMPTE":
                    modeTiersViewMode = "COMPTE";
                    setData2();
                    break;
                case "Modifier Tiers":
                    if (allTiersList.getSelectedValue() != null) {
                        Tiers tcc = (Tiers) allTiersList.getSelectedValue();
                         
                    TiersInterface tiersInterface = new TiersInterface(cashier.db, tcc);
                         
                        
                    } else {
                        JOptionPane.showMessageDialog(frame, "CHOISISSEZ D'ABORD UN TIERS DANS LA LISTE DES TIERS SVP",
                                "VERIFIEZ VOTRE SELECTION !", JOptionPane.WARNING_MESSAGE);
                    }
                    break;
                default:
                    break;
            }
                controlColor("tiers");
            }
        }
            ;
@Override
            public void mousePressed(MouseEvent e) {
            }

            ;
@Override
            public void mouseReleased(MouseEvent e) {
            }

            ;
@Override
            public void mouseEntered(MouseEvent e) {
            }
        });
    }

    public void journauxClicked() {
        allJournaux.addMouseListener(new MouseListener() {

            public void actionPerformed(ActionEvent ae) {
            }
@Override
            public void mouseExited(MouseEvent ae) {
            }
@Override
            public void mouseClicked(MouseEvent e) {

                setData2();
//                if (allJournaux.getSelectedValue() != null) {
//                }
            }

            ;
@Override
            public void mousePressed(MouseEvent e) {
            }

            ;
@Override
            public void mouseReleased(MouseEvent e) {
            }

            ;
@Override
            public void mouseEntered(MouseEvent e) {
            }
        });

    }

    void controlColor(String action) {
        System.out.println(action);
        if (action.equals("clear")) {
          ////  allJournaux.setBackground(new Color(222, 150, 160));
            allTiersList.setBackground(new Color(222, 210, 160));
            profitJList.setBackground(Color.white);
            allCompteList.setBackground(new Color(255, 175, 160));
        } else if (action.equals("journaux") && allJournaux.getSelectedValue() != null
                && ((Journaux) (allJournaux.getSelectedValue())).id.contains("VENTE")) {
         //   allJournaux.setBackground(new Color(222, 150, 160));
            profitJList.setBackground(Color.GREEN);
        } else if (action.equals("profit")) {
            profitJList.setBackground(Color.white);
            allTiersList.setBackground(Color.GREEN);
        } else if (action.equals("journaux")) {
        //    allJournaux.setBackground(new Color(222, 150, 160));
            allTiersList.setBackground(Color.GREEN);
        } else if (action.equals("tiers")) {
            allTiersList.setBackground(new Color(222, 210, 160));
            allCompteList.setBackground(Color.GREEN);
        } else if (action.equals("compte")) {
            allCompteList.setBackground(new Color(255, 175, 160));
        }

    }

    /**
     * ******************************************************************************
     * QUAND ON CLICK LE BOUTON ONE
     * *****************************************************************************
     */
    public void removeLine() {
        removeButton.addActionListener((ActionEvent ae) -> {
            if (ae.getSource() == removeButton && jTable.getSelectedRow() != -1) {
                if (labelCurrentOp.getText().contains("UPDAT")) {
                    JOptionPane.showMessageDialog(null, "VOUS NE POUVEZ PAS ENLEVER UNE LIGNE");
                } else {
                    Journal p = cashier.jolist.get(jTable.getSelectedRow());
//On enleve par le Code du Produit
cashier.jolist.remove(p);
montantDebit = p.montantDebit * (-1);
montantCredit = p.montantCredit * (-1);
// Mise a jour d interface et du prix
setData();
if (cashier.jolist.isEmpty()) {
    credit = false;
}
                }
            }
        }); 
    }
 
      
    

    /**
     * ******************************************************************************
     * QUAND ON CLICK LE BOUTON DEBIT
     * *****************************************************************************
     */
    

    /**
     * ******************************************************************************
     * QUAND ON CLICK LE BOUTON CREDIT
     * *****************************************************************************
     */
    

    /**
     * ****************************************************************************************************
     * FONCTION POUR DEBITER UN COMPTE
     *
     * @param p
     * ****************************************************************************************************
     */
    
    /**
     * ****************************************************************************************************
     * FONCTION POUR CREDITER UN COMPTE
     *
     * @param p
     * ****************************************************************************************************
     */
    
    public static int getIn(String s, String s1, int i, String def) {
        int entier = -1;

        String in = JOptionPane.showInputDialog(null, s + s1, def);
        boolean done = true;

        while (in != null && i < 3 && done == true) {
            try {
                if (in.equals("")) {
                    i++;
                    in = JOptionPane.showInputDialog(null, s + " Enter a number ", "1");
                } else {
                    Integer in_int = new Integer(in);
                    entier = in_int;
                    done = false;
                    if (entier < 0) {
                        entier = -1;
                    }
                }

            } catch (NumberFormatException e) {
                i++;
                in = JOptionPane.showInputDialog(null, s + " = " + in + "= Not a number; Enter a number ");
                done = true;
            }
        }
        if (i == 3) {
            JOptionPane.showMessageDialog(frame, " Too many try ", " Sorry we have to Exit ", JOptionPane.PLAIN_MESSAGE);

        }

        return entier;
    }

    String setVirgule(double frw) {
        //System.out.println("  bb"+frw);
        StringTokenizer st1 = new StringTokenizer("" + frw);

        String entier = st1.nextToken(".");
//System.out.println(frw);
        String decimal = st1.nextToken("").replace(".", "");

        if (decimal.length() == 1 && decimal.equals("0")) {
            decimal = ".00";
        } else if (decimal.length() == 1 && !decimal.equals("0")) {
            decimal = "." + decimal + "0";
        } else {
            decimal = "." + decimal.substring(0, 2);
        }

        String setString = entier + decimal;

        int l = setString.length();
        if (l < 2) {
            setString = "    " + frw;
        } else if (l < 3) {
            setString = "  " + frw;
        } else if (l < 4) {
            setString = "  " + frw;
        }

        int up = 6;
        int ju = up + 3;

        if (l > up && l <= ju) {
            setString = setString.substring(0, l - up) + "," + setString.substring(l - up);
        } else if (l > ju) {
            setString = setString.substring(0, l - 9) + "," + setString.substring(l - 9, l - 6) + "," + setString.substring(l - 6, l);

        }

        return setString;
    }

    String setVirgule(int frw) {

        String setString = "" + frw;
        int l = setString.length();

        if (l > 3 && l <= 6) {
            setString = setString.substring(0, l - 3) + " " + setString.substring(l - 3);
        }

        if (l > 6) {
            String s111 = setString.substring(0, l - 3);
            int sl = s111.length();
            setString = s111.substring(0, sl - 3) + " " + s111.substring(sl - 3) + " " + setString.substring(l - 3);
        }
        return setString;
    }

    String setVirgule(String setString) {

        int l = setString.length();
        if (l > 3 && l <= 6) {
            setString = setString.substring(0, l - 3) + " " + setString.substring(l - 3);
        }
        if (l > 6) {

            String s11111 = setString.substring(0, l - 3);
            int sl = s11111.length();
            setString = s11111.substring(0, sl - 3) + " " + s11111.substring(sl - 3) + " " + setString.substring(l - 3);

        }
        return setString;
    }

    
       public static String setVirgule(double frw, int NEG, double arr, String separ) {

        String sep2 = ".";

        if (separ.equals(".")) {
            sep2 = ",";
        }
//        System.out.println("frw:"+frw);
        if (frw>0 && arr == 0.005) {
            frw = frw + 0.00500000000;
        }
        else if (frw<0 && arr == 0.005) {
            frw = frw - 0.00500000000;
        }
//        System.out.println("frw arre:"+frw);
        BigDecimal big = new BigDecimal(frw);
//        big = big.setScale(2, BigDecimal.ROUND_UP);
        String getBigString = big.toString();
//        System.out.println("big:"+getBigString);
        getBigString = getBigString.replace('.', ';');
//  System.out.println("after;"+getBigString);
        String[] sp = getBigString.split(";");

        String decimal = "";
        if (sp.length > 1) {
            decimal = sp[1];
        }

        if (decimal.equals("") || (decimal.length() == 1 && decimal.equals("0"))) {
            decimal = sep2 + "00";
        } else if (decimal.length() == 1 && !decimal.equals("0")) {
            decimal = sep2 + "" + decimal + "0";
        } else {
            decimal = sep2 + "" + decimal.substring(0, 2);
        }


        String toret = setVirgule(sp[0], separ) + decimal;

        if (NEG == -1 && frw > 0) {
            toret = "-" + toret;
        }

        return toret;
    } 
    
    
    public static String setVirgule2(double frw, int NEG, double arr, String separ) {

        String sep2 = ".";

        if (separ.equals("")) {
            sep2 = ",";
        }
//        System.out.println("frw:"+frw);
        if (arr == 0.005) {
            frw = frw + 0.00500000000;
        }
//        System.out.println("frw arre:"+frw);
        BigDecimal big = new BigDecimal(frw);
//        big = big.setScale(2, BigDecimal.ROUND_UP);
        String getBigString = big.toString();
//        System.out.println("big:"+getBigString);
        getBigString = getBigString.replace('.', ';');
//  System.out.println("after;"+getBigString);
        String[] sp = getBigString.split(";");

        String decimal = "";
        if (sp.length > 1) {
            decimal = sp[1];
        }

        if (decimal.equals("") || (decimal.length() == 1 && decimal.equals("0"))) {
            decimal = sep2 + "00";
        } else if (decimal.length() == 1 && !decimal.equals("0")) {
            decimal = sep2 + "" + decimal + "0";
        } else {
            decimal = sep2 + "" + decimal.substring(0, 2);
        }


        String toret = setVirgule(sp[0], separ) + decimal;

        if (NEG == -1 && frw > 0) {
            toret = "-" + toret;
        }

        return toret;
    }

    public static String setVirgule(String frw, String separateur) {
        String setString = "";
        int k = 0;
        for (int i = (frw.length() - 1); i >= 0; i--) {
            k++;
            if ((k - 1) % 3 == 0 && (k - 1) != 0) {
                setString += "" + separateur;
            }
            setString += frw.charAt(i);
        }
        return inverse(setString);
    }

    static String inverse(String frw) {
        String l = "";
        for (int i = (frw.length() - 1); i >= 0; i--) {
            l += frw.charAt(i);
        }
        return l;
    }

    String setVirguleUbwato(String frw) {
        String setString = "";
        for (int i = 0; i < frw.length(); i++) {
            if (i % 3 == 0 && i != 0) {
                setString += ",";
            }
            setString += frw.charAt(i);
        }
        return setString;
    }

    void setData() {

        int ligne = cashier.jolist.size();
        //String eloge="";
        //eloge=((Journaux) allJournaux.getSelectedValue()).id;
        sommeDebit = 0;
        sommeCredit = 0;

        s1Compta = new String[ligne][7];

        for (int x = 0; x < ligne; x++) {

            Journal C = cashier.jolist.get(x);

            s1Compta[x][0] = "" + C.compteDebit;
            s1Compta[x][1] = "" + C.compteCredit;
            s1Compta[x][2] = "" + cashier.db.getCompte(C.compteDebit);
            s1Compta[x][3] = "" + cashier.db.getCompte(C.compteCredit);
//            s1Compta[x][4] = "" + DbHandler.setVirgule(C.montantDebit, 1, 0.005);
//            s1Compta[x][5] = "" + DbHandler.setVirgule(C.montantCredit, 1, 0.005);
            s1Compta[x][4] = "" + cashier.db.setVirgule((int) C.montantDebit);
            s1Compta[x][5] = "" + cashier.db.setVirgule((int) C.montantCredit);
            //s1Compta[x][6] = op.journaux; 
            //s1Compta[x][6] = ((Journaux) allJournaux.getSelectedValue()).cpte;
            s1Compta[x][6] = "" + C.journaux;


            sommeDebit = sommeDebit +( C.montantDebit*C.DEVISE_RATE);
            sommeCredit = sommeCredit + (C.montantCredit*C.DEVISE_RATE);
Main.CURRENT_CURRENCY=C.DEVISE ;
        }

        totalDebit.setText(DbHandler.setVirgule(sommeDebit, 1, 0.005) + "  " +LOCAL_CURRENCY2);
        totalCredit.setText(DbHandler.setVirgule(sommeCredit, 1, 0.005) + "  " +LOCAL_CURRENCY2);


        jTable.setModel(new javax.swing.table.DefaultTableModel(s1Compta, new String[]{"C_D", "C_C", "N_D", "N_C",
                    "M_D", "M_C", "ID_J"}));

        jTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
//        jTable.getColumnModel().getColumn(0).setPreferredWidth(40);
//        jTable.getColumnModel().getColumn(1).setPreferredWidth(30);
//        jTable.getColumnModel().getColumn(2).setPreferredWidth(50);
//        jTable.getColumnModel().getColumn(3).setPreferredWidth(180);//.setWidth(200);
//        jTable.getColumnModel().getColumn(4).setPreferredWidth(180);
//        jTable.getColumnModel().getColumn(5).setPreferredWidth(100);
//        jTable.getColumnModel().getColumn(6).setPreferredWidth(50);
        jTable.doLayout();
        jTable.validate();

    }

    void setData2() {

        String toSearch = " where ";

        if (allJournaux.getSelectedValue() != null) {/////////////////// twahereye kuri alljournaux

            controlColor("journaux");

            Journaux j = (Journaux) allJournaux.getSelectedValue();
             Main.CURRENT_CURRENCY=j.devise;
            labelCurrentOp.setText("NEXT OPERATION " + j.id + ": " + cashier.db.getNumOperation2(j.id, cashier.db.state));
//            Journaux j = (Journaux) allJournaux.getSelectedValue();
            resLabel.setText("J: " + j.id + " \nT: ");

            if (j.id.equals("TOUT") || j.id.equals("ALL")) {

                toSearch = toSearch + " postdater=0 AND ETAT <> 'DELETED' ";

            } else {

                toSearch = toSearch + " CPT_JOURNAL.ID_JOURNAUX = '" + j.id + "' and postdater=0 AND ETAT <> 'DELETED' ";

            }

//            cashier.db.selectCpteJournalTier(byname, j.groupe);

//            allCompteList.setListData(cashier.db.allCompte.toArray());

            if (allTiersList.getSelectedValue() != null) {

                Tiers ti = (Tiers) allTiersList.getSelectedValue();
                resLabel.setText("J: " + j.id + " \nT: " + ti.designation);

                if (modeTiersViewMode.equals("APPARITION")) {

                    toSearch = toSearch + " and CPT_JOURNAL.NUM_TIERS ='" + ti.numTiers + "'  ";

                } else if (modeTiersViewMode.equals("COMPTE")) {

                    toSearch = toSearch + " and (compte_debit = '" + ti.numCompte + "' "
                          + " or compte_credit = '" + ti.numCompte + "' ) "
                            + " and CPT_JOURNAL.NUM_TIERS = '" + ti.numTiers + "' ";

                }

            }

            if (allCompteList.getSelectedValue() != null) {

                Compte p = (Compte) allCompteList.getSelectedValue();
                resLabel.setText("COMPTE " + p.nomCompte);
//                toSearch = toSearch + " and (compte_debit like '" + p.numCompte + "%' or compte_credit like '" + p.numCompte + "%') ";
             
if(p.numCompte.charAt(0)=='4' && MODE_RAPPORT)
{  
  int nn = JOptionPane.showConfirmDialog(frame,
  " CLASS 4 DETECTED \n VIEW OTHER THAN ?  "+p.numCompte ,"CONFIRM", JOptionPane.YES_NO_OPTION);
                     
if(nn==0)
{  toSearch += " and (compte_debit != '" + p.numCompte + "' and compte_credit != '" + p.numCompte + "') ";}
else
{toSearch += " and (compte_debit = '" + p.numCompte + "' or compte_credit = '" + p.numCompte + "') ";}

}
else
{toSearch += " and (compte_debit = '" + p.numCompte + "' or compte_credit = '" + p.numCompte + "') ";
}

            }

            

        } else if (allTiersList.getSelectedValue() != null) { /////////////////////  sans selection ya journal runaka

            Tiers ti = (Tiers) allTiersList.getSelectedValue();
            resLabel.setText("J:   T: " + ti.designation);

//            toSearch = toSearch + " CPT_JOURNAL.NUM_TIERS ='" + ti.numTiers + "' ";

            if (modeTiersViewMode.equals("APPARITION")) {

                toSearch = toSearch + "  CPT_JOURNAL.NUM_TIERS ='" + ti.numTiers + "' and postdater=0 AND ETAT <> 'DELETED' ";

            } else if (modeTiersViewMode.equals("COMPTE")) {

                toSearch = toSearch + " ( compte_debit = '" + ti.numCompte + "' "
                        + " or compte_credit = '" + ti.numCompte + "' ) "
                        + " and CPT_JOURNAL.NUM_TIERS = '" + ti.numTiers + "' and postdater=0 AND ETAT <> 'DELETED' ";

            }


            System.out.println("vanessa mdee ...... " + ti.toString());

            if (allCompteList.getSelectedValue() != null) {

                Compte p = (Compte) allCompteList.getSelectedValue();
                resLabel.setText("COMPTE " + p.nomCompte);
if(p.numCompte.charAt(0)=='4')
{  
  int nn = JOptionPane.showConfirmDialog(frame,
  " CLASS 4 DETECTED \n VIEW OTHER THAN ?  "+p.numCompte ,"CONFIRM", JOptionPane.YES_NO_OPTION);
                     
if(nn==0)
{  toSearch += " and (compte_debit != '" + p.numCompte + "' and compte_credit != '" + p.numCompte + "') ";}
else
{toSearch += " and (compte_debit = '" + p.numCompte + "' or compte_credit = '" + p.numCompte + "') ";}

}
else
{toSearch += " and (compte_debit = '" + p.numCompte + "' or compte_credit = '" + p.numCompte + "') ";
}
            }

            if (allJournaux.getSelectedValue() != null) {

                Journaux j = (Journaux) allJournaux.getSelectedValue();

                if (j.id.equals("TOUT")) {

                    toSearch = toSearch + " and postdater=0 ";

                } else {

                    toSearch = toSearch + " and CPT_JOURNAL.ID_JOURNAUX= '" + j.id + "' and postdater=0 ";

                }

            }

           
        } 
else if (allCompteList.getSelectedValue() != null) 
{  
System.out.println("bounce along....... ");
Compte p = (Compte) allCompteList.getSelectedValue();
resLabel.setText("COMPTE " + p.nomCompte);
// toSearch = toSearch + " compte_debit like '" + p.numCompte + "%' or compte_credit like '" + p.numCompte + "%'  and postdater=0 AND ETAT='OPEN' ";
toSearch = toSearch + " (compte_debit = '" + p.numCompte + "' or compte_credit = '" + p.numCompte + "') and postdater=0 AND ETAT <> 'DELETED' ";
System.out.println("fresh azimiz....... " + toSearch);
if (allTiersList.getSelectedValue() != null) { 
Tiers ti = (Tiers) allTiersList.getSelectedValue();
resLabel.setText("J:  \nT: " + ti.designation);
//                toSearch = toSearch + " and CPT_JOURNAL.NUM_TIERS ='" + ti.numTiers + "' ";
if (modeTiersViewMode.equals("APPARITION")) {
toSearch = toSearch + " and CPT_JOURNAL.NUM_TIERS ='" + ti.numTiers + "' ";
} else if (modeTiersViewMode.equals("COMPTE")) {
toSearch = toSearch + " and ( compte_debit = '" + ti.numCompte + "' "
+ " or compte_credit = '" + ti.numCompte + "' ) "
+ " and CPT_JOURNAL.NUM_TIERS = '" + ti.numTiers + "' "; 
}
}
if (allJournaux.getSelectedValue() != null) {
Journaux j = (Journaux) allJournaux.getSelectedValue();
if (j.id.equals("TOUT")) {
toSearch = toSearch + " and postdater=0 ";
} else { toSearch = toSearch + " and CPT_JOURNAL.ID_JOURNAUX = '" + j.id + "' and postdater=0 "; }
}
}////////////////////////////////////////////////////////////////////////////////////////
joList2 = cashier.db.getJournalSearch(toSearch + searchPeriode());
int linge = joList2.size(); 
sommeResDebit = 0;
sommeResCredit = 0;
soldeRes = 0; 
String[][] s1list = new String[linge][9]; 
for (int x = 0; x < linge; x++) { 
Journal C = joList2.get(x); 
s1list[x][0] = "" + C.journaux;
s1list[x][1] = "" + C.NumOperation;
s1list[x][2] = "" + C.dateDoc; 
String name="" + C.numTiers;
if(linge<100)
{Tiers tier =cashier.db.getTiersb(C.numTiers);  

if(tier!=null)
{ name=tier.sigleTiers;  } 
}

s1list[x][3] = name;
s1list[x][4] = "" + C.libelle;
s1list[x][5] = "" + C.compteDebit;
s1list[x][6] = "" + C.compteCredit;
//            s1[x][6] = "" + DbHandler.setVirgule(C.montantDebit, 1, 0.005) ;
//            s1[x][7] = "" +  DbHandler.setVirgule(C.montantCredit, 1, 0.005) ;
s1list[x][7] = "" + cashier.db.setVirgule((int) C.montantDebit);
s1list[x][8] = "" + cashier.db.setVirgule((int) C.montantCredit); 
sommeResDebit = sommeResDebit + C.montantDebit;
sommeResCredit = sommeResCredit + C.montantCredit; 
Main.CURRENT_CURRENCY=C.DEVISE ;
} 
totalDebitRES.setText(DbHandler.setVirgule(sommeResDebit, 1, 0.005) + "  " +CURRENT_CURRENCY);
totalCreditRES.setText(DbHandler.setVirgule(sommeResCredit, 1, 0.005) + "  " +CURRENT_CURRENCY);

String solde  ;

soldeRes = Math.abs(sommeResCredit - sommeResDebit);

if (sommeResCredit > sommeResDebit) {  solde = " CR. " + setVirgule((int) (sommeResCredit - sommeResDebit)) ;
} else { solde = " DEB. " + setVirgule((int) (sommeResDebit - sommeResCredit))  ; }
soldeLabel.setText(solde); search.setText("");
setTitle(Main.Title+solde);
jTable2.setModel(new javax.swing.table.DefaultTableModel(s1list, 
new String[]{"JOURNAL", "OP", "DATE", "TIERS", "LIBELLE", 
             "CPTE D" , "CPTE C"    , "DEBIT", " CREDIT"}));
 
jTable2.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

jTable2.getColumnModel().getColumn(3).setPreferredWidth(100);
jTable2.getColumnModel().getColumn(3).setPreferredWidth(200);
jTable2.getColumnModel().getColumn(4).setPreferredWidth(200);
jTable2.getColumnModel().getColumn(7).setPreferredWidth(110);
jTable2.getColumnModel().getColumn(8).setPreferredWidth(110);
jTable2.doLayout();
jTable2.validate();

}

    void SetDataEnfant(String numCompte)
    {
String     toSearch =  
 " where (compte_debit like '" + numCompte + "%' or compte_credit like '" + numCompte + "%') ";
         
     joList2 = cashier.db.getJournalSearch(toSearch + searchPeriode());
int linge = joList2.size(); 
sommeResDebit = 0;
sommeResCredit = 0;
soldeRes = 0; 
String[][] s1list = new String[linge][9]; 
for (int x = 0; x < linge; x++) { 
Journal C = joList2.get(x); 
s1list[x][0] = "" + C.journaux;
s1list[x][1] = "" + C.NumOperation;
s1list[x][2] = "" + C.dateDoc; 
String name="" + C.numTiers;
if(linge<100)
{Tiers tier =cashier.db.getTiersb(C.numTiers);  

if(tier!=null)
{ name=tier.sigleTiers;  } 
}

s1list[x][3] = name;
s1list[x][4] = "" + C.libelle;
s1list[x][5] = "" + C.compteDebit;
s1list[x][6] = "" + C.compteCredit;
//            s1[x][6] = "" + DbHandler.setVirgule(C.montantDebit, 1, 0.005) ;
//            s1[x][7] = "" +  DbHandler.setVirgule(C.montantCredit, 1, 0.005) ;
s1list[x][7] = "" + cashier.db.setVirgule((int) C.montantDebit);
s1list[x][8] = "" + cashier.db.setVirgule((int) C.montantCredit); 
sommeResDebit = sommeResDebit + C.montantDebit;
sommeResCredit = sommeResCredit + C.montantCredit; 
Main.CURRENT_CURRENCY=C.DEVISE ;
} 
totalDebitRES.setText(DbHandler.setVirgule(sommeResDebit, 1, 0.005) + "  " +CURRENT_CURRENCY);
totalCreditRES.setText(DbHandler.setVirgule(sommeResCredit, 1, 0.005) + "  " +CURRENT_CURRENCY);

String solde  ;

soldeRes = Math.abs(sommeResCredit - sommeResDebit);

if (sommeResCredit > sommeResDebit) {  solde = "  SOLDE CREDITEUR DE " + setVirgule((int) (sommeResCredit - sommeResDebit)) + "  " +CURRENT_CURRENCY;
} else { solde = "  SOLDE DEBITEUR DE " + setVirgule((int) (sommeResDebit - sommeResCredit)) + "  " +CURRENT_CURRENCY; }
soldeLabel.setText(solde); search.setText("");
setTitle(Main.Title+solde);
jTable2.setModel(new javax.swing.table.DefaultTableModel(s1list, 
new String[]{"JOURNAL", "OP", "DATE", "TIERS", "LIBELLE", 
             "CPTE D" , "CPTE C"    , "DEBIT", " CREDIT"}));
 
jTable2.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
jTable2.getColumnModel().getColumn(2).setPreferredWidth(130);
jTable2.getColumnModel().getColumn(3).setPreferredWidth(180);
jTable2.getColumnModel().getColumn(6).setPreferredWidth(110);
jTable2.getColumnModel().getColumn(7).setPreferredWidth(110);
jTable2.doLayout();
jTable2.validate();   
    }
    
    void setData33(String NumCompte) {

        String toSearch = " where ";

//        if (allCompteList.getSelectedValue() != null) { ///////////////////////////////////////////////

        Object cpt = NumCompte;
        allCompteList.setSelectedValue(cpt, true);


        System.out.println("like wo.so.  ....... " + NumCompte);

        resLabel.setText("COMPTE " + NumCompte);
//            toSearch = toSearch + " compte_debit like '" + p.numCompte + "%' or compte_credit like '" + p.numCompte + "%'  and postdater=0 AND ETAT='OPEN' ";
        toSearch = toSearch + " (compte_debit = '" + NumCompte + "' or compte_credit = '"
                + NumCompte + "') and postdater = 0 AND ETAT <> 'DELETED' ";

        System.out.println(" nickelback  ....... " + toSearch);

//        }////////////////////////////////////////////////////////////////////////////////////////

        joList2 = cashier.db.getJournalSearch(toSearch + searchPeriode());

        int linge = joList2.size();

        sommeResDebit = 0;
        sommeResCredit = 0;
        soldeRes = 0;

        String[][] sList = new String[linge][9];

        for (int x = 0; x < linge; x++) {

            Journal C = joList2.get(x);

            sList[x][0] = "" + C.journaux;
            sList[x][1] = "" + C.NumOperation;
            sList[x][2] = "" + C.dateDoc;
//            s1[x][2] = "" + cashier.db.getTiers(C.numTiers, "");
            sList[x][3] = "" + C.numTiers;
            sList[x][4] = "" + C.libelle;
            sList[x][5] = "" + C.compteDebit;
            sList[x][6] = "" + C.compteCredit;
//            s1[x][6] = "" + DbHandler.setVirgule(C.montantDebit, 1, 0.005) ;
//            s1[x][7] = "" +  DbHandler.setVirgule(C.montantCredit, 1, 0.005) ;
            sList[x][7] = "" + cashier.db.setVirgule((int) C.montantDebit);
            sList[x][8] = "" + cashier.db.setVirgule((int) C.montantCredit);

            sommeResDebit = sommeResDebit + C.montantDebit;
            sommeResCredit = sommeResCredit + C.montantCredit;
Main.CURRENT_CURRENCY=C.DEVISE ;
        }

        totalDebitRES.setText(DbHandler.setVirgule(sommeResDebit, 1, 0.005) + "  " +CURRENT_CURRENCY);
        totalCreditRES.setText(DbHandler.setVirgule(sommeResCredit, 1, 0.005) + "  " +CURRENT_CURRENCY);
        String solde ;

        soldeRes = Math.abs(sommeResCredit - sommeResDebit);

        if (sommeResCredit > sommeResDebit) {
            solde = "  SOLDE CREDITEUR DE " + setVirgule((int) (sommeResCredit - sommeResDebit)) + "  " +CURRENT_CURRENCY;
        } else {
            solde = "  SOLDE DEBITEUR DE " + setVirgule((int) (sommeResDebit - sommeResCredit)) + "  " +CURRENT_CURRENCY;
        }
        soldeLabel.setText(solde);
        search.setText("");
setTitle(Main.Title+solde);
        jTable2.setModel(new javax.swing.table.DefaultTableModel(sList, new String[]{"JOURNAL", "OP", "DATE", "TIERS", "LIBELLE", "CPTE D", "CPTE C", " DEBIT", " CREDIT"}));
        jTable2.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        jTable2.getColumnModel().getColumn(2).setPreferredWidth(130);
        jTable2.getColumnModel().getColumn(3).setPreferredWidth(180);
        jTable2.getColumnModel().getColumn(6).setPreferredWidth(110);
        jTable2.getColumnModel().getColumn(7).setPreferredWidth(110);
        jTable2.doLayout();
        jTable2.validate();

    }

    void setDataTiers() {

        String toSearch = " where ";

        if (allTiersList.getSelectedValue() != null) {

            Tiers ti = (Tiers) allTiersList.getSelectedValue();

            resLabel.setText("J:   T: " + ti.designation);







            toSearch = toSearch + " ( compte_debit = '" + ti.numCompte + "' "
                    + " or compte_credit = '" + ti.numCompte + "' ) "
                    + " and (CPT_JOURNAL.NUM_TIERS = '" + ti.numTiers + "') ";






        }
//System.out.println();
        joList2 = cashier.db.getJournalSearch(toSearch + searchPeriode());

        int linge = joList2.size();
        sommeResDebit = 0;
        sommeResCredit = 0;
        soldeRes = 0;

        String[][] sList = new String[linge][9];

        for (int x = 0; x < linge; x++) {

            Journal C = joList2.get(x);

            sList[x][0] = "" + C.journaux;
            sList[x][1] = "" + C.NumOperation;
            sList[x][2] = "" + C.dateDoc;
            sList[x][3] = "" + cashier.db.getTiers(C.numTiers, "");
            sList[x][4] = "" + C.libelle;
            sList[x][5] = "" + C.compteDebit;
            sList[x][6] = "" + C.compteCredit;
            sList[x][7] = "" + cashier.db.setVirgule((int) C.montantDebit);
            sList[x][8] = "" + cashier.db.setVirgule((int) C.montantCredit);

            sommeResDebit = sommeResDebit + C.montantDebit;
            sommeResCredit = sommeResCredit + C.montantCredit;
Main.CURRENT_CURRENCY=C.DEVISE ;
        }

        totalDebitRES.setText(DbHandler.setVirgule(sommeResDebit, 1, 0.005) + "  " +CURRENT_CURRENCY);
        totalCreditRES.setText(DbHandler.setVirgule(sommeResCredit, 1, 0.005) + "  " +CURRENT_CURRENCY);
        String solde  ;
        soldeRes = Math.abs(sommeResCredit - sommeResDebit);

        if (sommeResCredit > sommeResDebit) {
            solde = "  SOLDE CREDITEUR DE " + setVirgule((int) (sommeResCredit - sommeResDebit)) + "  " +CURRENT_CURRENCY;
        } else {
            solde = "  SOLDE DEBITEUR DE " + setVirgule((int) (sommeResDebit - sommeResCredit)) + "  " +CURRENT_CURRENCY;
        }

        soldeLabel.setText(solde);setTitle(Main.Title+solde);
        search.setText("");
        jTable2.setModel(new javax.swing.table.DefaultTableModel(sList, new String[]{"JOURNAL", "OP", "DATE", "TIERS", "LIBELLE", "CPTE D", "CPTE C", " DEBIT", " CREDIT"}));
        jTable2.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        jTable2.getColumnModel().getColumn(2).setPreferredWidth(130);
        jTable2.getColumnModel().getColumn(3).setPreferredWidth(180);
        jTable2.getColumnModel().getColumn(6).setPreferredWidth(110);
        jTable2.getColumnModel().getColumn(7).setPreferredWidth(110);
        jTable2.doLayout();
        jTable2.validate();
    }

    void setData4(String numCompte) {

        String toSearch = " where ";
        resLabel.setText("BILAN  POUR COMPTE " + numCompte);
        toSearch = toSearch + " ( compte_debit like '" + numCompte + "%' or compte_credit like '" + numCompte + "%' ) ";
        joList2 = cashier.db.getJournalSearch(toSearch);

        int linge = joList2.size();
        sommeResDebit = 0;
        sommeResCredit = 0;
        String[][] sList = new String[linge][8];
        for (int x = 0; x < linge; x++) {
            Journal C = joList2.get(x);
            sList[x][0] = "" + C.NumOperation;
            sList[x][1] = "" + C.dateDoc;
            sList[x][2] = "" + C.numTiers;
            sList[x][3] = "" + C.libelle;
            sList[x][4] = "" + C.compteDebit;
            sList[x][5] = "" + C.compteCredit;
            sList[x][6] = "" + DbHandler.setVirgule(C.montantDebit, 1, 0.005);
            sList[x][7] = "" + DbHandler.setVirgule(C.montantCredit, 1, 0.005);
            sommeResDebit = sommeResDebit + C.montantDebit;
            sommeResCredit = sommeResCredit + C.montantCredit;
            Main.CURRENT_CURRENCY=C.DEVISE ;
        }
        totalDebitRES.setText(DbHandler.setVirgule(sommeResDebit, 1, 0.005) + "  " +CURRENT_CURRENCY);
        totalCreditRES.setText(DbHandler.setVirgule(sommeResCredit, 1, 0.005) + "  " +CURRENT_CURRENCY);
        String solde ;
        soldeRes = Math.abs(sommeResCredit - sommeResDebit);
        if (sommeResCredit > sommeResDebit) {
            solde = "  SOLDE CREDITEUR DE " + setVirgule((int) (sommeResCredit - sommeResDebit)) + "  " +CURRENT_CURRENCY;
        } else {
            solde = "  SOLDE DEBITEUR DE " + setVirgule((int) (sommeResDebit - sommeResCredit)) + "  " +CURRENT_CURRENCY;
        }
        soldeLabel.setText(solde);setTitle(Main.Title+solde);
        search.setText("");
        jTable2.setModel(new javax.swing.table.DefaultTableModel(sList, new String[]{"OP", "DATE", "TIERS", "LIBELLE", "CPTE D", "CPTE C", " DEBIT", " CREDIT"}));
        jTable2.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        jTable2.getColumnModel().getColumn(2).setPreferredWidth(130);
        jTable2.getColumnModel().getColumn(3).setPreferredWidth(180);
        jTable2.getColumnModel().getColumn(6).setPreferredWidth(110);
        jTable2.getColumnModel().getColumn(7).setPreferredWidth(110);
        jTable2.doLayout();
        jTable2.validate();

    }

    String montant() {
        return MONTANT.getText().replaceAll(" ", "");
    }

    public void montantAkadomo() {
        MONTANT.addKeyListener(
                new KeyListener() {

                    public void actionPerformed(ActionEvent ae) {
                    }
 @Override
                    public void keyTyped(KeyEvent e) {
                    }
 @Override
                    public void keyPressed(KeyEvent e) {
                    }
 @Override
                    public void keyReleased(KeyEvent e) {
                        MONTANT.setText(setVirgule(montant()));
                    }
                });

    }

    public void searchTier() {

        searchTier.addKeyListener(
                new KeyListener() {

                    public void actionPerformed(ActionEvent ae) {
                    }
 @Override                    public void keyTyped(KeyEvent e) {
                        {
                            String cc = "" + e.getKeyChar();
                            String find = (searchTier.getText() + cc).toUpperCase();
//                            System.out.println(find);
//                            System.out.println("cc.hashCode(): "+cc.hashCode());
                            if (cc.hashCode() == 8 && find.length() > 0) {
                                find = (find.substring(0, find.length() - 1));
                            }
                            System.out.println("findFinal: " + find);
                            searchedTier(find);
                        }
                    }
 @Override
                    public void keyPressed(KeyEvent e) {
                    }
 @Override
                    public void keyReleased(KeyEvent e) {
                    }
                });
    }

    void searchedTier(String findIt) {

        int linge = cashier.db.allTiers.size();

        LinkedList<Tiers> allTiers2 = new LinkedList<>();

        for (int x = 0; x < linge; x++) {
            Tiers tier = cashier.db.allTiers.get(x);
            // System.out.println(tier);
            if (tier != null && (((tier.numCompte) != null && (tier.numCompte).contains(findIt))
                    || ((tier.designation) != null && (tier.designation).contains(findIt))
                    || ((tier.numAffilie) != null && (tier.numAffilie).contains(findIt))
                    || (((tier.numTiers) != null && ("" + tier.numTiers).contains(findIt))))) {
                allTiers2.add(tier);
            }
        }
        allTiersList.setListData(allTiers2.toArray());
    }

    public void searchCompte() {

        searchCompte.addKeyListener(
                new KeyListener() {
            public void actionPerformed(ActionEvent ae) {  }
 @Override  public void keyTyped(KeyEvent e) {

                        String cc = "" + e.getKeyChar();
                        String find = (searchCompte.getText() + cc).toUpperCase();
                        if (cc.hashCode() == 8 && find.length() > 0) {
                            find = (find.substring(0, find.length() - 1));
                        }

                        searchedCpte(find); 
                    }
 @Override public void keyPressed(KeyEvent e) { }
 @Override public void keyReleased(KeyEvent e) { }
                });
    }

    
   
    
    void searchedCpte(String findIt) { 
        int linge = cashier.db.allCompte.size(); 
        LinkedList<Compte> allCompte2 = new LinkedList<>(); 
        for (int x = 0; x < linge; x++) { 
            Compte C = cashier.db.allCompte.get(x);

            if ((("" + C.nomCompte).contains(findIt)) || (("" + C.numCompte).contains(findIt))) {
                allCompte2.add(C);
            }
        }
        allCompteList.setListData(allCompte2.toArray());
    }

    public void superSearchIt() {

        search.addKeyListener(
                new KeyListener() {

                    public void actionPerformed(ActionEvent ae) {
                    }
 @Override
                    public void keyTyped(KeyEvent e) {

                        String cc = "" + e.getKeyChar();
//                        String find = (search.getText() + cc).toUpperCase();
                        String find = (search.getText() + cc);

                        if (cc.hashCode() == 8 && find.length() > 0) {
                            find = (find.substring(0, find.length() - 1));
                        }
                        setData3(find);
                    }
 @Override
                    public void keyPressed(KeyEvent e) {
                    }
 @Override
                    public void keyReleased(KeyEvent e) {
                    }
                });
    }

    void setData3(String findIt) {

        int linge = joList2.size();

        joList2ResTmt = new LinkedList<>();

        sommeResDebit = 0;
        sommeResCredit = 0;

        String[][] sList = new String[linge][8];

        int var = 0;

        for (int x = 0; x < linge; x++) {

            Journal C = joList2.get(x);
findIt=findIt.toUpperCase();
            if ((findIt.length() != 4 && C.dateDoc.length() == 6 && C.dateDoc.toUpperCase().contains(findIt))
                   
                    || (RADIO_LIBELE.isSelected() && C.libelle.toUpperCase().contains(findIt))
                    || (RADIO_OPERATION.isSelected() && ("" + C.NumOperation).contains(findIt))
                    ) {

                sList[var][0] = "" + C.NumOperation;
                sList[var][1] = "" + C.dateDoc;
                sList[var][2] = "" + C.numTiers;
                sList[var][3] = "" + C.libelle;
                sList[var][4] = "" + C.compteDebit;
                sList[var][5] = "" + C.compteCredit;
                sList[var][6] = "" + setVirgule((int) C.montantDebit);
                sList[var][7] = "" + setVirgule((int) C.montantCredit);

                joList2ResTmt.add(C);

                sommeResDebit = sommeResDebit + C.montantDebit;
                sommeResCredit = sommeResCredit + C.montantCredit;
                var++;
                Main.CURRENT_CURRENCY=C.DEVISE ;
            }
        }
        totalDebitRES.setText(setVirgule((int) (sommeResDebit)) + "  " +CURRENT_CURRENCY);
        totalCreditRES.setText(setVirgule((int) (sommeResCredit)) + "  " +CURRENT_CURRENCY);
        String solde  ;
        soldeRes = Math.abs((sommeResCredit - sommeResDebit));

        if (sommeResCredit > sommeResDebit) {
            solde = "  SOLDE CREDITEUR DE " + setVirgule((int) (sommeResCredit - sommeResDebit)) + "  " +CURRENT_CURRENCY;
        } else {
            solde = "  SOLDE DEBITEUR DE " + setVirgule((int) (sommeResDebit - sommeResCredit)) + "  " +CURRENT_CURRENCY;
        }
        soldeLabel.setText(solde);setTitle(Main.Title+solde);

        jTable2.setModel(new javax.swing.table.DefaultTableModel(sList, new String[]{"OP", "DATE", "TIERS", "LIBELLE", "CPTE D", "CPTE C", " DEBIT", " CREDIT"}));
        jTable2.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        jTable2.getColumnModel().getColumn(2).setPreferredWidth(130);
        jTable2.getColumnModel().getColumn(3).setPreferredWidth(180);
        jTable2.getColumnModel().getColumn(6).setPreferredWidth(110);
        jTable2.getColumnModel().getColumn(7).setPreferredWidth(110);
        jTable2.doLayout();
        jTable2.validate();

        joList2 = joList2ResTmt;

    }
boolean konteIgaruka()
{
    if(labelCurrentOp.getText().contains("IMPORTING"))
    {return false;}
    
    int linge = cashier.jolist.size();
            for (int x2 = 0; x2 < linge; x2++) {
                Journal C = cashier.jolist.get(x2);
                
                System.out.println(x2+" "+C);
                
                for (int j = x2+1; j < linge; j++) {
                   
                Journal C2 = cashier.jolist.get(j);
                 System.out.println(j+" "+C2);
                if(
                        (C2.compteCredit.equals(C.compteDebit)&& !C2.compteCredit.equals(""))||
                        (C2.compteDebit.equals(C.compteCredit)&& !C2.compteDebit.equals(""))||
                        (C2.compteDebit.equals(C.compteDebit) && !C2.compteDebit.equals("")))
                {  JOptionPane.showMessageDialog(searchCompte, C2.compteCredit+"  "+C2.compteDebit);
                 
                return true;}
               
                }
            }
            
            return false;
}
     
    public void save() {

        save.addActionListener((ActionEvent ae) -> {
            if (ae.getSource() == save &&
                    umukozi.PWD_EMPLOYE.toUpperCase().contains("SAVECOMPTE")) {
                
                boolean ninjiye=true;
                
                
                if ((sommeDebit != sommeCredit) || (sommeDebit < 0 && sommeCredit < 0)) {
                    JOptionPane.showConfirmDialog(null, "VOTRE OPERATION EST INCORRECT CAR LE DEBIT: "
                            + sommeDebit + " ET LE CREDIT: " + sommeCredit + " SONT DIFFERENTS", "PROBLEM !!!",
                            JOptionPane.YES_NO_OPTION);
                } else  if (konteIgaruka()) {
                    JOptionPane.showConfirmDialog(null, "VOTRE OPERATION EST INCORRECT EXISTE COMPTE IDENTIQUE", "PROBLEM !!!",
                            JOptionPane.YES_NO_OPTION);
                } else {
                    String inPutString = "";
                    for (int x1 = 0; x1 < cashier.jolist.size(); x1++) {
                        Journal C = cashier.jolist.get(x1);
                        inPutString = inPutString + "\n" + C;
                    }
                    int nn = JOptionPane.showConfirmDialog(frame, "VOULEZ VOUS : " + labelCurrentOp.getText()
                            + " \n" + libelle.getText() + " \n" + inPutString, "MESSAGE", JOptionPane.YES_NO_OPTION);
                    if (nn == 0) {
                        System.out.println("********" + save.getText() + "******montantDebit: " + montantDebit + "*******montantCredit: " + montantCredit);
                        System.out.println("********" + save.getText() + "******sommeDebit: " + sommeDebit + "*******sommeCredit: " + sommeCredit);
                        if ((sommeDebit > 0 && sommeCredit > 0) && save.getText().equals("SAVE")) {
                            String profit_center = "";
                            
                            int linge = cashier.jolist.size();
                            
                            
                            if(!LibelleLettrage.equals("NOPE"))
{  

int coix = JOptionPane.showConfirmDialog(frame,
  " LETTRAGE DETECTER ? \n "+LibelleLettrage ,"CONFIRM", JOptionPane.YES_NO_OPTION);
                     
if(coix==0)
{ 
        Journal C =  (Journal) JOptionPane.showInputDialog
    (null, " CHOOSE THE RIGTH INVOICE", "LETTRAGE", JOptionPane.QUESTION_MESSAGE, null,
           cashier.jolist.toArray(),"" )    ;
 cashier.db.upDateJournalLettrage (C.NumOperation, C.journaux,LibelleLettrage,C.libelle);
 
 if(lautre.DEVISE_RATE<C.DEVISE_RATE)
 { 
     double loss =C.montantDebit*(C.DEVISE_RATE-lautre.DEVISE_RATE);
 cashier.db.insertJournal(C.NumOperation, C.numTiers, C.dateOperation,
"6221", "", C.libelle, loss, 0,
C.utilisateur, C.postDater,
C.dateDoc, C.journaux, profit_center,LOCAL_CURRENCY2,1);
 
 JOptionPane.showMessageDialog(null, " LOSS ON EXCHANGE "+setVirgule((int)loss));

 }
 else if(lautre.DEVISE_RATE>C.DEVISE_RATE)
 {
  double profit =C.montantDebit*(lautre.DEVISE_RATE-C.DEVISE_RATE);
 cashier.db.insertJournal(C.NumOperation, C.numTiers, C.dateOperation,
"", "741", C.libelle, 0, profit,
C.utilisateur, C.postDater,
C.dateDoc, C.journaux, profit_center,LOCAL_CURRENCY2,1);
 
 JOptionPane.showInputDialog(null, " PROFIT ON EXCHANGE "+setVirgule((int)profit));
     
 }
  C.DEVISE_RATE=  lautre.DEVISE_RATE;
 
}
}
LibelleLettrage="NOPE"; 
                            
                            
                            
                            for (int x2 = 0; x2 < linge; x2++) {
                                Journal C = cashier.jolist.get(x2);
                               
                ninjiye=  cashier.db.insertJournal(C.NumOperation, C.numTiers, C.dateOperation,
                        C.compteDebit, C.compteCredit, C.libelle, C.montantDebit, C.montantCredit,
                        C.utilisateur, C.postDater,
                        C.dateDoc, C.journaux, profit_center,C.DEVISE,C.DEVISE_RATE);
                
                if(!ninjiye)
                {
                break;
                }     }
                            


              if(ninjiye)
                {
                JOptionPane.showConfirmDialog(null, "SAVED SUCCESSFUL ", "saved", JOptionPane.INFORMATION_MESSAGE);
                cashier.jolist = new LinkedList();
                setData();
                MONTANT.setText("");
                libelle.setText("");
                allJournaux.clearSelection();
                allTiersList.clearSelection();
                }
              else
              {
                JOptionPane.showConfirmDialog(null, "SAVED FAILED ", "saved", JOptionPane.ERROR_MESSAGE);
                  
              }
                        
 }  
else {
                            String etat = ",ETAT='UPDATED'";
                            String ikibaye = "UPDATED";
                            int linge = cashier.jolist.size();
                            
                            String raison=JOptionPane.showInputDialog(" ENTER A REASON");
                            
for (int x3 = 0; x3 < linge; x3++) {
    Journal C = cashier.jolist.get(x3);
    System.out.println("*******  >>>>>>>>>>" + C.toString());
    C.dateDoc = dateFormat();
      ninjiye= cashier.db.upDateJournal2(C, etat,umukozi.NOM_EMPLOYE,raison); 
    if(!ninjiye)
      {  break; }
  }
if(ninjiye)
    {
JOptionPane.showConfirmDialog(null, ikibaye + " UPDATE DONE SUCCESSFULLY", "INFO", JOptionPane.YES_NO_OPTION);
cashier.jolist = new LinkedList();
setData();
MONTANT.setText("");
libelle.setText("");
allJournaux.clearSelection();
save.setText("SAVE");
save.setBackground(new Color(105, 200, 155));   
     }
                            else
                            {
                            JOptionPane.showConfirmDialog(null, ikibaye + " FAILED ", "INFO", JOptionPane.ERROR_MESSAGE);
                      
                            }
                           
                        }
                    }
                     Azero();
                controlColor("clear");
                }
            } else {
                JOptionPane.showMessageDialog(null, " NOT AUTHORISED",
                        " LA MISE A JOUR A ECHOUE "
                                + "\n PAS AUTHORISE", JOptionPane.ERROR_MESSAGE);
            }
        });
    }

    //////////////////////////////////////////////////////////////////////////////
    public void delete() {

        delete.addActionListener((ActionEvent ae) -> {
            if (ae.getSource() == delete &&
                    umukozi.PWD_EMPLOYE.toUpperCase().contains("DELETE")) {
                int n = JOptionPane.showConfirmDialog(frame, "VOULEZ VOUS SUPPRIMER " + labelCurrentOp.getText(), "MESSAGE", JOptionPane.YES_NO_OPTION);
                if (n == 0) {
                    String etat = ", ETAT = 'DELETED' ";
                    String ikibaye = "DELETED";
                    int linge = cashier.jolist.size();
                    
                     String raison=JOptionPane.showInputDialog(" ENTER A REASON");
                    
                    for (int x1 = 0; x1 < linge; x1++) {
                        Journal C = cashier.jolist.get(x1);
                        C.dateDoc = dateFormat();
                        cashier.db.upDateJournal2(C, etat,umukozi.NOM_EMPLOYE,raison);
                        System.out.println(">?>?>?>?> " + C.toString());
                    }
                    JOptionPane.showConfirmDialog(null, ikibaye + " SUCCESSFUL ", "DELETED", JOptionPane.YES_NO_OPTION);
                    cashier.jolist = new LinkedList();
                    setData();
                    MONTANT.setText("");
                    libelle.setText("");
                    allJournaux.clearSelection();
                }
            } else {
                JOptionPane.showMessageDialog(null, " NOT AUTHORISED",
                        " LA MISE A JOUR A ECHOUE DELETE"
                                + "\n PAS AUTHORISE", JOptionPane.ERROR_MESSAGE);
            }
        });
    }

    public void print() {

        print.addActionListener((ActionEvent ae) -> {
            if (ae.getSource() == print) {
                String opt = "OPERATION";
                if (!joList2.isEmpty()) {
                    opt = "JOURNAL";
                }
                if (!cashier.jolist.isEmpty()) {
                    opt = "OPERATION";
                }
                if (!joList2.isEmpty()) {
                    opt = (String) JOptionPane.showInputDialog(null, "VOUS AVEZ DEUX OPTIONS D'IMPRESSION ", "OPTIONS",
                   JOptionPane.QUESTION_MESSAGE, null, 
                   new String[]{"OPERATION", "JOURNAL", "RELEVE TIER"}, "JOURNAL");
                }
                
                if (opt.equals("OPERATION")) {
                    int linge = s1Compta.length;
                    LinkedList<Lot> lotList = new LinkedList();
                    Journal C = null;
                    double cashN=0,creditN=0;
                    for (int x1 = 0; x1 < linge; x1++) {
                        C = cashier.jolist.get(x1);
                        Tiers tier = cashier.db.getTiersb(s1Compta[x1][2]);
                        String name1 = "";
                        if (tier!=null) {
                            name1 = tier.designation;
                        }
                        cashN +=C.montantDebit;
                        creditN += C.montantCredit;
                        
                        Lot lot = new Lot(s1Compta[x1][0], s1Compta[x1][1], s1Compta[x1][2], s1Compta[x1][3] + " " + name1, s1Compta[x1][4].replaceAll(" ", ""), s1Compta[x1][5].replaceAll(" ", ""), LIB, LIB, LIB, LIB, LIB);
                        lotList.add(lot);
                    }
                    if (C != null) {
                        
                        Tiers i = ((Tiers) allTiersList.getSelectedValue());
                        
                        PrintTiers pt  ;
                        
                        if (i == null) {
                            JOptionPane.showMessageDialog(searchCompte, "SELECT A LINE TO VIEW ATIER ON PRINT OUT ");
                             pt = new PrintTiers(C.numTiers, "", "", "", "", "");
                          
                        } else {
                            
                            pt = new PrintTiers(i.sigleTiers, i.designation, i.adresse, i.info, "", "");
                            
                        }
                        
String Facture = "JOURNAL : " + C.journaux + "  OP: " + C.NumOperation;

String libele = "LIBELLE : " + libelle.getText();

PrintVars pv = new PrintVars(C.dateDoc, libele, "OPERATION", "", Facture, Main.CURRENT_CURRENCY, "CODE", 1);

double[] ibiciro = new double[]{cashN, creditN, Math.abs(cashN - creditN), 0};

PrintV2 printer = new PrintV2(cashier.db, DbHandler.img, pt, pv, lotList, ibiciro,
        new String[]{"", "", "", "", "", ""},0);
                        
                    }
                }
                
                if (opt.equals("JOURNAL")) {
                    LinkedList<Journal> joList3 = joList2;
                    if (search != null && search.getText().length() > 0) {
                        joList3 = joList2ResTmt;
                    }
                    int linge = joList3.size();
                    LinkedList<Lot> lotList = new LinkedList();
                    Journal C = null;
                    for (int x2 = 0; x2 < linge; x2++) {
                        C = joList3.get(x2);
                        Tiers tier =cashier.db.getTiersb(C.numTiers);
                        String name2 = "";
                        if (tier!=null) {
                            name2 = tier.designation;
                            name2 = name2.substring(0, Math.min(35, name2.length()));
                        }
                        Lot lot = new Lot("" + C.NumOperation, "" + C.dateDoc, "" + C.numTiers, 
                                "" + C.libelle+  "  " + name2, "" + C.compteDebit, 
                                "" + C.compteCredit, "" + setVirgule(C.montantDebit, 1, 0.005, " ") 
                               , 
                                "" + setVirgule(C.montantCredit, 1, 0.005, " "), LIB, LIB, LIB);
                        
                        System.out.println(setVirgule(  C.montantCredit*C.DEVISE_RATE, 1, 0.005, " "));
                        
                        
                        lotList.add(lot);
                    }
                    if (C != null) {
 PrintTiers pt  ; 



String Facture = ""; //resLabel.getText();
String libele   = "PERIODE : " + deb.getText()+" to "+fin.getText();
Compte p = (Compte) allCompteList.getSelectedValue();
Tiers i =  (Tiers)  allTiersList.getSelectedValue();
Journaux j = ((Journaux) allJournaux.getSelectedValue()); 
if (i == null) {
    pt = new PrintTiers("", "", "", "", "", "");
} else {
    pt = new PrintTiers(i.sigleTiers, i.designation, i.adresse, i.info, "", "");
} 
if(p!=null)
{ libele  = "ACCOUNT : " + p.nomCompte; }
if(j!=null)
{ Facture = "JOURNAL "+ j.id;}
if(j!=null && i == null)
{ Facture = "JOURNAL "+ j.id;
pt = new PrintTiers("JOURNAL "+ j.id, "", "", "", "", "");
} 
 

 System.out.print(" Main.CURRENT_CURRENCY    "+Main.CURRENT_CURRENCY);

PrintVars pv = new PrintVars(C.dateDoc, libele, "JOURNAL", "", 
        Facture, Main.CURRENT_CURRENCY, "CODE", 1);

double[] ibiciro = new double[]{sommeResDebit, sommeResCredit, Math.abs(sommeResDebit - sommeResCredit), 0};
PrintV2 printer = new PrintV2(cashier.db, DbHandler.img, pt, pv, lotList, ibiciro,
        new String[]{"", "", "", "", "", ""},0);
                    }
                }
                
                if (opt.equals("RELEVE TIER")) {
                    
                    Tiers i = ((Tiers) allTiersList.getSelectedValue());
                    
                    if(allJournaux.getSelectedValue() !=null)
                    {
                       Journaux journo = (Journaux) allJournaux.getSelectedValue();
                        JOptionPane.showMessageDialog(searchCompte, 
                                " ATTENTION !! VOUS AVEZ CHOISI UN JOURNAL "+journo.name);
                         
                    }
                    
                    if(i==null)
                    {
                        JOptionPane.showMessageDialog(searchCompte, 
                                "SELECT A TIER PLEASE BEFORE PRINTING ");
                         
                    }
                    else
                    {
                    LinkedList<Journal> joList3 = joList2;
                    if (search != null && search.getText().length() > 0) {
                        joList3 = joList2ResTmt;
                    }
                    int linge = joList3.size();
                    LinkedList<Lot> lotList = new LinkedList();
                    Journal C = null;
                String sqlTier="APP.CPT_JOURNAL.NUM_TIERS = '" + i.numTiers + "'  AND ";    
                    double solde=cashier.db.getOpenBalance2(cashier.db.state3,
                            sqlTier, i.numCompte,searchPeriodeStartBeta(deb.getText()) );
                    
                    if(solde>0)
                    { sommeResDebit +=solde;}
                    else
                    {
                        sommeResCredit +=  (solde*-1);
                    } 
                    
                    
                    
                    Lot lot = new Lot("" ,""  ,   
          " SOLDE D OUVERTURE AU "  +  deb.getText(),   
          ""  , ""   ,setVirgule(  solde, 1, 0.005, " ") , LIB, LIB,LIB, LIB, LIB);
               lotList.add(lot);
               
                    for (int x2 = 0; x2 < linge; x2++) {
                        C = joList3.get(x2); 
                        solde +=C.montantDebit-C.montantCredit;
    lot = new Lot("" + C.NumOperation, "" + C.dateDoc,   
                   C.libelle+  "  "+C.journaux, setVirgule(  C.montantDebit, 1, 0.005, " ")  
                 , setVirgule(  C.montantCredit, 1, 0.005, " ")   ,
                  setVirgule(  solde, 1, 0.005, " "), LIB, LIB,LIB, LIB, LIB);
                        lotList.add(lot);
                    }
                    
                    if (C != null) {
                        
                        PrintTiers pt = new PrintTiers(i.sigleTiers, i.designation, i.adresse, i.info, "", "");
                        String Facture = resLabel.getText();
                        String libele = "PERIODE : " + deb.getText()+" to "+fin.getText();
                        
                        PrintVars pv = new PrintVars(C.dateDoc, libele, "RELEVETIER", "", Facture, Main.CURRENT_CURRENCY, "CODE", 1);
                        
                        double[] ibiciro = new double[]{sommeResDebit, sommeResCredit, solde, 0};
                        PrintV2 printer = new PrintV2(cashier.db, DbHandler.img, pt, pv, lotList, ibiciro,
                                new String[]{"", "", "", "", "", ""},0);
                    }
                }
                }
                
                
            }
        });
    }
   
    /**
     * **************************************************************************************
     * QUAND ON CLICK LE BOUTON ALL
     * **************************************************************************************
     */
    public void all() {
        resetButton.addActionListener((ActionEvent ae) -> {
            if (ae.getSource() == resetButton) {
                Azero();
                controlColor("clear");
            }
        });
    }

    /**
     * ****************************************************************************************************
     * quand on appuiye sur le bouton bilan
     * ****************************************************************************************************
     */
    
    
    
              public void journalButton() {

        journalButton.addActionListener((ActionEvent ae) -> {
            if (ae.getSource() == journalButton) {
                  
                 if (allJournaux.getSelectedValue() != null) {
                    
                    controlColor("journaux");
                    Journaux j = (Journaux) allJournaux.getSelectedValue();
                    new JournauxInterface(cashier.db,true,j);
                }
                 else
                 {
                    new JournauxInterface(cashier.db,false,null); 
                 }
                
                
            }
        });
    }
            
    
 public static String setVirgule(double frw,int NEG,double arr)
    {  
        
//        System.out.println("frw:"+frw);
        if(arr==0.005)
            frw=frw+0.0050000000;
//        System.out.println("frw arre:"+frw);
        BigDecimal big=new BigDecimal(frw);
//        big = big.setScale(2, BigDecimal.ROUND_UP);
        String getBigString=big.toString();
//        System.out.println("big:"+getBigString);
  getBigString=getBigString.replace('.', ';');
//  System.out.println("after;"+getBigString);
         String[]sp=getBigString.split(";");  
          
          String decimal="";
          if(sp.length>1)
          {
              decimal=sp[1];
          }
            
            if(decimal.equals("") || ( decimal.length()==1 && decimal.equals("0")) )
            {
                decimal=".00";
            }
            else if (decimal.length()==1 && !decimal.equals("0") )
            {
                decimal="."+decimal+"0";
            }
            else
            {
                decimal="."+decimal.substring(0, 2);
            }
             
            
            String toret=setVirguleB(sp[0])+decimal;
            
            if(NEG==-1 && frw>0)
            {
                toret="-"+toret;
            }
            
            return toret;
}

public static String setVirguleB(String frw)
   {
       String setString="";
       int k=0;
       for(int i=(frw.length()-1);i>=0;i--)
       {
           k++;
           if((k-1)%3==0 && (k-1)!=0)
           {
               setString+=",";
           }
           setString+=frw.charAt(i);
       }
       return inverse(setString);
   }
public static String getTomorowDate(int toget)
{
String convertedDate = "";
SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");   
Calendar cal = Calendar.getInstance();  

try {     
cal.add(Calendar.DATE, toget );    //IYI IZANA ITARIKI YEJO (1) USHAKA INDI TARIKI UBWO USHYIRAMO UNDI MUBARE
 convertedDate=dateFormat.format(cal.getTime()); 
//System.out.println("DATE FORMATED IS:"+convertedDate);

} catch (Exception ex) {
    JOptionPane.showMessageDialog(null, "ERROR IN DATE PLEASE REPEAT DATE!");
}
return convertedDate;
}
   
    public void updateDoc() {

        upDateDocButton.addActionListener((ActionEvent ae) -> {
            if (ae.getSource() == upDateDocButton) {
                int numj = jTable.getSelectedRow();
                Tiers t1 = (Tiers) allTiersList.getSelectedValue();
                for (int i = 0; i < cashier.jolist.size(); i++) {
                    cashier.jolist.get(i).libelle = libelle.getText();
                    cashier.jolist.get(i).dateOperation = dateFormat(); 
                    
                    if (t1 != null && cashier.jolist.size()<3) {
                        cashier.jolist.get(numj).numTiers = t1.numTiers;
                    }
                    
                }
                 if (t1 != null) {
                        cashier.jolist.get(numj).numTiers = t1.numTiers;
                    }
                 
                if (isDebit) {
                    
                    cashier.jolist.get(numj).montantDebit = Double.parseDouble(montant());
                    cashier.jolist.get(numj).compteDebit = ((Compte) allCompteList.getSelectedValue()).numCompte;
                    
                } else {
                    
                    cashier.jolist.get(numj).montantCredit = Double.parseDouble(montant());
                    cashier.jolist.get(numj).compteCredit = ((Compte) allCompteList.getSelectedValue()).numCompte;
                    
                }
                ///////////////////////////////////////////////////////////////
                showCompte();
                setData();
            }
        });
    }

    /**
     * **INITIALISATION DE VARIABLE A ZERO APRES L'ENREGISTREMENT**************
     */
    public void Azero() {
        LibelleLettrage="NOPE";
        credit = false;
        cashier.jolist.clear();
        NUM_TIERS = null;
        CTIERS = null;
        t = null;
        codejr = null;
        CPTE_DEBIT = "";
        CPTE_CREDIT = "";
        LIB = "";
        DATEDOC = "";
        montantDebit = 0;
        montantCredit = 0;
        TYPE_OPERATION = "";
        MONTANT.setText("");
        libelle.setText("");
        Date dd=new Date();
        dateOperation2.setText(Cashier.getDate());//.setDate(new Date());
        showCompte();
        allJournaux.clearSelection();
        allJournaux.clearSelection();
        profitJList.clearSelection();
        setData();
        save.setText("SAVE");
        save.setBackground(new Color(105, 200, 155));
        LinkedList <String> lesLib=new LinkedList<>();
        lesLib.add("LIBELLE");
        //allLibelle.setListData(lesLib.toArray());
        
    }
 
    public void compte() {

        compteButton.addActionListener((ActionEvent ae) -> {
            if (ae.getSource() == compteButton) {
                //                    String choix = (String) JOptionPane.showInputDialog(null, " MAKE A CHOICE !", "Les Comptes", JOptionPane.QUESTION_MESSAGE, null, new String[]{"Nouveau Compte", "Sous Compte - niveau existant",  "Sous Compte - creer un sous niveau", "Modifier Compte", "Liste des comptes"}, "");
                
                String choix = (String) JOptionPane.showInputDialog
                                                                (null, " MAKE A CHOICE !", "Les Comptes", JOptionPane.QUESTION_MESSAGE, null,
                            new String[]{
                                "Nouveau Compte",
                                "Sous Compte - New Sub-Level",
                                "Modifier Compte",
                                "Effacer Compte",
                                "Liste des comptes",
                                "voir tout les sous comptes"}, "");
                switch (choix) {
                    case "Effacer Compte":
                        if (allCompteList.getSelectedValue() != null) {
                            Compte tcc = (Compte) allCompteList.getSelectedValue();
                             
                            modeTiersViewMode = "APPARITION";
                            setData2();
                            
                            int linge = joList2.size();
                             
                            if(linge==0)
                            {
                                int n = JOptionPane.showConfirmDialog(frame, "VOULEZ-VOUS EFFACER LE COMPTE ? \n  "+tcc,
                                        "MESSAGE", JOptionPane.YES_NO_OPTION);
                                
                                if (n == 0) {
                                    boolean deleted=    
                                            cashier.db.deleteCompte(tcc,umukozi.NOM_EMPLOYE);
                                     if(deleted)
                                    {
                                     
                                        JOptionPane.showMessageDialog(null, tcc.nomCompte+" SUCCESSFULLY DELETED ", " EFFACER COMPTE ", JOptionPane.ERROR_MESSAGE);
                                     
                                    }
                                }
                            }
                            else
                            {
                            JOptionPane.showMessageDialog(frame, "REGARDE DANS LE PANIER EXISTE DES OPERATIONS EFFACER LES D'ABORD!",
                                    "VERIFIEZ VOTRE SELECTION !", JOptionPane.WARNING_MESSAGE);
                       
                            }
                
                            
                        } else {
                            JOptionPane.showMessageDialog(frame, "CHOISISSEZ D'ABORD UN COMPTE DANS LA LISTE DES COMPTE SVP",
                                    "VERIFIEZ VOTRE SELECTION !", JOptionPane.WARNING_MESSAGE);
                        }   
                    case "voir tout les sous comptes":
                        ///////////////////////////////////
                        
                        Compte  co=(Compte)allCompteList.getSelectedValue();
                        
                        SetDataEnfant(co.numCompte);
                        
                        break;
                    case "Nouveau Compte":
                        ///////////////////////////////////
                        
                        new CompteInterface(cashier.db);
                        break;
                        //                    else if (choix.equals("Sous Compte - creer un sous niveau")) {////////////////////////////////////////////
                    case "Sous Compte - New Sub-Level":
                        ////////////////////////////////////
                        
                        String sousCpte;
                        //cashier.db.sizeSous=0;
                        cashier.db.selectCpte(byname);
                        Compte  c=(Compte)allCompteList.getSelectedValue();
                        if(c==null)
                        { c = (Compte) JOptionPane.showInputDialog(null, "Faites votre choix", "Liste des Comptes",
                                JOptionPane.QUESTION_MESSAGE, null, cashier.db.allCompte.toArray(), null);}
                        cashier.db.selectSousCpte(c.numCompte); //selectionner le sous compte dont le compte parent est c.numCompte
                        //                        Compte n = new Compte("0", "NEW SAME-LEVEL", 0, "0");
                        Compte n = new Compte("0", "NEW SUB-LEVEL", "0",0, "0");
                        cashier.db.allSousCompte.add(n);
                        //                        cashier.db.allSousCompte.add(nn);
                        Compte s = (Compte) JOptionPane.showInputDialog(null, "Faites votre choix", "Liste des Comptes",
                                JOptionPane.QUESTION_MESSAGE, null, cashier.db.allSousCompte.toArray(), null);
//                        if (state.numCompte == 0) { // a 20, 201, 202 tu veux ajouter 203
                        if (s.numCompte.equals("0")) {
                            // a 20, 201, 202 tu veux ajouter 203
                            
                            sousCpte = cashier.db.getNumSouscpte("" + c.numCompte);
                            
                            CompteInterface compteInterface = new CompteInterface(cashier.db, sousCpte, c);
                        } //                        else if  (state.numCompte.equals("00")) { // a 20, 201, 202 tu veux ajouter 2021
                        else if (!(s.numCompte.equals("0"))) {
                            cashier.db.selectSousCpte(s.numCompte);
                            Compte n1 = new Compte("0", "NEW SUB-LEVEL","0", 0, "0");
                            cashier.db.allSousCompte.add(n1);
                            Compte s3 = (Compte) JOptionPane.showInputDialog(null, "Faites votre choix", "Liste des Comptes",
                                    JOptionPane.QUESTION_MESSAGE, null, cashier.db.allSousCompte.toArray(), null);
                            if (s3.numCompte.equals("0")) {
                                sousCpte = cashier.db.getNumSouscpte("" + s.numCompte);
                                
                                CompteInterface compteInterface = new CompteInterface(cashier.db, sousCpte, s);
                            } else if (!(s3.numCompte.equals("0"))) {
                                cashier.db.selectSousCpte(s3.numCompte);
                                Compte n2 = new Compte("0", "NEW SUB -LEVEL","0", 0, "0");
                                cashier.db.allSousCompte.add(n2);
                                Compte s4 = (Compte) JOptionPane.showInputDialog(null, "Faites votre choix", "Liste des Comptes",
                                        JOptionPane.QUESTION_MESSAGE, null, cashier.db.allSousCompte.toArray(), null);
                                if (s4.numCompte.equals("0")) {
                                    sousCpte = cashier.db.getNumSouscpte("" + s3.numCompte);
                                    CompteInterface compteInterface = new CompteInterface(cashier.db, sousCpte, s3);
                                } else if (!(s4.numCompte.equals("0"))) {
                                    cashier.db.selectSousCpte(s4.numCompte);
                                    Compte n3 = new Compte("0", "NEW SUB -LEVEL","0", 0, "0");
                                    cashier.db.allSousCompte.add(n3);
                                    Compte s5 = (Compte) JOptionPane.showInputDialog(null, "Faites votre choix", "Liste des Comptes",
                                            JOptionPane.QUESTION_MESSAGE, null, cashier.db.allSousCompte.toArray(), null);
                                    if (s5.numCompte.equals("0")) {
                                        sousCpte = cashier.db.getNumSouscpte("" + s4.numCompte);
                                        CompteInterface compteInterface = new CompteInterface(cashier.db, sousCpte, s4);
                                    } else if (!(s5.numCompte.equals("0"))) {
                                        JOptionPane.showMessageDialog(null, "LES NIVEAUX DE SOUS COMPTE SONT TERMINE");
                                    }
                                }
                            }
                        }
                        break;
                    case "Modifier Compte":
                        ////////////////////////////////////////
                        
                        cashier.db.selectCpte(byname);
                        Compte t1 = (Compte)  allCompteList.getSelectedValue();
                        if (t1 == null) {
                            t1 = (Compte) JOptionPane.showInputDialog(null, "Faites votre choix", "Liste des Comptes",
                                    JOptionPane.QUESTION_MESSAGE, null, cashier.db.allCompte.toArray(), null);
                        }
                        int row = cashier.db.possibleUpdate("" + t1.numCompte);
                        if (row != 0) {
                            if (umukozi.PWD_EMPLOYE.toUpperCase().contains("COMPTE")) {
                                CompteInterface compteInterface = new CompteInterface(cashier.db, t1);
                            } else {
                                JOptionPane.showMessageDialog(null, "LE COMPTE EST DEJA UTILISE",
                                        " LA MISE A JOUR A ECHOUE COMPTE"
                                                + "\n PAS AUTHORISE", JOptionPane.ERROR_MESSAGE);
                            }
                        } else {
                            CompteInterface compteInterface = new CompteInterface(cashier.db, t1);
                        }
                        break;
                    default:
                        ////////////////// voir la liste de tous les comptes
                        
                        cashier.db.getAllCompte();
                        JTable jjj = cashier.db.listeCompte();
                        new showJTable(jjj, "LISTE DE COMPTES ");
                        break;
                }
            }
        });
    }

    /**
     * *************************************************************************
     * actualisation de compte
     * **************************************************************************
     */
    public void actualise() {
        actualise.addActionListener((ActionEvent ae) -> {
            if (ae.getSource() == actualise) {
                showCompte();
            }
        });
    }

    /**
     * ********************************************************************************************************************
     * quand on appuiye sur le Finance
     * ****************************************************************************************************************
     */
    
    /**
     * *********************************************************************************************************************
 quand on appuiye sur salariesButton
 ****************************************************************************************************************
     */
    public void data() {

        dataButton.addActionListener((ActionEvent ae) -> {
            if (ae.getSource() == dataButton) {
                /*
                
                System.out.println("database:" + dataBase);
                System.out.println("server:" + Server);
                
                new VoirFichier(dataBase, umukozi.NOM_EMPLOYE, cashier, Server);
                
            */
                }
        });
    }

    /**
     * ********************************************************************************************************************
     * quand on appuiye sur le bouton dettes
     * ****************************************************************************************************************
     */


    public void conge() {

        conge.addActionListener((ActionEvent ae) -> {
            if (ae.getSource() == conge) {
               // new InterfaceSalaire(cashier);
            }       });
    }
    
    
    public void data3() {

        salariesButton.addActionListener((ActionEvent ae) -> {
            if (ae.getSource() == salariesButton) {
                new InterfaceSalaire(cashier);
            }       });
    }
    
    
    
    public void attendance() {

        attendance.addActionListener((ActionEvent ae) -> {
            if (ae.getSource() == attendance) {
               // new InterfaceSalaire(cashier);
            }       });
    }
    
public String getSelectedExport() {
         
expLength=joList2.size();
String res="";
        
System.out.println(expLength);
for (int x = 0; x < expLength; x++) {

Journal  C = joList2.get(x);
Tiers tier =cashier.db.getTiersb(C.numTiers); 
String name="";
if(tier!=null)
{ name=tier.sigleTiers;  }

//030216;18171;701100;VENLOC; 282   PHARMACURE PHARMACY  ; ;68521 NC1600282;282;11900;0
  String factureSage = 
          C.dateDoc + ";" +C.NumOperation+ ";" + C.journaux + ";" + C.compteDebit+C.compteCredit + ";  " 
          + C.libelle+" " + name + " ;" + name+";" +  C.NumOperation  + ";" +C.numJournal   
          + ";" + setVirgule2(C.montantDebit, 1, 0.005, "")
          + ";" + setVirgule2(C.montantCredit, 1, 0.005, "") ;
  
           //  System.out.println(factureSage);       
 res+=factureSage+"\n";
  }
              
  return res;
        
}
   
    
    public void tiers() {

        tiersButton.addActionListener((ActionEvent ae) -> {
            if (ae.getSource() == tiersButton) {
                String numTiers;
                String choix = (String) JOptionPane.showInputDialog(null, " MAKE A CHOICE !", "Le Tier",
                        JOptionPane.QUESTION_MESSAGE, null, new String[]
                        {   "Nouveau Tiers",
                            "Voir Tiers",
                            "Modifier Tiers", 
                            "Effacer Tiers"},
                            "Nouveau Tiers");
                
                switch (choix) {
                    case "Nouveau Tiers":
                        String cNew = (String) JOptionPane.showInputDialog(null, " MAKE A CHOICE !",
                                "Le Tier", JOptionPane.QUESTION_MESSAGE, null, new String[]
                                {"Clients", "Fournisseurs", "Personnel", "Organismes Sociaux",
                                    "Etat", "Groupe et associé"}, "");
                        String sc="";
                        if (null == cNew) {
                            JOptionPane.showMessageDialog(null, "ANNOMALIE",
                                    " QUESTION ", JOptionPane.WARNING_MESSAGE);
                        }
                        else switch (cNew) {
                    case "Clients":
                        sc=("" + 41);
                        break;
                    case "Fournisseurs":
                        sc=("" + 40);
                        break;
                    case "Personnel":
                        sc=("" + 42);
                        break;
                    case "Organismes Sociaux":
                        sc=("" + 43);
                        break;
                    case "Etat":
                        sc=("" + 44);
                        break;
                    case "Groupe et associé":
                        sc=("" + 45);
                        break;
                    default:
                        break;
                }
                        
                        Compte sous = (Compte) JOptionPane.showInputDialog(null, "Faites votre choix", "Liste des Comptes",
                                JOptionPane.QUESTION_MESSAGE, null, cashier.db.cpteListByNameCompte(sc).toArray(), null);
                        System.out.println("sous cpte: " + sous.numCompte);
                        numTiers = cashier.db.getNumTiers("" + sous.numCompte);
                        new TiersInterface(cashier.db, numTiers, sous.numCompte, 0,"");
                        break;
                    case "Effacer Tiers":
                        if (allTiersList.getSelectedValue() != null) {
                            Tiers tcc = (Tiers) allTiersList.getSelectedValue();
                            
                            
                            modeTiersViewMode = "APPARITION";
                            setData2();
                            
                            int linge = joList2.size();
                            
                            if (cashier.db.estComptabilise(tcc.numAffilie)) {
                                linge = 5;
                                JOptionPane.showConfirmDialog(null, "Update aborted EXIST INVOICE NON COMTABILISE ", "Anomalie ", JOptionPane.YES_NO_OPTION);
                            }
                            
                            if(linge==0)
                            {
                                int n = JOptionPane.showConfirmDialog(frame, "VOULEZ-VOUS EFFACER LETIERS ? \n  "+tcc,
                                        "MESSAGE", JOptionPane.YES_NO_OPTION);
                                
                                if (n == 0) {
                                    boolean deleted=    
                                            cashier.db.deleteTier(tcc,umukozi.NOM_EMPLOYE);
                                     if(deleted)
                                    {
                                    deleted= cashier.db.sleepClient(tcc.numAffilie);
                                    if(deleted)
                                    {
                                        JOptionPane.showMessageDialog(null, tcc.designation+" SUCCESSFULLY DELETED ", " EFFACER TIER ", JOptionPane.ERROR_MESSAGE);
                                        
                                    }
                                    }
                                }
                            }
                            else
                            {
                                //default icon, custom title
                                int n = JOptionPane.showConfirmDialog(frame, "VOULEZ-VOUS TRANSFEREZ LES TRANSACTION? \n DE "+tcc,
                                        "MESSAGE", JOptionPane.YES_NO_OPTION);
                                
                                if (n == 0) {
                                    Tiers toReplace = (Tiers) JOptionPane.showInputDialog(null, " SELECT A TIER ", "Le Tier",
                                            JOptionPane.QUESTION_MESSAGE, null,cashier.db.getAllTiers().toArray(),
                                            "");
                                    n = JOptionPane.showConfirmDialog(frame, "VOULEZ-VOUS TRANSFEREZ LES TRANSACTION \n DE "+tcc+" A \n "+toReplace,
                                            "MESSAGE", JOptionPane.YES_NO_OPTION);
                                    
                                    if (n == 0) {
                                        
                                        if(tcc.numCompte.equals(toReplace.numCompte))
                                        {
                                            boolean updated =cashier.db.upDateJournalTier(tcc.numTiers, toReplace.numTiers,umukozi.NOM_EMPLOYE);
                                            if(updated)
                                            {
                                                JOptionPane.showMessageDialog(null, toReplace.designation+" SUCCESSFULLY TRANSFERD "
                                                        + "\n to "+tcc.numTiers, " TRANSFER TIER ", JOptionPane.ERROR_MESSAGE);
                                            }
                                        }
                                        else
                                        {
                                            String ss=  JOptionPane.showInputDialog(frame,
                                                    " NUM COMPTE DIFFERENT \n DE "+tcc.numCompte+" A   "+toReplace.numCompte,
                                                    " MESSAGE FORCE?", JOptionPane.ERROR_MESSAGE)   ;
                                            

                                            if(ss!=null && ss.equals("FORCE"))
                                            {
                                                boolean updated =   cashier.db.upDateJournalTier(tcc.numTiers, toReplace.numTiers,umukozi.NOM_EMPLOYE);
                                                if(updated)
                                                {
                                                    JOptionPane.showMessageDialog(null, toReplace.designation+" SUCCESSFULLY TRANSFERD "
                                                            + "\n to "+tcc.numTiers, " TRANSFER TIER ", JOptionPane.ERROR_MESSAGE);
                                                }
                                            }
                                            
                                            
                                        }
                                        
                                        
                                    }
                                    
                                }
                                
                                
                                
                            }
                            
                            
                            
                        } else {
                            JOptionPane.showMessageDialog(frame, "CHOISISSEZ D'ABORD UN TIERS DANS LA LISTE DES TIERS SVP",
                                    "VERIFIEZ VOTRE SELECTION !", JOptionPane.WARNING_MESSAGE);
                        }                           break;
                    case "Modifier Tiers":
                        if (allTiersList.getSelectedValue() != null) {
                            Tiers tcc = (Tiers) allTiersList.getSelectedValue();
                            
                            if(umukozi.PWD_EMPLOYE.toUpperCase().contains("TIER"))
                            { new TiersInterface(cashier.db, tcc); }
                            else
                            {
                                JOptionPane.showMessageDialog(null, "LE TIERS EST DEJA UTILISE",
                                        " LA MISE A JOUR A ECHOUE TIER"
                                                + "\n PAS AUTHORISE", JOptionPane.ERROR_MESSAGE);
                            }
                            
                            
                        } else {
                            JOptionPane.showMessageDialog(frame, "CHOISISSEZ D'ABORD UN TIERS DANS LA LISTE DES TIERS SVP",
                                    "VERIFIEZ VOTRE SELECTION !", JOptionPane.WARNING_MESSAGE);
                        }   break;
                    default:
                        cashier.db.getAllTiers();
                        JTable jjj = cashier.db.listeTiers();
                        new showJTable(jjj, "LISTE DES TIERS ");
                        break;
                }
            }
        });
    }
 

    public void voirTier() {
        voirTierButton.addActionListener((ActionEvent ae) -> {
            if (ae.getSource() == voirTierButton && allTiersList.getSelectedValue() != null) {
                String choix = (String) JOptionPane.showInputDialog(null, " MAKE A CHOICE !", "Les Views", JOptionPane.QUESTION_MESSAGE, null,
                        new String[]{"TIER", "APPARITION TIER", "DETTE TIER", "TOUTE LES DETTES"}, "");
                
//                    Tiers t = ((Tiers) allTiersList.getSelectedValue());
//                    if (choix.equals("APPARITION TIER")) {
//                        // new showJTable(profitLoss(1),"GRAND LIVRE DETAILLE DU TIER:"+((Tiers)allTiersList.getSelectedValue()).designation+" SD EST "+setVirgule((int)SDebit)+" SC EST "+setVirgule((int)SCredit)+" ET DETTES "+setVirgule((int)dette));
//                        new Document(profitLoss(1), "", toPrint, cashier.db, "GRAND LIVRE DETAILLE DU TIER:" + t.designation + " SD EST " + setVirgule((int) SDebit) + " SC EST " + setVirgule((int) SCredit) + " ET DETTES " + setVirgule((int) dette), (int) SDebit, (int) SCredit, "JOURNAL", t);
//                    } else if (choix.equals("TIER")) {
//                        new Document(profitLoss(2), "", toPrint, cashier.db, "GRAND LIVRE SIMPLIFIE DU TIER:" + t.designation + " SD EST " + setVirgule((int) SDebit) + " SC EST " + setVirgule((int) SCredit) + " ET DETTES " + setVirgule((int) dette), (int) SDebit, (int) SCredit, "TIERS", t);
//                    } else if (choix.equals("DETTE TIER")) {
//                        new showJTable(laDette(1), "DETTE DU TIER:" + t.designation);
//                    } else {
//                        new showJTable(laDette(2), "LISTE DE DETTE");
//                    }
            }
        });
    }

    String dateFormat() {

        System.out.println(dateOperation2.getText().replaceAll("-",""));
        
        String datet=dateOperation2.getText();
        
        String []sp=datet.split("-");
        boolean jour=0<Integer.parseInt(sp[0]) && Integer.parseInt(sp[0]) <32;
        
        if(!jour)
        {
            JOptionPane.showConfirmDialog(searchCompte, sp[0]+" DATE FORMAT DD NOT ACCEPTED 1 < x < 31 ");
        }
         
         System.out.println("jour "+jour);
         boolean mois=0<Integer.parseInt(sp[1]) && Integer.parseInt(sp[1]) <13;
         
         if(!mois)
         {
             JOptionPane.showConfirmDialog(searchCompte, sp[1]+" DATE FORMAT MM NOT ACCEPTED 1 < x < 12  ");
         }   
           
        
        return dateOperation2.getText().replaceAll("-","");
        
         
    }

    String getDateRepete(String dd, int mm, int yy) {

        String ret  ;

        if (mm == 12) {
            ret = dd + "01" + (yy + 1);
        } else if (mm > 8) {
            ret = dd + (mm + 1) + (yy);
        } else {
            ret = dd + "0" + (mm + 1) + yy;
        }
        return ret;
    }

    String searchPeriode() {

        String debut = deb.getText();
        String fini  = fin.getText();

        if (datePeriode.isSelected()) {

            if (debut != null && debut.length() == 10) {

                if (fini != null && fini.length() == 10) {

                    String de = debut.substring(2, 4) + debut.substring(5, 7) + debut.substring(8, 10);
                    String fi = fini.substring(2, 4) + fini.substring(5, 7) + fini.substring(8, 10);

                    int debb ;
                    int finn  ;

                    try {

                        debb = Integer.parseInt(de);
                        finn = Integer.parseInt(fi);

                    } catch (NumberFormatException e) {
                        JOptionPane.showMessageDialog(null, "FORMAT ACCEPTE: 2013-01-01 \n   " + e);
                        return "";
                    }

                    if (debb > 0 && finn > 0) {
                        return " AND DATE_DOC_INT >= " + debb + " AND DATE_DOC_INT <= " + finn;
                    } else {
                        JOptionPane.showMessageDialog(null, "FORMAT ACCEPTE: 2013-01-01\n  Verifiez  l'ecriture du debut (" + debut + ") \n et de la fin (" + fini + ") de la periode");
                        return "";
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "FORMAT ACCEPTE: 2013-01-31\n  Verifiez l'ecriture \n de la fin de la periode " + fini);
                    return "";
                }

            } else {
                JOptionPane.showMessageDialog(null, "FORMAT ACCEPTE: 2013-01-31\n Verifiez l'ecriture \n du debut de la periode " + debut);
                return "";
            }

        } else {
            return "";
        }

    }
    
    
     String searchPeriodeFini2(String fini) {

        String debut = "2010-01-01";
 
            if (  debut.length() == 10) {

                if (fini != null && fini.length() == 10) {

                    String de = debut.substring(2, 4) + debut.substring(5, 7) + debut.substring(8, 10);
                    String fi = fini.substring(2, 4) + fini.substring(5, 7) + fini.substring(8, 10);

                    int debb ;
                    int finn  ;

                    try {

                        debb = Integer.parseInt(de);
                        finn = Integer.parseInt(fi);

                    } catch (NumberFormatException e) {
                        JOptionPane.showMessageDialog(null, "FORMAT ACCEPTE: 2013-01-01 \n   " + e);
                        return "";
                    }

                    if (debb > 0 && finn > 0) {
                        return " AND DATE_DOC_INT >= " + debb + " AND DATE_DOC_INT <= " + finn;
                    } else {
                        JOptionPane.showMessageDialog(null, "FORMAT ACCEPTE: 2013-01-01\n  Verifiez  l'ecriture du debut (" + debut + ") \n et de la fin (" + fini + ") de la periode");
                        return "";
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "FORMAT ACCEPTE: 2013-01-31\n  Verifiez l'ecriture \n de la fin de la periode " + fini);
                    return "";
                }

            } else {
                JOptionPane.showMessageDialog(null, "FORMAT ACCEPTE: 2013-01-31\n Verifiez l'ecriture \n du debut de la periode " + debut);
                return "";
            }


    }
    
    
  public static  String searchPeriodePublic(String debut,String fini) {
  
      
      
      if (debut != null && debut.length() == 19) {
          debut=debut.substring(0, 10);
      }
      if (fini != null && fini.length() == 19) {
          fini=fini.substring(0, 10);
      }
      
            if (debut != null && debut.length() == 10) {

                if (fini != null && fini.length() == 10) {

                    String de = debut.substring(2, 4) + debut.substring(5, 7) + debut.substring(8, 10);
                    String fi = fini.substring(2, 4) + fini.substring(5, 7) + fini.substring(8, 10);

                    int debb ;
                    int finn  ;

                    try {

                        debb = Integer.parseInt(de);
                        finn = Integer.parseInt(fi);

                    } catch (NumberFormatException e) {
                        JOptionPane.showMessageDialog(null, "FORMAT ACCEPTE: 2013-01-01 \n   " + e);
                        return "";
                    }

                    if (debb > 0 && finn > 0) {
                        return " AND DATE_DOC_INT >= " + debb + " AND DATE_DOC_INT <= " + finn;
                    } else {
                        JOptionPane.showMessageDialog(null, "FORMAT ACCEPTE: 2013-01-01\n  Verifiez  l'ecriture du debut (" + debut + ") \n et de la fin (" + fini + ") de la periode");
                        return "";
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "FORMAT ACCEPTE: 2013-01-31\n  Verifiez l'ecriture \n de la fin de la periode " + fini);
                    return "";
                }

            } else {
                JOptionPane.showMessageDialog(null, "FORMAT ACCEPTE: 2013-01-31\n Verifiez l'ecriture \n du debut de la periode " + debut);
                return "";
            }
 

    }
    
    
   public static  String searchPeriodeStartBeta(String debut) {

       // String debut = deb.getText(); 

        if (debut != null && debut.length() == 19) {
          debut=debut.substring(0, 10);
      }
            if (debut != null && debut.length() == 10) {
  
                    String de = debut.substring(2, 4) + debut.substring(5, 7) + debut.substring(8, 10); 
                    int debb ; 
                    try {
                         debb = Integer.parseInt(de); 
                        } catch (NumberFormatException e) {
                        JOptionPane.showMessageDialog(null, "FORMAT ACCEPTE: 2013-01-01 \n   " + e);
                        return "";
                    }
                 if (debb > 0 ) {
                        return " AND DATE_DOC_INT >= 100101 AND DATE_DOC_INT < " + debb;
                    } else {
                        JOptionPane.showMessageDialog(null, "FORMAT ACCEPTE: 2013-01-01\n  Verifiez  l'ecriture du debut (" + debut + ") \n  de la periode");
                        return "";
                    } 
            } else {
                JOptionPane.showMessageDialog(null, "FORMAT ACCEPTE: 2013-01-31\n Verifiez l'ecriture \n du debut de la periode " + debut);
                return "";
            }

        

    }

    String searchPeriodeTimeStamp() {

        String debut = deb.getText();
        String fini = fin.getText();

        if (datePeriode.isSelected()) {
            if (debut != null && debut.length() == 10) {
                if (fini != null && fini.length() == 10) {

//                    String de = debut.substring(2, 4) + debut.substring(5, 7) + debut.substring(8, 10);
//                    String fi = fini.substring(2, 4) + fini.substring(5, 7) + fini.substring(8, 10);

                    debut = debut + " 00:00:00.000";
                    fini = fini + " 23:59:00.000";

                    return " AND TIME_SAVED>= '" + debut + "' AND TIME_SAVED <= '" + fini + "'";

                } else {
                    JOptionPane.showMessageDialog(null, "FORMAT ACCEPTE: 2013-01-31\n  Verifiez la fin de la periode " + fini);
                    return "";
                }
            } else {
                JOptionPane.showMessageDialog(null, "FORMAT ACCEPTE: 2013-01-31\n Verifiez le debut de la periode " + debut);
                return "";
            }
        } else {
            return "";
        }
    }
    
    
     
    @Override
    protected void processWindowEvent(WindowEvent evt) {

        if (WindowEvent.WINDOW_CLOSING == evt.getID()) {

            //default icon, custom title
            int n = JOptionPane.showConfirmDialog(frame, "VOULEZ-VOUS TERMINER LA SESSION ACTUELLE ?", "MESSAGE", JOptionPane.YES_NO_OPTION);

            if (n == 0) {
                //cashier.basketEnd();
                //saleJList.setListData(new String [0]);

//try
//{
//cashier.db.insertLogin(umukozi,"out");
//String sto = " COMPTABLE : "+umukozi+" TERMINE SA SESSION ";
//sto=sto+"\n TEMPS :  "+new Date ().toLocaleString();
//String sto1="\n-----------------------"; String sto2="\n----------------------- ";
////sto1=sto1+"\n";  sto2=sto2+"\n ";
//sto1=sto1+"\n VOUS AVEZ COMPTABILISÉ :  ";sto2=sto2+"\n"+setVirgule(Cashier.yose);
//sto1=sto1+"\n-----------------------";  sto2=sto2+"\n----------------------- ";
//sto1=sto1+"\n ";   sto2=sto2+"\n ";
//sto1=sto1+"\n           BONNE JOURNEE";   sto2=sto2+"\n ";
//PrintCommand(sto,sto1,sto2,"");
////cashier.db.backUpDatabase(cashier.db.conn,variable.value);
//cashier.db.closeConnection();
//
//        }  catch (Throwable e)  {cashier.db.insertError(e+"","processWindowEvent");}

                JOptionPane.showMessageDialog(frame, " MERCI DE TRAVAIL AVEC ISHYIGA", "FIN DE LA SESSION", JOptionPane.PLAIN_MESSAGE);

                System.exit(0);

            }

        }

    }
 
    public void printInt(Graphics pg, String s, int w, int h) {

        int back = 0;

        for (int i = s.length() - 1; i >= 0; i--) {
            if (s.charAt(i) == ' ') {
                back += 2;
            } else {
                back += 5;
            }

            pg.drawString("" + s.charAt(i), w - back, h);

        }
    }
    
   
     
public void numOperRadioClicked() {

        RADIO_OPERATION.addActionListener((ActionEvent ae) -> {
            System.out.println("arriving in numOperRadioClicked ... ");
            
            String choixb = (String) JOptionPane.showInputDialog(null, " MAKE A CHOICE !",
                    "NUMERO D'OPERATION", JOptionPane.QUESTION_MESSAGE, null,
                    new String[]{"ORDER BY NUM_OP"}, "ORDER BY NUM_OP");
            
            if (choixb != null && choixb.equals("ORDER BY NUM_OP")) {
                
                setData5();
                
                if (allJournaux.getSelectedValue() != null) {
                    
                    controlColor("journaux");
                    Journaux j = (Journaux) allJournaux.getSelectedValue();
                    labelCurrentOp.setText("NEXT OPERATION " + j.id + ": " + cashier.db.getNumOperation2(j.id, cashier.db.state));
                    
                }
                
            }
        });
    }
     public void libRadioClicked() {

        RADIO_LIBELE.addActionListener((ActionEvent ae) -> { 
            
            String choixb = (String) JOptionPane.showInputDialog(null, " MAKE A CHOICE !",
                    "LIBELLE", JOptionPane.QUESTION_MESSAGE, null,
                    new String[]{"ORDER BY LIBELLE","NON"}, "ORDER BY LIBELLE");
            
            if (choixb != null && choixb.equals("ORDER BY LIBELLE")) {
                
                setDataLib();
             
                
            }
        });
    }

    
    void setData5() {

        Collections.sort(joList2);

        int linge = joList2.size();
        sommeResDebit = 0;
        sommeResCredit = 0;
        soldeRes = 0;

        String[][] SList = new String[linge][9];

        for (int x = 0; x < linge; x++) {

            Journal C = joList2.get(x);

            SList[x][0] = "" + C.journaux;
            SList[x][1] = "" + C.NumOperation;
            SList[x][2] = "" + C.dateDoc;
//            s1[x][2] = "" + cashier.db.getTiers(C.numTiers, "");
            SList[x][3] = "" + C.numTiers;
            SList[x][4] = "" + C.libelle;
            SList[x][5] = "" + C.compteDebit;
            SList[x][6] = "" + C.compteCredit;
//            s1[x][6] = "" + DbHandler.setVirgule(C.montantDebit, 1, 0.005) ;
//            s1[x][7] = "" +  DbHandler.setVirgule(C.montantCredit, 1, 0.005) ;
            SList[x][7] = "" + cashier.db.setVirgule((int) C.montantDebit);
            SList[x][8] = "" + cashier.db.setVirgule((int) C.montantCredit);

            sommeResDebit = sommeResDebit + C.montantDebit;
            sommeResCredit = sommeResCredit + C.montantCredit;


        }

        totalDebitRES.setText(DbHandler.setVirgule(sommeResDebit, 1, 0.005) + "  " +CURRENT_CURRENCY);
        totalCreditRES.setText(DbHandler.setVirgule(sommeResCredit, 1, 0.005) + "  " +LOCAL_CURRENCY2);
        String solde  ;
        soldeRes = Math.abs(sommeResCredit - sommeResDebit); 
        if (sommeResCredit > sommeResDebit) {
            solde = "  SOLDE CREDITEUR DE " + setVirgule((int) (sommeResCredit - sommeResDebit)) + "  " +CURRENT_CURRENCY;
        } else {
            solde = "  SOLDE DEBITEUR DE " + setVirgule((int) (sommeResDebit - sommeResCredit)) + "  " +CURRENT_CURRENCY;
        }
        soldeLabel.setText(solde);setTitle(Main.Title+solde);
        search.setText("");
        jTable2.setModel(new javax.swing.table.DefaultTableModel(SList, new String[]{"JOURNAL", "OP", "DATE", "TIERS", "LIBELLE", "CPTE D", "CPTE C", " DEBIT", " CREDIT"}));
        jTable2.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        jTable2.getColumnModel().getColumn(2).setPreferredWidth(130);
        jTable2.getColumnModel().getColumn(3).setPreferredWidth(180);
        jTable2.getColumnModel().getColumn(6).setPreferredWidth(110);
        jTable2.getColumnModel().getColumn(7).setPreferredWidth(110);
        jTable2.doLayout();
        jTable2.validate();
    }


    void setDataDevise() {
 

        int linge = joList2.size();
        sommeResDebit = 0;
        sommeResCredit = 0;
        soldeRes = 0;

        String[][] SList = new String[linge][9];

        for (int x = 0; x < linge; x++) {

            Journal C = joList2.get(x);

            SList[x][0] = "" + C.journaux;
            SList[x][1] = "" + C.NumOperation;
            SList[x][2] = "" + C.dateDoc;
//            s1[x][2] = "" + cashier.db.getTiers(C.numTiers, "");
            SList[x][3] = "" + C.numTiers;
            SList[x][4] = "" + C.libelle;
            SList[x][5] = "" + C.compteDebit;
            SList[x][6] = "" + C.compteCredit;
//            s1[x][6] = "" + DbHandler.setVirgule(C.montantDebit, 1, 0.005) ;
//            s1[x][7] = "" +  DbHandler.setVirgule(C.montantCredit, 1, 0.005) ;
            SList[x][7] = "" + cashier.db.setVirgule((int)( C.montantDebit*C.DEVISE_RATE));
            SList[x][8] = "" + cashier.db.setVirgule((int) ( C.montantCredit*C.DEVISE_RATE));

            sommeResDebit = sommeResDebit + ( C.montantDebit*C.DEVISE_RATE);
            sommeResCredit = sommeResCredit + ( C.montantCredit*C.DEVISE_RATE);
        }

        totalDebitRES.setText(DbHandler.setVirgule(sommeResDebit, 1, 0.005) + "  " +LOCAL_CURRENCY2);
        totalCreditRES.setText(DbHandler.setVirgule(sommeResCredit, 1, 0.005) + "  " +LOCAL_CURRENCY2);
        String solde  ;
        soldeRes = Math.abs(sommeResCredit - sommeResDebit); 
        CURRENT_CURRENCY=LOCAL_CURRENCY2;
        if (sommeResCredit > sommeResDebit) {
            solde = "  SOLDE CREDITEUR DE " + setVirgule((int) (sommeResCredit - sommeResDebit)) + "  " +CURRENT_CURRENCY;
        } else {
            solde = "  SOLDE DEBITEUR DE " + setVirgule((int) (sommeResDebit - sommeResCredit)) + "  " +CURRENT_CURRENCY;
        }
        soldeLabel.setText(solde);setTitle(Main.Title+solde);
        search.setText("");
        jTable2.setModel(new javax.swing.table.DefaultTableModel(SList, new String[]{"JOURNAL", "OP", "DATE", "TIERS", "LIBELLE", "CPTE D", "CPTE C", " DEBIT", " CREDIT"}));
        jTable2.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        jTable2.getColumnModel().getColumn(2).setPreferredWidth(130);
        jTable2.getColumnModel().getColumn(3).setPreferredWidth(180);
        jTable2.getColumnModel().getColumn(6).setPreferredWidth(110);
        jTable2.getColumnModel().getColumn(7).setPreferredWidth(110);
        jTable2.doLayout();
        jTable2.validate();
    }

    
    void setDataLib() {

        int linge = joList2.size(); 
        for (int x = 0; x < linge; x++) { 
           joList2.get(x).sortOnLib=true;
        } 
        
        Collections.sort(joList2); 
        
        sommeResDebit = 0;
        sommeResCredit = 0;
        soldeRes = 0;

        String[][] SList = new String[linge][9];

        for (int x = 0; x < linge; x++) {

            Journal C = joList2.get(x);

            SList[x][0] = "" + C.journaux;
            SList[x][1] = "" + C.NumOperation;
            SList[x][2] = "" + C.dateDoc;
//            s1[x][2] = "" + cashier.db.getTiers(C.numTiers, "");
            SList[x][3] = "" + C.numTiers;
            SList[x][4] = "" + C.libelle;
            SList[x][5] = "" + C.compteDebit;
            SList[x][6] = "" + C.compteCredit;
//            s1[x][6] = "" + DbHandler.setVirgule(C.montantDebit, 1, 0.005) ;
//            s1[x][7] = "" +  DbHandler.setVirgule(C.montantCredit, 1, 0.005) ;
            SList[x][7] = "" + cashier.db.setVirgule((int) C.montantDebit);
            SList[x][8] = "" + cashier.db.setVirgule((int) C.montantCredit);

            sommeResDebit = sommeResDebit + C.montantDebit;
            sommeResCredit = sommeResCredit + C.montantCredit;

        }

        totalDebitRES.setText(DbHandler.setVirgule(sommeResDebit, 1, 0.005)   + "  " +CURRENT_CURRENCY);
        totalCreditRES.setText(DbHandler.setVirgule(sommeResCredit, 1, 0.005) + "  " +CURRENT_CURRENCY);
        String solde  ;
        soldeRes = Math.abs(sommeResCredit - sommeResDebit); 
        if (sommeResCredit > sommeResDebit) {
            solde = "  SOLDE CREDITEUR DE " + setVirgule((int) (sommeResCredit - sommeResDebit)) + "  " +CURRENT_CURRENCY;
        } else {
            solde = "  SOLDE DEBITEUR DE " + setVirgule((int) (sommeResDebit - sommeResCredit)) + "  " +CURRENT_CURRENCY;
        }
        soldeLabel.setText(solde);setTitle(Main.Title+solde);
        search.setText("");
        jTable2.setModel(new javax.swing.table.DefaultTableModel(SList, new String[]{"JOURNAL", "OP", "DATE", "TIERS", "LIBELLE", "CPTE D", "CPTE C", " DEBIT", " CREDIT"}));
        jTable2.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        jTable2.getColumnModel().getColumn(2).setPreferredWidth(130);
        jTable2.getColumnModel().getColumn(3).setPreferredWidth(180);
        jTable2.getColumnModel().getColumn(6).setPreferredWidth(110);
        jTable2.getColumnModel().getColumn(7).setPreferredWidth(110);
        jTable2.doLayout();
        jTable2.validate();
    }




    public static int getQuantity(String s, String s1, int i) {

        int entier = -1;

        String in = JOptionPane.showInputDialog(null, s + s1);
        boolean done = true;

        while (in != null && i < 3 && done == true) {

            try {
                if (in.equals("")) {
                    i++;
                    in = JOptionPane.showInputDialog(null, s + " Enter a number ", "1");
                } else {
                    Integer in_int = new Integer(in);
                    entier = in_int ;
                    done = false;
                    if (entier < 0 || entier == 0) {
                        entier = -1;
                    }
                }

            } catch (NumberFormatException e) {

                i++;
                in = JOptionPane.showInputDialog(null, s + " = " + in + "= Not a number; Enter a number ");
                done = true;

            }
        }
        if (i == 3) {
            JOptionPane.showMessageDialog(frame, " Too many try ", " Sorry we have to Exit ", JOptionPane.PLAIN_MESSAGE);
        }

        return entier;

    }
 

   
    
    
     /**
     * *************************************************************************
     * le main qui lance le clavardage
     * **************************************************************************
     * @param arghs
     */
    public static void main(String[] arghs) {

        byname = false;
        StartServer d;

        try {

            d = new StartServer();

            System.out.println("ip : ...... " + d.ip);

            //          int id = getIn("Nomero yumukozi ", " : umubare", 0, "");
//            int id = 1;
            int id = getQuantity("ID ", " INT ", 0);

            if (id != -1) {

                JPanel pw = new JPanel();
                JPasswordField passwordField = new JPasswordField(10);
                JLabel label = new JLabel("PWD: ");
                pw.add(label);
                pw.add(passwordField);

                passwordField.setText("");
                JOptionPane.showConfirmDialog(null, pw);
                Integer in_int = new Integer(passwordField.getText());
//                Integer in_int = 2;
//                Integer in_int = new Integer(passwordField.getText());
                int carte = in_int ;

                LinkedList<String> give = new LinkedList();

                File productFile = new File("log.txt");
                String ligne = "";
                
                
               
                try {
                    try (BufferedReader entree = new BufferedReader(new FileReader(productFile))) {
                        while (ligne != null) {
                            ligne = entree.readLine();
                            give.add(ligne);
                            
                        }                   }
     } catch (FileNotFoundException e) {
        JOptionPane.showMessageDialog(null, e+" ERROR ", "  log ", JOptionPane.PLAIN_MESSAGE);
    }
                

                String dataBase = give.get(2);
                String serv = give.get(1);
                
                Main.LOCAL_CURRENCY2= give.get(3).replaceAll(" ", "");
                if (carte != -1) {
                    
                    //System.out.println(carte);
                    Cashier c = new Cashier(id, serv, dataBase); 
                    //System.out.println(c.db.allEmploye);
                    if ( c.db.check_db2(id, carte)) {

                        Employe njyewe = c.db.getName(id); 

                        if(njyewe!=null && njyewe.PWD_EMPLOYE.contains("COMPTA"))
                        {  
                             c.db.insertLogin(njyewe.NOM_EMPLOYE, "in");
                             new Main(njyewe, c, serv, dataBase);
                        }
                        else {
                        JOptionPane.showMessageDialog(null,
                                "  ID "+id, " NOT AUTHORISED COMPTA", JOptionPane.PLAIN_MESSAGE);
                    }

                    } else {
                        JOptionPane.showMessageDialog(null, "  PWD INCORRECT ", " ACCESS DENIED ", JOptionPane.PLAIN_MESSAGE);
                    }

                }
            }
        } catch (Throwable t) {
            JOptionPane.showMessageDialog(null, " Fail to Open " + t, " ERROR ", JOptionPane.PLAIN_MESSAGE);
        }
    }
}
