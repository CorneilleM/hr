/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HR;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import javax.swing.*;
import java.sql.*;
//import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

/**
 *
 * @author Odette
 */
public class employee_charge extends JFrame {
    
    
    
    String tie;
    String chrg;
    
    
    JLabel tier,charge,amountt;
    JComboBox tieri, chargee;
    
    JTextField amount;
    JButton Save;
    JButton all;
    JPanel main = new JPanel();
    Font police;
    Color color; 
    DbHandler db;
    Container content;
    
    String numt,name;
    InterfaceSalaire interf;
    
    
    
    
     public void draw() {
        
        try{
       
        this.setTitle("Add charge to employee");
        enableEvents(WindowEvent.WINDOW_CLOSING);
        this.setResizable(false);
        this.setSize(500, 300);
         Pane();
        setSize(500, 300);
        setVisible(true);
        
        } catch (Throwable e) {
            System.out.println(" exception thrown:" + e);          
        }        
    } 
   
   
   public void load()
   {
   
   
   try{
        
        tier = new JLabel("TIER: ");
        police = new Font("Rockwell", Font.BOLD, 15);
        tier.setFont(police);
        
        
        
        charge = new JLabel("CHARGE: ");
        police = new Font("Rockwell", Font.BOLD, 15);
        charge.setFont(police);

        
        amountt = new JLabel("CHARGE AMOUNT: ");
        police = new Font("Rockwell", Font.BOLD, 15);
        amountt.setFont(police);
        
        amount = new JTextField("");
        police = new Font("Rockwell", Font.BOLD, 15);
        amount.setFont(police);

       tieri=new JComboBox();
       chargee=new JComboBox();
        
        
       
        
        Save= new JButton("SAVE");
        police = new Font("Rockwell", Font.BOLD, 15);
        Save.setFont(police);
        Save.setBackground(Color.green);
   
        all = new JButton("APPLY TO ALL");
        police = new Font("Rockwell", Font.BOLD, 15);
        all.setFont(police);
        all.setBackground(Color.green);
        
        } catch (Exception e ) {
            System.out.println(" . . makeFrameTiers . exception thrown:" + e);
//            insertError(e + "", " getBalanceVerification  ");
        }
   
   }
   
   
   
    public employee_charge(String numt, String name) {
        
        this.numt=numt;
        this.name=name;
  }
   
   
   
   
   public employee_charge(DbHandler db,String s) {

        try {
             
            
             this.db=db;
            content = getContentPane();
            

            load();
            draw();
            save();
            ToAll();
        
       
           tieri.addItem(s);
            db.fillcombo2(chargee);
            
        } catch (Throwable e) {
            System.out.println(e);

        }
    }
   
   
   
   public JPanel Pane() {
        JPanel panelmain = new JPanel();
        panelmain.setLayout(new GridLayout(4, 2, 5, 5));
        panelmain.setPreferredSize(new Dimension(450,150));
        

        panelmain.add(tier);
        panelmain.add(tieri);
        panelmain.add(charge);
//    
        panelmain.add(chargee);
        panelmain.add(amountt);
//    
        panelmain.add(amount);
  
        panelmain.add(Save);
        panelmain.add(all);
        
        

        main.add(panelmain);
        

        content.add(main);
        return main;
    }
   
   
   
   
   
   public void save() {

        Save.addActionListener(new ActionListener() {
 @Override
                    public void actionPerformed(ActionEvent ae) {

                        if (ae.getSource() ==Save) {
                            
                            
                         
                          String tr=(String) tieri.getSelectedItem();
                          String[] part = tr.split("(?<=\\D)(?=\\d)");
                            String tie=part[1];
                          
                         int ct=((cpt_lacharge) chargee.getSelectedItem()).code;
                         double montant =Double.parseDouble((String)amount.getText());
                         
   
                           db.ToOne(tie, ct, montant);

                        }
                    }
                });
    }  
    
   
   
   public void ToAll() {

        all.addActionListener(new ActionListener() {
 @Override
                    public void actionPerformed(ActionEvent ae) {

                        if (ae.getSource() ==all) {
                             
                            
                         
                          
                         String numCompte = "42";
                         
                         int ct=((cpt_lacharge) chargee.getSelectedItem()).code;
                         double montant =Double.parseDouble((String)amount.getText());
   
                           db.ToAll(numCompte, ct,montant);
                        }
                    }
                });
    }  
    
    
}

