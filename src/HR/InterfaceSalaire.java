 
package HR;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.StringTokenizer;
import javax.swing.*;

public class InterfaceSalaire extends JFrame {

    JList employees;
    JList paylist;
    JTextField searchTier;
    Font police;
    JLabel search;
    private JButton addButton;
    private JButton removeButton;
    private JButton calculSalaire;
    private JButton prep;
    private JButton payementSalaire;
    private JButton saveButton;
    private JButton cancelButton;
    private JButton clearButton;
    private JButton addnewButton;
    private JButton updateButton;
    private JButton actualize;
    Container content;
    Container content2;
    Container content3;
    Container content4;
    JPanel mainSalaire = new JPanel();
    JPanel otherbuttons = new JPanel();
    JPanel journalPreview = new JPanel();
    JPanel otherbuttons2 = new JPanel();
    private Cashier cashier;
    LinkedList<Tiers> abangaba = new LinkedList();
    LinkedList<cpt_lacharge> aba = new LinkedList();
    private JTable jTable6;
    private JLabel soldeLabel = new JLabel("          ");
//    private InterfaceAbishuwe interfAbishuwe;
    private boolean confirmSave = false;
    private JTable prepSal = new JTable();
    boolean vava = false;
//    LinkedList<Lot> ayangaya1 = new LinkedList<Lot>();
    LinkedList<Tiers> abangaba1 = new LinkedList<Tiers>();
    JList listeYabo = new JList();
    JScrollPane jScrollPane1;
    private JScrollPane scrollPaneallaccounts;
    LinkedList<Lot> ayangaya = new LinkedList<Lot>();
    LinkedList<cpt_lacharge> gutya = new LinkedList<cpt_lacharge>();
    LinkedList<cpt_lacharge> gutya2 = new LinkedList<cpt_lacharge>();
    LinkedList<cpt_lacharge> rapport = new LinkedList<cpt_lacharge>();
    LinkedList<cpt_lacharge> rapport1 = new LinkedList<cpt_lacharge>();
    LinkedList<preparation> dprep = new LinkedList<preparation>();
   
    String cas = "";
    String modePayement = "";
    String datedoc="";
    String datedoc2="";
    private static JFrame frame;
    private Object db;
    public static String s="";
    
    DecimalFormat df = new DecimalFormat("###.##");
    //DbHandler d;

    String tier_number;
    
    int dettes=0;
    Journaux choix;
    
    String month;
    
    public InterfaceSalaire(Cashier c) {
        this.cashier = c;
        initialize();
        draw();
    }

    public InterfaceSalaire(Cashier c, JTable tab6) {
        this.cashier = c;
        this.prepSal = tab6;
        initialize();
        draw();
    }

    public void initialize() {

        this.employees = new JList();
        this.employees.setListData(cashier.db.getAllEmployees().toArray());

        this.paylist = new JList();
        this.paylist.setListData(abangaba.toArray());
        
    }

    public void draw() {

        this.setTitle("GESTION DES SALAIRES ");
//        enableEvents(WindowEvent.COMPONENT_HIDDEN);

        content = getContentPane();

        content.add(NorthPane());
        content.add(centerPane());
        content.add(SouthPane1(prepSal));
        content.add(SouthPane2());

        this.setSize(1200, 900);
        this.setLayout(new GridLayout(4, 1));
        this.setResizable(true);
        setVisible(true);

        add();
        remove();
        calculsalaire();
        payementsalaire();
        clear();
        save();
        cancel();
        update();
        addcharge();
        actualise();
        prep();
        searchtier();
        
       
    }

    public JPanel NorthPane() {
  JPanel CommonNorthPane = new JPanel();
        CommonNorthPane.setPreferredSize(new Dimension(1000, 1000));

        JPanel panelEmployee = new JPanel();
        //panelEmployee.setLayout(new GridLayout(2, 1, 1, 1));//
        panelEmployee.setPreferredSize(new Dimension(450, 200));

        JScrollPane jScrollEmployee = new JScrollPane(employees);
        jScrollEmployee.setPreferredSize(new Dimension(450, 200));

        JPanel panelpaylist = new JPanel();
        panelpaylist.setLayout(new GridLayout(1, 1, 5, 5));//
        panelpaylist.setPreferredSize(new Dimension(450, 200));

        JScrollPane jScrollpaylist = new JScrollPane(paylist);
        jScrollpaylist.setPreferredSize(new Dimension(450, 200));

        JPanel panelbuttons = new JPanel();
        panelbuttons.setLayout(new GridLayout(2, 1, 2, 2));//
        panelbuttons.setPreferredSize(new Dimension(100, 200));
        
        
        
        searchTier = new JTextField("");
        police = new Font("Consolas", Font.BOLD, 15);
        searchTier.setFont(police);  
        
        search = new JLabel("Search ");
        police = new Font("Rockwell", Font.BOLD, 15);
         search.setFont(police);
         
         addnewButton = new JButton("CREATE");
        addnewButton.setBackground(Color.orange);
       addnewButton.setPreferredSize(new Dimension(100, 20));
        
        updateButton = new JButton("VIEW&UPDATE");
        updateButton.setBackground(Color.green);
       updateButton.setPreferredSize(new Dimension(100, 20));

        addButton = new JButton(">>");
        addButton.setBackground(Color.green);

        removeButton = new JButton("<<");
        removeButton.setBackground(Color.orange);
        
        
        JPanel topbuttons = new JPanel();
        topbuttons.setSize(new Dimension(400, 30));
        topbuttons.setPreferredSize(new Dimension(400,30));
       topbuttons.setLayout(new GridLayout(1, 2, 2, 0));
       
       topbuttons.add(addnewButton, BorderLayout.WEST);
        topbuttons.add(updateButton, BorderLayout.EAST);
        
        
        JPanel searchp = new JPanel();
       searchp.setSize(new Dimension(150, 30));
        searchp.setPreferredSize(new Dimension(150, 30));
        searchp.setLayout(new GridLayout(1, 2, 2, 0));
        
        searchp.add(searchTier, BorderLayout.WEST);
        searchp.add(search, BorderLayout.EAST);
        
        JPanel top = new JPanel();
       top.setSize(new Dimension(400, 30));
        top.setPreferredSize(new Dimension(400, 30));
        top.setLayout(new GridLayout(1, 2, 2, 0));
        
        top.add(topbuttons, BorderLayout.EAST);
        top.add(searchp, BorderLayout.WEST);
        
        
        
        panelEmployee.add(top, BorderLayout.NORTH);

        panelEmployee.add(jScrollEmployee, BorderLayout.CENTER);
        
       // panelEmployee.add(topbuttons, BorderLayout.EAST);
        
        panelbuttons.add(addButton);
        panelbuttons.add(removeButton);
        //panelbuttons.add(topbuttons);
        panelpaylist.add(jScrollpaylist);
        
        
        // CommonNorthPane.add(searchp, BorderLayout.NORTH);
        CommonNorthPane.add(panelEmployee, BorderLayout.WEST);
        CommonNorthPane.add(panelbuttons, BorderLayout.CENTER);
        CommonNorthPane.add(panelpaylist, BorderLayout.EAST);
        
        
        return CommonNorthPane;
    }


    public JPanel centerPane() {

        JPanel CommonCenterPane = new JPanel();
        CommonCenterPane.setPreferredSize(new Dimension(1000, 50));

        JPanel panelbuttons2 = new JPanel();
        panelbuttons2.setLayout(new GridLayout(1, 5, 5, 5));
        panelbuttons2.setPreferredSize(new Dimension(1000, 50));

        calculSalaire = new JButton("CALCUL DES SALAIRES");
        calculSalaire.setBackground(Color.LIGHT_GRAY);
        
        prep = new JButton(" DYNAMIC PREP");
        prep.setBackground(Color.yellow);

        payementSalaire = new JButton("PAYEMENT");
        payementSalaire.setBackground(Color.GRAY);

        clearButton = new JButton("CLEAR");
        clearButton.setBackground(Color.LIGHT_GRAY);
                
                
                actualize = new JButton("ACTUALIZE");
        actualize.setBackground(Color.PINK);

        panelbuttons2.add(calculSalaire);
        panelbuttons2.add(prep);
        panelbuttons2.add(payementSalaire);
        panelbuttons2.add(clearButton);
        panelbuttons2.add(actualize);

//        otherbuttons.add(panelbuttons2, BorderLayout.CENTER);
        CommonCenterPane.add(panelbuttons2, BorderLayout.CENTER);

        return CommonCenterPane;
    }

    public JPanel SouthPane1(JTable j) {

        JPanel CommonSouth1Pane = new JPanel();
        CommonSouth1Pane.setPreferredSize(new Dimension(1000, 350));


        jTable6 = imishahara();
        scrollPaneallaccounts = new JScrollPane(jTable6);
        scrollPaneallaccounts.setPreferredSize(new Dimension(1000, 230));
        jTable6.setBackground(Color.white);

//        jTableList.setPreferredSize(new Dimension(1000, 200));
//        jTableList.setPreferredSize(new Dimension(450, 670));       

        CommonSouth1Pane.add(scrollPaneallaccounts);

        return CommonSouth1Pane;
    }



    public JPanel SouthPane2() {

        JPanel CommonSouth2Pane = new JPanel();
        CommonSouth2Pane.setPreferredSize(new Dimension(1000, 50));

        JPanel panelbuttons2 = new JPanel();
        panelbuttons2.setLayout(new GridLayout(1, 2, 5, 5));
        panelbuttons2.setPreferredSize(new Dimension(200, 50));

        saveButton = new JButton("SAVE");
        saveButton.setBackground(Color.LIGHT_GRAY);

        cancelButton = new JButton("CANCEL");
        cancelButton.setBackground(Color.GRAY);

        panelbuttons2.add(saveButton);
        panelbuttons2.add(cancelButton);

        CommonSouth2Pane.add(panelbuttons2, BorderLayout.CENTER);
//        content.add(otherbuttons2);
        return CommonSouth2Pane;
    }

    public void clear() {

        clearButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == clearButton) {

                    abangaba.clear();
                    liste2paye();
//                                gutya.clear();
//                                abishyuwe(gutya, abangaba, 0.0);                     
                }
            }
        });
    }

    public void cancel() {

        cancelButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == cancelButton) {
                    abangaba.clear();
                    gutya.clear();
                    liste2paye();
                    abishyuwe(gutya);
                }
            }
        });
    }

    public void add() {
        addButton.addActionListener(
                new ActionListener() {

                    public void actionPerformed(ActionEvent ae) {
                        if (ae.getSource() == addButton && employees.getSelectedValue() != null) {

                            Object[] selectedList = employees.getSelectedValues();

                            for (int i = 0; i < selectedList.length; i++) {

                                Tiers pk = (Tiers) (selectedList[i]);

                                if (pk != null && !(abangaba.contains(pk))) {
                                    abangaba.add(pk);
                                }
                                liste2paye();
                            }
                        }
                    }
                });
    }
    
    public void actualise() {
        actualize.addActionListener(
                new ActionListener() {

                    public void actionPerformed(ActionEvent ae) {
                        if (ae.getSource() == actualize) {
                            
                            employees.clearSelection();
                            LinkedList <Tiers> emp=new LinkedList<>();
        
                           emp=cashier.db.getAllEmployees();
                          
                           employees.setListData(emp.toArray());
                          
                        }
                    }
                });
    }

    
    

    @Override
    protected void processWindowEvent(WindowEvent evt) {
        if (WindowEvent.WINDOW_CLOSING == evt.getID()) {

            int n = JOptionPane.showConfirmDialog(frame, "AVEZ-VOUS TERMINE DE COMPTABILISER LES SALAIRES ? ", "MESSAGE", JOptionPane.YES_NO_OPTION);

            if (n == 0) {
                this.dispose();    //THIS FRAME				
            }
        }
    }

    public void remove() {
        removeButton.addActionListener(
                new ActionListener() {

                    public void actionPerformed(ActionEvent ae) {
                        if (ae.getSource() == removeButton && paylist.getSelectedValue() != null) {

                            Object [] selectedList = paylist.getSelectedValues();                           

                            for (int i = 0; i < selectedList.length; i++) {

                                Tiers pk = (Tiers) (selectedList[i]);

                                abangaba.remove(pk);

                            }

                            liste2paye();

                        }
                    }
                });
    }

    public void liste2paye() {
        if (abangaba.size() != 0) {

            paylist.setListData(abangaba.toArray());

        } else {
            paylist.setListData(abangaba.toArray());
            JOptionPane.showMessageDialog(null, "PAS DE SALARIES SELECTIONNES");
        }
    }

    public boolean IsInt_ByJonas(String str) {
        if (str == null) {
            return false;
        }
        int length = str.length();
        if (length == 0) {
            return false;
        }

        int i = 0;

        if (str.charAt(0) == '-') {
            if (length == 1) {
                return false;
            }
            i = 1;
        }
        for (; i < length; i++) {
            char c = str.charAt(i);
            if (c <= '/' || c >= ':') {
                return false;
            }
        }
        return true;
    }

    String setVirgule(double frw) {
        //System.out.println("  bb"+frw);
        StringTokenizer st1 = new StringTokenizer("" + frw);

        String entier = st1.nextToken(".");
//System.out.println(frw);
        String decimal = st1.nextToken("").replace(".", "");

        if (decimal.length() == 1 && decimal.equals("0")) {
            decimal = ".00";
        } else if (decimal.length() == 1 && !decimal.equals("0")) {
            decimal = "." + decimal + "0";
        } else {
            decimal = "." + decimal.substring(0, 2);
        }

        String setString = entier + decimal;

        int l = setString.length();

        if (l < 2) {
            setString = "    " + frw;
        } else if (l < 3) {
            setString = "  " + frw;
        } else if (l < 4) {
            setString = "  " + frw;
        }

        int up = 6;
        int ju = up + 3;
        if (l > up && l <= ju) {
            setString = setString.substring(0, l - up) + "," + setString.substring(l - up);
        } else if (l > ju) {
            setString = setString.substring(0, l - 9) + "," + setString.substring(l - 9, l - 6) + "," + setString.substring(l - 6, l);
        }
        return setString;
    }


    public void calculsalaire() {

        calculSalaire.addActionListener(

                new ActionListener() {

                    public void actionPerformed(ActionEvent ae) {
                      if (ae.getSource() == calculSalaire) {
//                           
                      try {
                          
                          
                          String activity = (String) JOptionPane.showInputDialog(null, " CHOOSE?!",
                        "Your choice", JOptionPane.QUESTION_MESSAGE, null,
                        new String[]{"SALAIRE PREPARATION","SEND PREPARED SALARIES"}, "");
                          
                            
                          if( activity.equalsIgnoreCase("SALAIRE PREPARATION") && abangaba != null )
                          {
                                
                                System.out.println("abangaba.size(): " + abangaba.size());

                                int bb = abangaba.size();

                               double solde = 0.0;
                               datedoc2 = JOptionPane.showInputDialog(frame, " ENTER DATE DOC: ");
                               month = (String) JOptionPane.showInputDialog(null, " CHOOSE MONTH OF SALARY PREP!",
                        "Your choice", JOptionPane.QUESTION_MESSAGE, null,
                        new String[]{"JAN" ,"FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"}, "");
                                            
                                            
                                String mode = (String) JOptionPane.showInputDialog(null, " CHOOSE?!",
                        "Your choice", JOptionPane.QUESTION_MESSAGE, null,
                        new String[]{"SALAIRE","PRIMES ET COMMISSIONS","OTHER RETENUES"}, "");
                                    

                                        for (int x = 0; x < bb; x++) {

                                            Tiers C = abangaba.get(x);

                                            String numtiers = C.numTiers;
                                            
                                            String designation = C.designation;
                                            String info = C.info;
                                            String address = C.adresse;
                                            String devise = C.devise;
                                            String sigletiers = C.sigleTiers;
                                            String email=C.email;
                                            double salaireBrute=cashier.db.getSalaireBrute(numtiers);
                                            
                                            Double Dynamic=cashier.db.GetDynamicSalaire(month,numtiers);
                                            
                                            salaireBrute=salaireBrute+Dynamic;
                                            
                                            
                                            System.out.println("Dynamic " + Dynamic);
                                            
                                            tier_number=numtiers;
                                            
                                            double taxable=salaireBrute;
                                            
                                    
                                            System.out.println("salaireBrute: " + salaireBrute);
                                            
                                            
                                            System.out.println("Tiers: " + numtiers);
                                      gutya.add(new cpt_lacharge(designation,"" , "","","", 0, 0,numtiers,""));      
                                            
                                            if( mode.equalsIgnoreCase("SALAIRE") )
                                          {
                                              
                                            String way = (String) JOptionPane.showInputDialog(null, " WHAT DO YOU HAVE?",
                        "Your choice", JOptionPane.QUESTION_MESSAGE, null,
                        new String[]{"BASIC","NET" }, ""); 
                                            
                                           if( way.equalsIgnoreCase("NET") )
                                          { /*
                                            
                                          cashier.db.salaire_prep2(numtiers, gutya, rapport, ayangaya, designation,salaireBrute,info, address,devise, sigletiers,bb,month);
                                           
                                           preparation(rapport) ;
                                 
                                 abishyuwe(gutya);
                                 cas = "calcul";*/
                                          }
                                           
                                           else if ( way.equalsIgnoreCase("BASIC") )
                                           {
                                               
                                           
                                           cashier.db.salaire_prep(numtiers, gutya, rapport, ayangaya, designation,taxable,salaireBrute,email, address,devise, sigletiers,bb,month);
                                           
                                           preparation2(rapport) ;
                                 
                                 abishyuwe(gutya);
                                 cas = "calcul";    
                                           }
                                            
                                            
             
                                         } 
                                 else if( mode.equalsIgnoreCase("PRIMES ET COMMISSIONS") )
                                          {
                                              
                                            
                                    String nn = JOptionPane.showInputDialog(frame, "SALARIE: "+designation+"\n PRIMES ET COMMISSIONS\n AMOUNT GIVEN: ");
            String reason = JOptionPane.showInputDialog(frame, "REASON");
            if(nn == null || (nn != null && ("".equals(nn))))   
{
   JOptionPane.showMessageDialog(null, "EMPTY INPUT PLEASE", 
" QUESTION ", JOptionPane.WARNING_MESSAGE);
}
            //double odd_net=getPreparation(numtiers,mFounth).net_salary;
            
           double nn1 = Double.parseDouble(nn);
            gutya.add(new cpt_lacharge("","6411","","CHARGES COMMISSIONS ET COURTAGES SUR ACHATS","",nn1,0.0,numtiers,month+"-"+reason));
            gutya.add(new cpt_lacharge("","","421","","TIERS PERSONNEL SALAIRE NET",0.0,nn1,numtiers,month+"-"+reason));
           
            cashier.db.insertPayement(numtiers,0.0,month,0.0,nn1);
            
             abishyuwe(gutya);
                                            cas = "calcul";
            
            
                                          
                                           
                                      
                                          }
                                            
                                 else if(mode.equalsIgnoreCase("OTHER RETENUES"))
                                 {
                                 
                                 String nn = JOptionPane.showInputDialog(frame, "SALARIE: "+designation+"\n RETENUE\n AMOUNT: ");
                                    
                                    String reason = JOptionPane.showInputDialog(frame, "REASON");
            
            if(nn == null || (nn != null && ("".equals(nn))))   
{
   JOptionPane.showMessageDialog(null, "EMPTY INPUT PLEASE", 
" QUESTION ", JOptionPane.WARNING_MESSAGE);
}
            //double odd_net=getPreparation(numtiers,mounth).net_salary;
            
           double nn1 = Double.parseDouble(nn);
            gutya.add(new cpt_lacharge("","421","","TIERS PERSONNEL","",nn1,0.0,numtiers,month+"-"+reason));
            gutya.add(new cpt_lacharge("","","57","","CAISEE",0.0,nn1,numtiers,month+"-"+reason));
            
            cashier.db.insertPayement(numtiers,0.0,month,nn1,0.0);
            abishyuwe(gutya);
                                            cas = "calcul";
            
                                 
                                 }
                                  
                                        }
                                        
                          }
                          
                          else{
                          
                         String  month = (String) JOptionPane.showInputDialog(null, " CHOOSE MONTH OF SALARY PREP!",
                        "Your choice", JOptionPane.QUESTION_MESSAGE, null,
                        new String[]{"JAN" ,"FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"}, ""); 
                         
                         
                     cashier.db.sendAllMails(month);
                         
                           
                          }
                            
                              
                
          } catch (NumberFormatException e) {
                        System.out.println("1-byakwamye kabisa: " + e);

                        JOptionPane.showMessageDialog(null, "" + e, " Error ", JOptionPane.ERROR_MESSAGE);
                    }
                        } else {
                            JOptionPane.showMessageDialog(null, "CHOISISSEZ D'ABORD UN TIERS SVP", "Ooooop !!!", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                });
    }





    public void abishyuwe(LinkedList<cpt_lacharge> bibaregutya) {

        try {

            

            System.out.println(" . . bibaregutya2.size()  :" + bibaregutya.size());

            String[][] s1 = new String[bibaregutya.size() + 2][7];

            int vv = 0;

            for (; vv < bibaregutya.size(); vv++) {

                cpt_lacharge C = bibaregutya.get(vv);

                s1[vv][0] = ""+ C.employee;
                s1[vv][1] = "" + C.COMPTE_DEBITER;
                s1[vv][2] = "" + C.COMPTE_CREDITER;
                s1[vv][3] = "" + C.name_cpt;
                s1[vv][4] = "" + C.name_CRD;
                s1[vv][5] = "" + C.montant_deb;
                s1[vv][6] = "" + C.montant_cred;
            }

            //soldeLabel.setText("MONTANT A PAYER: " + solde + " FRW");

            jTable6.setModel(new javax.swing.table.DefaultTableModel(s1, new String[]{"EMPLOYE", "C_D", "C_C","N_D", "N_C", "S_D", "S_C"}));
            jTable6.setAutoResizeMode(jTable6.AUTO_RESIZE_ALL_COLUMNS);
            jTable6.getColumnModel().getColumn(0).setPreferredWidth(80);
            jTable6.getColumnModel().getColumn(1).setPreferredWidth(50);
            jTable6.getColumnModel().getColumn(2).setPreferredWidth(50);
            jTable6.getColumnModel().getColumn(3).setPreferredWidth(100);
            jTable6.getColumnModel().getColumn(4).setPreferredWidth(100);
            jTable6.getColumnModel().getColumn(5).setPreferredWidth(50);
            jTable6.getColumnModel().getColumn(6).setPreferredWidth(50);
            jTable6.doLayout();
            jTable6.validate();

        } catch (Exception e) {
            System.out.println(" . . abishyuwe() . exception thrown:" + e);
//       insertError(e + "", " getDetteFournisseur()  ");
            JOptionPane.showMessageDialog(null, " abishyuwe():  " + e, " Ikibazo ", JOptionPane.PLAIN_MESSAGE);
        }        
    }

    public void abishyuwe2(LinkedList<cpt_lacharge> bibaregutya) {

        try {


            System.out.println(" . . bibaregutya2.size()  :" + bibaregutya.size());

            String[][] s1 = new String[bibaregutya.size() + 2][7];

            int vv = 0;

            for (; vv < bibaregutya.size(); vv++) {

                cpt_lacharge C = bibaregutya.get(vv);

                s1[vv][0] = ""+ C.employee;
                s1[vv][1] = "" + C.COMPTE_DEBITER;
                s1[vv][2] = "" + C.COMPTE_CREDITER;  
                s1[vv][3] = "" + C.name_cpt;
                s1[vv][4] = "" + C.name_CRD;
                s1[vv][5] = "" + C.montant_deb;
                s1[vv][6] = "" + C.montant_cred;
                
            }

            //soldeLabel.setText("MONTANT A PAYER: " + solde + " FRW");

            jTable6.setModel(new javax.swing.table.DefaultTableModel(s1, new String[]{"EMPLOYE", "C_D", "C_C","N_D", "N_C", "S_D", "S_C"}));
            jTable6.setAutoResizeMode(jTable6.AUTO_RESIZE_ALL_COLUMNS);
            jTable6.getColumnModel().getColumn(0).setPreferredWidth(80);
            jTable6.getColumnModel().getColumn(1).setPreferredWidth(50);
            jTable6.getColumnModel().getColumn(2).setPreferredWidth(50);
            jTable6.getColumnModel().getColumn(3).setPreferredWidth(100);
            jTable6.getColumnModel().getColumn(4).setPreferredWidth(100);
            jTable6.getColumnModel().getColumn(5).setPreferredWidth(50);
            jTable6.getColumnModel().getColumn(6).setPreferredWidth(50);
           
            jTable6.doLayout();
            jTable6.validate();

        } catch (Exception e) {
            System.out.println(" . . abishyuwe() . exception thrown:" + e);
//			insertError(e + "", " getDetteFournisseur()  ");
            JOptionPane.showMessageDialog(null, " abishyuwe():  " + e, " Ikibazo ", JOptionPane.PLAIN_MESSAGE);
        }        
    }

    private JTable imishahara() {

        jScrollPane1 = new javax.swing.JScrollPane();

        jTable6 = new javax.swing.JTable();

        String[][] s1 = new String[0][7];

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTable6.setModel(new javax.swing.table.DefaultTableModel(s1, new String[]{"EMPLOYE", "COMPTE DEBITE", "COMPTE CREDIT", "SOMME DEBITE", "SOMME CREDITE"}) {

            Class[] types = new Class[]{java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class};

            @Override
            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }
        });
        return jTable6;
    }

    public void payementsalaire() {
        
        payementSalaire.addActionListener(
                new ActionListener() {

                    public void actionPerformed(ActionEvent ae) {
                        
                        if (ae.getSource() == payementSalaire && abangaba != null) {

                            System.out.println("abangaba.size(): " + abangaba.size());
                          
                          datedoc = JOptionPane.showInputDialog(frame, " ENTER DATE DOC: ");  
                          String  month = (String) JOptionPane.showInputDialog(null, " CHOOSE MONTH FOR PAYEMENT!",
                        "Your choice", JOptionPane.QUESTION_MESSAGE, null,
                        new String[]{"JAN" ,"FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"}, "");
                            
                            String type = (String) JOptionPane.showInputDialog(null, " WHICH PAYEMENT? ",
                        "Your choice", JOptionPane.QUESTION_MESSAGE, null,
                        new String[]{"PAYEMENT DE SALAIRE" ,"AVANCE OR CREDIT","RSSB & RRA"}, "");
                            
                      
                            
                            try {
                               
                                int bb = abangaba.size();

                                System.out.println("//////////////-----------/////////////");

                                System.out.println("abangaba.size(): " + abangaba.size());
                                
                                choix = (Journaux)JOptionPane.showInputDialog(null, " Veuillez faire votre choix !", "PAYEMENT DES SALAIRES", JOptionPane.QUESTION_MESSAGE, null,cashier.db.getJournaux().toArray(), "");
                              
                                if (choix == null) {
                                    JOptionPane.showMessageDialog(null, "NTACYO MWIGEZE MUHITAMO ", "IKIBAZO", JOptionPane.WARNING_MESSAGE);
                                }
                                
                                else if ((choix.id).contains("CAISSE")) {
                                    modePayement = "CAISSE";
                                } else if ((choix.id).contains("BANQUE")) {
                                    modePayement = "BANQUE";
                                }  

                                for (int x = 0; x < bb; x++) {

                                    Tiers C = abangaba.get(x);

                                    String numtiers = C.numTiers;
                                    String numcompte = C.numCompte;
                                    String designation = C.designation;
                                    String info = C.info;
                                    String address = C.adresse;
                                    String devise = C.devise;
                                    String sigletiers = C.sigleTiers;
                                    int salaireBrute=cashier.db.getSalaireBrute(numtiers);
                                    
                        gutya.add(new cpt_lacharge(designation,"" , "","" , "", 0, 0,numtiers,""));            
                                    
                                    
                                    
                         
                                
                                double amount=0.0;
                                double nn1=0.0;
                                double rssb=0.0;
                                double rssb2=0.0;
                                double tpr=0.0;
                                double rama=0.0;
                                switch(type)
                                    
                                { 
                                    
                                case"PAYEMENT DE SALAIRE":
                                    
                                {
                                
            amount=cashier.db.getPreparation(numtiers,month).net_salary;
            Timestamp date=cashier.db.getPreparation(numtiers,month).date;
            
            
            
            
           
            //double avance=cashier.db.getPayement(numtiers,month).avance;
            
            double retenu=cashier.db.getPayement2(numtiers,month,date).retenu;
            double avance=cashier.db.getPayement2(numtiers,month,date).avance;
            double prime=cashier.db.getPayement2(numtiers,month,date).prime;
            
            
            double to_pay=amount-avance-retenu+prime;
         
            
            gutya.add(new cpt_lacharge("","421","","TIERS PERSONNEL","",to_pay,0.0,numtiers,month+"-SOLDE"));
            gutya.add(new cpt_lacharge("","",""+choix.cpte,"",""+choix.id,0.0,to_pay,numtiers,month+"-SOLDE"));
           
                                break;
                                }
                                
                                    case"RSSB & RRA":
                                    
                                {
                                
           
            rssb=cashier.db.getPreparation(numtiers,month).rssb;
            tpr=cashier.db.getPreparation(numtiers,month).tpr;
            rama=cashier.db.getPreparation(numtiers,month).rama;
            rssb2=cashier.db.getPreparation(numtiers,month).rssb2;
            
            if(rssb!=0)
            {
            gutya.add(new cpt_lacharge("","432","","SECURITE SOCIAL-PENSION","",rssb,0.0,numtiers,month+"-PENSION"));
            gutya.add(new cpt_lacharge("","",""+choix.cpte,"",""+choix.id,0.0,rssb,numtiers,month+"-PENSION"));
            }
            
            if(rssb2!=0)
            {
            gutya.add(new cpt_lacharge("","432","","SECURITE SOCIAL-MATERNITY","",rssb2,0.0,numtiers,month+"-MATERNITY"));
            gutya.add(new cpt_lacharge("","",""+choix.cpte,"",""+choix.id,0.0,rssb2,numtiers,month+"-MATERNITY"));
            }
            
            if(rama!=0)
            {
            gutya.add(new cpt_lacharge("","432","","SECURITE SOCIAL-MEDICAL CARE","",rama,0.0,numtiers,month+"-MEDICAL CARE "));
            gutya.add(new cpt_lacharge("","",""+choix.cpte,"",""+choix.id,0.0,rama,numtiers,month+"-MEDICAL CARE"));
            }
            
            if(tpr!=0)
            {
            gutya.add(new cpt_lacharge("","431","","TIERS TAXES TPR","",tpr,0.0,numtiers,month+"-TPR"));
            gutya.add(new cpt_lacharge("","",""+choix.cpte,"",""+choix.id,0.0,tpr,numtiers,month+"-TPR"));
            }
           
           
                                break;
                                }
                                
                                case"AVANCE OR CREDIT":
                                {
                                    
            String nn = JOptionPane.showInputDialog(frame, "SALARIE: "+designation+"\n AVANCE SUR SALAIRE OU CREDIT\n AMOUNT PAID: ");
            
            if(nn == null || (nn != null && ("".equals(nn))))   
{
   JOptionPane.showMessageDialog(null, "EMPTY INPUT PLEASE", 
" QUESTION ", JOptionPane.WARNING_MESSAGE);
}
            
            nn1 = Double.parseDouble(nn);
            gutya.add(new cpt_lacharge("","422","","TIERS PERSONNEL","",nn1,0.0,numtiers,month+"-AVANCE SUR SALAIRE"));
            gutya.add(new cpt_lacharge("","",choix.cpte,"",choix.id,0.0,nn1,numtiers,month+"-AVANCE SUR SALAIRE"));
            
            
            cashier.db.insertPayement(numtiers,nn1,month,0.0,0.0);
            
                                break;
                                }
                               
                                default:
                                {   
                                 break;
                                }
                                }
                                    
                                    
                                 
                                   
                                   
                                
                                   ayangaya.add(new Lot(Integer.parseInt(numtiers), numcompte, designation, info, address, devise, sigletiers, salaireBrute));
                                   
                                   
           
            
            
                                 
                                 
                                
                                }


                                abishyuwe2(gutya);
                                cas = "payement";

                            } catch (Exception e) {
                                System.out.println("1-byakwamye kabisa: " + e);

                                JOptionPane.showMessageDialog(null, "" + e, " Error ", JOptionPane.ERROR_MESSAGE);
                            }
                        }
                    }
                });
    }

    public void save() {
        saveButton.addActionListener(
                new ActionListener() {

                    public void actionPerformed(ActionEvent ae) {
                        if (ae.getSource() == saveButton) {
                            vava = true;
                            if (vava == true && ayangaya != null) {
                            
                                insertCalculsSalaire(ayangaya, abangaba, listeYabo, jTable6); 
     
                             
                            } else {
                                System.out.println("ntabwo cash yahageze muri save !!!");
                            }
                        }
                    }
                });
    }

    
    String getDateRepeteSalaire(String dd, int mm, int yy) {

        String ret = dd + mm + yy;

        if (mm == 12) {
            ret = dd + "01" + (yy + 1);
        } else if (mm > 8) {
            ret = dd + (mm + 1) + (yy);
        } else {
            ret = dd + "0" + (mm + 1) + yy;
        }
        return ret;
    }

   

public void insertCalculsSalaire(LinkedList<Lot> ayangaya1, LinkedList<Tiers> abangaba1, JList JL, JTable JT) 
    {
        
       
        
        try{
            
            int hh = ayangaya1.size();

            System.out.println(" size ya ABAKOZI : " + hh);

            String date = cashier.db.dateToday;
            String dateyanone;
            if(date.length()==5)
            {
            dateyanone ="0"+date ;
            }
           else
            {     
           dateyanone =date ;
           }
            System.out.println("dateyanone: " + dateyanone);
            
            
            String curentTier = "";
            
            
                    if (cas.equals("calcul"))
                        
                    {
                        
   int num1 = cashier.db.getNumOperation2("SALAIRE",cashier.db.state);
                        
                if(gutya.size() >0 )
                curentTier = gutya.get(0).num_tier;

            for (int i = 0; i < gutya.size(); i++) {

             
                 
 
                if(!gutya.get(i).num_tier.equals(curentTier) )
                {
                num1 = cashier.db.getNumOperation2("SALAIRE",cashier.db.state);
                curentTier = gutya.get(i).num_tier;
                }
                    
                     
                         
                
                         
                    cpt_lacharge kk = gutya.get(i);
                    
                        
                        
                        
                        
                        
                        
                        
                        

                   if(kk.montant_deb!=0.0 || kk.montant_cred!=0.0) 
                    {
                        cashier.db.insertJournal(num1, "" + kk.num_tier, dateyanone, "" + kk.COMPTE_DEBITER, ""+ kk.COMPTE_CREDITER, 
                            "RémunPers-"+kk.charge_name, kk.montant_deb, kk.montant_cred, "AUTO", 0.0, datedoc2, "SALAIRE", " ","",0.0);

                   
                     
                    }
                    
                    }
                    }
                    
                    else if (cas.equals("payement"))
                        
                        
                    {
                        
    int num = cashier.db.getNumOperation2(choix.id, cashier.db.state);                    
                        if(gutya.size() >0 )
                curentTier = gutya.get(0).num_tier;

            for (int i = 0; i < gutya.size(); i++) {

             
                 
 
                if(!gutya.get(i).num_tier.equals(curentTier) )
                {
                 num = cashier.db.getNumOperation2(choix.id, cashier.db.state);
               curentTier = gutya.get(i).num_tier;
                }
                    
                     
                         
                
                         
                    cpt_lacharge kk = gutya.get(i);
                    
 
                        
                        if(kk.montant_deb!=0.0 || kk.montant_cred!=0.0) 
                    {

                       cashier.db.insertJournal(num, "" + kk.num_tier, dateyanone, "" + kk.COMPTE_DEBITER, ""+ kk.COMPTE_CREDITER, 
                            "RémunPers-"+kk.charge_name ,  kk.montant_deb, kk.montant_cred, "AUTO", 0, datedoc, ""+choix.id, " ","",0.0);
                       
                       System.out.println("Dore journal"+choix.id);
                      
                    }
                   

                       
                    //}
                    
                    }
                  
                    } 
            
           
            
            if(cas.equals("calcul"))
            {
                //detteSalaire("" + num, "" + tier_number, "SALAIRE",0);
                JOptionPane.showMessageDialog(null, " CALCUL SALAIRE SAVED SUCCESSFULY !!  ", "", JOptionPane.PLAIN_MESSAGE);
            }
            else if(cas.equals("payement"))
            {
             //paiementDetteSalaire("" + tier_number, "SALAIRE", dettes);
            JOptionPane.showMessageDialog(null, " PROCESSUS DE PAYEMENT DES SALAIRES TERMINE !!  ", "", JOptionPane.PLAIN_MESSAGE);
            
            
            }
            //dispose();
                    abangaba.clear();
                    gutya.clear();
                    liste2paye();
                    abishyuwe(gutya);

        }catch(Exception e)
        {
            System.out.println(" Error muri insertcalculsalaire:"+e);
        }
        
        
        
    }

    
    // Codes for update button
    
    public void update() {

        updateButton.addActionListener(new ActionListener() {
 @Override
                    public void actionPerformed(ActionEvent ae) {

                        if (ae.getSource() == updateButton) {
                            
                            
                            String choix; 
                choix = (String) JOptionPane.showInputDialog(null, " Make a choice !",
                        "Your choice", JOptionPane.QUESTION_MESSAGE, null,
                        new String[]{"EMPLOYEE-CHARGE" ,"EMPLOYEE","CHARGE","DELETE"}, "");

            switch (choix) {
               
                    
                    case "EMPLOYEE-CHARGE":
                        if (ae.getSource() == updateButton && employees.getSelectedValue() == null) {
                             JOptionPane.showMessageDialog(null," No Employee Selected");}
                        else{
                    
                   new updatecharge(cashier.db,employees.getSelectedValue().toString());
                    
                  
                        } 
                   
                    break;
                case "EMPLOYEE":
                    
                    
                    if (ae.getSource() == updateButton && employees.getSelectedValue() == null) {
                             JOptionPane.showMessageDialog(null," No Employee Selected");}
                        else{
                    
                    Tiers tcc = (Tiers) employees.getSelectedValue();
                        
                       
                        
                        new TiersInterface(cashier.db, tcc);
                        
                      
                   
                  
                        } 
                break;
                   
                
                case "DELETE":
                    
                    
                    if (ae.getSource() == updateButton && employees.getSelectedValue() == null) {
                             JOptionPane.showMessageDialog(null," No Employee Selected");}
                        else{
                        Tiers tcc = (Tiers) employees.getSelectedValue();
                        boolean deleted=    
                                            cashier.db.deleteTier(tcc,tcc.numTiers);
                    
                   if(deleted)
                                    {
                                        JOptionPane.showMessageDialog(null, tcc.designation+" SUCCESSFULLY DELETED ", " EFFACER TIER ", JOptionPane.ERROR_MESSAGE);
                                        
                                    }
                   
                  
                        } 
                break;
                    case "CHARGE":
                    
                    
                  
                    
                   new Viewcharge(cashier.db);
                    break;
                      
                   
                       
                        
                         
                default:
                    break;
            }
                         
                           
                
                  }
                    
                    }
 
                });
    }
    
    
    
    public void addcharge() {

        addnewButton.addActionListener(new ActionListener() {
 @Override
                    public void actionPerformed(ActionEvent ae) {

                        if (ae.getSource() == addnewButton) {
                            
                            
                            
                             String choix; 
                choix = (String) JOptionPane.showInputDialog(null, " Make a choice !",
                        "Add new", JOptionPane.QUESTION_MESSAGE, null,
                        new String[]{"CREATE CHARGE", "CREATE EMPLOYEE"}, "");

            switch (choix) {
                case "CREATE CHARGE":
                    
                   new AddCharge(cashier.db);
                    break;
                  
                case "CREATE EMPLOYEE":
                    String sc="";
                    String numTiers;
                    sc=("" + 42);
                    
                   
                    Compte sous = (Compte) JOptionPane.showInputDialog(null, "Faites votre choix", "Liste des Comptes",
    JOptionPane.QUESTION_MESSAGE, null, cashier.db.cpteListByNameCompte(sc).toArray(), null);
                                System.out.println("sous cpte: " + sous.numCompte);
                                numTiers = cashier.db.getNumTiers("" + sous.numCompte);
                                new TiersInterface(cashier.db, numTiers, sous.numCompte, 0,"");
                                break;
                   
                            
                default:
                    break;
            }

               
                }
              
                        }
                   
                });
    }
    
    
    
    public void preparation(LinkedList<cpt_lacharge> rapport) {

        try {

            
             double total=0.0;

            System.out.println(" Rapport size:" + rapport.size());

            String[][] s1 = new String[rapport.size() + 2][22];

            int vv = 0;

            for (; vv < rapport.size(); vv++) {

                cpt_lacharge C = rapport.get(vv);

                s1[vv][0] = ""+ C.designation;
                s1[vv][1] = "" + C.salaire_base;
                s1[vv][2] = "" + C.housing;
                s1[vv][3] = "" + C.transport;
                s1[vv][4] = "" + C.prime;
                s1[vv][5] = "" + C.g_budget;
                s1[vv][6] = "" + C.ass_rssb;
                s1[vv][7] = "" + df.format(C.farg);
                s1[vv][8] = "" + df.format(C.pension_employee);
                s1[vv][9] = "" + df.format(C.pension_employer);
                s1[vv][10] = "" + df.format(C.maternity_employee);
                s1[vv][11] = "" + df.format(C.maternity_employer);
                s1[vv][12] = "" + df.format(C.medical1);
                s1[vv][13] = "" + df.format(C.medical2);
                s1[vv][14] = "" + df.format(C.tpr);
                s1[vv][15] = "" + df.format(C.net_salary);
                
                s1[vv][16] = "" + C.tot_retenu;
                s1[vv][17] = "" + C.avance;
                s1[vv][18] = "" + C.restauration;
                s1[vv][19] = "" + C.solidarite;
                s1[vv][20] = "" + C.commission;
                s1[vv][21] = "" + df.format(C.netapayer);
                
                total=total+C.netapayer;
            }
            
          JTable jTable1= new javax.swing.JTable();  

          //Image img=cashier.db.img();
            String solde = "  ISHYIGA  COMPTA - PREPARATION DES SALAIRES -"+month+"-TOTAL TO PAY:"+setVirgule(total);
            jTable1.setModel(new javax.swing.table.DefaultTableModel(s1, new String[]{"NAMES", "BASIC_SALARY", "HOUSING","TRANSPORT","PRIME", "GROSS BADGET","ASSIETE RSSB","FARG",
                "PENSION EMPLOYER 5%","PENSION EMPLOYEE 3%","MATERNITE EMPLOYER 0.3%","MATERNITE EMPLOYER 0.3%", "MEDICAL CARE-EMPLOYEE ","MEDICAL CARE-EMPLOYER","TPR","TOT_NET", "RETENUES","AVANCE&CREDIT","RESTAURATION","SOLIDALITE","COMMISSION","NET A PAYER"}));
            jTable1.setAutoResizeMode(jTable1.AUTO_RESIZE_ALL_COLUMNS);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(80);
            jTable1.getColumnModel().getColumn(1).setPreferredWidth(50);
            jTable1.doLayout();
            jTable1.validate();
            
            new showJTable(jTable1, solde, s1, "",0.0, 0.0,cashier.db,"" + cashier.dateDoc, " SALAIRE","",""+setVirgule(total));

        } catch (Exception e) {
            System.out.println(" . . JTable1 . exception thrown:" + e);
//       insertError(e + "", " getDetteFournisseur()  ");
            JOptionPane.showMessageDialog(null, " JTable1:  " + e, " Ikibazo ", JOptionPane.PLAIN_MESSAGE);
        }        
    }
    
    
    
    
    
    public void preparation2(LinkedList<cpt_lacharge> rapport) {

        try {
            
            double total=0.0;

          
            System.out.println(" Rapport size:" + rapport.size());

            String[][] s1 = new String[rapport.size() + 2][23];

            int vv = 0;

            for (; vv < rapport.size(); vv++) {

                cpt_lacharge C = rapport.get(vv);

                s1[vv][0] = ""+ C.designation;
                s1[vv][1] = "" + C.salaire_base;
                s1[vv][2] = "" + C.housing;
                s1[vv][3] = "" + C.transport;
                s1[vv][4] = "" + C.indemnite;
                s1[vv][5] = "" + C.other;
                s1[vv][6] = "" + C.g_budget;
                s1[vv][7] = "" + C.ass_rssb;
                s1[vv][8] = "" + C.farg;
                s1[vv][9] = "" + C.pension_employee;
                s1[vv][10] = "" + C.pension_employer;
                s1[vv][11] = "" + C.maternity_employee;
                s1[vv][12] = "" + C.maternity_employer;
                s1[vv][13] = "" + C.medical1;
                s1[vv][14] = "" + C.medical2;
                s1[vv][15] = "" + C.tpr;
                s1[vv][16] = "" + C.net_salary;
                
                s1[vv][17] = "" + C.tot_retenu;
                s1[vv][18] = "" + C.avance;
                s1[vv][19] = "" + C.restauration;
                s1[vv][20] = "" + C.solidarite;
                s1[vv][21] = "" + C.prime;
                s1[vv][22] = "" + C.netapayer;
                
                total=total+C.netapayer;
            }
            
          JTable jTable1= new javax.swing.JTable();  

          //Image img=cashier.db.img();
            String solde = "  ISHYIGA  COMPTA - PREPARATION DES SALAIRES:"+total;
            jTable1.setModel(new javax.swing.table.DefaultTableModel(s1, new String[]{"NAMES", "BASIC_SALARY", "HOUSING","TRANSPORT","INDEMNITE DE CAISSE", "OTHER", "GROSS BADGET","ASSIETE RSSB", "FARG",
                "PENSION EMPLOYER 5%","PENSION EMPLOYEE 3%","MATERNITE EMPLOYER 0.3%","MATERNITE EMPLOYER 0.3%", "MEDICAL CARE-EMPLOYEE ","MEDICAL CARE-EMPLOYER","TPR","TOT_NET", "RETENUES","AVANCE&CREDIT","RESTAURATION","SOLIDALITE","PRIME","NET A PAYER"}));
            jTable1.setAutoResizeMode(jTable1.AUTO_RESIZE_ALL_COLUMNS);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(80);
            jTable1.doLayout();
            jTable1.validate();
            
            new showJTable(jTable1, solde, s1, "",0.0, 0.0,cashier.db,"" + cashier.dateDoc, " SALAIRE","",""+setVirgule(total));

        } catch (Exception e) {
            System.out.println(" . . JTable1 . exception thrown:" + e);
//       insertError(e + "", " getDetteFournisseur()  ");
            JOptionPane.showMessageDialog(null, " JTable1:  " + e, " Ikibazo ", JOptionPane.PLAIN_MESSAGE);
        }        
    }
    
     public void prep() {

        prep.addActionListener(new ActionListener() {
 @Override
                    public void actionPerformed(ActionEvent ae) {

                        if (ae.getSource() == prep)
                        {
                          String ti_name="";
                          String ti="";
                          String choice = (String) JOptionPane.showInputDialog(null, " Make a choice !",
                        "", JOptionPane.QUESTION_MESSAGE, null,
                        new String[]{"VIEW", "ADD"}, "");
                      if (choice=="ADD" )
                      {
                          
                           if (employees.getSelectedValue() == null)
                      
                      {JOptionPane.showMessageDialog(null," No Employee Selected");} 
                     
                     new DynamicPreparation(cashier.db,employees.getSelectedValue().toString());}
                      
                      else
                      {
                      String  month = (String) JOptionPane.showInputDialog(null, " CHOOSE MONTH TO VIEW!",
                        "Your choice", JOptionPane.QUESTION_MESSAGE, null,
                        new String[]{"JAN" ,"FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"}, "");
                      
                      String mode = (String) JOptionPane.showInputDialog(null, " CHOOSE!!",
                        "Your choice", JOptionPane.QUESTION_MESSAGE, null,
                        new String[]{"ALL","SELECTED" }, "");
                      
                      if (mode=="SELECTED")
                      {
                          
                      if (employees.getSelectedValue() == null)
                      
                      {JOptionPane.showMessageDialog(null," No Employee Selected");} 
                       
                      else{
                       String t=employees.getSelectedValue().toString();
                            String[] part = t.split("(?<=\\D)(?=\\d)");
                       ti_name=part[0];
                       ti=part[1];
                               
                      }    
                      DynamicView(cashier.db.Dynamicprep1(month,ti)) ; 
                      }
                      
                      
                      
                      else{
                      
                      DynamicView(cashier.db.Dynamicprep(month)) ;   
                          
                      }
                      
                      
                      
                      }     
                          
                    }  
                    }
                });
    }
  
     
     public void DynamicView(LinkedList<preparation> p)
   {
   
                dprep = p;
                
                
                
                double total=0.0;

           
            String[][] s1 = new String[dprep.size()+2][5];
            
            System.out.println("Dore Size: " +dprep.size());

            int vv = 0;

            for (; vv < dprep.size(); vv++) {

                preparation C = dprep.get(vv);

                s1[vv][0] = ""+ C.num_tier;
                
                s1[vv][1] = "" + C.deriverable;
                s1[vv][2] = "" + C.amount;
                s1[vv][3] = "" + C.month;
                s1[vv][4] = "" + C.date;
                
            total=total+C.amount;
                
            }
            String title = "  ISHYIGA  COMPTA - DYNAMIC SALAIRES : "+total;

            JTable jTable1= new javax.swing.JTable(); 

            jTable1.setModel(new javax.swing.table.DefaultTableModel(s1, new String[]{"EMPLOYE", "DERIVERABLE", "AMOUNT","MONTH", "DATE_DOC"}));
            jTable1.setAutoResizeMode(jTable1.AUTO_RESIZE_ALL_COLUMNS);
           
            jTable1.doLayout();
            jTable1.validate();
            
            new showJTable(jTable1, title, s1, "",0.0, 0.0,cashier.db,"" , " SALAIRE","",""+total);
   
   
   }
     
     
     
     
     public void searchtier() {

       searchTier.addKeyListener(
                new KeyListener() {
            public void actionPerformed(ActionEvent ae) {  }
 @Override  public void keyTyped(KeyEvent e) {

                        String cc = "" + e.getKeyChar();
                        String find = (searchTier.getText() + cc).toUpperCase();
                        if (cc.hashCode() == 8 && find.length() > 0) {
                            find = (find.substring(0, find.length() - 1));
                        }

                        searchedTier(find);
                    }
 @Override public void keyPressed(KeyEvent e) { }
 @Override public void keyReleased(KeyEvent e) { }
                });
    }
     
     
     
  void searchedTier(String findIt) { 
        int linge = cashier.db.getAllEmployees().size(); 
        LinkedList<Tiers> allTiers = new LinkedList<>(); 
        for (int x = 0; x < linge; x++) { 
            Tiers tier = cashier.db.getAllEmployees().get(x);

            if ((("" + tier .designation).contains(findIt)) || (("" + tier .numCompte).contains(findIt))) {
                allTiers.add(tier);
            }
        }
        employees.setListData(allTiers.toArray());
    }
   
    

}
