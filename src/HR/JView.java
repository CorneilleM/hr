/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package HR;

import java.awt.Container;
import java.awt.event.WindowEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Kimenyi Aimable
 */
public class JView extends JFrame {

    Rapport[] rap;
    double viewMaxQte = 0;

    public JView(DbHandler db) {
        String idBonDebut = JOptionPane.showInputDialog(" Debut", "0");
        String idBonFin = JOptionPane.showInputDialog(" Fin ", "21578");
        this.rap = db.getMoisSums(idBonDebut, idBonFin);
        this.viewMaxQte = db.viewMaxQte;

        setTitle("");

        enableEvents(WindowEvent.WINDOW_CLOSING);
        Container content = getContentPane();
        eastPane(content);

//on fixe  les composant del'interface
        setSize(700, 700);
        setVisible(true);

    }

    public static int getIn(String s) {
        int entier = -1;

        try {

            Integer in_int = new Integer(s);
            entier = in_int.intValue();


        } catch (NumberFormatException e) {

            JOptionPane.showMessageDialog(null, "  ", " Erreur dans les parametres ", JOptionPane.PLAIN_MESSAGE);

        }

        return entier;
    }

    public static double getDoub(String s) {
        double d = 0;
        try {
            Double in_int = new Double(s);
            d = in_int.doubleValue();

        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "  ", " erreur dans les parametre ", JOptionPane.PLAIN_MESSAGE);

        }


        return d;
    }

    void eastPane(Container content) {
        JPanel all = new View(rap, 700, 600, viewMaxQte);
        content.add(all);
    }

    @Override
    protected void processWindowEvent(WindowEvent evt) {
        if (WindowEvent.WINDOW_CLOSING == evt.getID()) {


            this.dispose();
            // System.exit(0);

            //default icon, custom title
//JOptionPane.showMessageDialog(this,"RIRAFUNGIRA RIMWE N'ISHYIGA"," IKIBAZO",JOptionPane.PLAIN_MESSAGE);
            //this.setDefaultCloseOperation(HIDE_ON_CLOSE);


        }

    }

    String setVirgule(String setString) {



        int l = setString.length();



        if (l > 3 && l <= 6) {
            setString = setString.substring(0, l - 3) + "," + setString.substring(l - 3);
        }
        if (l > 6) {

            String s1 = setString.substring(0, l - 3);
            int sl = s1.length();
            setString = s1.substring(0, sl - 3) + "," + s1.substring(sl - 3) + "," + setString.substring(l - 3);

        }

        System.out.println(setString);
        return setString;
    }
}
