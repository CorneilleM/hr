/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package HR;

import static com.barcodelib.barcode.a.f.d.w;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.sql.*;

import javax.swing.*;
//import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;


/**
 *
 * @author Odette
 */
public class Update extends JFrame {
  
            JLabel codee,compted,comptec,type,payable,name;
    JTextField code,compte_d,compte_c,namee;
    JRadioButton percentage, fix, employee, company;
    
    JPanel main= new JPanel();
    JButton Save;
    JButton view;
    Font police;
    Color color;
    Container content;
    Viewcharge v;
    Cashier cashier;
    DbHandler db;
    
   
    
   public void draw() {
        
        try{
        //Set the title to the Comptabilitée frame
        this.setTitle("Update charge");
//End the process when clicking on Close
        enableEvents(WindowEvent.WINDOW_CLOSING);
//Make the Comptabilitée frame not resizable
        this.setResizable(false);
//Define the size of the Comptabilité  ; here, 500 pixels of width and 650 pixels of height
        this.setSize(550, 550);
         Pane();
        setSize(550, 550);
        setVisible(true);
        //showCompte();
        
        } catch (Throwable e) {
            System.out.println(" . . inv . exception thrown:" + e);
//            insertError(e + "", " getBalanceVerification  ");
        }        
    } 
   
   
   public void load()
   {
   
   
   try{
       
       codee = new JLabel("CODE: ");
        police = new Font("Rockwell", Font.BOLD, 15);
        codee.setFont(police);
       
        code= new JTextField("");
        police = new Font("Consolas", Font.BOLD, 15);
        code.setFont(police);
        
        name = new JLabel("NAME: ");
        police = new Font("Rockwell", Font.BOLD, 15);
        name.setFont(police);
       
        namee= new JTextField("");
        police = new Font("Consolas", Font.BOLD, 15);
        namee.setFont(police);
        
    
        compted = new JLabel("COMPTE DEBUT: ");
        police = new Font("Rockwell", Font.BOLD, 15);
        compted.setFont(police);
        
        comptec = new JLabel("COMPTE CREDIT: ");
        police = new Font("Rockwell", Font.BOLD, 15);
        comptec.setFont(police);

        
        compte_d = new JTextField("");
        police = new Font("Consolas", Font.BOLD, 15);
        compte_d.setFont(police);
        
        compte_c = new JTextField("");
        police = new Font("Consolas", Font.BOLD, 15);
        compte_c.setFont(police);
        
        
        type = new JLabel("TYPE: ");
        police = new Font("Rockwell", Font.BOLD, 15);
        type.setFont(police);
        
        
        percentage = new JRadioButton("PERCENTAGE");
        fix = new JRadioButton("FIX");
        
        payable = new JLabel("PAYABLE");
        police = new Font("Rockwell", Font.BOLD, 15);
        payable.setFont(police);
        
        employee = new JRadioButton("EMPLOYEE");
        company = new JRadioButton("COMPANY");
        
      
        Save= new JButton("UPDATE");
        police = new Font("Rockwell", Font.BOLD, 15);
        Save.setFont(police);
        Save.setBackground(Color.orange);
   
        view = new JButton("VIEW");
        police = new Font("Rockwell", Font.BOLD, 15);
        view.setFont(police);
        view.setBackground(Color.green);
        
        
           
        } catch (Exception e ) {
            System.out.println(" . . makeFrameTiers . exception thrown:" + e);
//            insertError(e + "", " getBalanceVerification  ");
        }
   
   }
   
   
   
   
   public Update(DbHandler db ) {

        try {
            this.db=db;
            
             
            
            
            content = getContentPane();
            
          
            load();
            draw();
            groupButton( );
            save();
            view();
            
         
            //double va=(double) v.getselected().valeur;
            
            namee.setText(""+v.getselected().name);
            code.setText(""+v.getselected().code);
                   compte_d.setText(v.getselected().compte);
                    compte_c.setText(v.getselected().NOM_COMPTE);
                    
                    code.setEditable(false);
                    
            
           
        } catch (Throwable e) {
            System.out.println("Failllll"+e);

        }
    }
   
   
   
   public JPanel Pane() {
        JPanel panel= new JPanel();
        panel.setLayout(new GridLayout(7, 2, 5, 5));
        panel.setPreferredSize(new Dimension(450,400));
        
        JPanel radio1 = new JPanel();
        radio1.setLayout(new GridLayout(1, 2, 5, 5));
        
        radio1.add(percentage);
        radio1.add(fix);
        
        JPanel radio2 = new JPanel();
        radio2.setLayout(new GridLayout(1, 2, 5, 5));
        radio2.add(employee);
        radio2.add(company);

        panel.add(codee);
        panel.add(code);
        
        panel.add(name);
        panel.add(namee);
        panel.add(compted);
//    
        panel.add(compte_d);
        panel.add(comptec);
        panel.add(compte_c);
        panel.add(type);
      
        panel.add(radio1);
        panel.add(payable);
        panel.add(radio2);
        panel.add(Save);
        panel.add(view);
       
        
        

        main.add(panel);
        

        content.add(main);
        return main;
    }
   
   
 private void groupButton( ) {

ButtonGroup group1 = new ButtonGroup( );

group1.add(percentage);
group1.add(fix);

ButtonGroup group2 = new ButtonGroup( );

group2.add(employee);
group2.add(company);

} 
    
  public void save() {

        Save.addActionListener(new ActionListener() {
 @Override
                    public void actionPerformed(ActionEvent ae) {

                        if (ae.getSource() ==Save) {
                            
                            int f=Integer.parseInt((String)code.getText());
                            
                          String name= namee.getText();
                          String cmptd= compte_d.getText();
                          String cmpt_name= compte_c.getText();
                     
                       
                          String typ="";
                          if(percentage.isSelected())
                          {typ=percentage.getText();}
                          else if(fix.isSelected())
                        {typ=fix.getText();}
                          
                          String pay="";
                          if(employee.isSelected())
                          {pay=employee.getText();}
                          else if(company.isSelected())
                        {pay=company.getText();}
                        
                         db.updateCharge(typ, f,cmpt_name,cmptd,pay,name);
                          compte_d.setText("");
                          compte_c.setText("");
                          percentage.setSelected(false);
                          fix.setSelected(false);
                          
                        
                        }
                    }
                });
    }  
  
  
  
  
  
  public void view() {

        view.addActionListener(new ActionListener() {
 @Override
                    public void actionPerformed(ActionEvent ae) {

                        if (ae.getSource()==view) {
                            
                     new Viewcharge(db);
                        }
                    }
                });
    }
  
  
  
    
}
