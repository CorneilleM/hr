package HR;

/*
 * Product for Ishyiga copyright 2007 Kimenyi Aimable
 */
import java.io.*;
import java.util.*;
import javax.swing.JOptionPane;

public class Product implements Comparable {

    public int productCode, qty;
    public String productName, code;
    public String codeBar, observation, codeSoc;
    public double tva, currentPrice, prix_revient;
    public int quantity;
    public int choixLot = 1;
    DbHandler db;
    public int qtyLive;
    //         courrant.qtyLive;
    public Product(int productCode, String productName, String code, int qty, double currentPrice, double tva, String codeBar, String observation, String codeSoc, DbHandler db) {
        this.productCode = productCode;
        this.productName = productName;
        if (productName.length() > 55) {
            this.productName = productName.substring(0, 50);
        }
        this.code = code;
        this.qty = qty;
        this.currentPrice = currentPrice;
        this.db = db;
        this.tva = tva;
        this.codeBar = codeBar;
        this.observation = observation;
        this.codeSoc = codeSoc;
    }

    public Product(int productCode, String productName, String code, int qty, double currentPrice, double tva, String codeBar, String observation, String codeSoc, DbHandler db, double pr) {
        this.productCode = productCode;
        this.prix_revient = pr;
        this.productName = productName;
        if (productName.length() > 55) {
            this.productName = productName.substring(0, 50);
        }
        this.code = code;
        this.qty = qty;
        this.currentPrice = currentPrice;
        this.db = db;
        this.tva = tva;
        this.codeBar = codeBar;
        this.observation = observation;
        this.codeSoc = codeSoc;
    }

    public int qty() {
        return qty;
    }
    /*
     * gestion de vente
     */

    public String toPrint() {
        String nom = "";

        if (productName.length() > 20) {
            nom = productName.substring(0, 20) + ".";
        } else {
            nom = productName;
        }

        int i = nom.length();

        while (i <= 20) {
            nom = nom + " ";
            i++;
        }

        return nom + " * " + qty + " = " + currentPrice * qty + " rwf";
    }

    public double salePrice() {
        return currentPrice * qty;
    }

    public int productCode() {
        return productCode;
    }

    @Override
    public String toString() {

        return productName + "****" + setVirgule((int) currentPrice) + " RWF";
    }

    public String toSell() {
        String g = "****" + codeSoc;
        return productCode + "****" + qty + "****" + (int) (currentPrice * qty) + " RWF" + "****" + productName + g;
    }

    public String toStock() {
        // getQuantite();
        return productCode + "***" + productName;//+"****"+courrant.id_LotS+"****"+courrant.qtyLive;
    }

    public String toSee() {
        return productName + "****" + setVirgule((int) currentPrice) + " RWF";
    }

    public Product clone(int q) {

        Product p = new Product(productCode, productName, code, qty, currentPrice, tva, codeBar, observation, codeSoc, this.db, prix_revient);
        p.qty = q;

        return p;

    }

    public String setVirgule(int frw) {

        String setString = "" + frw;
        int l = setString.length();

        if (l > 3 && l <= 6) {
            setString = setString.substring(0, l - 3) + "." + setString.substring(l - 3);
        }
        if (l > 6) {

            String s1 = setString.substring(0, l - 3);
            int sl = s1.length();
            setString = s1.substring(0, sl - 3) + "." + s1.substring(sl - 3) + "." + setString.substring(l - 3);

        }

        return setString;
    }

    public Product cloneRet(int q) {
        Product p = new Product(productCode, productName, code, qty, currentPrice, tva, codeBar, observation, codeSoc, this.db);
        p.qty = -q;

        return p;

    }

    public int compareTo(Object o) {
        if (o instanceof Product) {
            Product r = (Product) o;


            return this.productName.compareTo(r.productName);
        }
        return -1;
    }
}