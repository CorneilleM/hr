/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package HR;
public class Lot {

    public int productCode, qtyStart, qtyLive;
    public int id_lot, v;
    public String id_LotS, bon_livraison, dateExp, code, proname;
    public double price, tva, double1, d1, d2, d3, d4, d5, d6, d7;
    public int int0, int1, int2, int3, int4, int5, int6, int7, int26;
    //Product p;
    public String u1, u2, u3, u4, u5, u6, u7, u8, u9, u0, u11, u12, u13, u14, u15, u16, u17;
    public String[] vars;
    int id_bon_livraison;
    String dateN ;
    int totalBL;
    String numClient;
    double totalLot;
    double totalUsedLot;
    int solde;
    int payed;
    String status;
    String reason;
    String fournisseur;
    String numTiers;
    
    String dateToday;
    double prixHorsTaxes;
    String reference;
    String compteAchat0;
    String compteTva;
    
    

    public Lot(String u1, String u2, String u3, String u4, String u5, String u6,
            String u7, String u8, String u9, String u0, String u11) {
        //System.out.println("  "+u1);
        this.u1 = u1;
        this.u2 = u2;
        this.u3 = u3;
        this.u4 = u4;
        this.u5 = u5;
        this.u6 = u6;
        this.u7 = u7;
        this.u8 = u8;
        this.u9 = u9;
        this.u0 = u0;
        this.u11 = u11;

        this.vars = new String[12];
        
        vars[0] = u1;
        vars[1] = u2;
        vars[2] = u3;
        vars[3] = u4;
        vars[4] = u5;
        vars[5] = u6;
        vars[6] = u7;
        vars[7] = u8;
        vars[8] = u9;
        vars[9] = u0;
        vars[10] = u11;
        
    }
    
    
    public Lot(int productCode, String proname, String code, String numLot, String dateeX, int dateDoc, int qtyStart, int qtyLive, String bon_livraison) {
        v = 0;
        this.id_LotS = numLot;
        this.bon_livraison = bon_livraison;
        this.dateExp = dateeX;
        this.qtyLive = qtyLive;
        this.qtyStart = qtyStart;
        this.productCode = productCode;
        this.id_lot = dateDoc;
        this.code = code;
        this.proname = proname;
    }

    //comptabiliser les bons de livraison valide
    public Lot(String u1, String u2, String u3, double d1, double d2, double d3, double d4, int int2, int int3, int int4, String u4, String u5, String u6, String u7, String u8) {

        this.u1 = u1;
        this.u2 = u2;
        this.u3 = u3;
        this.d1 = d1;
        this.d2 = d2;
        this.d3 = d3;
        this.d4 = d4;
        this.int2 = int2;
        this.int3 = int3;
        this.int4 = int4;
        this.u4 = u4;
        this.u5 = u5;
        this.u6 = u6;
        this.u7 = u7;
        this.u8 = u8;
    }

    //INVALID BON DE LIVRAISON
    public Lot(String u1, String u2, String u3, int int1, int int2, String u4) {

        this.u1 = u1;
        this.u2 = u2;
        this.u3 = u3;
        this.int1 = int1;
        this.int2 = int2;
        this.u4 = u4;

    }

    public Lot(String u1, String u2, String u3, String u4, String u5, String u6, int int1, int int2, int int3, int int4) {
        this.u1 = u1;
        this.u2 = u2;
        this.u3 = u3;
        this.u4 = u4;
        this.u5 = u5;
        this.u6 = u6;
        //////////////////////
        this.int1 = int1;
        this.int2 = int2;
        this.int3 = int3;
        this.int4 = int4;
    }

    public Lot(String u1, String u2, String u3, String u4, String u5) {
        this.u1 = u1;
        this.u2 = u2;
        this.u3 = u3;
        this.u4 = u4;
        this.u5 = u5;
    }

    public Lot(int int1, String u1, String u2, String u3, String u4, String u5, String u6, double d1, double d2, double d3, double d4, double d5, double d6) {
        this.int1 = int1;
        this.u1 = u1;
        this.u2 = u2;
        this.u3 = u3;
        this.u4 = u4;
        this.u5 = u5;
        this.u6 = u6;
        this.d1 = d1;
        this.d2 = d2;
        this.d3 = d3;
        this.d4 = d4;
        this.d5 = d5;
        this.d6 = d6;
    }

    public Lot(int int1, String u1, String u2, String u3, String u4, String u5, String u6, int int2) {
        this.int1 = int1;
        this.u1 = u1;
        this.u2 = u2;
        this.u3 = u3;
        this.u4 = u4;
        this.u5 = u5;
        this.u6 = u6;
        this.int2 = int2;
    }

    public Lot(int productCode, String proname, String code, String numLot, String dateeX, int dateDoc, int qtyStart, int qtyLive, String bon_livraison, double price, double tva) {
        v = 0;
        this.id_LotS = numLot;
        this.bon_livraison = bon_livraison;
        this.dateExp = dateeX;
        this.qtyLive = qtyLive;
        this.qtyStart = qtyStart;
        this.productCode = productCode;
        this.id_lot = dateDoc;
        this.code = code;
        this.proname = proname;
        this.price = price;
        this.tva = tva;
    }

    public Lot(int productCode, String proname, String code, int dateDoc, int v, int qtyStart, double price, double tva) {
        this.v = v;
        this.qtyStart = qtyStart;
        this.productCode = productCode;
        this.id_lot = dateDoc;
        this.code = code;
        this.proname = proname;
        this.price = price;
        this.tva = tva;
       // this.p = p;
    }

//    @Override
//    public String toString() {
//
//        String see = "";
//
//        int comp = 30 - proname.length();
//        String s = "";
//        if (comp < 0) {
//            s = proname.substring(0, 30);
//        } else {
//            s = proname;
//            for (int d = 0; d < comp; d++) {
//                s = s + " ";
//            }
//        }
//
//        see = s + "   /  " + qtyLive + "  /   " + dateExp + "  /   " + id_LotS + "  /   " + (price) + " RWF";
//
//        if (v != 0) {
//            see = s + "   /  " + qtyStart + "  /   " + (price * qtyStart) + " RWF";
//        }
//        return see;
//    }

    @Override
    public String toString() {
        
        return "Lot{" + "u1=" + u1 + ", u2=" + u2 + ", u3=" + u3 + ", u4=" + u4 + ", u5=" + u5 + ", u6=" + u6 + 
                ", u7=" + u7 + ", u8=" + u8 + ", u9=" + u9 + ", u0=" + u0 + ", u11=" + u11 + '}';
    }
    
    
    
    
    

    public Lot(String u1, String u2, String u3, int u4, int u5) {
        System.out.println("  " + u1);
        this.u1 = u1;
        this.u2 = u2;
        this.u3 = u3;
        this.int1 = u4;
        this.int2 = u5;
    }

    public Lot(int int1, String u1, String u2, int int2, String u3, String u4, int int3, double double1, int int4, int int5, int int6, String u5, String u17) {
        this.int1 = int1;
        this.u1 = u1;
        this.u2 = u2;
        this.int2 = int2;
        this.u3 = u3;
        this.u4 = u4;
        this.int3 = int3;
        this.double1 = double1;
        this.int4 = int4;
        this.int5 = int5;
        this.int6 = int6;
        this.u5 = u5;
        this.u17 = u17;
    }

    public Lot(int int1, String u1, int int2, String u3, String u4, int int5, int int6, String u5) {
        this.int1 = int1;
        this.u1 = u1;
        //this.u2 = u2;
        this.int2 = int2;
        this.u3 = u3;
        this.u4 = u4;
        //this.int3 = int3;
        //this.double1 = double1;
        //this.int4 = int4;
        this.int5 = int5;
        this.int6 = int6;
        this.u5 = u5;
        //this.u17 = u17;    
    }

    public Lot(int int1, int int2, int int26) {
        this.int1 = int1;
        this.int2 = int2;
        this.int26 = int26;
    }

    public Lot(int int1, int int2, String u1) {
        this.int1 = int1;
        this.int2 = int2;
        this.u1 = u1;
    }

    public Lot(int int1, String u1, int int26, int int2, int int3) {
        this.int1 = int1;
        this.u1 = u1;
        this.int26 = int26;
        this.int2 = int2;
        this.int3 = int3;
    }

    public Lot(String u1, int int1, String u2, String u3) {
        this.u1 = u1;
        this.int1 = int1;
        this.u2 = u2;
        this.u3 = u3;
    }
    
    public Lot(String u1, int int1, String u2) {
        this.u1 = u1;
        this.int1 = int1;
        this.u2 = u2;
    }

    public Lot(String u1, double d1, String u2, String u3) {
        this.u1 = u1;
        this.d1 = d1;
        this.u2 = u2;
        this.u3 = u3;
    }

    public Lot(int int1, double d1) {
        this.int1 = int1;
        this.d1 = d1;
    }

    
     public Lot(String u1, String u2) {        
   
        this.u1 = u1;
        this.u2 = u2;
        
    }
    
    
    public Lot(String u0,String u1, String u2) {
        
        this.u0 = u0;
        this.u1 = u1;
        this.u2 = u2;
        
    }
    
    
}
