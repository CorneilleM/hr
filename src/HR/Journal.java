/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package HR;


public class Journal implements Comparable<Journal>  {

    public int NumOperation, numJournal, postDater;
    public String numTiers, heure, libelle, compteDebit, compteCredit, journaux,DEVISE;
    public double montantDebit, montantCredit, soldeProgressif,DEVISE_RATE;
    public String dateOperation, dateDoc, ctiers2, numAffilie, profitCenter;
//String typeOperation;
    public String utilisateur;
    public String nomFrss;
    public String u1, u2, u3, u4, u5, u6;
    Tiers tiers;
    
    boolean isDebit = true;
    boolean sortOnLib=false;
    String lettre;
    Journal payee;
    

    public Journal(int NumOperation, String numTiers, String dateOperation, String compteDebit,
            String compteCredit, String libelle, double montantDebit, double montantCredit, 
            String utilisateur,
            int postDater, String dateDoc, String ctiers, String numAffilie, String journaux,
            String profitCenter
            , int  numJournal,String devise,double devise_rate) {
        
        this.DEVISE=devise;
        this.DEVISE_RATE=devise_rate;
        this.NumOperation = NumOperation;
        this.dateOperation = dateOperation;
        this.compteDebit = compteDebit;
        this.compteCredit = compteCredit;
        this.libelle = libelle;
        this.montantCredit = montantCredit;
        this.montantDebit = montantDebit;
        this.utilisateur = utilisateur;
        this.numTiers = numTiers;
        this.postDater = postDater;
        this.dateDoc = dateDoc;
        this.numAffilie = numAffilie;
        this.journaux = journaux;
        this.ctiers2 = ctiers;
        this.profitCenter = profitCenter;
        this.numJournal=numJournal;
        
        if (montantCredit > 0) {
            isDebit = false;
        }
    }
 
    public int toInt() {
        int k = NumOperation;
        return k;
    }

    @Override
    public String toString() {
        String valeur = "";

        if (isDebit) {
            valeur = "" + (int) montantDebit;
        } else {
            valeur = "           " + (int) montantCredit;
        }

        return NumOperation + "**" + numTiers + "*D*" +compteDebit + "*C*" +compteCredit + "**" + dateOperation + "**" + valeur;

    }
 
    @Override
public int compareTo(Journal o) {
        
    if(sortOnLib)
    {return o.libelle.compareToIgnoreCase(libelle);}
    else
    {return this.NumOperation-o.NumOperation; } 
                
  }
       
}