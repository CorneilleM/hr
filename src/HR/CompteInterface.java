package HR;
/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author SHIMWE
 */
public class CompteInterface extends JFrame {

    JTextField NUMERO;
    JTextField INTITULE;
    JTextField CL;
    JTextField sousC,ext;
    JList category;
    JComboBox CLASSE;
    Container content;
    JLabel NUMEROL, INTITULEL, CLASSEL, numl, SOUSCPTEL, SOUSCPTE, cat,extL;
    static String num, nom, clas;
    JButton saveButton;
    JButton cancelButton;
    Font police;
    Color color;
    String what;
    DbHandler db;
    Compte cd;
    String sous;
    String cla;

    public CompteInterface(DbHandler db) {
        this.db = db;
        content = getContentPane();
        //Set the title to the Comptabilitée frame
        what = "";
        Compte n3 = new Compte("0", "NEW","0", 0, "0");
        makeFrameJournal(n3 );
        draw();
        insertCompte();
        cancelCompte();
        
    }

    public CompteInterface(DbHandler db, Compte cd) {

        this.db = db;
        what = "UPDATE";
        this.db = db;
        this.cd = cd;
        content = getContentPane();

        makeFrameJournal(cd );

        draw();
        update();
        cancelCompte();

    }

    public CompteInterface(DbHandler db, String sous, Compte cd) {

        this.db = db;
        what = "SOUS";
        this.db = db;
        this.sous = sous;
        this.cd = cd;
        cla = "" + sous;
        cla = cla.substring(0, 1);
        content = getContentPane();
        makeFrameJournal(cd );
        draw();
        insertSousCompte();
        cancelCompte();

    }

    public void draw() {
        this.setTitle("Saisie de nouveau compte");
//End the process when clicking on Close
        enableEvents(WindowEvent.WINDOW_CLOSING);
//Make the Comptabilitée frame not resizable
        this.setResizable(false);
//Define the size of the Comptabilité  ; here, 500 pixels of width and 650 pixels of height
        switch (what) {
            case "UPDATE":
                getContentPane().add(UpDatePane(content));
                break;
            case "SOUS":
                getContentPane().add(sousPane(content));
                break;
            default:
                getContentPane().add(westPane(content));
                break;
        }

        this.setSize(800, 600);
        setVisible(true);
    }

    public void makeFrameJournal(Compte cpt) {

        NUMEROL = new JLabel("NUMERO COMPTE :");
        police = new Font("Rockwell", Font.BOLD, 14);
        NUMEROL.setFont(police);

        SOUSCPTEL = new JLabel("NUMERO SOUS COMPTE :");
        police = new Font("Rockwell", Font.BOLD, 14);
        SOUSCPTEL.setFont(police);

        SOUSCPTE = new JLabel("" + sous);
        police = new Font("Rockwell", Font.BOLD, 14);
        SOUSCPTE.setFont(police);


        numl = new JLabel("" + cpt.numCompte);
        police = new Font("Rockwell", Font.BOLD, 14);
        numl.setFont(police);

        NUMERO = new JTextField("");
        police = new Font("Consolas", Font.BOLD, 16);
        NUMERO.setFont(police);
        NUMERO.setForeground(Color.BLUE);
        color = new Color(175, 215, 255);//(r,g,b)
        NUMERO.setBackground(color);
        // NUMERO.setText(""+cpte);

        INTITULEL = new JLabel("INTITULE :");
        police = new Font("Rockwell", Font.BOLD, 14);
        INTITULEL.setFont(police);
         extL = new JLabel("EXT REF :");
        police = new Font("Rockwell", Font.BOLD, 14);
        extL.setFont(police);
        ext = new JTextField("");
        police = new Font("Consolas", Font.BOLD, 14);
        ext.setFont(police);
        ext.setForeground(Color.red); 
        ext.setText(cpt.ext);
        ext.setToolTipText("EX: MINI ACCOUNT DESIGNATION");
        
        INTITULE = new JTextField("");
        police = new Font("Consolas", Font.BOLD, 14);
        INTITULE.setFont(police);
        INTITULE.setForeground(Color.BLUE);
        color = new Color(175, 215, 255);//(r,g,b)
        INTITULE.setBackground(color);
        INTITULE.setText(cpt.nomCompte);

        CLASSEL = new JLabel("CLASSE :");
        police = new Font("Rockwell", Font.BOLD, 14);
        CLASSEL.setFont(police);

        CLASSE = new JComboBox();
        police = new Font("Consolas", Font.BOLD, 14);
        CLASSE.setFont(police);
        CLASSE.setForeground(Color.BLUE);
        color = new Color(175, 215, 255);//(r,g,b)
        CLASSE.setBackground(color);
        CLASSE.addItem(" ");
        CLASSE.addItem("1");
        CLASSE.addItem("2");
        CLASSE.addItem("3");
        CLASSE.addItem("4");
        CLASSE.addItem("5");
        CLASSE.addItem("6");
        CLASSE.addItem("7");
        CLASSE.addItem("8");

        CL = new JTextField("");
        police = new Font("Consolas", Font.BOLD, 14);
        CL.setFont(police);
        CL.setForeground(Color.BLUE);
        color = new Color(175, 215, 255);//(r,g,b)
        CL.setBackground(color);
        CL.setText("" + cpt.classe);

        sousC = new JTextField("");
        police = new Font("Consolas", Font.BOLD, 14);
        sousC.setFont(police);
        sousC.setForeground(Color.BLUE);
        color = new Color(175, 215, 255);//(r,g,b)
        sousC.setBackground(color);
        sousC.setText(cpt.cpteParent);

        category = new JList();
       
        cat = new JLabel("CATEGORIE");
        police = new Font("Consolas", Font.BOLD, 14);
        cat.setFont(police);
        category.setListData(db.categorieCompte().toArray());
        category.setSelectedValue(cpt.category, true);
        saveButton = new JButton("SAVE");
        police = new Font("Rockwell", Font.BOLD, 14);
        saveButton.setFont(police);
        color = new Color(55, 155, 255);//(r,g,b)
        saveButton.setBackground(color);

        cancelButton = new JButton("CANCEL");
        police = new Font("Rockwell", Font.BOLD, 14);
        cancelButton.setFont(police);
    }

    public JPanel westPane(Container content) {
        JPanel panelJournal = new JPanel();
        panelJournal.setLayout(new GridLayout(3, 2, 5, 20));//
        panelJournal.setPreferredSize(new Dimension(780, 125));

        JPanel paneJournal = new JPanel();
        paneJournal.setLayout(new GridLayout(3, 2, 6, 20));//
        paneJournal.setPreferredSize(new Dimension(50, 50));


        panelJournal.add(NUMEROL);
        panelJournal.add(NUMERO);
        panelJournal.add(INTITULEL);
        panelJournal.add(INTITULE);
        panelJournal.add(CLASSEL);
        panelJournal.add(CLASSE);
        panelJournal.add(cat);

        JScrollPane scrollcategory = new JScrollPane(category);
        scrollcategory.setPreferredSize(new Dimension(110, 50));
        scrollcategory.setBackground(new Color(255, 175, 160));
        panelJournal.add(scrollcategory);

        panelJournal.add(saveButton);
        panelJournal.add(cancelButton);

        return panelJournal;
    }

    /**
     * **********************************************************************************
     * UPDATE FRAME
     * ***************************************************************
     */
      JPanel UpDatePane(Container content) {

        JPanel panelJournal = new JPanel();
        panelJournal.setLayout(new GridLayout(6, 2, 6, 20));//
        panelJournal.setPreferredSize(new Dimension(750, 500));

        NUMERO.setText("" + cd.numCompte);
        INTITULE.setText(cd.nomCompte);
        CL.setText("" + cd.classe);


        panelJournal.add(NUMEROL);
        panelJournal.add(numl);
        panelJournal.add(INTITULEL);
        panelJournal.add(INTITULE);
        panelJournal.add(CLASSEL);
        panelJournal.add(CL);
        panelJournal.add(cat); 
        JScrollPane scrollcategory = new JScrollPane(category);
        scrollcategory.setPreferredSize(new Dimension(110, 50));
        scrollcategory.setBackground(new Color(255, 175, 160));
        panelJournal.add(scrollcategory); 
 panelJournal.add(extL);
        panelJournal.add(ext);
        saveButton.setText("UPDATE");
        panelJournal.add(saveButton);
        panelJournal.add(cancelButton);

        return panelJournal;
    }

    /**
     * **********************************************************************************
     * SOUSPANE FRAME
     * ***************************************************************
     */
      JPanel sousPane(Container content) {
        JPanel panelJournal = new JPanel();
        panelJournal.setLayout(new GridLayout(5, 2));//
        panelJournal.setPreferredSize(new Dimension(750, 125));

        // paneJournal.setPreferredSize(new Dimension(20,20));
        numl = new JLabel("" + cd.numCompte);
        CL.setText(cla);
        NUMEROL.setText("" + sous);
        INTITULE.setText(cd.nomCompte);

        panelJournal.add(SOUSCPTEL);
        panelJournal.add(NUMEROL);
        panelJournal.add(INTITULEL);
        panelJournal.add(INTITULE);
        panelJournal.add(CLASSEL);
        panelJournal.add(CL);
        panelJournal.add(cat);
        JScrollPane scrollcategory = new JScrollPane(category);
        scrollcategory.setPreferredSize(new Dimension(110, 50));
        scrollcategory.setBackground(new Color(255, 175, 160));
        panelJournal.add(scrollcategory);

        saveButton.setText("SAVE");
        panelJournal.add(saveButton);
        panelJournal.add(cancelButton);

        return panelJournal;
    }

    public void insertCompte() {

        saveButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {

                if (ae.getSource() == saveButton) {

                    try {

                        if (NUMERO.getText().equals("") || INTITULE.getText().equals("") || CLASSE.getSelectedItem() == null) {

                            JOptionPane.showMessageDialog(null, "VEUILLEZ REMPLIR TOUTES LES INFORMATIONS NECESSAIRE");

                        } else {

                            String Nom_compte = db.getCompte(NUMERO.getText());

                            if (!(Nom_compte.equals(""))) {

                                JOptionPane.showMessageDialog(null, "CE COMPTE EXISTE DEJA");

                                NUMERO.setText("");
                                INTITULE.setText("");
                                category.setSelectedIndex(-1);
                                CLASSE.setSelectedIndex(0);

                            } else {

                                int classe  ;
                                int compteParent;

                                int compteInserted = Integer.parseInt(NUMERO.getText());

                                if (compteInserted <= 9) {

                                    compteParent = 0;

                                } else {

                                    compteParent = (compteInserted - (compteInserted % 10)) / 10;

                                }

                                if (((String) CLASSE.getSelectedItem()).equals(" ")) {

                                    classe = 0;

                                } else {

                                    classe = Integer.parseInt((String) CLASSE.getSelectedItem());

                                }

                                //////////////////////////////////////

                                String catego = "";

                                if (category.getSelectedValue() != null) {

                                    catego = (String) category.getSelectedValue();

                                }

                                Compte ce = new Compte(NUMERO.getText(), INTITULE.getText(), classe, "" + compteParent, catego);

                                db.insertCompte(ce.numCompte, ce.nomCompte, ce.classe, ce.cpteParent, ce.category);

                                NUMERO.setText("");
                                INTITULE.setText("");
                                category.setSelectedIndex(-1);
                                CLASSE.setSelectedIndex(0);

                            }

                        }

                    } catch (HeadlessException | NumberFormatException ex) {
                        System.out.println(" error in ... saveButton.addActionListener ... insertCompte() " + ex);
                    }
                }

            }
        });
    }

    public void cancelCompte() {
        cancelButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == cancelButton) {
                    dispose();

                }

            }
        });
    }

    /**
     * ********************************************************************************************
     * MIS A JOUR TIERS
     * ********************************************************************************************
     */
    public void update() {
        saveButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                
                if (ae.getSource() == saveButton && category.getSelectedValue()!=null) {
                    
                    String numCompte = (NUMERO.getText());
                    String nomCompte = INTITULE.getText();
                    int classe = Integer.parseInt(CL.getText());
                    String cpteParent =  (String)(category.getSelectedValue());
                    String extr = ext.getText();
                    
                    System.out.println("cpteParent  "+cpteParent);
                    try {

if (what.equals("UPDATE")) {

    db.upDateCompte(new Compte(  numCompte,   nomCompte,   cpteParent,   classe,  extr), "" + numCompte);
}

} catch (Throwable ex) {
JOptionPane.showMessageDialog(null, " PROBLEME MODIFICATION "+ex, " ERROR ", JOptionPane.PLAIN_MESSAGE);
}
                    
                }
                else {
JOptionPane.showMessageDialog(null, " SELECT CATEGORY " , " ERROR ", JOptionPane.PLAIN_MESSAGE);
}
                    
            }
        });

    }

    public void insertSousCompte() {
        
        saveButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                
                if (ae.getSource() == saveButton) {

                    Object all    = CL.getText();
                    // System.out.print("all"+all);
                    clas = "" + all;
                    int a  ;
                    System.out.print("all:" + clas);
                    
                    if (clas.equals(" ")) {
                        a = 0;
                    } else {
                        a = Integer.parseInt(clas);
                    }

                    if (INTITULE.getText().equals("") || a == 0) {
                        
                        JOptionPane.showMessageDialog(null, "VEUILLEZ REMPLIR LES CASES VIDE");
                        
                    } else {
                        
                        String catego = "";

                        if (category.getSelectedValue() != null) {
                            
                            catego = (String) category.getSelectedValue();
                            
                        }
                        
                        db.insertCompte(NUMEROL.getText(), INTITULE.getText(), a, "" + cd.numCompte, catego);

//            System.out.println("NUMERO.getText():"+NUMERO.getText()); 
//            System.out.println("INTITULE.getText():"+INTITULE.getText());
//            System.out.println("a .getText():"+a);
//            System.out.println("sous:"+cd.numCompte);
//            System.out.println("cat "+catego);

                        NUMERO.setText("");
                        NUMEROL.setText("");
                        INTITULE.setText("");
                        CL.setText("");
                        CLASSE.setSelectedIndex(0);
                        saveButton.disable();
                    }

                }

            }
        });
    }

    public static void main(String[] args) {
//    Cashier c=new Cashier(server,database);
//new CompteInterface(c);
    }
}