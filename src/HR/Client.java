package HR;

public class Client {

    int DATE_EXP, PERCENTAGE, REDUCTION;
    String NUM_AFFILIATION, NOM_CLIENT, PRENOM_CLIENT, ASSURANCE, EMPLOYEUR, SECTEUR, VISA, CODE, STATUS;
    String devise;

    public Client(String NUM_AFFILIATION, String NOM_CLIENT, String PRENOM_CLIENT, int PERCENTAGE, int DATE_EXP, String ASSURANCE, String EMPLOYEUR, String SECTEUR, String VISA, String CODE, int REDUCTION, String STATUS) {
        this.STATUS = STATUS;
        this.NUM_AFFILIATION = NUM_AFFILIATION;
        this.NOM_CLIENT = NOM_CLIENT;
        this.PRENOM_CLIENT = PRENOM_CLIENT;
        this.PERCENTAGE = PERCENTAGE;
        this.DATE_EXP = DATE_EXP;
        this.ASSURANCE = ASSURANCE;
        this.EMPLOYEUR = EMPLOYEUR;
        this.SECTEUR = SECTEUR;
        this.VISA = VISA;
        this.CODE = CODE;
        this.REDUCTION = REDUCTION;


    }

    public Client(String NUM_AFFILIATION, String NOM_CLIENT, String PRENOM_CLIENT, int PERCENTAGE, int DATE_EXP, String ASSURANCE, String EMPLOYEUR, String SECTEUR, String VISA, String CODE) {

        this.NUM_AFFILIATION = NUM_AFFILIATION;
        this.NOM_CLIENT = NOM_CLIENT;
        this.PRENOM_CLIENT = PRENOM_CLIENT;
        this.PERCENTAGE = PERCENTAGE;
        this.DATE_EXP = DATE_EXP;
        this.ASSURANCE = ASSURANCE;
        this.EMPLOYEUR = EMPLOYEUR;
        this.SECTEUR = SECTEUR;
        this.VISA = VISA;
        this.CODE = CODE;
        this.devise = "RFW";

    }

    @Override
    public String toString() {
        return "" + NUM_AFFILIATION + " " + NOM_CLIENT + " " + ASSURANCE;
    }
}