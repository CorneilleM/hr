package HR;

/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */ 

import java.awt.*;
import java.awt.event.*;
import java.util.LinkedList;
import javax.swing.*;

/**
 *
 * @author SHIMWE
 */
public class TiersInterface extends JFrame {

    JLabel NUMERO;
   
    JList compte = new JList();
    //String[] dev = {"FRW", "USD", "EURO", "FBU"};
    JList devisej = new JList();
    LinkedList<Journal> joList = new LinkedList();
    String cpte, save;
    Container content;
     JTextField DESIGNATION,SIGLE,INFO,ADRESSE,DEVISE,EMAIL,AFFILIE,SALAIRE,EXT_NUM;
    JLabel NUMEROL, COMPTEL, DESIGNATIONL, INFOL,EMAILL, ADRESSEL, DEVISEL,
            CPTETIERS, SIGLEL, AFFILIEL, SALAIREL,EXT_NUML;
    JPanel mainJournal = new JPanel();
    JButton saveButton;
    JButton analyseButton;
    String devise;
    Font police;
    Color color;
    Compte[] control2;
    Tiers tier; 
    DbHandler db;
    String what = ""; 
    
    public TiersInterface(DbHandler db, String tiersNum, String cptee,int salaireBrute,String designation) {
        try {
            this.cpte = cptee;
            this.db = db;
            content = getContentPane();
            makeFrameTiers(tiersNum, cpte, designation , designation,designation,designation,"", "",salaireBrute,"");
            draw();
            insertTiers();
            
            cancel();
        } catch (Throwable e) {
            System.out.println(" . . TiersInterface . exception thrown :" + e);
//            insertError(e + "", " getBalanceVerification  ");
        }
    }

    public TiersInterface(DbHandler db, Tiers t) {

        try {
             
            this.db=db;
            this.tier = t; 
            //this.db=db;
            content = getContentPane();
            what = "update";

            makeFrameTiers(t.numTiers, t.numCompte, t.designation, t.info, t.adresse,t.email, t.devise, 
                    t.sigleTiers, t.salaireBrute,t.numExt);
            draw();
            update();
            cancel();
        } catch (Throwable e) {
            System.out.println(" . . TiersInterface . exception thrown:" + e);
//            insertError(e + "", " getBalanceVerification  ");
        }
    }

    public void draw() {
        
        try{
        //Set the title to the Comptabilitée frame
        this.setTitle("Saisie des tiers");
//End the process when clicking on Close
        enableEvents(WindowEvent.WINDOW_CLOSING);
//Make the Comptabilitée frame not resizable
        this.setResizable(false);
//Define the size of the Comptabilité  ; here, 500 pixels of width and 650 pixels of height
        this.setSize(500, 500);

        if (what.equals("update")) {
            UpDatePane(content, tier);
        } else {
            westPane();
        }

        setSize(600, 500);
        setVisible(true);
        
        } catch (Throwable e) {
            System.out.println(" . . inv . exception thrown:" + e);
//            insertError(e + "", " getBalanceVerification  ");
        }        
    }

    /**
     * ************************************************************************************
     * METHODE POUR CONSTRUIRE L'INTERFACE
     * ************************************************************************************
     */
    void makeFrameTiers(String numtiers, String numCompte, String designation, String info, String adresse, String email,
            String devise, String sigle, int salaireBrute, String ext) {
               
        try{
        
        CPTETIERS = new JLabel(numCompte);
        police = new Font("Rockwell", Font.BOLD, 14);
        CPTETIERS.setFont(police);

        SIGLEL = new JLabel("SIGLE TIERS");
        police = new Font("Rockwell", Font.BOLD, 14);
        SIGLEL.setFont(police);

        AFFILIEL = new JLabel("NUM AFFILIATION");
        police = new Font("Rockwell", Font.BOLD, 14);
        AFFILIEL.setFont(police);

        NUMEROL = new JLabel("NUMERO TIERS :");
        police = new Font("Rockwell", Font.BOLD, 14);
        NUMEROL.setFont(police);

        NUMERO = new JLabel();
        police = new Font("Consolas", Font.BOLD, 16);
        NUMERO.setFont(police);
        NUMERO.setForeground(Color.BLUE);
        color = new Color(175, 215, 255);//(r,g,b)
        NUMERO.setBackground(color);
        NUMERO.setText(numtiers);

        COMPTEL = new JLabel("NUMERO COMPTE :");
        police = new Font("Rockwell", Font.BOLD, 14);
        COMPTEL.setFont(police);

//    getCompte();//APPEL DE LA FONCTION QUI CAPTE LE CPTE DS LA BD
//   compte.setListData(control2);//INSERTION DE CPTE DANS JLIST

        compte.setForeground(Color.BLUE);
        police = new Font("Consolas", Font.BOLD, 14);
        compte.setFont(police);
        color = new Color(255, 145, 200);//(r,g,b)
        compte.setBackground(color);

        DESIGNATIONL = new JLabel("DESIGNATION :");
        police = new Font("Rockwell", Font.BOLD, 14);
        DESIGNATIONL.setFont(police);

        DESIGNATION = new JTextField("");
        police = new Font("Consolas", Font.BOLD, 16);
        DESIGNATION.setFont(police);
        DESIGNATION.setForeground(Color.BLUE);
        color = new Color(175, 215, 255);//(r,g,b)
        DESIGNATION.setBackground(color);
        DESIGNATION.setText(designation);

        INFOL = new JLabel("INFORMATION :");
        police = new Font("Rockwell", Font.BOLD, 14);
        INFOL.setFont(police);

        INFO = new JTextField("");
        police = new Font("Consolas", Font.BOLD, 16);
        INFO.setFont(police);
        INFO.setForeground(Color.BLUE);
        color = new Color(175, 215, 255);//(r,g,b)
        INFO.setBackground(color);
        INFO.setText(info);

        ADRESSEL = new JLabel("ADRESSE :");
        police = new Font("Rockwell", Font.BOLD, 14);
        ADRESSEL.setFont(police);

         EMAILL = new JLabel("EMAIL :");
        police = new Font("Rockwell", Font.BOLD, 14);
        EMAILL.setFont(police);
        
        
        EMAIL = new JTextField("");
        police = new Font("Consolas", Font.BOLD, 16);
        EMAIL.setFont(police);
        EMAIL.setForeground(Color.BLUE);
        color = new Color(175, 215, 255);//(r,g,b)
        EMAIL.setBackground(color);
        EMAIL.setText(email);

        ADRESSE = new JTextField("");
        police = new Font("Consolas", Font.BOLD, 16);
        ADRESSE.setFont(police);
        ADRESSE.setForeground(Color.BLUE);
        color = new Color(175, 215, 255);//(r,g,b)
        ADRESSE.setBackground(color);
        ADRESSE.setText(adresse);

        SIGLE = new JTextField("");
        police = new Font("Consolas", Font.BOLD, 16);
        SIGLE.setFont(police);
        SIGLE.setForeground(Color.BLUE);
        color = new Color(175, 215, 255);//(r,g,b)
        SIGLE.setBackground(color);
        SIGLE.setText(sigle);

        AFFILIE = new JTextField("");
        police = new Font("Consolas", Font.BOLD, 16);
        AFFILIE.setFont(police);
        SIGLE.setForeground(Color.BLUE);
        color = new Color(175, 215, 255);//(r,g,b)
        AFFILIE.setBackground(color);
        AFFILIE.setText(sigle);


        SALAIRE = new JTextField("");
        police = new Font("Consolas", Font.BOLD, 16);
        SALAIRE.setFont(police);
        SALAIRE.setForeground(Color.BLUE);
        color = new Color(175, 215, 255);//(r,g,b)
        SALAIRE.setBackground(color);
        SALAIRE.setText(""+salaireBrute);
        

        EXT_NUM = new JTextField(""+ext);
        police = new Font("Consolas", Font.BOLD, 16);
        EXT_NUM.setFont(police);
        EXT_NUM.setForeground(Color.red);
         EXT_NUM.setToolTipText("EX: MINI TIER DESIGNATION");
        
        EXT_NUML = new JLabel("EXT_NUM :");
        police = new Font("Rockwell", Font.BOLD, 14);
        EXT_NUML.setFont(police);
        
        DEVISEL = new JLabel("DEVISE :");
        police = new Font("Rockwell", Font.BOLD, 14);
        DEVISEL.setFont(police);


        SALAIREL = new JLabel("DEFAULT AMOUNT :");
        police = new Font("Rockwell", Font.BOLD, 14);
        SALAIREL.setFont(police);

        devisej.setListData(db.getVarListFam("DEVISE").toArray());
//    DEVISE = new JTextField("");
//    police = new Font("Consolas", Font.BOLD, 16);
//    DEVISE.setFont(police);
//    DEVISE.setForeground(Color.BLUE);
//    color= new Color(175,215,255);//(r,g,b)
//    DEVISE.setBackground(color);
//    DEVISE.setText(devise);


        saveButton = new JButton("SAVE");
        police = new Font("Rockwell", Font.BOLD, 14);
        saveButton.setFont(police);
        color = new Color(55, 155, 255);//(r,g,b)
        saveButton.setBackground(color);

        analyseButton = new JButton("ANALYSE");
        police = new Font("Rockwell", Font.BOLD, 14);
        analyseButton.setFont(police);

        } catch (Exception e ) {
            System.out.println(" . . makeFrameTiers . exception thrown:" + e);
//            insertError(e + "", " getBalanceVerification  ");
        }

    }

    public JPanel westPane() {
        JPanel panelJournal = new JPanel();
        panelJournal.setLayout(new GridLayout(10, 2, 5, 20));//
        panelJournal.setPreferredSize(new Dimension(450, 400));


        panelJournal.add(NUMEROL);
        panelJournal.add(NUMERO);
        panelJournal.add(COMPTEL);
//     JScrollPane scCompte = new JScrollPane(compte);
//    scCompte.setPreferredSize(new Dimension(500,150));
        panelJournal.add(CPTETIERS);
        panelJournal.add(AFFILIEL);
        panelJournal.add(AFFILIE);
        panelJournal.add(DESIGNATIONL);
        panelJournal.add(DESIGNATION);
        panelJournal.add(SIGLEL);
        panelJournal.add(SIGLE);
        panelJournal.add(INFOL);
        panelJournal.add(INFO);
        panelJournal.add(ADRESSEL);
        panelJournal.add(ADRESSE);
        panelJournal.add(EMAILL); 
        panelJournal.add(EMAIL); 
        panelJournal.add(DEVISEL);


        JScrollPane sCompte = new JScrollPane(devisej);
        sCompte.setPreferredSize(new Dimension(500, 550));
        panelJournal.add(sCompte);

        panelJournal.add(SALAIREL);
        panelJournal.add(SALAIRE);

        JPanel panelJournal2 = new JPanel();
        panelJournal2.setLayout(new GridLayout(1, 4, 2, 4));
        panelJournal2.setPreferredSize(new Dimension(450, 40));

        panelJournal2.add(saveButton);
        panelJournal2.add(analyseButton);

        mainJournal.add(panelJournal);
        mainJournal.add(panelJournal2);

        content.add(mainJournal);
        return mainJournal;
    }

    /**
     * **********************************************************************************
     * UPDATE FRAME
     * ***************************************************************
     */
      JPanel UpDatePane(Container content, Tiers bb ) {

        JPanel panelJournal = new JPanel();
        panelJournal.setLayout(new GridLayout(10, 2, 5, 20));//
        panelJournal.setPreferredSize(new Dimension(450, 400));

        NUMERO.setText("" + tier.numTiers);
        DESIGNATION.setText(tier.designation);
        INFO.setText(tier.info);
        ADRESSE.setText(tier.adresse);
        EMAIL.setText(tier.email);
        Object al = tier.devise;
        devisej.setSelectedValue(al, true);
        SALAIRE.setText(""+tier.salaireBrute);
        JScrollPane sCompte = new JScrollPane(devisej);
        sCompte.setPreferredSize(new Dimension(500, 250));
        //DEVISE.setText(tier.devise);


        panelJournal.add(NUMEROL);
        panelJournal.add(new JLabel("" + tier.numTiers));
        panelJournal.add(COMPTEL);
        panelJournal.add(CPTETIERS);
        panelJournal.add(DESIGNATIONL);
        panelJournal.add(DESIGNATION);
        panelJournal.add(SIGLEL);
        panelJournal.add(SIGLE);
        panelJournal.add(INFOL);
        panelJournal.add(INFO);
        panelJournal.add(ADRESSEL);
        panelJournal.add(ADRESSE);
        panelJournal.add(EMAILL);
        panelJournal.add(EMAIL);
        panelJournal.add(DEVISEL);
        panelJournal.add(sCompte);
        panelJournal.add(SALAIREL);
        panelJournal.add(SALAIRE);
        panelJournal.add(EXT_NUML);
        panelJournal.add(EXT_NUM);
        saveButton.setText("UPDATE");

        JPanel panelJournal2 = new JPanel();
        panelJournal2.setLayout(new GridLayout(1, 4, 2, 4));
        panelJournal2.setPreferredSize(new Dimension(450, 40));

        panelJournal2.add(saveButton);
        panelJournal2.add(analyseButton);

        mainJournal.add(panelJournal);
        mainJournal.add(panelJournal2);

        content.add(mainJournal);
        return mainJournal;
    }

    /**
     * ************************************************************************************
     * METHODE POUR INSERTIION DE TIERS DANS LA BD
     * ************************************************************************************
     */
    public void insertTiers() {

        saveButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == saveButton) {

                    try {
 
                        Object d  = devisej.getSelectedValue();
                        devise = "" + d; 
                        System.out.println("Compte:" + cpte); 
                        System.out.println("Compte:" + save);

                        if (INFO.getText().equals("") || DESIGNATION.getText().equals("") || d.equals("") || ADRESSE.getText().equals("")|| EMAIL.getText().equals("") || SIGLE.getText().equals("") || AFFILIE.getText().equals("") || SALAIRE.getText().equals("")) {
                            JOptionPane.showMessageDialog(null, "VEUILLEZ REMPLIR LES CASES VIDES !!");
                        } else {
                            
                        
                            db.insertTiers2(NUMERO.getText(), CPTETIERS.getText(), 
                                    DESIGNATION.getText(), INFO.getText(), ADRESSE.getText() ,EMAIL.getText(),
                                    devise, SIGLE.getText(), AFFILIE.getText(), SALAIRE.getText()
                                    , EXT_NUM.getText());

                            JOptionPane.showMessageDialog(null, "DONNEES ENREGISTRES AVEC SUCCES!!");
                            NUMERO.setText("");
                            DESIGNATION.setText("");
                            INFO.setText("");
                            ADRESSE.setText("");
                            EMAIL.setText("");
                            CPTETIERS.setText("");
                            SIGLE.setText("");
                            AFFILIE.setText("");
                            SALAIRE.setText("");
                        }

                    } catch (Exception e) {
                        System.out.println(" . . method : insertTiers. exception thrown:" + e);
//                        insertError(e + "", " method :  ");
                        JOptionPane.showMessageDialog(null, "VEUILLEZ REMPLIR LES CASES VIDES SANS OUBLIER LA DEVISE !!  \n" + e, " Ikibazo in: ", JOptionPane.PLAIN_MESSAGE);
                    }
                }
            }
        });
    }

    /**
     * ********************************************************************************************
     * MIS A JOUR TIERS
     * ********************************************************************************************
     */
    public void update() {

        saveButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == saveButton) {
                    String tiersnum = NUMERO.getText();
//                    int tnum = Integer.parseInt(tiersnum);
                    String g = CPTETIERS.getText();
                    Object ob = devisej.getSelectedValue();
                    String k = "" + ob;
                    System.out.println("tiers num1: " + k);
                    tier = new Tiers(tiersnum, g, DESIGNATION.getText(), INFO.getText(), ADRESSE.getText(),EMAIL.getText(),
                            k, SIGLE.getText(), AFFILIE.getText(), Integer.parseInt(SALAIRE.getText()), EXT_NUM.getText());

                    try {
                        if (what.equals("update")) {
                            System.out.println("nahageze: 1");
                            db.upDateTiers(tier, "" + tiersnum);
                            System.out.println("nahageze: 2");
                        }
                    } catch (Throwable ex) {
                        System.out.println("EX " + ex);
                        JOptionPane.showMessageDialog(null, " PROBLEME MODIFICATION ", " ATTENTION ", JOptionPane.PLAIN_MESSAGE);
                    }
                }
            }
        });

    }

    public void showCompte() {
        int c = db.allCompte.size();
        System.out.println("size cc" + c);

        Compte[] control = new Compte[c];
        for (int i = 0; i < c; i++) {
            //Product p= cashier.allProduct.get(i);
            control[i] = db.allCompte.get(i);
        }
        compte.setListData(control);
        //  allProductList.
    }

    public void cancel() {
        analyseButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                if (ae.getSource() == analyseButton) {
                     
                    
               new TiersAnalytics(  tier,db); 
                    
                    
                }

            }
        });
    }
}